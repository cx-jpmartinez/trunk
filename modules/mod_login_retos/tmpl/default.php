<?php
/**
 * @version		$Id: default.php 21322 2011-05-11 01:10:29Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<script>
    function myButton(label) {
        if(label==1){
	     document.adminform.username.value = "";
        }else{
         document.adminform.password.value = "";
        }
	}
</script>


<?php if ($type == 'logout') : ?>
<div class="sesion">
    <?php
        $usersConfig = JComponentHelper::getParams('com_users');
        if ($usersConfig->get('allowUserRegistration')) : ?>
           
           <span class="reg_txt" style="font-size: 13px !important;">
            <?php  echo cargarMensajeRetos(); ?>
           </span>
           
           
        <?php endif; ?>
</div>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
<?php if ($params->get('greeting')) : ?>
	<div class="sesion">
	<?php if($params->get('name') == 0) : {
		echo JText::sprintf('MOD_LOGIN_HINAME', $user->get('name'));
	} else : {
		echo JText::sprintf('MOD_LOGIN_HINAME', $user->get('username'));
	} endif; ?>
	</div>
<?php endif; ?>
	<div class="logout-button">
		<input type="submit" name="Submit" class="button" value="<?php echo JText::_('MOD_LOGIN_RETOS_FINALIZAR_SECCION'); ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<?php else : ?>
<div class="sesion">
    <?php
        $usersConfig = JComponentHelper::getParams('com_users');
        if ($usersConfig->get('allowUserRegistration')) : ?>
           
            <span class="reg_txt" style="font-size: 13px !important;">
            <?php  echo cargarMensajeRetos(); ?>
           </span>
           
           <!-- a class="button" id="reg_btn" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"><?php echo JText::_('MOD_LOGIN_RETOS_REGISTRATE'); ?></a-->
        <?php endif; ?>
</div>
<div class="sesion">
 <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" name="adminform">
	<?php if ($params->get('pretext')): ?>
		<div class="pretext">
		<p><?php echo $params->get('pretext'); ?></p>
		</div>
	<?php endif; ?>
		<p style="font-size: 12px;">Iniciar sesión</p>
		
		<p id="form-login-username">
			<input id="modlgn-username" type="text" name="username" class="inputbox" size="12" onclick="myButton(1)"/>
		</p>
		<p id="form-login-password">
			<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="12"  onclick="myButton(2)"/>
		</p>
		<input style="font-size: 12px;" type="submit" name="Submit" class="button" value="<?php echo JText::_('Aceptar') ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.login" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
		
		
		<?php if ($params->get('posttext')): ?>
			<div class="posttext">
			<p><?php echo $params->get('posttext'); ?></p>
			</div>
		<?php endif; ?>
 </form>
    <a class="password" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>" >
	<?php //echo JText::_('MOD_LOGIN_RETOS_FORGOT_YOUR_PASSWORD'); ?></a>

</div>
<?php endif; ?>
