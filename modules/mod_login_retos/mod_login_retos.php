<?php

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';
$lang = JFactory::getLanguage();

$params->def('greeting', 1);


$type	= mod_login_retosHelper::getType();
$return	= mod_login_retosHelper::getReturnURL($params, $type);
$user	= JFactory::getUser();

require JModuleHelper::getLayoutPath('mod_login_retos', $params->get('layout', 'default'));
