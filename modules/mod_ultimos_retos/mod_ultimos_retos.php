<?php

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';
$lang = JFactory::getLanguage();
$areaInteres = new areainteres();
$selecArea = $areaInteres->selectAreasInteres(false,true);
$retos = new reto();
$objs = $retos->listarRetosHome();
require JModuleHelper::getLayoutPath('mod_ultimos_retos', $params->get('layout', 'default'));
