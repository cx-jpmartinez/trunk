<?php defined('_JEXEC') or die; ?>
<div id="ultimos_retos">
    <div id="titulo_retos">
        <h3 style="font-size: 18px;"><?php echo JText::_('MOD_ULTIMOS_RETOS');?></h3>
            <form action="<?php echo JRoute::_('index.php');?>" method="post" name="busquedahome" id="busquedahome">
                <?php echo JText::_('MOD_ULTIMOS_RETOS_AREA_INTERES'); ?>
                <?php  echo $selecArea; ?>
                <input type="hidden" name="task" value="search" />
                <input type="hidden" name="option" value="com_retos" />
                <input type="hidden" name="Itemid" value="444" />
	       </form>
    </div>
    <div id="retos">
    <?php 
    $reto = new reto();
    foreach ($objs as $obj){
       $paticipantes = $reto->numeroPostulaciones($obj->ret_id);
       $alto = '70px';
       /*if($obj->ret_id==49 || $obj->ret_id==46){
           $alto='90px';
       }*/
       if(strlen($obj->ret_nombre) > 170){
           $alto='90px';
       }
    ?>
		<div class="reto">
			<a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $obj->ret_id; ?>&Itemid=444">
				<?php if($obj->ret_imagenPeq){ 
					if (file_exists("./images/retos/$obj->ret_imagenPeq")) { ?>
				    <img src="./images/retos/<?php echo $obj->ret_imagenPeq; ?>" width="74" height="50" alt="<?php  echo $obj->ret_imagenPeq; ?>" border="0">
				<?php }
				} 
				?>
				<p style='height: <?php echo $alto; ?>; overflow: hidden; margin-bottom: 10px;'>
					<b>
					   <?php  echo $obj->ret_nombre; ?>
					</b><br>
					<?php echo  JText::_('MOD_ULTIMOS_RETOS_PUNTOS'); ?> <?php echo $obj->ret_puntos; ?> |
					<?php echo  JText::_('MOD_ULTIMOS_RETOS_FECHA_LIMITE'); ?> <?php echo $obj->_fechaVigencia ?> | 
					<?php echo  JText::_('MOD_ULTIMOS_RETOS_SOLUCIONADORES'); ?>
					<?php 
					   //$obj->_ret_postulacion = cargarNumeroParticipantes($obj->ret_id);
                                            $obj->_ret_postulacion = $paticipantes;
					   if($obj->_ret_postulacion){
					       echo $obj->_ret_postulacion; 
                       }else{
                    	   echo 0;
                       }
                    ?>
                </p>
			</a>
        </div>
    <?php } ?>
    </div>
</div>                  