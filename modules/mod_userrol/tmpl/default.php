<?php

// No direct access
defined('_JEXEC') or die; ?>


<!-- Menu con items segun los permisos del usuario  -->
<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-rol" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="sr-only navbar-brand" href="#">Nutresa</a>
</div>

<div class="collapse navbar-collapse" id="bs-navbar-rol">
    <ul class="nav navbar-nav">
        <?php foreach( $list as $item ) : ?>

            <li>
                <a href="<?php echo $item['url'] ?>"> <?php echo $item['label'] ?></a>
            </li>

        <?php endforeach; ?>
    </ul>
</div>



