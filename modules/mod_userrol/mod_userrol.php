<?php
/**
 * Hello World! Module Entry Point
 *
 * @package		    Joomla.Site
 * @subpackage	    mod_userrol
 * @license        GNU/GPL, see LICENSE.php
*  @author         CognoX
 */

defined('_JEXEC') or die;
require_once dirname(__FILE__) . '/helper.php';

$userRol  = new ModUserRolHelper;
$list     = $userRol->getList();

if( !empty($list)  )
{
    require JModuleHelper::getLayoutPath('mod_userrol');
}

