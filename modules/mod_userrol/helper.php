<?php

/**
 * Joomla Helper
 * que trae datos de usuarios con permisos
 * para indicadores.
 * @user Objeto de JFactory::getUser
 * @user_id variable que retiene el identificador de usuario
 *
 */
class ModUserRolHelper
{

    /**
     * @var int
     */
    private $user_id;

    public function __construct()
    {
        $user           = JFactory::getUser();
        $this->user_id  =  $user->id;


        settype($this->user_id, 'integer');
    }

    /**
     * retorna un array con el listado de enlaces permitidos
     * @return array
     */
    public function getList()
    {
        $list     = array();
        $usuario  = new usuario($this->user_id);


        if( !empty($usuario->usu_id) )
        {

            $list[0]['url']     =   "index.php?option=com_retos&view=misretos&Itemid=444";
            $list[0]['label']   =   "Mis retos";

            if( $this->validarExperto($usuario->usu_email) )
            {
                $list[1]['url']     =   "index.php?option=com_preguntas&amp;task=experto&amp;Itemid=622";
                $list[1]['label']   =   "Expertos";

            }

            if( $this->validarComite($usuario->usu_email) || $this->validarDirector($usuario->usu_email) )
            {
                $list[2]['url']     =   "http://aplica.gruponutresa.com/Aplicaciones/Portales/Soluciones_Innovadoras/si.nsf";
                $list[2]['label']   =   "Comité Evaluador";

            }

            if( $this->validarVerParticipantes() )
            {
                $list[3]['url']     =   "index.php?option=com_retos&view=participantes&id=2&Itemid=444";
                $list[3]['label']   =   "Ver Participantes";

            }

            if( $this->validarIngresoGraficas($usuario->usu_email) )
            {
                $list[4]['url']     =   "index.php?option=com_graphic&view=graphic&Itemid=629";
                $list[4]['label']   =   "Indicadores";

            }


            if( $usuario->validarAccesoAdministracionUsuarios(false) )
            {
                $list[5]['url']     =   "index.php?option=com_usuario";
                $list[5]['label']   =   "Ingresar Solución";
            }

            $list[6]['url']     =   "index.php?option=com_users&view=profile";
            $list[6]['label']   =   "Perfil";


        }

        return $list;


    }


    private function validateLoginSystemUser()
    {
        if( empty($this->user_id)  )
        {
            return false;
        }

        return true ;

    }

    private function getInternalUser($id)
    {
        if( !empty($id))
        {
           $usuario =   new usuario($id);

            if($usuario)
            {
                return $usuario;
            }
        }

        return null;

    }

    /**
     * @param $usu_email : mail Usuario Interno
     * @return obj Experto
     */
    private function validarExperto($usu_email)
    {
        $db = JFactory::getDBO();
        $sql = "SELECT
            count(exp_id)
            FROM usuario
                inner join experto on (exp_mail=usu_email)
                inner join reto ret on (ret_exp_id=exp_id)
                where usu_email = '$usu_email'
            group by ret_id desc";

        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult();
        return $obj;
    }


    /**
     * @param $usu_email : mail Usuario Interno
     * @return obj comite
     */
    private function validarComite($usu_email)
    {
        $db = JFactory::getDBO();
        $sql = "SELECT
            count(cim_id)
            FROM usuario
               inner join comite on (cim_email=usu_email)
                inner join reto ret on (cim_ret_id=ret_id)
                where usu_email = '$usu_email'
            group by ret_id desc";
        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult();
        return $obj;
    }

    /**
     * @param $usu_email
     * @return Obj  director
     */
    private function validarDirector($usu_email)
    {
        $db = JFactory::getDBO();
        $sql = "SELECT
            count(dir_id)
            FROM usuario
               inner join director on (dir_email=usu_email)
                inner join reto ret on (dir_ret_id=ret_id)
                where usu_email = '$usu_email'
            group by ret_id desc";
        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult();
        return $obj;
    }


    /**
     * @return bool
     */
    private function validarVerParticipantes()
    {
        $user = JFactory::getUser();
        $arrper = array('pagaviria@serviciosnutresa.com','narbelaez@serviciosnutresa.com','mnino@cremhelado.com.co','mig@cognox.com','mrochel@serviciosnutresa.com','pinnovacionsn@serviciosnutresa.com');
        if(in_array("$user->email",$arrper)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $usu_email : Internal User
     * @return bool
     */
    private function validarIngresoGraficas($usu_email)
    {
        $db = JFactory::getDBO();

        if($usu_email){
            $sql ="SELECT usu_email FROM usuario WHERE usu_email
            IN ('amrestrepo@noel.com.co',
                'cmontoya@noel.com.co',
                'ecgomez@chocolates.com.co',
                'hospina@comercialnutresa.com.co',
                'jecheverri@colcafe.com.co',
                'KOLARTE@COMERCIALNUTRESA.COM.CO',
                'lilianafeghali@hotmail.com',
                'lvaron@pastasdoria.com',
                'mcrodriguez@larecetta.com',
                'mrochel@serviciosnutresa.com',
                'narbelaez@serviciosnutresa.com',
                'ocgiraldo@chocolates.com.co',
                'ricky-gd@hotmail.com',
                'sjimenez@zenu.com.co',
                'pagaviria@serviciosnutresa.com',
                'mig@cognox.com') AND usu_email='{$usu_email}'
                ";
            $db->retos->setQuery( $sql );
            $result = $db->retos->loadResult();
            if($result){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }






}
