<?php
defined('_JEXEC') or die;
class modBuscarHelper
{
	public static function getBuscarImage($button_text)
	{
		$img = JHtml::_('image','searchButton.gif', $button_text, NULL, true, true);
		return $img;
	}
}