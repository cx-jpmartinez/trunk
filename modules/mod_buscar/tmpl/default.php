<?php defined('_JEXEC') or die;?>
<?php 
  $vigenteCK = JRequest::getVar('vigente');  
  $cerradaCK = JRequest::getVar('cerrada');  
  $checkedVigente = '';
  $checkedCerrada = '';
  if($vigenteCK){
      $checkedVigente = 'checked';
  }
  if($cerradaCK){
       $checkedCerrada = 'checked';
  }
  $contenidotemplate = JRequest::getVar( 'option','' );
  switch ($contenidotemplate){
  	case 'com_home': $idbuscar = 'search';break;
  	default : $idbuscar = 'search search_internas';break;
  }
?>
<div id="buscador" class="<?php echo $idbuscar;?>">
    <h3><?php  echo JText::_('MOD_BUSCAR_TITULO'); ?></h3>
    <form action="<?php echo JRoute::_('index.php');?>" method="post" name="busqueda" id="busqueda">  
	    <div class="linea_punteada">
	       <p><?php  echo JText::_('MOD_BUSCAR_BUSCA_PALABRAS'); ?></p>
	       <?php
	       $output = '<input name="buscar" id="mod-buscar-searchword" maxlength="'.$maxlength.'"   type="text"  value=""  onblur="if (this.value==\'\') this.value=\''.$text.'\';" onfocus="if (this.value==\''.$text.'\') this.value=\'\';" />';
	       $button = '<input type="submit" value="'.$button_text.'"  src="" onclick="this.form.searchword.focus();"/>';
	       echo $output;
	       echo $button;
	       ?>
	   </div>
	   <div class="linea_punteada">
	       <p><?php  echo JText::_('MOD_BUSCAR_BUSQUEDA_AVANZADA'); ?></p>
	       <?php 
	       $areaInteres = new areainteres();
	       $selecArea = $areaInteres->selectAreasInteres();
	       echo $selecArea;
	       $button2 = '<input type="submit" value="'.$button_text.'" class="button" src="" onclick="this.form.searchword.focus();"/>'; 
	       ?>
	       <input type="checkbox" name="vigente" value="1" <?php echo $checkedVigente; ?>> <span><?php  echo JText::_('MOD_BUSCAR_OPCION_VIGENTE'); ?></span> <br>
	       <input type="checkbox" name="cerrada" value="2" <?php echo $checkedCerrada; ?>> <span><?php  echo JText::_('MOD_BUSCAR_OPCION_CERRADA'); ?></span> <br>
	       <p id="ordenar">
	           <?php  echo JText::_('MOD_BUSCAR_ORDENAR_POR'); ?>
	           <select id="fecha">
	               <option value="fechaVencimeinto"><?php  echo JText::_('MOD_BUSCAR_FECHA_EXPIRACION'); ?></option>
	           </select>
	           <?php echo $button2; ?>
                </p>
           <input type="hidden" name="task" value="search" />
           <input type="hidden" name="option" value="com_retos" />
           <input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
        </div><br>
	        <!-- a id="rss" href="#"><?php  echo JText::_('MOD_BUSCAR_SUSCRIBIRSE'); ?></a-->
    </form>
</div>
         
                     