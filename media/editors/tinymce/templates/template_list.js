/**
 * Created by jpmartinez on 21/04/2016.
 */
var tinyMCETemplateList = [
    // Name, URL, Description
    ["novedad", "media/editors/tinymce/templates/novedad.html", "HTMLLayout."],
    ["Simple snippet", "media/editors/tinymce/templates/snippet1.html", "Simple HTML snippet."],
    ["Layout", "media/editors/tinymce/templates/layout1.html", "HTMLLayout."],
];