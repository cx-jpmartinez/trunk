<?php
/**
 * @version     2011-11-12
 * @author      afperez
 * @package     retos
 * @subpackage  retos
 */

defined('JPATH_BASE') or die;

jimport('joomla.plugin.plugin');
jimport('joomla.error.error');
jimport('joomla.utilities.utility');

/**
 * Plugin class for retos .
 *
 * @package		Joomla.Plugin
 * @subpackage	System.logout
 */
class plgSystemRetos extends JPlugin
{
	/**
	 * Object Constructor.
	 *
	 * @access	public
	 * @param	object	The object to observe -- event dispatcher.
	 * @param	object	The configuration object for the plugin.
	 * @return	void
	 * @since	1.5
	 */
    function plgSystemRetos(& $subject, $config)
    {
        //$this->_db = JFactory::getDBO();
        parent :: __construct($subject, $config);

    }

    function require_object(){
    	require_once (JPATH_CONFIGURATION.'/_objects/includes/funciones.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/usuario.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/reto.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/_reto.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/categoria.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/areainteres.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/favoritos.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/tyc.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/solucioncompartida.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/terminosycondiciones.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/solucion.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/anexo.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/pregunta.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/retoxareainteres.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/retoxnegocio.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/retoxcompania.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/compania.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/companiaxnegocio.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/classlog.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/comite.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/ws/lotus.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/notificacion.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/respuesta.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/anexoxreto.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/alerta.php');
    	require_once (JPATH_CONFIGURATION.'/_objects/modelos/director.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/foro.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/respuestaforo.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/negocio.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/formulario.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/propiedad.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/regsolucion.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/detallesolucion.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/notificarretovencido.php');
        require_once (JPATH_CONFIGURATION.'/_objects/modelos/grafica.php');


    }

    /**
     * Crea conexión a la base de datos Retos con los parametros definidos en el plugin
     */
    function ini_DBRetos(){
        jimport('joomla.database.database');
        jimport( 'joomla.database.table' );

        $conf = JFactory::getConfig();
        $user = JFactory::getUSer();

        /**
         * Parametros definidos en el plugin
         */
        //TODO: agregar parametros al plugin de conexion a base de datos Retos
        $host       = $conf->get('host');
        $user       = $conf->get('user');
        $password   = $conf->get('password');
        $database   = 'retos';
        //$database   = $this->params->get('db_retos', '');
        $prefix     = $conf->get('dbprefix');
        $driver     = $conf->get('dbtype');
        $debug      = $conf->get('debug');


        $options    = array ( 'driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix );
        $db_retos = JDatabase::getInstance( $options );
        if ( JError::isError($db_retos) ) {
            header('HTTP/1.1 500 Internal Server Error');
            jexit('Database Error: ' . $db_retos->toString() );
        }

        if ($db_retos->getErrorNum() > 0) {
            JError::raiseError(500 , 'JDatabase::getInstance: Could not connect to database <br />' . 'joomla.library:'.$db_retos->getErrorNum().' - '.$db_retos->getErrorMsg() );
        }
        $db_retos->debug( $debug );
        $GLOBALS['db_retos'] = $db_retos;
        $db = JFactory::getDBO();
        $db->retos =& $db_retos;
    }

    function onAfterInitialise()
    {
        $this->require_object();
        $this->ini_DBRetos();
    }
}
