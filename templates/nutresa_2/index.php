<?php
/**
 * @version                $Id: index.php 21518 2011-06-10 21:38:12Z chdemko $
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

// check modules
$showRightColumn = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn == 0 and $showleft == 0) {
    $showno = 0;
}

JHtml::_('behavior.framework', true);
JHtml::_('behavior.modal');

// get params
$color = $this->params->get('templatecolor');
$logo = $this->params->get('logo');
$navposition = $this->params->get('navposition');
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$templateparams = $app->getTemplate(true)->params;
$user = JFactory::getUser();
$option = JRequest::getVar('option');
$view = JRequest::getVar('view');
$idreto = JRequest::getVar('idreto');

//$doc->addScript($this->baseurl.'/templates/beez_20/javascript/md_stylechanger.js', 'text/javascript', true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
    <head>
        <jdoc:include type="head" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/media/system/css/system.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/javascript/custom-form-elements.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/_objects/scripts/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/javascript/jquery.filestyle.mini.js"></script>
        <script type="text/javascript">
            window.addEvent('domready', function() {
                setTimeout( function() {
                    $$('div#contact-slider .pane-slider' ).removeClass( 'pane-hide' ).setStyle( 'height', 'auto');
                }, 350 );
            } );
<?php /*
  $(document).ready(function() {
  $("input[type='file']").filestyle({
  image: '<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/upload.png',
  imageheight: 21,
  imagewidth: 21,
  width: 100
  });
  }); */ ?>
        </script>

        <?php /*
          <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/javascript/otro_fix_png.js"></script>
         */
        ?>

    </head>

    <body>
        <div id="header">
            <div class="centrado">
                <div id="logos">
                    <h1>
                        <a href="#">
                            <?php if ($logo): ?>
                                <img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>"  alt="<?php echo htmlspecialchars($templateparams->get('sitetitle')); ?>" />
                            <?php endif; ?>
                            <?php if (!$logo): ?>
                                <?php echo htmlspecialchars($templateparams->get('sitetitle')); ?>
                            <?php endif; ?>
                            <span class="header1">
                                <?php echo htmlspecialchars($templateparams->get('sitedescription')); ?>
                            </span>
                        </a>
                    </h1>
                    <img src="templates/<?php echo $this->template ?>/images/logo_soluciones.jpg" width="178" height="85" alt="Imagix soluciones innovadoras">
                </div>
                <div id="nav">
                    <div>
                        <jdoc:include type="modules" name="position-1" />
                    </div>
                    <jdoc:include type="modules" name="position-0" />
                    <?php if ($user->id > 0) { ?>
                        <ul class="menu" >
                            <li class="item-467">
                                <a class="mainlevel" style="text-align: right;" href="index.php?option=com_users&view=profile" title="Preguntas Frecuentes" >Mi Perfil</a>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
                <div id="mainlevel-nav">
                    <jdoc:include type="modules" name="position-2" />
                    <?php
                    $usuario = new usuario($user->id);
                    //$usuario->usu_id = 20;
                    if ($usuario->usu_id) {

                        ?>
                        <ul class="menu">
                            <?php if (validarExperto($usuario->usu_email)) { ?>
                                <li class="item-622">
                                    <a href="index.php?option=com_preguntas&amp;task=experto&amp;Itemid=622">Expertos</a>
                                </li>
                            <?php } ?>
                            <?php if (validarComite($usuario->usu_email) || validarDirector($usuario->usu_email)) { ?>
                                <li class="item-621">
                                    <a target="_blank" href="http://aplica.gruponutresa.com/Aplicaciones/Portales/Soluciones_Innovadoras/si.nsf">Comité Evaluador</a>
                                </li>
                            <?php } ?>
                            <?php
                            $mostrar = validarVerParticipantes();
                            if ($mostrar) {
                                ?>
                                <li class="item-621">
                                    <a href="index.php?option=com_retos&view=participantes&id=2&Itemid=444">Ver Participantes</a>
                                </li>
                            <?php } ?>
                           <?php
                            $indicadores = validarIngresoGraficas($usuario->usu_email);
                            if ($indicadores) {
                            ?>
                                <li class="item-621">
                                    <a href="index.php?option=com_graphic&view=graphic&Itemid=629">Indicadores</a>
                                </li>
                            <?php } ?>
                            <?php
                              $redir=false;
                              $suplantacion = $usuario->validarAccesoAdministracionUsuarios($redir);
                              if($suplantacion){
                            ?>
                                <li class="item-621">
                                    <a href="index.php?option=com_usuario">Ingresar Solución</a>
                                </li>
                            <?php } ?>


                        </ul>
<?php } ?>
                </div>

                <div id="sesion_cont">
                    <?php if ($this->countModules('login-retos')) { ?>
                        <jdoc:include type="modules" name="login-retos" />
<?php } ?>
                </div>


            </div>

        </div>
        <?php
        $contenidotemplate = JRequest::getVar('option', '');
        switch ($contenidotemplate) {
            case 'com_home':
                $idcontenido = 'contenido_lineas';
                break;
            default : $idcontenido = 'contenido';
                break;
        }
        ?>
        <div id="<?php echo $idcontenido; ?>">
            <div class="centrado">
                <jdoc:include type="message" />
                <?php if ($option == 'com_home') { ?>
                    <?php if ($this->countModules('ultimos-retos')) { ?>
                        <jdoc:include type="modules" name="ultimos-retos" />
                    <?php }if ($this->countModules('buscador-retos')) { ?>
                        <jdoc:include type="modules" name="buscador-retos" />
                    <?php
                    }
                } else {
                    ?>
                    <jdoc:include type="component" />
                    <div style="margin-left: 700px; margin-bottom: 20px;">
                        <?php
                        isset($GLOBALS['galeriaRetos']) ? $rtimagen = "images/retos/{$GLOBALS['galeriaRetos']}" : $rtimagen = '';
                        if (isset($GLOBALS['galeriaRetos']) && file_exists($rtimagen)) {
                            if ($this->countModules('galeria') && $option == 'com_retos' && $idreto && $GLOBALS['galeriaRetos'] != '') {
                                ?>
                                <jdoc:include type="modules" name="galeria" />
                            <?php
                            }
                        } else {
                            ?>
                            <?php if ($this->countModules('buscador-retos')) { ?>
                                <jdoc:include type="modules" name="buscador-retos" />
                        <?php
                        }
                    }
                    ?>
                    </div>
<?php } ?>
                <div class="clearfix"></div>

<?php if ($contenidotemplate == 'com_home') { ?>
                    <div id="tabs">
                        <h3><?php echo JText::_('JACTUALIDAD'); ?></h3>
                        <jdoc:include type="modules" name="position-13" />
                        <div id="tabs_cont">
                            <jdoc:include type="modules" name="novedades-retos"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
<?php } ?>
            </div>


            <?php
            $session = JFactory::getSession();
            $idSpl = $session->get('idSuplantacion', false);
            if(!$idSpl){
            ?>
            <div id="footer">
                <div class="centrado">
                    <div id="carrusel">
                        <div id="slide">
                            <img src="templates/<?php echo $this->template ?>/images/helado.gif" width="960" height="135" alt="nutresa">
                        </div>
                        <jdoc:include type="modules" name="position-15" />
                    </div>
                    <p>© Grupo Nutresa - 2013 All rights reserved Terms of Use and Legal Conditions - solucionesinnovadoras@gruponutresa.com - Tel: +574 3655999 opci&oacute;n 3 -&nbsp; Medellin - Colombia</p>
                </div>
            </div>
            <?php } ?>
        </div>




    </body>
<?php
    $session = JFactory::getSession();
    $usuAdmin = $session->get('idUsuarioAdmin', false);
    if($usuAdmin){
?>
    <script>
        jQuery(document).ready(function(){
            var isIframe = (window.location != window.parent.location);
            var contentIframe = jQuery('#ifrSuplantacion').val();
            if(isIframe == false && typeof contentIframe == 'undefined'){
                jQuery.post('index.php?option=com_usuario&task=dejarSuplantacionAjax&format=raw', '', function(){location.reload();});
            }
        });
    </script>
<?php } ?>
</html>
