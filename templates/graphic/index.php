<?php
/**
 * @version                $Id: index.php 21518 2011-06-10 21:38:12Z chdemko $
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

// check modules
$showRightColumn = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn == 0 and $showleft == 0) {
    $showno = 0;
}

JHtml::_('behavior.framework', true);
JHtml::_('behavior.modal');

// get params
$color = $this->params->get('templatecolor');
$logo = $this->params->get('logo');
$navposition = $this->params->get('navposition');
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$templateparams = $app->getTemplate(true)->params;
$user = JFactory::getUser();
$option = JRequest::getVar('option');
$view = JRequest::getVar('view');
$idreto = JRequest::getVar('idreto');
$task = JRequest::getVar('task');
$rep_id = JRequest::getVar('rep_id');
$Itemid = JRequest::getVar('Itemid','629');
$cabecera = JURI::base() . 'templates/' . $this->template . '/images/cabecera2.jpg';
$color = "style='background-color: #53AF31;   margin: -0px 1px 1px 0px;'";
//$color='#53AF31';
if (isset($task)) {
    $cabecera = JURI::base() . 'templates/' . $this->template . '/images/cabezoteinterno.png';
    $color='';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
      xml:lang="<?php echo $this->language; ?>" 
      lang="<?php echo $this->language; ?>" 
      dir="<?php echo $this->direction; ?>">
    <head>
         <link rel="shortcut icon" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/favicon.ico"  />
     <style type="text/css">td img {display: block;}</style> 
     <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/style.css" type="text/css" />
     <script type="text/javascript" src="<?php echo $this->baseurl ?>/_objects/scripts/jquery.js"></script>
     <script type="text/javascript"  src='<?php echo $this->baseurl; ?>/_objects/scripts/jquery-ui.js' ></script>
        <script type="text/javascript"  src='<?php echo $this->baseurl; ?>/_objects/scripts/jquery.ui.dialog.js' ></script>
        <title>Indicadores</title>
</head>
<body <?php echo $color;?>>
<?php
if (isset($task) && isset($rep_id) ) {
    ?>  
    <img name="cabezote" src="<?php echo $cabecera; ?>"  border="0" id="cabezote" alt=""/>
        <jdoc:include type="component" />
    <?php
        }else{
            $cabecera = JURI::base() . 'templates/' . $this->template . '/images/cabecera4.jpg';
            include_once 'menu.php';
        }
    ?>
</body>
</html> 