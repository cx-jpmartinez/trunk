<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Nutresa
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

 // No direct access.
 defined('_JEXEC') or die;

 JLoader::import('joomla.filesystem.file');

 // Get params

$logo           = $this->params->get('logo');
$navposition    = $this->params->get('navposition');
$headerImage    = $this->params->get('headerImage');
$option         = JFactory::getApplication()->input->get('option');
$doc            = JFactory::getDocument();
$app            = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;
$config         = JFactory::getConfig();
$user           = JFactory::getUser();
$class          = "";

/**
 * Remover los scripts obligatorios de joomla.
 */
JHtml::_('bootstrap.framework');

unset($doc->_scripts[JURI::root(true) . '/media/system/js/mootools-core.js']);
unset($doc->_scripts[JURI::root(true) . '/media/system/js/mootools-more.js']);
unset($doc->_scripts[JURI::root(true) . '/media/system/js/core.js']);
unset($doc->_scripts[JURI::root(true) . '/media/system/js/caption.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);

//PROD
//$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.min.css', $type = 'text/css', $media = 'screen,projection');

// DEV
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', $type = 'text/css', $media = 'screen,projection');


 if ($option == 'com_retos')
 {
   JHtml::_('behavior.modal');
 }


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:jdoc="http://www.w3.org/1999/XSL/Transform"
      xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- head joomla include -->
        <script src="<?php echo $this->baseurl . '/templates/'.$this->template.'/js/libs/jquery-2.2.1.min.js' ?>"></script>
		<jdoc:include type="head" />
    <!-- end include -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500" rel="stylesheet" type="text/css" />

  </head>

  <body>
    <div id="top-header" class="container-fluid">
      <div class="row">
        <?php if ($this->countModules('user1')) { ?>
          <div id="user-area" class="navbar navbar-default">
                <div class="container">
                  <div class="  col-xs-offset-0 col-md-offset-3 col-xs-12 col-md-8  col-xs-offset-0  ">
                        <jdoc:include type="modules" name="user1" />
                  </div>
                </div>
          </div>
        <?php } ?>

        </div>

        <div class="row">
         <!-- <div class="logo col-xs-offset-0 col-md-offset-4 col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4">
            <figure> </figure>
          </div> -->
          <div class="col-xs-offset-0 col-md-offset-4 col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4">
            <img style="width: 150px;" width="150" height="100" src="<?php echo $this->baseurl . '/templates/' . $this->template . '/images/logos/logo.png' ?>" alt="">
          </div>
        </div>

      </div>
    </div>

    <div id="main-header" class="container-fluid">

      <div class="row">
        <nav id="main-menu" class="navbar navbar-default">
            <div class=" col-xs-offset-0 col-md-offset-2 col-xs-12 col-md-8">
              <!-- menu principal -->
              <?php if ($this->countModules('menu')) : ?>
                  <jdoc:include type="modules" name="menu" />
              <?php endif; ?>
            </div>
        </nav>
      </div>
      <?php if ($user->id > 0) : ?>
      <?php if ($this->countModules('menu2')) : ?>
        <div class="row">
          <nav id="menu-rol" class="navbar navbar-default">
            <div class="row">
              <div class=" col-xs-offset-0 col-md-offset-2 col-xs-12 col-md-8">
                <!-- menu secundario -->

                  <jdoc:include type="modules" name="menu2" />

              </div>
            </div>
          </nav>
        </div>
        <?php endif; ?>
      <?php endif; ?>


    </div>

    <!-- main -->
    <div id="main-page" class="container">
      <!-- Content -->
      <div id="main-content" class="col-xs-12 <?php echo $class ?>">
        <jdoc:include type="message" />
        <jdoc:include type="component" />
      </div>
    </div>

    <?php if ($option == 'com_home') : ?>
    <div id="home-page-1" class="container-fluid">
      <!-- Content -->
      <div id="home-content-1" class="col-xs-12 col-md-offset-2 col-md-8 ">

          <?php if ($this->countModules('ultimos-retos')) : ?>
            <jdoc:include type="modules" name="ultimos-retos" />
          <?php endif; ?>

            <jdoc:include type="message" />
            <jdoc:include type="component" />
      </div>
    </div>

    <div id="home-page-2" class="container">
      <!-- Content -->
      <div id="home-content-2" class="col-xs-12 col-md-offset-1 col-md-10 ">
        <jdoc:include type="modules" name="novedades"/>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($option == 'com_usuario') : ?>
        <?php $class="col-md-offset-1 col-md-10" ?>
    <?php else: ?>
      <?php $class="col-md-offset-2 col-md-8" ?>
    <?php endif; ?>


    <footer id="main-footer" class="container-fluid">

      <div class="footer-content row">

       <jdoc:include type="modules" name="footer" />

        <div class="footer-1 col-xs-offset-1 col-md-offset-1 col-xs-10 col-md-4  ">
          <span class="img-medium img-nutresa"></span>
        </div>

        <div class="footer-2 col-xs-offset-1 col-md-offset-1 col-xs-10 col-md-5  ">

          <div class="col-xs-offset-0 col-md-12  footer-content-element">
            <ul class="footer-content-element-menu">
              <li class="col-xs-12 col-md-4">
                <a href="index.php?option=com_contact&view=contact&id=11&Itemid=534"><span class="ico-42 ico-actualidad"></span><span class="footer-content-element-menu-span">Contactenos</span></a>

              </li>
              <li class="col-xs-12 col-md-4">
                <a href="index.php?option=com_content&view=category&id=79&Itemid=532"><span class="ico-42 ico-contacto"></span><span class="footer-content-element-menu-span">Actualidad</span></a>
              </li>
            </ul>
          </div>

        </div>
      </div>


      <div class="content-content row">
        <?php if ($this->countModules('footer')) { ?>
          <div class="row">
            <nav id="menu-footer" class="navbar navbar-default">
              <!-- menu principal -->
              <?php if ($this->countModules('footer')) : ?>
                <jdoc:include type="modules" name="footer" />
              <?php endif; ?>
            </nav>
          </div>
        <?php } ?>
      </div>

    </footer>


  <!-- <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/script.min.js' ?>"></script> -->
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/libs/bootstrap.min.js' ?>"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/script-dev.js' ?>"></script>

  </body>
</html>
