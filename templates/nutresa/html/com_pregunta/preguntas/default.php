<?php
defined('_JEXEC') or die('Restricted access');
$reto= $this->reto;
$preguntas = $this->preguntas;
$editarReto = false;
$fechaActual = date('Y-m-d H:i');
if($fechaActual >= $reto->ret_fechaCreacion && $fechaActual<=$reto->ret_fechaVigencia && $reto->ret_estado='Vigente')
{
    $editarReto = true;
}

?>


<?php if($editarReto) { ?>
<script type="text/javascript">
function validarPreg ()
{
    form = document.forms.preguntaFrm;
    if($("#pre_nombre").val().length <= 0) {
        alert("El campo titulo es requerido");
        return false;
    }
    if($("#pre_comentario").val().length <= 0) {
        alert("El campo pregunta es requerido");
        return false;
    }
    $("#Enviar").attr('disabled','disabled');
    form.submit();
}
</script>

    <article class="article-component">
        <header><h1>Pregunta al experto</h1></header>
        <section>
            <h4><?php echo $this->reto->ret_nombre; ?></h4>

            <form action="index.php" method="post" name="preguntaFrm" id="preguntaFrm" class="preguntaFrm form-horizontal">
                <div class="form-group">
                    <label  class="col-xs-3  control-label"><?php echo JText::_('COM_PREGUNTAS_TITULO');?>:</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control"  name="pre_nombre" id="pre_nombre"/>
                    </div>
                </div>
                <div class="form-group">
                    <label  class=" col-xs-3 control-label"><?php echo JText::_('COM_PREGUNTAS_DESCRIPCION');?>:</label>
                    <div class="col-xs-8">
                        <textarea  name="pre_comentario" class="form-control"   id="pre_comentario"></textarea>
                    </div>
                </div>
                <div class="form-group ">
                        <div id="acepto" class="col-xs-offset-3 col-xs-8" >
                            <input type="button" value="Enviar" id="Enviar" class="btn-readmore" src="" onclick="validarPreg();"/>
                            <input type="hidden" name="task" value="preguntar" />
                            <input type="hidden" name="option" value="com_pregunta" />
                            <input type="hidden" name="idRet" value="<?php echo $this->ret_id; ?>" />
                            <input type="hidden" name="idUsu" value="<?php echo $this->usuario->usu_id; ?>" />
                            <input type="hidden" name="pregunt" value="1"/>

                        </div>
                </div>

            </form>
        </section>
</article>

<?php } ?>
<?php if($this->preguntas) {?>


    <article class="article-component article-historial row">
        <header>
            <h1><?php echo JText::_('COM_PREGUNTAS_HISTORIAL');?></h1>
        </header>
        <section>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
            <?php foreach ($this->preguntas as $key => $pregunta){ ?>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading<?php echo $key; ?>">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>"
                               aria-expanded="true" aria-controls="collapse<?php echo $key ?>">
                                <?php echo $pregunta->pre_titulo; ?>

                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $key ?>" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading<?php echo $key ?>">
                        <div class="panel-body">
                            <p><label class="control-label">Innovador</label>:
                             <?php echo $pregunta->usu_nombre; ?>
                            </p>
                            <p>
                                <label class="control-label"><?php echo JText::_('COM_PREGUNTAS_PREGUNTA'); ?></label>:
                                <?php echo $pregunta->pre_descipcion; ?>
                            </p>
                            <p>
                                <label class="control-label"><?php echo JText::_('Fecha'); ?></label>:
                            <?php echo $pregunta->pre_fechaCreacion; ?>

                            <?php if ($pregunta->pre_respuesta) {
                                $respuesta = new respuesta();
                                $respuestas = $respuesta->cargarRespuestas($pregunta->pre_id);
                                foreach ($respuestas as $rest) {

                                    ?>

                                    <span><label><?php echo JText::_('COM_PREGUNTAS_RESPUESTA'); ?></label></span>

                                    <span><?php echo $rest->res_respuesta; ?></span>

                                    <span><label><?php echo JText::_('Fecha'); ?>:</label></span>

                                    <span><?php echo $rest->res_fecha; ?></span>

                                <?php }
                            } ?>
                            </p>
                        </div>
                    </div>
                </div>
                    <?php } ?>
            </div>
        </section>
    </article>

<?php } ?>
<?php if( count($this->preguntas) < 1 && !$editarReto  ) :  ?>
  <article class="article-component article-historial row">
    <header>
        <h1>No hay preguntas disponibles</h1>
    </header>
  </article>
<?php endif; ?>
