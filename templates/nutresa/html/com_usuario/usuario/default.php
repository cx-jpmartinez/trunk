<?php
defined('_JEXEC') or die('Restricted access');

?>

<h1><?php echo 'Ingresar Solución'; ?></h1>

<div class="formBuscar form-horizontal col-xs-12 col-md-12">
    <div class="form-group">
        <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USUARIO_LAB_BUSCAR_USUARIO'); ?>:</label>
        <div class="controls col-xs-12 col-md-4">
            <input type="text" name="strBuscar" class="form-control" placeholder="Buscar" requerid="required"/>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-12 col-md-2">Filtro:</label>
        <div class="controls col-xs-12 col-md-4">
            <select class="col-xs-12" name="filtro">
                <option value="usu_nombre"><?php echo JText::_('COM_USUARIO_OPTION_NOMBRE');?></option>
                <option value="usu_email"><?php echo JText::_('COM_USUARIO_OPTION_EMAIL');?></option>
                <option value="usu_cedula"><?php echo JText::_('COM_USUARIO_OPTION_CEDULA');?></option>
            </select>
        </div>
    </div>

    <div class="col-xs-12 col-md-offset-2 col-md-4  button-area">
        <input type="button" class="simple-button" value="<?php echo JText::_('COM_USUARIO_BTN_BUSCAR'); ?>" onclick="ajaxBuscarUsuarios();"/>
        <input type="button" class="simple-button" name="btnCrear" value="<?php echo JText::_('COM_USUARIO_BTN_CREAR'); ?>" onclick="javascript: window.location.href = 'index.php?option=com_usuario&task=verRegistro';"/>
    </div>
</div>



<div id="resultadoBusqueda" class="row"></div>

<div id="content_locker" class="row">
    <img src="<?php echo $this->baseurl; ?>/images/loadingAnimation.gif"/>
</div>

<script>
    
    function ajaxBuscarUsuarios(){
        jQuery("#content_locker").show();
        var filtro = jQuery('select[name="filtro"]').val();
        var criterio = jQuery('input[name="strBuscar"]').val();
        jQuery.ajax({
            url: 'index.php?option=com_usuario &task=buscarUsuario&format=raw',
            type: 'post',
            data:{'strBuscar': criterio, 'filtro': filtro},
            dataType: "html",
            success: function(html){ 
                        jQuery("#resultadoBusqueda").html(html);
                        jQuery("#content_locker").hide();
                    },
            error: function(){
                    jQuery("#content_locker").hide();
                }
        });
    }

</script>