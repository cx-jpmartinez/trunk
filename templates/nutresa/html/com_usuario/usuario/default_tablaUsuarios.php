<?php
defined('_JEXEC') or die('Restricted access');

if(count((array)$this->usuarios) == 0) : ?>
    <div class="col-xs-12"><p class="bg-info"><?php echo JText::_('COM_USUARIO_USUARIO_NO_DATA') ?></p></div>;
<?php else: ?>
<div class="table-responsive col-xs-12">
    <table id="box-table-a" class=" table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_NOMBRE'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_NOMBREUSUARIO'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_EMAIL'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_TELEFONO'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_DIRECCION'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_CEDULA'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_COMPANIA'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_ACCION'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($this->usuarios as $usuario){ ?>
            <tr>
                <td><?php echo $usuario->usu_nombre; ?></td>
                <td><?php echo $usuario->usu_nombreUsuario; ?></td>
                <td class="td-mail"><a href="mailto:<?php echo $usuario->usu_email; ?>"> <?php echo $usuario->usu_email; ?></a></td>
                <td><?php echo $usuario->usu_telefono; ?></td>
                <td><?php echo $usuario->usu_direccion; ?></td>
                <td><?php echo $usuario->usu_cedula; ?></td>
                <td><?php echo $usuario->usu_compania; ?></td>
                <td><a href="javascript:;" onclick="suplantar('<?php echo $usuario->usu_id; ?>')"><?php echo JText::_('COM_USUARIO_USUARIO_SUPLANTAR'); ?></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function suplantar(id){
        if(typeof id != 'undefined'){
            window.location.href = "index.php?option=com_usuario&task=suplantarUsuario&usu_id="+id;
        }
    }
</script>
<?php endif ?>