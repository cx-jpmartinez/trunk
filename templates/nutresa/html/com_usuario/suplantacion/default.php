<?php
    defined('_JEXEC') or die('Restricted access');

    $session = JFactory::getSession();
    $idSpl = $session->get('idSuplantacion', false);
    if(!$idSpl){
        return;
    }
    $user = JFactory::getUser();
    $usuario = usuario::getUsuario($user->id);

?>

<article id="suplantacion">
    <header>
        <h1><?php echo JText::_('COM_USUARIO_TIT_SUPLANTACION'); ?></h1>
    </header>
    <section class="suplantacion-head">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div>
                    <span><?php echo JText::_('COM_USUARIO_LAB_NOMBRE');?>: </span>
                    <span><?php echo $usuario->usu_nombre;?></span>
                </div>
                <div>
                    <div class="button-area">
                        <input type="button" class="simple-button" value="<?php echo JText::_('COM_USUARIO_BTN_DEJAR_SUPLANTACION'); ?>" id="btnNoSuplantar" onclick="dejardesuplantar()"/>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="suplantacion-details">
        <div class="row">
            <div class="col-xs-12">
                <iframe id="ifrSuplantacion" name="ifrSuplantacion" src="<?php echo JURI::base(); ?>"></iframe>
            </div>
        </div>
    </section>
</article>
<script>
    function dejardesuplantar(){
        window.location.href = 'index.php?option=com_usuario&task=dejarSuplantacion';
    }
</script>
    