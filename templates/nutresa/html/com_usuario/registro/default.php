<?php
    defined('_JEXEC') or die('Restricted access');
?>
<article>
    <h1><?php  echo JText::_('COM_USUARIO_REGISTRO_TIT_REGISTRO');?></h1>
    <form class="form-horizontal" method="post" action="index.php?option=com_usuario&task=guardarRegistro">


        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_NOMBRE'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control"  name="usu_nombre" id="nombre" required="required" value="<?php echo array_key_exists('usu_nombre', $this->arrData) ?  $this->arrData['usu_nombre'] : ''; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_NOMBREUSUARIO'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control"  name="usu_nombreUsuario" id="nombreUsuario" required="required" value="<?php echo array_key_exists('usu_nombreUsuario', $this->arrData) ?  $this->arrData['usu_nombreUsuario'] : ''; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_EMAIL'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control"  name="usu_email" id="email"  required="required" value="<?php echo array_key_exists('usu_email', $this->arrData) ?  $this->arrData['usu_email'] : ''; ?>"/></td>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_TELEFONO'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control"  name="usu_telefono" id="telefono" required="required" pattern="^(0|[1-9][0-9]*)$" title="<?php echo JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_NUMERICO');?>" value="<?php echo array_key_exists('usu_telefono', $this->arrData) ?  $this->arrData['usu_telefono'] : ''; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_DIRECCION'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="usu_direccion" id="direccion" required="required" value="<?php echo array_key_exists('usu_direccion', $this->arrData) ?  $this->arrData['usu_direccion'] : ''; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_CEDULA'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="usu_cedula" id="cedula" required="required" pattern="^(0|[1-9][0-9]*)$" title="<?php echo JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_NUMERICO');?>" value="<?php echo array_key_exists('usu_cedula', $this->arrData) ?  $this->arrData['usu_cedula'] : ''; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label  class=" col-md-2 control-label"><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_COMPANIA'); ?> <span class="text-danger">*</span></label>
            <div class="col-md-4">
                <select name="usu_compania" id="compania" class="form-control" required="required">
                    <?php
                    $valSelect = array_key_exists('usu_compania', $this->arrData) ? $this->arrData['usu_compania'] : '';
                    foreach ($this->objcompania as $row){
                        ?>
                        <option value="<?php echo $row->neg_nombre; ?>" <?php if($valSelect == $row->neg_nombre){echo "selected='selected' ";} ?> ><?php echo $row->neg_nombre?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class=" col-sm-offset-2 col-sm-10">
                <div class="button-area">
                    <input type="submit" class="simple-button" value="<?php echo JText::_('COM_USUARIO_REGISTRO_BTN_SUBMIT');?>" id="btnRegistrar"/>
                    <input type="hidden" name="option" value="com_usuario"/>
                    <input type="hidden" name="task" value="guardarRegistro"/>
                </div>
            </div>
        </div>
    </form>
</article>
