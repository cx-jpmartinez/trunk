<?php defined('_JEXEC') or die; ?>
<?php

$default   = true;
$i         = 1;
?>
<article id="ultimos-retos">
    <section id="gallery-full" class="row">
        <?php foreach ($objs as $obj) : ?>

            <?php
                    $paticipantes = $retos->numeroPostulaciones($obj->ret_id);
                    $imgReto      = false;
                    $class = ( $default ) ? "active" : "";

                    if ($obj->ret_imagenPeq) : ?>
                        <?php if ( file_exists( JPATH_BASE."/images/retos/".$obj->ret_imagenPeq) ) : ?>

                                <figure id="rel-<?php echo $obj->ret_id ?>" class="<?php echo $class ?>" >
                                    <img class="img-responsive" src="<?php echo JURI::base().'images/retos/'.$obj->ret_imagenPeq ?>" />
                                </figure>
                                <?php $imgReto = true; ?>
                        <?php endif; ?>

                <?php endif; ?>

                <?php if( !$imgReto ) : ?>
                    <figure id="rel-<?php echo $obj->ret_id ?>" class="<?php echo $class ?> ">
                        <img class="img-responsive"
                             src="<?php echo JURI::base() . '/templates/nutresa/images/icons/img.ppal.png' ?>"/>
                    </figure>
                <?php endif; ?>
                <?php $default = false; ?>

            <?php endforeach; ?>
    </section>
    <section id="details" class="row">
        <?php foreach ($objs as $obj) : ?>

            <?php $paticipantes = $retos->numeroPostulaciones($obj->ret_id) ?>

            <div class="item item-type-<?php echo $i ?> col-xs-12 col-md-3">
                <div class="col-md-10 col-md-offset-1 col-xs-12 col-md-offset-0">
                    <figure class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <span class="ico-50 ico-reto-<?php echo $i ?>"></span>
                        </div>
                    </figure>
                    <p class="item-text">
                        <?php echo $obj->ret_nombre; ?>
                    </p>
                    <div class="button-area">
                        <a id="btn-<?php echo $obj->ret_id ?>" class="btn btn-readmore-inverse" href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $obj->ret_id; ?>&Itemid=444">
                            <span class="ico-20 ico-readmore"></span> <?php echo JText::_("TPL_NUTRESA_LABEL_READ_MORE") ?>
                        </a>
                    </div>
                </div>
            </div>
            <?php $i = ($i == 3 ) ? $i = 1 : $i = $i +1; ?>
        <?php endforeach; ?>
    </section>
</article>
