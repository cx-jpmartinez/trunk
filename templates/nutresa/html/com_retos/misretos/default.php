<?php 	defined('_JEXEC') or die('Restricted access'); ?>
<?php   $objreto = new reto(); ?>

<article class="item-page row">
	<header class="item-page-header">
		<h1><?php echo JText::_('COM_RETOS_TITULO_MIS_RETOS');?></h1>
	</header>
		<?php if($this->obj): ?>
				<?php foreach ($this->obj as $reto): ?>
					<?php
							$tamTiutlo = '';
							$editarReto = false;
							$fechaActual = date('Y-m-d H:i');

							$solucion = new solucion();
							$solucion->cargarSolucion($reto->ret_id,$this->usuario->usu_id);

							if( ($fechaActual >= $reto->ret_fechaCreacion &&
								$fechaActual<=$reto->ret_fechaVigencia) &&
								($reto->ret_estado=='Vigente')
							)
							{
								$editarReto = true;
							}
					?>
					<section class="item-page-content reto col-xs-12">
						<div class="col-xs-12 col-md-offset-2 col-md-2">
								<div class="num-circle">
									<span>
										<?php
										$reto->_ret_postulacion = $objreto->numeroPostulaciones($reto->ret_id);
										if ($reto->_ret_postulacion)
										{
											echo $reto->_ret_postulacion;
										} else
										{
											echo 0;
										}
										?>
									</span>
							</div>
							<div class="text-circle-footer">
								Postulaciones
							</div>
						</div>

						<div class="col-xs-12 col-md-4">
							<div>
								<a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
									<?php  echo $reto->ret_nombre; ?>
								</a>
							</div>
							<label><?php echo JText::_('RETO_PUNTOS'); ?></label>
							<span>
								<?php echo $reto->ret_puntos; ?>
							</span>
							<label>
								<?php echo  JText::_('RETO_FECHA_LIMITE'); ?>
							</label>
							<span>
								<?php echo $reto->_fechaVigencia; ?>
							</span>

							<div class="button-area">

								<a  class="modal" rel="{handler: 'iframe', size: {x: 680, y: 350}}"
									id="modal"
									href="index.php?option=com_solucion&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">

									<input type="button" value="Solución" class="simple-button"  />
								</a>

								<a class="modal" rel="{handler: 'iframe', size: {x: 700, y: 500}}"
								   id="modal"
								   href="index.php?option=com_pregunta&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
									<input type="button" value="Pregunta al experto" class="simple-button"   border="0"/>
								</a>

							</div>
						</div>

						<figure class="hidden-xs col-md-4">
								<?php if (!empty($reto->ret_imagenPeq) && file_exists("./images/retos/$reto->ret_imagenPeq")) : ?>
									<img src="images/retos/<?php echo $reto->ret_imagenPeq; ?>" />
								<?php else: ?>
									<img src="<?php echo $this->baseurl . '/templates/nutresa/images/icons/img-interna.png' ?>" />
								<?php endif; ?>
						</figure>


					</section>
				<?php endforeach; ?>
		<?php else: ?>
			<section class="item-page-content reto col-xs-12">
				<h3> Sin resultados </h3>
				<div>
					<p>No hay retos disponibles.</p>
				</div>
			</section>
		<?php endif; ?>
	
</article>
