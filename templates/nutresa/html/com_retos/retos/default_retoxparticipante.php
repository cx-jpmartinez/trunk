<?php
    defined('_JEXEC') or die('Restricted access');
?>

<?php $objreto = new reto(); ?>
<article class="item-page ">
    <header class="item-page-header">
        <h1>Participantes</h1>
    </header>
    <?php if($this->obj): ?>
        <?php foreach ($this->obj as $reto): ?>
            <?php $paticipantes = $objreto->numeroPostulaciones($reto->ret_id); ?>
                <section id="reto-<?php echo $reto->ret_id; ?>" class="item-page-content reto col-xs-12">
                    <div class="col-xs-12 col-md-offset-2 col-md-2">
                        <a class="modal" rel="{handler: 'iframe', size: {x: 700, y: 500}}" id="modal" href="index.php?option=com_retos&amp;view=verparticipantes&amp;id=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                            <div class="num-circle">
                                    <span>
                                    <?php
                                    $ret_postulacion = $paticipantes;
                                    if($ret_postulacion)
                                    {
                                        ?>

                                            <span><?php echo $ret_postulacion ; ?></span>

                                        <?php
                                    }else
                                    {
                                        echo 0;
                                    }
                                    ?>
                                    </span>
                            </div>
                        </a>
                        <div class="text-circle-footer">
                            Participantes
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <div>
                            <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
                                <?php echo $reto->ret_nombre; ?>
                            </a>
                        </div>
                        <div>
                            <?php
                            $reto->ret_resumen = str_replace('<p>', '', $reto->ret_resumen);
                            $reto->ret_resumen = str_replace('</p>', '', $reto->ret_resumen);
                            $reto->ret_resumen = strip_tags($reto->ret_resumen);
                            echo substr($reto->ret_resumen, 0, 200);
                            ?>
                        </div>
                        <div>
                            <label><?php echo JText::_('RETO_PUNTOS'); ?></label><span><?php echo $reto->ret_puntos; ?></span>
                            |
                            <label><?php echo JText::_('RETO_FECHA_LIMITE'); ?></label><span><?php echo $reto->_fechaVigencia; ?></span>
                        </div>


                        <div class="button-area">
                            <label>Participantes : </label>
                            <?php
                            $ret_postulacion = $paticipantes;
                            if($ret_postulacion)
                            {
                                ?>
                                <a class="modal" rel="{handler: 'iframe', size: {x: 700, y: 500}}" id="modal" href="index.php?option=com_retos&amp;view=verparticipantes&amp;id=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                    <span><?php echo $ret_postulacion ; ?></span>
                                </a>
                                <?php
                            }else
                            {
                                echo 0;
                            }
                            ?>
                        </div>
                    </div>

                    <figure class="hidden-xs col-md-4">
                        <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
                            <?php if (!empty($reto->ret_imagenPeq) && file_exists("./images/retos/$reto->ret_imagenPeq")) : ?>
                                <img src="images/retos/<?php echo $reto->ret_imagenPeq; ?>" />
                            <?php else: ?>
                                <img src="<?php echo $this->baseurl . '/templates/nutresa/images/icons/img-interna.png' ?>" />
                            <?php endif; ?>
                        </a>
                    </figure>

                </section>
        <?php endforeach; ?>
    <?php else: ?>
        <div>
            <p  class="bg-info">
                <?php echo JText::_('MOD_BUSCAR_NO_BUSQUEDA'); ?>
            </p>
        </div>
    <?php endif; ?>
</article>


