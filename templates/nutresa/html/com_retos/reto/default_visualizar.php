<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$msFavorito = 'Agregar a Favoritos';
if($reto->_favorito){
    $msFavorito = 'Ver Favoritos';
}
if($reto->ret_galeria){
    $GLOBALS['galeriaRetos'] = $reto->ret_galeria;  
}
if(isset($this->solucion)){
    $solucion= $this->solucion;
}
$fechaActual = date('Y-m-d H:i');
$editarReto = false;
if(($fechaActual >= $reto->ret_fechaCreacion && $fechaActual<=$reto->ret_fechaVigencia) &&  $reto->ret_estado='Vigente'){
    $editarReto = true;
}
?>

<article id="page-reto">
    <header>
        <?php if ($reto->ret_imagenPeq) : ?>
            <?php if ( file_exists( JPATH_BASE."/images/retos/".$reto->ret_imagenPeq) ) : ?>
                <figure >
                    <img class="img-responsive" src="<?php echo JURI::base().'images/retos/'.$reto->ret_imagenPeq ?>" />
                </figure>
            <?php endif; ?>
        <?php endif; ?>
        <div class="sub-info row">
            <div class="sub-info-izq col-xs-6">
                <?php if($reto->ret_fechaVigencia): ?>
                    <label>
                        <?php echo JText::_('RETO_FECHA_LIMITE'); ?>
                    </label>

                    <span> <?php $fechaVigencia = explode(' ', $reto->ret_fechaVigencia); ?></span>
                    <span>
                        <?php echo $fechaVigencia[0]; ?>
                    </span>
                <?php endif ?>

                <?php if($reto->ret_puntos): ?>

                    <label>
                        <?php  echo JText::_('RETO_PUNTOS'); ?>
                    </label>
                    <span>
                        <?php echo $reto->ret_puntos;?>
                    </span>
                    <label><?php echo JText::_('RETO_SOLUCIONADORES'); ?></label>
                    <span><?php echo $reto->_postulaciones; ?></span>
                <?php endif ?>
            </div>

            <div class="sub-info-der col-xs-6">
                <?php
                if($usuario->usu_id>0) : ?>
                    <?php if($reto->_solucion && $reto->_tycusuario>0) : ?>
                        <?php
                        if(isset($reto->_categoria)) : ?>
                            <label >Categoría:</label>
                            <?php echo $reto->_categoria;
                        endif ?>
                        <?php
                        if(isset($reto->_negocios)) : ?>
                            <label >Negocio:</label>
                            <?php  echo "$reto->_negocios";
                        endif ?>
                        <?php if($reto->_areasInteres) : ?>
                            <label>Área de interés: </label>
                            <?php echo $reto->_areasInteres; ?>
                        <?php endif ?>
                        <?php
                    endif;
                endif;
                ?>
            </div>
        </div>
        <div class="resumen">
            <h1 class="titulo_reto"><?php  echo $reto->ret_nombre; ?></h1>
            <p><?php echo $reto->ret_resumen; ?></p>
        </div>
    </header>

    <?php
        if($reto->_solucion && $reto->_tycusuario>0){ ?>
            <section>
                <div>
                    <h4><?php echo JText::_('COM_RETOS_DESCRIPCION');  ?></h4>
                    <p><?php echo $reto->ret_descripcion; ?></p>
                </div>
                <?php if($reto->ret_antecedente  &&  strlen($reto->ret_antecedente)>8): ?>
                    <div>
                        <h4><?php echo JText::_('Antecedentes:'); ?></h4>
                        <p><?php echo $reto->ret_antecedente; ?></p>
                    </div>
                <?php endif; ?>

                <?php if($reto->ret_resultadoEsperado  &&  strlen($reto->ret_resultadoEsperado)>8): ?>
                    <div>
                        <h4><?php echo JText::_('COM_RETOS_RESULTADO_ESPERADO'); ?></h4>
                        <p><?php echo $reto->ret_resultadoEsperado; ?></p>
                    </div>
                <?php endif; ?>
            </section>

            <section class="objetivos">
                <?php if($reto->ret_nosebusca &&  strlen($reto->ret_nosebusca)>8) : ?>
                    <div>
                        <h4><?php echo JText::_('Información Complementaria:'); ?></h4>
                        <p><?php  echo $reto->ret_nosebusca;?></p>
                    </div>
                <?php endif; ?>
                <?php if($reto->ret_criterioEvaluacion  &&  strlen($reto->ret_criterioEvaluacion)>8): ?>
                    <div>
                        <span ><?php echo JText::_('Criterios de evaluación:'); ?></span>
                        <p><?php  echo $reto->ret_criterioEvaluacion;?></p>
                    </div>
                <?php endif; ?>
                <?php if($reto->ret_objetivo  &&  strlen($reto->ret_objetivo)>8): ?>
                    <div>
                        <span><?php echo JText::_('Objetivos Estratégicos:'); ?></span>
                        <p><?php  echo $reto->ret_objetivo;?></p>
                    </div>
                <?php endif; ?>
            </section>

            <?php if($this->retoAnexos): ?>
                <section>
                    <span><?php echo JText::_('Anexos:'); ?></span>
                    <p>
                        <?php foreach ($this->retoAnexos as $retoAnexos): ?>
                            <a href="images/retos/anexos/<?php echo $retoAnexos->axr_archivo; ?>" target="blank">
                                <?php echo $retoAnexos->axr_archivo; ?>
                            </a>
                        <?php endforeach; ?>
                    </p>
                </section>
            <?php  endif; ?>

        <?php } // end if $reto->_solucion ?>

        </section>
</article>