<?php defined('_JEXEC') or die('Restricted access'); ?>

<article class="article-component">
	<header><h1>Iniciar sesión </h1></header>
	<section>
		<form action="index.php" method="post" id="login-form" class="form-horizontal">
			<div class="form-group">
				<label class="col-xs-3 control-label">Usuario: </label>
				<div class="col-xs-8">
					<div class="input-group">
						<input id="modlgn-username" class="form-control" type="text" name="username"  />
						<div class="input-group-addon"><span >&commat;</span></div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-xs-3 control-label">Contraseña:</label>
				<div class="col-xs-8">
					<div class="input-group">
						<input id="modlgn-passwd" class="form-control" type="password" name="password"  />
						<div class="input-group-addon"><span>&#128274;</span></div>
					</div>
				</div>
			</div>

			<div class="form-group col-xs-6 col-xs-offset-6">
				<div id="acepto" class="button-area col-xs-6 col-xs-offset-6">
					<input type="submit" name="Submit" class="simple-button" value="<?php echo JText::_('JLOGIN') ?>" />
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="recargar" value="1" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</div>
		</form>
	</section>
</article>

