<?php
/**
 * @version		$Id: edit.php 21321 2011-05-11 01:05:59Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load( 'plg_user_profile', JPATH_ADMINISTRATOR );
?>


<article>
    <header>
        <?php if ($this->params->get('show_page_heading')){ ?>
            <h1><?php echo $this->escape($this->params->get('page_heading'));?></h1>
        <?php } ?>
    </header>
    <section>

        <form id="member-profile"  method="post" class="form-horizontal form-validate">
            <h1>Datos Personales : </h1>
            <?php foreach ($this->form->getFieldsets() as $group => $fieldset){// Iterate through the form fieldsets and display each one.?>
            <?php $fields = $this->form->getFieldset($group);?>
            <?php if (count($fields) && $group != 'params'){?>


                    <?php foreach ($fields as $field) {// Iterate through the fields in the set and display them.?>

                        <?php

                        try {

                            if (isset($this->datosUsuario->{$field->fieldname})) {
                                $this->form->setValue($field->fieldname, null, $this->datosUsuario->{$field->fieldname});
                            }

                            $fieldInput = $this->form->getInput($field->fieldname);

                        } catch (Exception $e) {
                            $fieldInput = 'Error en : ' . $field->fieldname . " > " . $e->getMessage() . "\n";
                        }
                        ?>

                        <?php if ($field->type == "Hidden") {?>
                            <?php echo $fieldInput ?>
                        <?php } else { ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <?php echo $field->label; ?>
                                    <?php if (!$field->required && $field->type != 'Spacer') { ?>
                                         <span class="text-warning"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
                                    <?php } ?>
                                </label>
                                <div class="col-md-10">
                                    <?php echo $fieldInput; ?>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>

                <?php } ?>
            <?php } ?>
            <div class="button-area col-sm-offset-2 col-sm-10">
                <input type="submit" class="simple-button" value="Aceptar">
                <input type="hidden" name="option" value="com_users"/>
                <input type="hidden" name="task" value="profile.save"/>
                <?php echo JHtml::_('form.token'); ?>
            </div>

    </section>
</article>

