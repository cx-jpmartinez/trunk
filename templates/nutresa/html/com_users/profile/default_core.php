<?php
/**
 * @version		$Id: default_core.php 21020 2011-03-27 06:52:01Z infograf768 $
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
$user = JFactory::getUser();
$usuario = new usuario($user->id);
$areas   = $this->areas;
if($usuario->usu_id <= 0)
    return;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');

?>
<script xmlns="http://www.w3.org/1999/html">
    function confor(){
        var answer= confirm('<?php echo JText::_('COM_USERS_REGISTER_CONFIRMACION');?>?');
        if(answer){
            return true;
        }else{
            return false;
        }
    }
</script>


<article>
    <header>
        <h1>Mi Perfil</h1>
    </header>
    <section>
        <figure class="image-form">


            <?php if (!$usuario->usu_foto) { ?>
                <img class="img-thumbnail" src="<?php echo JURI::base() .'images/retos/perfiles/perfilblanco.jpg'; ?>" width="200px;"/>
            <?php } else { ?>
                <?php /*list($width, $height)*/
                @ $arrSize = getimagesize(JURI::base()  . "images/retos/perfiles/" . $usuario->usu_foto); ?>
                <img class="img-thumbnail" style="text-align: center;"
                     src="<?php echo JURI::base() . 'images/retos/perfiles/' . $usuario->usu_foto; ?>"
                     width="<?php echo $arrSize[0] > 200 ? '200px' : $arrSize[0] . 'px'; ?>"/>
            <?php } ?>
        </figure>
        <h4> Datos Personales : </h4>
        <form action="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>"
              method="POST"
              name="formFotoPerfil"
              class="form-horizontal">

        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_REMIND_EMAIL_LABEL'); ?>:</label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_email; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_nombre; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('Cédula:'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_cedula; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('Télefono:'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_telefono; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('Compañia:'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_compania; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"> <?php echo JText::_('Área:'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_area; ?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('Cargo:'); ?></label>
            <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_cargo; ?></div>
        </div>
        <div class="form-group"><label class="control-label col-xs-12 col-md-2"><?php echo JText::_('Dirección  de residencia:'); ?></label>
            <div class="col-xs-12 col-md-10 "><?php echo $usuario->usu_direccion; ?></div>
        </div>

        <?php if($user->id  == 96 || $user->id  == 97 || $user->id  == 98 || $user->id  == 99){ ?>
            <div class="form-group">
                <label class="control-label col-xs-12 col-md-2"> <?php echo JText::_('COM_USERS_REGISTER_APELLIDO_LABEL:'); ?>: </label>
                <div class="col-xs-10"><?php echo $usuario->usu_apellido; ?></div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_REGISTER_PAIS_LABEL:'); ?>:</label>
                <div class="col-xs-10"><?php echo $usuario->usu_pais; ?></div>
            </div>

            <?php if($usuario->usu_ciudad) : ?>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('LAB_CIUDAD'); ?>:</label>
                    <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_ciudad; ?></div>
                </div>
            <?php endif;?>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_REGISTER_DIRECCION_LABEL'); ?>:</label>
                    <div class="col-xs-10"><?php echo $usuario->usu_direccion; ?></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_REGISTER_TELEFONO_LABEL'); ?>:</label>
                    <div class="col-xs-12 col-md-10"><?php echo $usuario->usu_telefono; ?></div>
                </div>
        <?php } ?>


            <div class="form-group">
                <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?></label>
                <div class="col-xs-12 col-md-10"><?php echo JHtml::_('date',$this->data->registerDate); ?></div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-12 col-md-2"><?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?></label>
                <div class="col-xs-12 col-md-10">
                    <?php if ($this->data->lastvisitDate != '0000-00-00 00:00:00'){?>
                            <?php echo JHtml::_('date',$this->data->lastvisitDate); ?>
                    <?php }
                    else {?>
                            <?php echo JText::_('COM_USERS_PROFILE_NEVER_VISITED'); ?>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-2 ">
                    <div id="acepto" class="button-area">
                        <input type="submit" class="simple-button" value="Editar">
                        <input type="hidden" name="option" value="com_users" />
                        <input type="hidden" name="view" value="profile.edit" />
                    </div>
                </div>
            </div>
    </form>
    </section>
    
    <section>
        <h4 ><?php echo JText::_('LAB DATOS ADICIONALES'); ?></h4>
        
        <form  enctype='multipart/form-data'
               action="<?php echo JRoute::_('index.php?option=com_users&task=profile.subirImagen'); ?>"
               method="POST"
               name="formFotoPerfil"
               class="form-horizontal"
        >
            <div class="form-group">
                <label class="control-label col-xs-12 col-md-2"> <?php echo JText::_('LAB CAMBIAR IMAGEN'); ?>:</label>
                <div class="col-xs-12 col-md-10">
                    <input type="file" name="usu_foto" />
                </div>
            </div>
            <div class="form-group">
                <div id="acepto" class="col-xs-8 col-xs-offset-2  button-area">
                    <input type="submit" class="simple-button" value="Aceptar">
                    <input type="hidden" name="formFotoPerfil[usu_id]" value="<?php echo $usuario->usu_id; ?>">
                    <input type="hidden" name="option" value="com_users">
                    <input type="hidden" name="task" value="profile.subirImagen">
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </div>
        </form>
            
            <h4><?php echo JText::_('JAREAINTERES_TITULO_MIS_INTERESES');?></h4>
            <p><?php echo JText::_('JAREAINTERES_FAVOR_MARQUE');?></p>
            <form action="index.php?option=com_users"
                  method="post"
                  name="retoFavorito"
                  id="retoFavorito"
                  class="form-horizontal"
                  onSubmit="return confor();">
                <table cellspacing="0" cellpadding="6"   >
                    <tr align="center">
                        <th ><?php echo JText::_('JAREAINTERES_TITULO');?></th>
                        <th ><?php echo JText::_('JAREAINTERES_ME_INTERESA');?></th>
                    </tr>
                    <?php
                    if( isset($areas) ){
                        foreach ($areas as $area){
                            $check  = '';
                            if( isset($this->areasxusuario) && in_array($area->are_id,$this->areasxusuario)){
                                $check = 'checked';
                            }else{
                                $check = '';
                            }
                            ?>
                            <tr>
                                <td  ><?php echo $area->are_nombre; ?></td>
                                <td  ><input type="checkbox" name="areaInteres[<?php echo $area->are_id; ?>]" value="<?php echo $area->are_id; ?>" <?php echo $check; ?>></td>
                            </tr>
                        <?php }
                    }else{
                        ?>
                        <tr align="center">
                            <td><?php echo JText::_('JAREAINTERES_NO_REGISTROS');?></td>
                        </tr>
                    <?php } ?>
                    <tr><td>&nbsp;</td></tr>
                </table>

                <div class="form-group"><label  class="control-label col-xs-2"></label>
                    <div class="col-xs-10"></div>
                </div>
                <div class="form-group"><label  class="control-label col-xs-2"></label>
                    <div class="col-xs-10"></div>
                </div>
                <div class="form-group"><label  class="control-label col-xs-2"></label>
                    <div class="col-xs-10"></div>
                </div>
               

                <?php
                if( isset($this->datosUsuario->usu_envioInfo)){
                    if($this->datosUsuario->usu_envioInfo == 'email'){
                        $checkemail = 'checked';
                        $checkrss = '';
                    }elseif($this->datosUsuario->usu_envioInfo == 'rss'){
                        $checkemail = '';
                        $checkrss = 'checked';
                    }else{
                        $checkemail = '';
                        $checkrss = '';
                    }
                }else{
                    $checkemail = '';
                    $checkrss = '';
                }?>

               <h4><?php echo JText::_('COM_USERS_REGISTER_INFORMACION'); ?>:</h4>
                <div class="form-group">
                    <div  class="control-label col-xs-2">
                        <div >Email:</div>
                    </div>
                    <div class="col-xs-10">
                        <input type="checkbox" name="sendinfo" value="email" <?php echo $checkemail; ?>>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-8 col-xs-offset-2 ">
                        <div id="acepto" class="button-area">
                            <input type="submit"  class="simple-button" value="Guardar Cambios">
                            <input type="hidden" name="option" value="com_users" />
                            <input type="hidden" name="view" value="profile" />
                        </div>
                    </div>
                </div>
            </form>
    </section>
</article>

