<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 *
 * En este caso, necesitamos que el login de joomla redirija al home, ya que
 * el login esta presente en toda la navegacion.
 */

defined('_JEXEC') or die;
JFactory::getApplication()->redirect("index.php");