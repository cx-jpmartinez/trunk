<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$solucion = $this->solucion;
$anexos = $this->anexos;

$modificar=true;
//$trueReadOnly="readonly='true'";
$readonly = '';
$noEnviar = '';
if($usuario->usu_id!=$solucion->sol_usu_id)
{
  $modificar= false;
  $readonly = 'disabled';  
  $estadoCapaForm = 'none';
  $estadoCapaAceptar = '';
  $solCom = validarSolucionCompatida($solucion->sol_id,$usuario->usu_id);
  if($solCom){
      $estadoCapaForm = '';
      $estadoCapaAceptar = 'none';
  }
}else{
    $estadoCapaAceptar = 'none';
    $estadoCapaForm = '';
}
?>

<?php 
$NombreAnexo1='';
$Anexo1='';
$IdAnexo1='';
$NombreAnexo2='';
$Anexo2='';
$IdAnexo2='';
$NombreAnexo3='';
$Anexo3='';
$IdAnexo3='';
$NombreAnexo4='';
$Anexo4='';
$IdAnexo4='';

if($anexos){
	if(isset($anexos[0]))
    {
       $NombreAnexo1 = $anexos[0]->anx_archivo;
       $IdAnexo1 = $anexos[0]->anx_id;
       $Anexo1 = $anexos[0]->anx_nombre;
    }
    if(isset($anexos[1]))
    {
       $NombreAnexo2 = $anexos[1]->anx_archivo;
       $IdAnexo2 = $anexos[1]->anx_id;
       $Anexo2 = $anexos[1]->anx_nombre;
    }
    if(isset($anexos[2]))
    {
       $NombreAnexo3 = $anexos[2]->anx_archivo;
       $IdAnexo3 = $anexos[2]->anx_id;
       $Anexo3 = $anexos[2]->anx_nombre;
    }
    if(isset($anexos[3]))
    {
       $NombreAnexo4 = $anexos[3]->anx_archivo;
       $IdAnexo4 = $anexos[3]->anx_id;
       $Anexo4 = $anexos[3]->anx_nombre;
    }
}


?>

<script>
    /**
     * rel: Boton guardar de formulario de soluciones .
     */

    function validar()
    {
        var form = document.forms.solucionFrm;

        /** anexos -solucion **/

        var sol_anexos1 = form.sol_anexos1;
        var sol_anexos2 = form.sol_anexos2;
        var sol_anexos3 = form.sol_anexos3;
        var sol_anexos4 = form.sol_anexos4;

        /** Deleted nodes if no exist **/

        if( sol_anexos1)
        {
            if( sol_anexos1.files.length < 1)
            {
                sol_anexos1.parentNode.removeChild(sol_anexos1);
            }
        }

        if( sol_anexos2)
        {
            if( sol_anexos2.files.length < 1)
            {
                sol_anexos2.parentNode.removeChild(sol_anexos2);
            }
        }

        if( sol_anexos3)
        {
            if( sol_anexos3.files.length < 1)
            {
                sol_anexos3.parentNode.removeChild(sol_anexos3);
            }
        }

        if( sol_anexos4)
        {
            if( sol_anexos4.files.length < 1)
            {
                sol_anexos4.parentNode.removeChild(sol_anexos4);
            }
        }


        jQuery("#aceptar").attr('disabled','disabled');
        jQuery("#Guardar").attr('disabled','disabled');

        form.submit();

    }
</script>

<div class="article-component">
    <header>
        <h1>Solución</h1>
    </header>
    <section id="aceptarSolucion" style="display:<?php echo $estadoCapaAceptar; ?>;">
        <!-- div con display segun regla del negocio --->
        <div >
            Acepta o rechaza la invitación de <?php echo cargarNombreUsuario ($solucion->sol_usu_id); ?>
            para participar en el equipo de la solución.
        </div>

        <form action="index.php" method="post" name="participarFrm" id="participarFrm"  class="form-horizontal">
            <div class="control-group">
                <div class="control-label"></div>
                <div class="controls">
                    <div id="acepto" >
                        <input type="button" value="Aceptar" name="Aceptar_par" class="button" onclick="mostrarSolucion('<?php echo $solucion->sol_id; ?>','<?php echo $usuario->usu_id; ?>','<?php echo $solucion->sol_usu_id; ?>');"/>
                        <input type="button" value="Rechazar" name="Rechazar_par" class="button" onclick="rechazarSolucion('<?php echo $solucion->sol_id; ?>','<?php echo $usuario->usu_id; ?>','<?php echo $solucion->sol_usu_id; ?>');"/>
                        <input type="hidden" name="option">
                        <input type="hidden" name="task">
                        <input type="hidden" name="idsol">
                        <input type="hidden" name="usu_id">
                        <input type="hidden" name="idamigo">
                    </div>
                </div>
            </div>
        </form>
    </section>

    <section>
        <h4><?php  echo $reto->ret_nombre; ?></h4>
        <form action="index.php" method="post" name="solucionFrm" id="solucionFrm"  class="solucionFrm form-horizontal" enctype="multipart/form-data">

            <?php
            if(!$solucion->sol_titulo){
                $tituloSol= $reto->ret_id.'-SOL-'.$usuario->usu_id;
            }else{
                $tituloSol = $solucion->sol_titulo;
            }
            ?>

            <div>
               <strong>Los campos con <span class="text-warning">*</span> son obligatorios de diligenciar.</strong>
            </div>

            <div class="control-group">
                <label class="control-label"><?php echo JText::_('Código de la solución:');?></label>
                <div class="controls">
                    <input type="text"
                           id="sol_titulo"
                           name="sol_titulo"
                           value="<?php echo $tituloSol;  ?>"
                           disabled
                           readonly="readonly" />
                </div>
            </div>

            <?php
            echo $validacion='';
            foreach ($this->formSolucion as $input){ ?>

                <?php $requerido = '';
                if($input->pro_requerido){
                    $campoReque = "'#$input->pro_nombre'";
                    $msjrequerido = "$input->pro_texto";
                    $requerido = 'required';
                    $validacion.="if ($($campoReque).length > 0) {";
                    $validacion.="if($($campoReque).val().length <= 0){";
                    $validacion.="alert('campo requerido');";
                    $validacion.="$($campoReque).focus();";
                    $validacion.="$($campoReque)".'.css("background-color","#FFFFCC");';
                    $validacion.='return false;';
                    $validacion.='}';
                    $validacion.='}';
                }
                $maxlength = '';
                if($input->pro_caracteres>0){
                    $maxlength =  $input->pro_caracteres;
                }
                ?>


                <div class="control-group">
                <?php echo $input->pro_texto; ?>
                        <?php if ($input->pro_requerido) : ?>

                         <label class="control-label">
                            <span class='text-warning' >*</span>
                          </label>

                        <?php endif; ?>
                    <div class="controls">
                        <?php
                        if ($input->pro_tipo == 'select'){ ?>
                            <select name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>"
                                    required="<?php echo $requerido ?>" title="<?php echo $input->pro_descripcion ?>">
                                <?php foreach ($input->options as $option) { ?>
                                    <option
                                        value="<?php echo $option->pro_valor; ?>" <?php if ($input->det_valor == $option->pro_valor) {
                                        echo 'selected';
                                    } ?>><?php echo $option->pro_texto; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        <?php } elseif ($input->pro_tipo == 'textbox') {
                        $width = '540px;';
                        if (!$input->pro_ancho) {
                            $width = "540px;";
                        } else {
                            $width = $input->pro_ancho . 'px;';
                        }

                        ?>
                            <input type="text" name="<?php echo $input->pro_nombre; ?>" class="inputbox"
                                   maxlength="<?php echo $maxlength; ?>" size="<?php echo $maxlength; ?>"
                                   id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido ?>"
                                   title="<?php echo $input->pro_descripcion ?>"
                                   value="<?php echo $input->det_valor; ?>" >

                        <?php } elseif ($input->pro_tipo == 'textarea') { ?>

                            <textarea name="<?php echo $input->pro_nombre; ?>" class="inputbox"
                                      id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido ?>"
                                      style="width: 540px;" cols="<?php //echo $input->pro_ancho;
                            ?>" rows="<?php echo $input->pro_alto; ?>"
                                      title="<?php echo $input->pro_descripcion ?>"><?php echo $input->det_valor; ?></textarea>

                        <?php } elseif ($input->pro_tipo == 'file')
                        {
                            if ($input->det_valor && $input->det_texto)
                            {
                                ?>
                                <a href="./images/retos/soluciones/<?php echo $input->det_valor; ?>"
                                   target="_blank"><?php echo $input->det_texto; ?></a>
                            <?php
                            } else { ?>

                                <input type="file" name="<?php echo $input->pro_nombre; ?>" class="inputbox"
                                       id="<?php echo $input->pro_nombre; ?>" size="70">
                            <?php } ?>

                        <?php } elseif ($input->pro_tipo == 'check')
                        { ?>

                            <?php foreach ($input->options as $option) { ?>

                            <input type="checkbox" name="<?php echo $option->pro_nombre; ?>" class="inputbox"
                                   id="<?php echo $option->pro_nombre; ?>" value="<?php echo $option->pro_valor; ?>"
                                   title="<?php echo $input->pro_descripcion ?>"

                                    <?php if ($option->det_valor == $option->pro_valor) {
                                        echo 'checked';
                                    } ?>>

                                    <?php echo $option->pro_texto ?>
                            <?php } ?>
                        <?php } elseif ($input->pro_tipo == 'radio') { ?>
                            <input type="radio" name="<?php echo $input->pro_nombre; ?>" class="inputbox"
                                   id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido ?>"
                                   value="<?php echo $input->pro_valor; ?>"
                                   title="<?php echo $input->pro_descripcion ?>" style="width:33px;">
                        <?php } elseif ($input->pro_tipo == 'fecha') { ?>

                            <input type="text" class="inputbox" size="<?php echo $maxlength; ?>"
                                   maxlength="<?php echo $maxlength; ?>" id="jform_ret_fechaPublicacion"
                                   name="jform[ret_fechaPublicacion]" title="<?php echo $input->pro_descripcion ?>"
                                   value="<?php echo $input->det_valor; ?>">
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <?php if($this->participantes && $modificar): ?>
                <div class="block block-compartida">
                    <h4 id="compartida">Solución compartida</h4>
                    <div class="control-group  col-xs-6">
                        <label class="control-label">Nombre:</label>
                        <div class="controls">
                            <?php echo $this->participantes->nombre; ?>
                        </div>
                    </div>

                    <div class="control-group  col-xs-6">
                        <label class="control-label">E-mail:</label>
                        <div class="controls">
                            <?php echo $this->participantes->usu_email; ?>
                        </div>
                    </div>

                    <div class="control-group">

                        <div class="controls">
                            <?php
                            $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id,  $this->participantes->usu_id);
                            if($anexoTYC)
                            {
                                ?>
                                <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                    <img src="./images/anexotyc.png" title="Térmnos y condiciones" border="0">
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
            <?php endif; ?>



            <?php
            $numeroSoluciones = 12;
            for ($i = 0; $i < $numeroSoluciones; $i++) {
                $nombreId = 'integrante' . $i;
                $mailId = 'mailintegrante' . $i;
                $slc_id = '';
                $nombre = '';
                $mail = '';
                $slc_acepto = '';
                $slc_aceptoSol = '';
                $disabled = '';
                $usuID = '';
                if ($solucion->_sol_solucionCompartida) {
                    if (isset($solucion->_sol_solucionCompartida[$i])) {
                        $slc_id = $solucion->_sol_solucionCompartida[$i]->slc_id;
                        $nombre = $solucion->_sol_solucionCompartida[$i]->slc_usu_nombre;
                        $mail = $solucion->_sol_solucionCompartida[$i]->slc_usu_email;
                        $slc_acepto = $solucion->_sol_solucionCompartida[$i]->slc_usu_id;
                        $slc_aceptoSol = $solucion->_sol_solucionCompartida[$i]->slc_estado;
                        $disabled = 'disabled';
                        $usuID = $solucion->_sol_solucionCompartida[$i]->slc_usu_id;
                    } else {
                        $slc_id = '';
                        $nombre = '';
                        $mail = '';
                        $slc_acepto = '';
                        $disabled = '';
                        $slc_aceptoSol = '';
                        $usuID = '';
                    }
                }
                ?>
                <?php if ($modificar) { ?>
                <div class="block-reg col-xs-12">
                    <div class="control-group col-xs-6">
                        <label class="control-label">Nombre:</label>
                        <div class="controls">
                            <input type="text"
                                   name="<?php echo $nombreId; ?>" id="<?php echo $nombreId; ?>"
                                   value="<?php echo $nombre; ?>"
                                <?php echo $disabled; ?>
                                <?php echo $readonly; ?>
                            />
                        </div>
                    </div>

                    <div class="control-group col-xs-6">
                        <label class="control-label">E-mail:</label>
                        <div class="controls">

                            <input type="text" name="<?php echo $mailId; ?>" id="<?php echo $mailId; ?>"
                                   value="<?php echo $mail; ?>"
                                <?php echo $disabled; ?>
                                   readonly="readonly"
                                <?php echo $readonly; ?>
                            />

                        </div>
                    </div>
                    <div class="control-group">
                        <?php if ($slc_acepto && $slc_aceptoSol <> 'Rechazada') { ?>
                            &nbsp; <!--img src="./images/acepto.png" title="<?php //echo $nombre; ?> acepto tu invitación"> &nbsp; | &nbsp; -->
                        <?php } elseif ($slc_acepto && $slc_aceptoSol == 'Rechazada') {
                            $noEnviar = true;
                            ?>
                            <label class="control-label">Rechazada :</label>
                        <?php } ?>

                        <div class="block-status">
                            <?php if ($slc_acepto && $slc_aceptoSol <> 'Rechazada') { ?>
                                &nbsp; <!--img src="./images/acepto.png" title="<?php //echo $nombre; ?> acepto tu invitación"> &nbsp; | &nbsp; -->
                            <?php } elseif ($slc_acepto && $slc_aceptoSol == 'Rechazada') {
                                $noEnviar = true;
                                ?>
                                <span style="color: red;"> Rechazada</span>  &nbsp; | &nbsp;
                            <?php } ?>
                            <?php if (($nombre && $mail) && $modificar) { ?>
                                <a title="Eliminar a <?php echo $nombre; ?>"
                                   href="index.php?option=com_solucion&amp;task=elimarParticipante&amp;idRet=<?php echo $reto->ret_id; ?>&idcsol=<?php echo $slc_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                    <img src="./images/eliminar.png">
                                </a>
                            <?php } ?>

                            <?php if ($slc_aceptoSol == 'Aceptada') { ?>
                                &nbsp; | &nbsp;
                                <img src="./images/agentes.png" title="Aceptó participar en el reto">
                            <?php } elseif ($slc_aceptoSol == 'Pendiente') {
                                $noEnviar = true;
                                ?>
                                &nbsp; | &nbsp;
                                <img src="./images/alerta.gif" title="Pendiente de respuesta">
                            <?php } ?>

                            <?php
                            $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id, $usuID);
                            if ($anexoTYC) {
                                ?>
                                <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                    <img src="./images/anexotyc.png" title="Térmnos y condiciones">
                                </a>
                                <?php
                            }
                            ?>


                        </div>

                    </div>
                </div>
                <?php } else {
                    if ($i == 0) {
                        if ($this->participantes) { ?>
                            <div class="block-reg">
                                <div class="control-group">
                                    <label for="" class="control-label">Nombre:</label>
                                    <div class="controls"><?php echo $this->participantes->nombre; ?></div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">E-mail:</label>
                                    <div class="controls"><?php echo $this->participantes->usu_email; ?></div>
                                </div>


                                <div class="control-group">

                                    <div class="controls">
                                        <?php
                                        $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id, $this->participantes->usu_id);
                                        if ($anexoTYC) {
                                            ?>
                                            &nbsp;|&nbsp;
                                            <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                                <img src="./images/anexotyc.png" title="Térmnos y condiciones" border="0">
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>
                        <?php }
                    }
                    if ($nombre && $mail) {
                        ?>
                    <div class="block-reg">
                        <div class="control-group">
                            <label for="" class="control-label">Nombre:</label>
                            <div class="controls">
                                <input type="hidden"
                                       name="<?php echo $nombreId; ?>"
                                       id="<?php echo $nombreId; ?>"/>
                            </div>
                        </div>
                        <div class=" control-group">
                            <label for="" class="control-label">E-mail: <?php echo $mail; ?></label>
                            <div class="controls">
                                <input type="hidden"
                                       ame="<?php echo $mailId; ?>"
                                       id="<?php echo $mailId; ?>"
                                >
                            </div>


                            <?php if ($slc_acepto) { ?>
                                <div class="control-group">

                                    <div class="controls">
                                         <img src="./images/acepto.png"
                                              title="<?php echo $nombre; ?> acepto tu invitación"> &nbsp; | &nbsp;
                                    </div>
                                </div>

                            <?php } ?>
                            <?php
                            $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id, $usuID);
                            if ($anexoTYC) {
                                ?>
                                <div class="control-group">

                                    <div class="controls">
                                        <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                            <img src="./images/anexotyc.png" title="Térmnos y condiciones">
                                        </a>
                                    </div>
                                </div>

                                <?php
                            }
                            ?>

                        </div>
                    </div>

                        <?php
                    }
                }
            }
            ?>

         </div>

<div id="block-interno" class="block">
    <h4 id="interno">Anexos complementarios a la solución:</h4>
    <div class="control-group">
        <div class="controls block-reg col-xs-12">
            <?php if($solucion->sol_anexo ) { ?>
                <div class="col-xs-10">

                        <?php echo $solucion->sol_anexo;
                        if ($solucion->sol_nameanexo)
                        {
                            echo " ($solucion->sol_nameanexo)";
                        }
                        ?>
                </div>
        </div>
        <?php } ?>
    </div>
    <div class="control-group">
        <label class="control-label" >
        <?php if($NombreAnexo1 || $modificar){ ?>
            Archivo Anexo:
        <?php } ?>
        </label>
        <div class="controls block-reg col-xs-12">
            <?php if($NombreAnexo1){ ?>
                <div class="col-xs-10">
                    <a href="./images/retos/soluciones/<?php echo $NombreAnexo1; ?>" target="_blank">
                        <?php
                        echo $NombreAnexo1;
                        if($Anexo1){
                            echo " ($Anexo1)";
                        }
                        ?>
                    </a>
                </div>
                <?php if($modificar){ ?>
                    <div class="col-xs-2">
                        <a title="Eliminar a <?php echo $NombreAnexo1; ?>"  href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo1; ?>&solucionID=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                            <img src="./images/eliminar.png">
                        </a>
                    </div>
                <?php }
            }else{
                if($modificar){ ?>
                    <input id="sol_anexos1" type="file" size="50" name="sol_anexos1" >
                <?php }
            }
            ?>

        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <?php if($NombreAnexo1 || $NombreAnexo2 || $NombreAnexo3 || $NombreAnexo4)
            {
                $capa='';
            }else{
                $capa='none';
                ?>
                <div id="anexos">
                    <?php   if($modificar){  ?>
                        <input type="button" name="btnanexos" id="btnanexos" value="Más Anexos" onclick="abrirAnexos();">
                    <?php } ?>
                </div>
            <?php } ?>

        </div>
    </div>

</div>

            <div id="capaAnexo" class="<?php echo $capa ?>">
                <div class="control-group">
                    <label class="control-label">
                        <?php if ($NombreAnexo2 || $modificar) { ?>
                            Archivo Anexo:
                        <?php } ?>
                    </label>
                    <div class="controls block-reg col-xs-12">

                        <?php if ($NombreAnexo2) { ?>

                            <div class="col-xs-10">
                                <a href="./images/retos/soluciones/<?php echo $NombreAnexo2; ?>" target="_blank">
                                    <?php echo $NombreAnexo2;
                                    if ($Anexo2) {
                                        echo " ($Anexo2)";
                                    }
                                    ?>
                                </a>
                            </div>

                            <?php if ($modificar) { ?>
                                    <div class="col-xs-2">
                                        <a title="Eliminar a <?php echo $NombreAnexo2; ?>"
                                           href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo2; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                            <img src="./images/eliminar.png">
                                        </a>
                                    </div>

                            <?php }
                        } else {
                            if ($modificar) {
                                ?>
                                <input id="sol_anexos2" type="file" size="50" name="sol_anexos2" >
                                <?php
                            }
                        } ?>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        <?php if ($NombreAnexo3 || $modificar) { ?>
                            Archivo Anexo:
                        <?php } ?>
                    </label>
                    <div class="controls block-reg col-xs-12">
                        <?php if ($NombreAnexo3) { ?>

                            <div class="col-xs-10">
                                <a href="./images/retos/soluciones/<?php echo $NombreAnexo3; ?>" target="_blank">
                                    <?php echo $NombreAnexo3;
                                    if ($Anexo3) {
                                        echo " ($Anexo3)";
                                    }
                                    ?>
                                </a>
                            </div>

                            <?php if ($modificar) { ?>
                                <div class="col-xs-2">
                                    <a title="Eliminar a <?php echo $NombreAnexo3; ?>"
                                       href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo3; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                        <img src="./images/eliminar.png">
                                    </a>
                                </div>

                            <?php }
                        } else {
                            if ($modificar) {
                                ?>
                                <input id="sol_anexos3" type="file" size="50" name="sol_anexos3">
                            <?php }
                        } ?>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        Archivo Anexo:
                    </label>
                    <div class="controls block-reg col-xs-12">
                        <?php if ($NombreAnexo4) { ?>

                            <div class="col-xs-10">
                                <a href="./images/retos/soluciones/<?php echo $NombreAnexo4; ?>" target="_blank">
                                    <?php echo $NombreAnexo4;
                                    if ($Anexo4) {
                                        echo " ($Anexo4)";
                                    }
                                    ?>
                                </a>
                            </div>

                            <?php if ($modificar) { ?>

                                <div class="col-xs-2">
                                    <a title="Eliminar a <?php echo $NombreAnexo4; ?>"
                                       href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo4; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                        <img src="./images/eliminar.png">
                                    </a>
                                </div>

                            <?php }
                        } else {
                            if ($modificar) {
                                ?>
                                <input id="sol_anexos4" type="file" size="50" name="sol_anexos4">
                            <?php }
                        }
                        ?>
                    </div>
                </div>


            </div>
            <div id="block-interno" class="block">
                <h4 id="interno"><?php echo JText::_('COM_SOLUCION_LABEL_ESTADO'); ?></h4>
                <div class="control-group">
                    <p>
                        Para enviar la solución al comité evaluador debes tener en cuenta que:<br><br>
                        &#9658; Todos los invitados a participar en el reto hayan aceptado los términos y
                        condiciones. <br><br>
                        &#9658; Si uno de los invitados no aceptó tu invitación, debes eliminarlo haciendo clic en
                        la “x” roja al lado de su nombre.<br><br>
                        &#9658; Sólo puede enviar la solución quien la ingresó.<br>
                    </p>
                    <div class="controls">
                        <?php if ($modificar) { ?>
                            <div id="acepto">
                                <?php if (!$solucion->_sol_aceptarInvitados && !$noEnviar) { ?>
                                    <input type="button" name="aceptar" id="aceptar"
                                           value="<?php echo JText::_('Enviar al comité evaluador'); ?>"
                                           onclick="enviarSolucion();">
                                    &nbsp;&nbsp;&nbsp;
                                <?php } ?>
                                <input type="button" name="Guardar" id="Guardar" value="Guardar" onclick="validar();">
                                <input type="hidden" name="task" value="solucion"/>
                                <input type="hidden" id="sol_estado" name="sol_estado" value=""/>
                                <input type="hidden" name="option" value="com_solucion"/>
                                <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>"/>
                                <input type="hidden" name="idSol" value="<?php echo $solucion->sol_id; ?>"/>
                            </div>

                        <?php } ?>
                    </div>
                </div>


            </div>

            <div class="block control-group">
                <div class="controls">
                    <div class="button-area">
                        <a title="Nueva Solución" class="btn simple-button"
                           href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                            <span >
                               Nueva Solución
                            </span>
                        </a>
                    </div>

                </div>
            </div>

        </form>
    </section>
</div>


    <?php if(isset($this->preguntasComite) && count($this->preguntasComite)>0){ ?>
        <h4> Preguntas y Respuestas Comité</h4>
        <?php
    foreach($this->preguntasComite as $preguntasComite){ ?>

        <div class="control-group">
            <label class="control-label">
        Fecha:
            </label>
            <div class="controls">
                <?php echo $preguntasComite->pco_fechapregunta; ?>
        </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <?php if($preguntasComite->pco_remitente!='Comité'){ ?>
                    <span style="font-weight: bold;  ">Innovador:</span> <?php echo $preguntasComite->pco_remitente; ?> <br>
                <?php }else{  ?>
                    <span style="font-weight: bold; ">Comité</span><br>
                <?php } ?>
                <span style="font-weight: bold;  font-family: '';">Mensaje:</span> <?php echo $preguntasComite->pco_pregunta; ?>
            </div>
        </div>


     <?php } ?>
        <div id="acepto" class="button-area" >
            <a class="modal"
               href="index.php?option=com_solucion&amp;view=formComite&amp;task=formComite&amp;idRet=<?php echo $reto->ret_id; ?>&amp;idSol=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <span >Dar una nueva respuesta</span>
            </a>
        </div>

     <?php } ?>

<script type="text/javascript">
    function enviarSolucion (){
    $("input, textarea").css("background-color","#ccc");
    form = document.forms.solucionFrm;

    <?php  echo $validacion; ?>
     if(confirm('Esta seguro de enviar la solución')){
         form.sol_estado.value = 'Finalizado';
         $("#aceptar").attr('disabled','disabled');
         $("#Guardar").attr('disabled','disabled');  
    	 form.submit();
    	 
    }else{
        return false;
    }
}
</script>