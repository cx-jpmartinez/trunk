<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
?>

<article class="article-component">
    <header>
        <h1>Mis Soluciones</h1>
    </header>
    <section>
        <h3>Plantilla de las soluciones.</h3>

        <p >
            <?php  echo $reto->ret_nombre; ?>
        </p>
        <?php foreach ($this->soluciones as $solucion){ ?>
            <div>
                <a href="index.php?option=com_solucion&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;solucionID=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">

                    &#9658; <?php
                    if($solucion->sol_titulo)
                    {
                        echo $solucion->sol_titulo;
                    }else{
                        echo $reto->ret_id.'-SOL-'.$usuario->usu_id;
                    }
                    ?>
                </a> <!-- &nbsp; | &nbsp;  Eliminar -->
            </div>
        <?php } ?>

        <div id="acepto" class="button-area">
            <a title="Nueva Solución" href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <input type="button" value="Nueva Solución" class="simple-button"  />
            </a>
        </div>
    </section>
</article>

