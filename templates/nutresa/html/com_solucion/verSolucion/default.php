<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$solucion = $this->solucion;
$anexos = $this->anexos;
?>

<article class="article-component">
    <section id="crear_solucion">
        <h4>&#9658;Participantes</h4>
        <?php if($this->participantes){
            foreach ($this->participantes as $participante){ ?>
                <?php echo "$participante->nombre ($participante->usu_email)"; ?>
                <?php
                $valTYC = validarTYC($participante->usu_email, $reto->ret_id); ?>
                <div class="blok-status">
                    <?php if($valTYC ){ ?>
                        <img src="./images/acepto.png" title="Aceptó Términos y Condiciones">
                    <?php }else{ ?>
                        <img src="./images/alerta.gif"  title="No Aceptó Términos y Condiciones">
                    <?php } ?>
                    <?php
                    $anexoTYC = cargarAnexoTYC($this->solucion->sol_ret_id, $participante->usu_id);
                    if($anexoTYC){
                        ?>
                        <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank"><?php echo $anexoTYC; ?></a>
                        <?php
                    }
                    ?>
                </div>
            <?php }
        }
        ?>
    </section>
    <header>
        <h2><?php echo JText::_('Plantilla de la Solución');?>:<?php echo $solucion->sol_titulo; ?></h2>
    </header>
    <section id="cont_reto_prof">
        <h4><?php  echo $reto->ret_nombre; ?></h4>
        <p ><b><?php echo JText::_('COM_SOLUCION_LABEL_SOLUCION');?> </b> <?php echo $solucion->sol_titulo; ?></p>
        <?php if($solucion->sol_descripcion){ ?>
            <p>&nbsp; <b><?php echo JText::_('COM_SOLUCION_LABEL_DESCRIPCION');?></b> <?php echo $solucion->sol_descripcion;?></p>
        <?php } ?>
        <?php foreach ($this->formSolucion as $input){ ?>
            <?php if($input->pro_tipo=='select'){ ?>
                <p> <b><?php echo $input->pro_texto ?></b>
                    <select name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" title="<?php echo $input->pro_descripcion ?>" disabled>
                        <?php foreach ($input->options as $option){ ?>
                            <option value="<?php echo $option->pro_valor; ?>" <?php if($input->det_valor==$option->pro_valor){echo 'selected';} ?>><?php echo $option->pro_texto; ?></option>
                        <?php } ?>
                    </select>
                </p>
            <?php }elseif($input->pro_tipo=='file'){ ?>
                <p> <b><?php echo $input->pro_texto ?></b> <a href="./images/retos/soluciones/<?php echo $input->det_valor; ?>" target="_blank"><?php echo $input->det_texto; ?></a></p>
            <?php }elseif($input->pro_tipo=='check'){  ?>
                <p> <b><?php echo $input->pro_texto ?></b>
                    <?php  foreach ($input->options as $option){  ?>
                        <input type="checkbox" name="<?php echo $option->pro_nombre; ?>" class="inputbox" id="<?php echo $option->pro_nombre; ?>"  value="<?php echo $option->pro_valor; ?>" title="<?php echo $input->pro_descripcion ?>"  <?php if($option->det_valor==$option->pro_valor){ echo 'checked';} ?> disabled>
                        <?php echo $option->pro_texto ?>
                    <?php } ?>

                </p>
            <?php }else{ ?>
                <p>&nbsp <b><?php echo $input->pro_texto ?></b> <?php echo $input->det_valor; ?></p>
            <?php } ?>
            <br>
            <?php

        }
        ?>
    </section>
    <section>
        <h4>Anexos</h4>
        <?php if($solucion->sol_anexo){ ?>
            <p style="padding-right: 10px;padding-left: 10px;">&nbsp;
                <a href="./images/retos/soluciones/<?php echo $solucion->sol_anexo; ?>" target="_blank">
                    <?php
                    if($solucion->sol_nameanexo){
                        echo $solucion->sol_nameanexo;
                    }else{
                        echo $solucion->sol_anexo;
                    }
                    ?>
                </a>
            </p>
        <?php } ?>
        <?php
        if($anexos){
            foreach ($anexos as $anexo){
                ?>
                <p>
                    <a href="./images/retos/soluciones/<?php echo $anexo->anx_archivo; ?>" target="_blank">
                        <?php
                        if($anexo->anx_nombre){
                            echo $anexo->anx_nombre;
                        }else{
                            echo $anexo->anx_archivo;
                        }
                        ?>
                    </a>
                </p>
                <?php
            }
        }
        ?>
    </section>
    <section>
        <h4>Estado de la solución</h4><br>
        <p>
            <?php
            if(!$solucion->sol_estado || $solucion->sol_estado=='Pendiente'){
                echo 'BORRADOR';
            }elseif($solucion->sol_estado=='Finalizado'){
                echo 'PENDIENTE DE APROBACIÓN';
            }elseif($solucion->sol_estado=='Aprobada'){
                echo 'SOLUCIÓN GANADORA';
            }elseif($solucion->sol_estado=='Rechazada'){
                echo 'RECHAZADA';
            }
            ?>
        </p>
        <?php
        $fechaActual = date('Y-m-d H:i');
        $view = JRequest::getVar('view');
        if($reto->ret_estado=='Vigente' &&  $fechaActual <= $reto->ret_fechaVigencia && $view!='verSolucion'){ ?>
            <div id="acepto" class="button-area">
                <a class="btn simple-button" title="Nueva Solución" href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <span >
                   Nueva Solución
                </span>
                </a>
            </div>
        <?php } ?>
    </section>

    <section>
        <?php if(isset($this->preguntasComite) && count($this->preguntasComite)>0){ ?>
            <h4>&#9658; Preguntas y Respuestas Comité</h4>
            <?php
            foreach($this->preguntasComite as $preguntasComite){
                ?>
                <div class="cont_campos" align="center">
                    <p >
                        <span >Fecha:</span> <?php echo $preguntasComite->pco_fechapregunta; ?> <br>
                        <?php if($preguntasComite->pco_remitente!='Comité'){ ?>
                            <span >Innovador:</span> <?php echo $preguntasComite->pco_remitente; ?> <br>
                        <?php }else{  ?>
                            <span >Comité</span><br>
                        <?php } ?>
                    </p>
                </div>
            <?php } ?>
            <div id="acepto" class="button-area">
                <a  class="simple-button" href="index.php?option=com_solucion&amp;view=formComite&amp;task=formComite&amp;idRet=<?php echo $reto->ret_id; ?>&amp;idSol=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                    <span >Dar una nueva respuesta</span>
                </a>
            </div>

        <?php } ?>

    </section>
</article>







