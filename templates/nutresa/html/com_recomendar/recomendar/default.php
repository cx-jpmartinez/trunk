<?php
    defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
?>
<article class="article-component">
    <header><h1><?php echo JText::_('Recomendar a un Amigo');?></h1></header>
    <section>
		<form action="index.php" method="post" name="recomendarFrm" id="recomendarFrm" class="form-horizontal recomendarFrm">
			<div class="form-group">
			    <label class=" col-xs-3 control-label"><?php echo JText::_('COM_RECOMENDAR_LABEL_NOMBRE');?>:</label>
                <div class="col-xs-8">
                    <input type="text" class="form-control" name="rec_nombre" id="rec_nombre"/>
                </div>
			</div>
			<div class="form-group">
			    <label class="col-xs-3 control-label"><?php echo JText::_('COM_RECOMENDAR_LABEL_EMAIL');?>:</label>
                <div class="col-xs-8">
                    <input type="text" class="form-control" name="rec_mail" id="rec_mail"/>
                </div>
			</div>
			<div class="form-group">
			   <label class=" col-xs-3 control-label"><?php echo JText::_('COM_RECOMENDAR_LABEL_COMENTARIO');?>:</label>
                <div class="col-xs-8">
                    <textarea rows="5" cols="50" class="form-control" name="ret_comentario"></textarea>
                </div>
			</div>
			<div class="form-group col-xs-offset-6 col-xs-6">
                <div class="button-area col-xs-offset-6 col-xs-6">
                    <input type="button" value="Enviar" id="Aceptar" class="simple-button" onclick="validarRec();">
                    <input type="hidden" name="task" value="recomendarAmigo" />
                    <input type="hidden" name="option" value="com_recomendar" />
                    <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>" />
                    <input type="hidden" name="enviarAmigo" value="1"/>
                </div>
			</div>
		</form>
	</section>
</article>