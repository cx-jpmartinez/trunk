<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_SITE . '/components/com_users/helpers/route.php';

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');

?>



<?php if ($type == 'logout') : ?>
	<form action="<?php echo JRoute::_(htmlspecialchars(JUri::getInstance()->toString()), true, $params->get('usesecure')); ?>" method="post" id="login-form" class="logout form-inline">
			<?php if ($params->get('greeting')) : ?>
			<div class="userdata">
				<div id="form-login-username" class="form-group ">
					<div class="input-prepend">
						<span class="ico btn-usu"></span>
						<span class="text-icon-aling">
							<?php if ($params->get('name') == 0) : {
								echo htmlspecialchars($user->get('name'));
							} else : {
								echo htmlspecialchars($user->get('username'));
							} endif; ?>
						</span>
					</div>
				</div>

			<?php endif; ?>
				<div id="form-login-submit" class="form-group">
					<span class="ico btn-sesion"></span>
					<input type="submit" name="Submit" class="form-control" value="<?php echo JText::_('JLOGOUT'); ?>" />
				</div>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.logout" />
				<input type="hidden" name="return" value="<?php echo $return; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
	</form>

<?php else : ?>

	<form action="<?php echo JRoute::_(htmlspecialchars(JUri::getInstance()->toString()), true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-inline">
		<?php if ($params->get('pretext')) : ?>
			<div class="pretext">
				<p><?php echo $params->get('pretext'); ?></p>
			</div>
		<?php endif; ?>
		<div class="userdata">
			<div id="form-login-username" class="form-group">
				<?php if (!$params->get('usetext')) : ?>
					<div class="input-prepend">
						<span class="ico btn-usu"></span>
						<input id="modlgn-username" type="text" name="username" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>" />
					</div>
				<?php else: ?>
					<input id="modlgn-username" type="text" name="username" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>" />
				<?php endif; ?>
			</div>

			<div id="form-login-password"  class="form-group">
				<?php if (!$params->get('usetext')) : ?>
					<div class="input-prepend">
						<span class="ico btn-pass"></span>
						<input id="modlgn-passwd" type="password" name="password" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
					</div>
				<?php else: ?>
					<input id="modlgn-passwd" type="password" name="password" class="form-control" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
				<?php endif; ?>
			</div>

			<div id="form-login-submit" class="form-group">
				<span class="ico btn-sesion"></span>
				<button type="submit" tabindex="0" name="Submit" class="form-control" class="btn btn-primary"><?php echo JText::_('JLOGIN') ?></button>
			</div>

			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="user.login" />
			<input type="hidden" name="return" value="<?php echo $return; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
<?php endif; ?>