<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>


<div class="col-xs-12 col-md-12">
	<?php echo $item->introtext; ?>
</div>
<div class="col-xs-12 col-md-6 ">
	<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) : ?>
		<div class="button-area">
			<?php echo '<a class="readmore btn btn-readmore" href="' . $item->link . '"><span class="ico-15 ico-readmore"></span>' .JText::_("TPL_NUTRESA_LABEL_READ_MORE").'</a>'; ?>
		</div>
	<?php endif; ?>
</div>
