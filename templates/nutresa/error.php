<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Nutresa
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

 // No direct access.
defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.file');

 // Get params


$option         = JFactory::getApplication()->input->get('option');
$doc            = JFactory::getDocument();
$app            = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;
$config         = JFactory::getConfig();
$user           = JFactory::getUser();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template . '/css/template.css'?>">

    <!-- end include -->
  </head>

  <body>
    <div class="container">
      <div class="row">

        <div class="logo"></div>
        <div class="main-header-title">
            <h1>
          <?php  if( $templateparams->get('sitetitle') )
              {
                echo htmlspecialchars($templateparams->get('sitetitle'));
              }
              elseif ($config->get('sitename') )
              {
                echo $config->get('sitename');
              }
          ?>
            </h1>
        </div>
      </div>

      <div id="main-page">
        <!-- Content -->
        <div id="main-content">

          <?php if (($this->error->getCode()) == '404') : ?>

            <article class="error-<?php echo $this->error->getCode() ?> ">

              <header class="error-<?php echo $this->error->getCode() ?>-article">
                <h1 class="error-<?php echo $this->error->getCode() ?>-title">
                  Error <?php echo $this->error->getCode() ?></h1>
              </header>

              <section class="error-404-content">
                <div><h3> Page no found </h3></div>
              </section>


            </article>

          <?php endif; ?>

        </div>
      </div>
    </div>

    <footer id="main-footer" class="container-fluid">

      <div class="footer-content row">
        <div class="col-xs-12 col-md-6">

          <div class="col-xs-8 col-md-8 footer-content-element">
            <p>


            </p>

          </div>

          <div class="col-xs-4 col-md-4  footer-content-element">

          </div>
        </div>
      </div>

    </footer>

    <!-- script area -->
    <script src="<?php echo $this->baseurl . '/templates/'.$this->template.'/js/libs/bootstrap.min.js' ?>"></script>

  </body>
</html>
