<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Nutresa
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.file');

// Get params

$logo           = $this->params->get('logo');
$navposition    = $this->params->get('navposition');
$headerImage    = $this->params->get('headerImage');
$doc            = JFactory::getDocument();
$app            = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;
$config         = JFactory::getConfig();
$user           = JFactory::getUser();


//PROD
//$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.min.css', $type = 'text/css', $media = 'screen,projection');

// DEV
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', $type = 'text/css', $media = 'screen,projection');
JHtml::_('behavior.modal');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:jdoc="http://www.w3.org/1999/XSL/Transform"
      xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- head joomla include -->
  <script src="<?php echo $this->baseurl . '/templates/'.$this->template.'/js/libs/jquery-2.2.1.min.js' ?>"></script>
  <jdoc:include type="head" />
  <!-- end include -->
</head>

<body>

<div id="main-page" class="container">
  <!-- Content -->
  <div id="main-content" class="col-xs-12">
    <jdoc:include type="message" />
    <jdoc:include type="component" />
  </div>
</div>


<script src="<?php echo $this->baseurl . '/templates/'.$this->template.'/js/libs/jquery-ui.min.js' ?>"></script>
<script src="<?php echo $this->baseurl . '/templates/'.$this->template.'/js/libs/jquery.ui.autocomplete.min.js' ?>"></script>

<!-- script area DEV -->
 <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/libs/bootstrap.min.js' ?>"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/script-dev.js' ?>"></script>

<!-- script area PROD -->
<!-- <script src="<?php echo $this->baseurl . '/templates/' . $this->template . '/js/script.min.js' ?>"></script> -->
</body>
</html>
