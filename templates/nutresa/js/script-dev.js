/**
 *  Archivo de script que carga en las paginas
 *  de la plantilla de nutresa, este es el archivo
 *  de desarrollo, posiblemente se este cargando la
 *  version minificada.
 *
 *  @author Pablo Martínez
 *
 */

jQuery(document).ready(function(){
    
    if(jQuery("#integrante0").length > 0)
    {
        jQuery('#integrante0').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante0').val(arr[1]);
                if(arr[1]!=''){
                    $('#mailintegrante0').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }

    if(jQuery('#integrante1').length > 0 )
    {
        jQuery('#integrante1').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante1').val(arr[1]);
                if(arr[1]!=''){
                    $('#mailintegrante1').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});


            }
        });
    }

    if( jQuery('#integrante2').length > 0)
    {
        jQuery('#integrante2').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante2').val(arr[1]);
                if(arr[1]!=''){
                    jQuery('#mailintegrante2').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }


    if(jQuery('#integrante3').length > 0)
    {
        jQuery('#integrante3').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante3').val(arr[1]);
                if(arr[1]!=''){
                    jQuery('#mailintegrante3').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }


    if( jQuery('#integrante4').length > 0)
    {
        jQuery('#integrante4').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                $('#mailintegrante4').val(arr[1]);
                if(arr[1]!=''){
                    $('#mailintegrante4').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }

    if (jQuery('#integrante5').length > 0) {

        jQuery('#integrante5').autocomplete({
            source: 'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: {text: ''}
            , options: {scroll: true}
            , select: function (event, ui) {
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante5').val(arr[1]);
                if (arr[1] != '') {
                    jQuery('#mailintegrante5').attr('readonly', true);
                }
                jQuery("#aceptar").attr('disabled', 'disabled');
                jQuery("#aceptar").css({'background-image': 'none', 'background-color': 'gray !important'});
            }
        });
    }

    if (jQuery('#integrante6').length > 0) {
        jQuery('#integrante6').autocomplete({
            source: 'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: {text: ''}
            , options: {scroll: true}
            , select: function (event, ui) {
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante6').val(arr[1]);
                if (arr[1] != '') {
                    jQuery('#mailintegrante6').attr('readonly', true);
                }
                jQuery("#aceptar").attr('disabled', 'disabled');
                jQuery("#aceptar").css({'background-image': 'none', 'background-color': 'gray !important'});
            }
        });
    }


 if( jQuery('#integrante8').length > 0 )
 {
     jQuery('#integrante8').autocomplete({
         source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
         , param: { text: '' }
         , options: { scroll: true }
         , select: function (event, ui){
             var arr = ui.item.value.split(' | ');
             ui.item.value = ui.item.text;
             jQuery('#mailintegrante8').val(arr[1]);
             if(arr[1]!=''){
                 jQuery('#mailintegrante8').attr('readonly',true);
             }
             jQuery("#aceptar").attr('disabled','disabled');
             jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
         }
     });
 }

if(jQuery('#integrante9').length > 0 )
{
    jQuery('#integrante9').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            jQuery('#mailintegrante9').val(arr[1]);
            if(arr[1]!=''){
                jQuery('#mailintegrante9').attr('readonly',true);
            }
            jQuery("#aceptar").attr('disabled','disabled');
            jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
    });
}

if( jQuery('#integrante7').length > 0)
{
    jQuery('#integrante7').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            jQuery('#mailintegrante7').val(arr[1]);
            if(arr[1]!=''){
                jQuery('#mailintegrante7').attr('readonly',true);
            }
            jQuery("#aceptar").attr('disabled','disabled');
            jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
    });
}

    if(jQuery('#integrante10').length > 0)
    {
        jQuery('#integrante10').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante10').val(arr[1]);
                if(arr[1]!=''){
                    jQuery('#mailintegrante10').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }


    if( jQuery('#integrante11').length > 0 )
    {
        jQuery('#integrante11').autocomplete({
            source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: { text: '' }
            , options: { scroll: true }
            , select: function (event, ui){
                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                jQuery('#mailintegrante11').val(arr[1]);
                if(arr[1]!=''){
                    jQuery('#mailintegrante11').attr('readonly',true);
                }
                jQuery("#aceptar").attr('disabled','disabled');
                jQuery("#aceptar").css({'background-image':'none','background-color':'gray !important'});
            }
        });
    }


});

/**
 * Evento que se ejecuta cuando encuentra la galeria
 * en el home, para tratar el modulo mod_ultimos retos
 * @path ../html/mod_ultimos_retos/
 *
 * */

jQuery(".active" ).find("img").ready( function(){

    jQuery(".btn-readmore-inverse").hover(function (e) {

        var rel = jQuery(this).attr("id");

        if (rel.length > 0)
        {
            /* obtenemos solo el identificador */
            var idBtn = rel.split("btn-");

            idBtn = parseInt(idBtn[1]);
            var idRel = "#rel-" + idBtn;

            if (jQuery(idRel).length > 0)
            {
                /** no activo **/
                if (!jQuery(idRel).hasClass("active"))
                {
                    showImage(idRel);
                }
            }
        }

    });

});


/**
 * Muestra una imagen oculta.
 *
 * @author Pablo Martínez
 * @param  id de elemento a mostrar
 */
function showImage(image)
{
    /** ocultamos todos los campos
     *  y quitamos la clase active
     *
     ***/
    jQuery(image).parent().find("figure").css("display", "none").removeAttr("class");
    jQuery(image).fadeIn("slow");
    /** asignamos el active **/

    jQuery(image).addClass("active");
    jQuery(image).removeAttr("style");

}


/** Formulario de Recordar amigo **/
/**
 *  Depende de libreria autocompletejs
 *  y jquery
 *
 */

jQuery("#rec_nombre").ready( function(){

    var recNombre = jQuery('#rec_nombre');

    if (recNombre.length)
    {
        recNombre.autocomplete({

            source: 'index.php?option=com_retos&task=cargarAjax&format=ajax'
            , param: {text: ''}
            , options: {scroll: true}
            , select: function (event, ui) {

                var arr = ui.item.value.split(' | ');
                ui.item.value = ui.item.text;
                $('#rec_mail').val(arr[1]);

                if (arr[1] != '')
                {
                    $('#rec_mail').attr('readonly', true);
                }
            }
        });
    }
});




function validarRec ()
{
    var form = document.forms.recomendarFrm;

    if(jQuery("#rec_nombre").val().length <= 0)
    {
        alert("El campo nombre es requerido");
        return false;
    }
    if(jQuery("#rec_mail").val().length <= 0)
    {
        alert("El campo E-mail es requerido");
        return false;
    }

    if(jQuery("#rec_mail").val().indexOf('@', 0) == -1 || $("#rec_mail").val().indexOf('.', 0) == -1)
    {
        alert("El E-mail es incorrecto");
        return false;
    }

    form.submit();
}




/** Formulario componente para ver las soluciones **/
/**
 *  Depende de libreria autocompletejs
 *  y jquery
 *
 *  Este es un codigo migrado de una version vieja
 *  su lofica y contenido no ha cambiado.
 *
 *  Se sugiere refactorizar , depurar y optiminzar este
 *  codigo.
 *
 */

function mostrarSolucion(idsol,usu_id,idamigo)
{
    form = document.forms.participarFrm;
    var answer = confirm("Estas seguro que deseas participar de la solución");
    if (answer){
        document.getElementById('crear_solucion').style.display='';
        document.getElementById('aceptarSolucion').style.display='none';
        $.ajax({
            url: 'index.php?option=com_retos&task=aceptarParticipar&idsol='+idsol+'&usu_id='+usu_id+'&idamigo='+idamigo,
            context: document.body
        }).done(function() {
            jQuery(this).addClass("done");
        });
        return true;
    }else{
        return false;
    }
}

function rechazarSolucion(idsol,usu_id,id_amigo)
{
    form = document.forms.participarFrm;
    var answer = confirm("Estas seguro que deseas rechazar la invitación");
    if (answer){
        form.option.value = 'com_retos';
        form.task.value = 'rechazarParticipar';
        form.idsol.value = idsol;
        form.usu_id.value = usu_id;
        form.idamigo.value = id_amigo;
        jQuery("#Aceptar_par").attr('disabled','disabled');
        jQuery("#Rechazar_par").attr('disabled','disabled');
        form.submit();

        return true;
    }else{
        return false;
    }

}



function abrirAnexos ()
{
    document.getElementById('capaAnexo').style.display  =   'block';
    document.getElementById('anexos').style.display     =   'none';
}



/*** Formulario de TYC 
 *  rel: acepto button -> tyc
 * 
 * **/

function mostrarSolucionTYC()
{
    if($("#sol_terminos").is(':checked')) {
        if(confirm('¿Esta seguro de aceptar los Términos y Condiciones?')){
            return true;
        }else{
            document.getElementById('sol_terminos').checked=false;
            return false;
        }
    }

}

function validarTYC (idSpl)
{
    form = document.forms.solucionFrm;
     if(idSpl)
     {
        if($("#sol_anexoTyc").val().length <= 0)
        {
            alert("El campo de anexo es requerido");
            return false;
        }
     }

    if($("#sol_terminos").is(':checked'))
    {
        form.submit();
    } else
    {
        alert("Debe aceptar los Términos y Condiciones");
    }
}





function validarMensaje (){
    form = document.forms.preguntaCoFrm;
    if(jQuery("#pco_descripcion").val().length <= 0) {
        alert("El campo mensaje es requerido");
        return false;
    }
    jQuery("#Enviar").attr('disabled','disabled');
    form.submit();
}


/**
 * Modulo Suplantacion
 *
 */
function dejardesuplantar()
{
    window.location.href = 'index.php?option=com_usuario&task=dejarSuplantacion';
}
