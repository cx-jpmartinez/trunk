<?php
// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_retos'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Retos', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Retos');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
