<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

class RetosModelReto extends JModelAdmin
{
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Retos', $prefix = 'RetosTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) 
	{
		// Get the form.
		$form = $this->loadForm('com_retos.reto', 'reto', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_retos.edit.reto.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
		}
		return $data;
	}
	
	public function save($data)
    {
    	$table     = $this->getTable();
    	$crearCarpeta = true;
        if( !$data['ret_id'] ){
            $data['ret_fechaCreacion'] = date( 'Y-m-d H:i:s' );
            $crearCarpeta = true;
    	}
        // Bind the data.
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }
        // Store the data.
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }else{
        	if($crearCarpeta){
        		$mypath=".../images/retos/{$data['ret_galeria']}";
                mkdir($mypath,0777,TRUE);
            }
            $nombrecomite1 = $data['ret_nombrecomite1'];
            if($nombrecomite1){
            	$mailcomite1 = $data['ret_mailcomite1'];
            	$integranteComite1 = new comite();
            	$integranteComite1->limpiarComite($table->ret_id);
            	$integranteComite1->cim_ret_id=$table->ret_id;
            	$integranteComite1->cim_nombre = $nombrecomite1;
            	if($mailcomite1){
            	   $integranteComite1->cim_email = $mailcomite1;
            	}
            	$integranteComite1->cim_fecha = date( 'Y-m-d H:i:s' );
            	$integranteComite1->store();
            }
            
            $nombrecomite2 = $data['ret_nombrecomite2'];
            if($nombrecomite2){
            	$mailcomite2 = $data['ret_mailcomite2'];
                $integranteComite2 = new comite();
                $integranteComite2->cim_ret_id=$table->ret_id;
                $integranteComite2->cim_nombre = $nombrecomite2;
                if($mailcomite2){
                    $integranteComite2->cim_email = $mailcomite2;
                }
                $integranteComite2->cim_fecha = date( 'Y-m-d H:i:s' );
                $integranteComite2->store();
            }
            $nombrecomite3 = $data['ret_nombrecomite3'];
            if($nombrecomite3){
            	$mailcomite3 = $data['ret_mailcomite3'];
                $integranteComite3 = new comite();
                $integranteComite3->cim_ret_id = $table->ret_id;
                $integranteComite3->cim_nombre = $nombrecomite3;
                if($mailcomite3){
                    $integranteComite3->cim_email = $mailcomite3;
                }
                $integranteComite3->cim_fecha = date( 'Y-m-d H:i:s' );
                $integranteComite3->store();
            }
            $nombrecomite4 = $data['ret_nombrecomite4'];
            if($nombrecomite4){
            	$mailcomite4 = $data['ret_mailcomite4'];
                $integranteComite4 = new comite();
                $integranteComite4->cim_ret_id = $table->ret_id;
                $integranteComite4->cim_nombre = $nombrecomite4;
                if($mailcomite4){
                    $integranteComite4->cim_email = $mailcomite4;
                }
                $integranteComite4->cim_fecha = date( 'Y-m-d H:i:s' );
                $integranteComite4->store();
            }
            $nombrecomite5 = $data['ret_nombrecomite5'];
            if($nombrecomite5){
            	$mailcomite5 = $data['ret_mailcomite5'];
                $integranteComite5 = new comite();
                $integranteComite5->cim_ret_id = $table->ret_id;
                $integranteComite5->cim_nombre = $nombrecomite5;
                if($mailcomite5){
                    $integranteComite5->cim_email = $mailcomite5;
                }
                $integranteComite5->cim_fecha = date( 'Y-m-d H:i:s' );
                $integranteComite5->store();
            }
            $nombrecomite6 = $data['ret_nombrecomite6'];
            if($nombrecomite6){
                $mailcomite6 = $data['ret_mailcomite6'];
                $integranteComite6 = new comite();
                $integranteComite6->cim_ret_id = $table->ret_id;
                $integranteComite6->cim_nombre = $nombrecomite6;
                if($mailcomite6){
                    $integranteComite6->cim_email = $mailcomite6;
                }
                $integranteComite6->cim_fecha = date( 'Y-m-d H:i:s' );
                $integranteComite6->store();
            }

            
            
            $director = new director();
            $director->limpiarDirectorRetos($table->ret_id);
            $nombredirector1 = $data['ret_nombredirector1'];
            if($nombredirector1){
            	$maildirector1 = $data['ret_maildirector1'];
            	$director = new director();
            	$director->dir_ret_id = $table->ret_id;
            	$director->dir_nombre = $nombredirector1;
            	if($maildirector1){
            	   $director->dir_email = $maildirector1;
            	}
            	$director->store();
            }
            
            $nombredirector2 = $data['ret_nombredirector2'];
            if($nombredirector2){
                $maildirector2 = $data['ret_maildirector2'];
                $director = new director();
                $director->dir_ret_id = $table->ret_id;
                $director->dir_nombre = $nombredirector2;
                if($maildirector2){
                   $director->dir_email = $maildirector2;
                }
                $director->store();
            }
            
            $nombredirector3 = $data['ret_nombredirector3'];
            if($nombredirector3){
                $maildirector3 = $data['ret_maildirector3'];
                $director = new director();
                $director->dir_ret_id = $table->ret_id;
                $director->dir_nombre = $nombredirector3;
                if($maildirector3){
                   $director->dir_email = $maildirector3;
                }
                $director->store();
            }
            
            
            
            $retoxarea = new retoxareainteres();
        	$retoxarea->limpiarAreasRetos($table->ret_id);
            $areas = $data['ret_ares_id'];
        	foreach ($areas  as $key => $value){
        		$retoxareainteres = new retoxareainteres();
        		$retoxareainteres->rxa_id =  '';
        		$retoxareainteres->rxa_ret_id =  $table->ret_id; 
        	   	$retoxareainteres->rxa_are_id = $value;
        		$retoxareainteres->store();
        	}
        	$negocios = $data['ret_coms_id'];
        	$retoxneg = new retoxnegocio();
        	$retoxneg->limpiarNegociosRetos($table->ret_id);
            foreach ($negocios  as $key => $value){
                $retoxnegocio = new retoxnegocio();
                $retoxnegocio->rxn_id =  '';
                $retoxnegocio->rxn_ret_id =  $table->ret_id; 
                $retoxnegocio->rxn_com_id = $value;
                $retoxnegocio->store();
            }
        	$_reto = new _reto($table->ret_id);
        }
        
        $retoxcompania = new retoxcompania();
        $retoxcompania->limpiarCompaniaRetos($table->ret_id);
        $compania1 = $data['ret_com1'];
        $puntos1 = $data['ret_compuntos1'];
        
        $compania2 = $data['ret_com2'];
        $puntos2 = $data['ret_compuntos2'];
        
        $compania3 = $data['ret_com3'];
        $puntos3 = $data['ret_compuntos3'];
        
        $compania4 = $data['ret_com4'];
        $puntos4 = $data['ret_compuntos4'];
        
        $compania5 = $data['ret_com5'];
        $puntos5 = $data['ret_compuntos5'];
        
        $compania6 = $data['ret_com6'];
        $puntos6 = $data['ret_compuntos6'];
        
        $compania7 = $data['ret_com7'];
        $puntos7 = $data['ret_compuntos7'];
        
        $compania8 = $data['ret_com8'];
        $puntos8 = $data['ret_compuntos8'];
        
        if($compania1 && $puntos1>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania1;
            $retoxcompania->rxc_puntos = $puntos1;
            $retoxcompania->store();
        }
         if($compania2 && $puntos2>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania2;
            $retoxcompania->rxc_puntos = $puntos2;
            $retoxcompania->store();
        }
         if($compania3 && $puntos3>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania3;
            $retoxcompania->rxc_puntos = $puntos3;
            $retoxcompania->store();
        }
         if($compania4 && $puntos4>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania4;
            $retoxcompania->rxc_puntos = $puntos4;
            $retoxcompania->store();
        }
         if($compania5 && $puntos5>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania5;
            $retoxcompania->rxc_puntos = $puntos5;
            $retoxcompania->store();
        }
         if($compania6 && $puntos6>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania6;
            $retoxcompania->rxc_puntos = $puntos6;
            $retoxcompania->store();
        }
         if($compania7 && $puntos7>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania7;
            $retoxcompania->rxc_puntos = $puntos7;
            $retoxcompania->store();
        }
         if($compania8 && $puntos8>0){
            $retoxcompania = new retoxcompania();
            $retoxcompania->rxc_ret_id = $table->ret_id;
            $retoxcompania->rxc_neg_id = $compania8;
            $retoxcompania->rxc_puntos = $puntos8;
            $retoxcompania->store();
        }
        
        
        
        
        if( $_FILES['jform']['name']['ret_imagen1'] ){
	        $uploads_dir = '../images/retos/';
	        $tamanoMax = 5000000;
	        $ext = explode('.', $_FILES['jform']['name']['ret_imagen1']);
	        $num = count($ext)-1;
	        if(!in_array($ext[$num], array('jpg','jpeg','png','gif'))){
	            return false;
	        }
	        if($_FILES['jform']['size']['ret_imagen1'] > $tamanoMax){
	            return false;
	        }
	        move_uploaded_file($_FILES['jform']['tmp_name']['ret_imagen1'], $uploads_dir . $ext[0].'_'.$table->ret_id.'.'.$ext[1] );         
	        $table->ret_imagen = $ext[0].'_'.$table->ret_id.'.'.$ext[1];
	        // Store the data.
	        if (!$table->store()) {
	            $this->setError($table->getError());
	            return false;
	        }
        }

        if( $_FILES['jform']['name']['ret_imagen2'] ){
            $uploads_dir = '../images/retos/';
            $tamanoMax = 5000000;
            $ext = explode('.', $_FILES['jform']['name']['ret_imagen2']);
            $num = count($ext)-1;
            if(!in_array($ext[$num], array('jpg','jpeg','png','gif'))){
                return false;
            }
            if($_FILES['jform']['size']['ret_imagen2'] > $tamanoMax){
                return false;
            }
            move_uploaded_file($_FILES['jform']['tmp_name']['ret_imagen2'], $uploads_dir . $ext[0].'_'.$table->ret_id.'peq.'.$ext[1] );         
            $table->ret_imagenPeq = $ext[0].'_'.$table->ret_id.'peq.'.$ext[1];
            // Store the data.
            if (!$table->store()) {
                $this->setError($table->getError());
                return false;
            }
        }
        
        
        if( $_FILES['jform']['name']['ret_imgDetalle'] ){   
            $uploads_dir = '../images/retos/';
            $tamanoMax = 5000000;
            $ext = explode('.', $_FILES['jform']['name']['ret_imgDetalle']);
            $num = count($ext)-1;
            if(!in_array($ext[$num], array('jpg','jpeg','png','gif'))){
                return false;
            }
            if($_FILES['jform']['size']['ret_imgDetalle'] > $tamanoMax){
                return false;
            }
            move_uploaded_file($_FILES['jform']['tmp_name']['ret_imgDetalle'], $uploads_dir . $ext[0].'_'.$table->ret_id.'det.'.$ext[1] );         
            $table->ret_imgDetalle = $ext[0].'_'.$table->ret_id.'det.'.$ext[1];
            // Store the data.
            if (!$table->store()) {
                $this->setError($table->getError());
                return false;
            }
        }
        
        
        
        if( $_FILES['jform']['name']['ret_anexo'] ){
            $uploads_dir = '../images/retos/anexos/';
            $tamanoMax = 5000000;
            $ext = explode('.', $_FILES['jform']['name']['ret_anexo']);
            $num = count($ext)-1;
            if(!in_array($ext[$num], array('doc','pdf','docx','xls','xlsx'))){
                //return false;
            }
            if($_FILES['jform']['size']['ret_anexo'] > $tamanoMax){
                return false;
            }
            move_uploaded_file($_FILES['jform']['tmp_name']['ret_anexo'], $uploads_dir . $ext[0].'_'.$table->ret_id.'.'.$ext[1] );         
            $table->ret_anexos = $ext[0].'_'.$table->ret_id.'.'.$ext[1];
            // Store the data.
            if (!$table->store()) {
                $this->setError($table->getError());
                return false;
            }
        } 
        return true;
    }
}
