<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

class RetosController extends JControllerLegacy {

    /**
     * display task
     *
     * @return void
     */
    function display($cachable = false, $urlparams = false) {
        $app = JFactory::getApplication();
        $task = JRequest::getVar('task');
        $sitio = JURI::base();
        if ($task == 'cargarAjax') {
            $buscar = JRequest::getVar('term');
            getAjaxUsuariosLotus($buscar);
        } elseif ($task == 'eliminarAnexo')
        {
            $anex_id    = JRequest::getVar('anex');
            $anexoxreto = new anexoxreto($anex_id);
            $filename   = "../images/retos/anexos/$anexoxreto->axr_archivo";

            if (!file_exists($filename))
            {

                $anexoxreto->delete();

            } else
            {
                $borrarImagen       = unlink("../images/retos/anexos/$anexoxreto->axr_archivo");

                if ($borrarImagen)
                {
                    $anexoxreto->delete();
                }
            }
            ?>
            <script>
                parent.location.reload();
            </script>
            <?php

            exit;
        }elseif($task == 'eliminImagenGrande' || $task == 'eliminImagenPeq'){
            $ret_id = JRequest::getVar('ret_id');
            if($ret_id){
                $reto = new reto($ret_id);
                if($task=='eliminImagenGrande'){
                   $borrarImagen = unlink("../images/retos/$reto->ret_imagen");
                   if($borrarImagen){
                        $reto->ret_imagen = '';
                   }
                }elseif($task == 'eliminImagenPeq'){
                     $borrarImagen = unlink("../images/retos/$reto->ret_imagenPeq");
                     if($borrarImagen){
                        $reto->ret_imagenPeq = '';
                     }
                }
                $reto->store();
                $app->redirect("index.php?option=com_retos&view=retos", "Imagen eliminada", "success");
            }
        } elseif ($task == 'anexos') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'anexo';
            $ret_id = JRequest::getVar('Item');
            $anexoxreto = new anexoxreto();
            $reto = new reto($ret_id);
            $anexoxreto = new anexoxreto();
            $anexos = $anexoxreto->cargarAnexos($reto->ret_id);
            $newView->anexos = $anexos;
            $newView->reto = $reto;
            $newView->display($tpl);
        } elseif ($task == 'notificarExperto') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'notificarExperto';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $experto = $reto->cargarExperto();
            $newView->experto = $experto;
            $newView->reto = $reto;
            $newView->display($tpl);
        } elseif ($task == 'enviarNotificacion') {
            $user = JFactory::getUser();
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $experto = $reto->cargarExperto();
            $notificacion = new notificacion();
            $notificacion->not_ret_id = $reto->ret_id;
            $notificacion->not_exp_id = $experto->exp_id;
            $notificacion->not_fecha = date('Y-m-d H:i');
            $notificacion->not_usuario = $user->username;
            if ($notificacion->store()) {
                $cuerpo = "<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.=" Hola, {$experto->exp_nombre}.</br></br>";
                $cuerpo.="Te damos la bienvenida a Soluciones Innovadoras.</br></br>";
                $cuerpo.="Has sido seleccionado como experto del reto {$reto->ret_nombre}. Como ";
                $cuerpo.="miembro del equipo tienes como responsabilidades:</br></br>";
                $cuerpo.="- Dar respuesta oportuna a las inquietudes de quienes quieran participar en tu reto.</br></br>";
                $cuerpo.="- Tener disposición para ayudar a quienes necesiten de tu asesoría.</br></br>";
                $cuerpo.="- Disfrutar del proceso de encontrar soluciones para el reto.</br></br>";
                $cuerpo.="Soluciones Innovadoras</br>";
                //$cuerpo.="<a href='http://www.solucionesinnovadoras.com.co'>www.solucionesinnovadoras.com.co</a></br>";
                $configMail =JFactory::getConfig();
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $sitename = $configMail->get('mailfrom');
                $correos = "$experto->exp_mail";
                $subject = "Has sido seleccionado como experto";
                //JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo, 1);

                $mail = JFactory::getMailer();
                $mail->addRecipient($from);
                $mail->addRecipient($correos);
                $mail->setSender(array($from, $fromname));
                $mail->setSubject($subject);
                $mail->setBody($cuerpo);
                $mail->IsHTML(true);

                $sent = $mail->Send();

                if ( $sent !== true ) {
                    echo 'Error sending email: ' . $sent->__toString();
                } else {
                    echo 'Mail sent';
                }

                ?>
                <script>
                    parent.location.reload();
                </script>
                <?php

                exit;
            } else {
                //exit('No fue posible enviar la notificación');
            }
        } elseif ($task == 'traduccion') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'traduccion';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $newView->tituloReto = $reto->ret_nombre;
            $newView->display($tpl);
        } elseif ($task == 'guardarTraduccion') {
            //exit('La traducción ha sido guardada con exito');
        } elseif ($task == 'terminos') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'tyc';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $terycon = new terminosycondiciones();
            $tyc = $terycon->cargarTerminosReto($ret_id);
            $newView->tituloReto = $reto->ret_nombre;
            $newView->tyc = $tyc;
            $newView->display($tpl);
        } elseif ($task == 'guardarTYC') {
            $idtyc_es = JRequest::getVar('idtyc_es');
            $idtyc_en = JRequest::getVar('idtyc_en');
            $ret_id = JRequest::getVar('idRet');
            $tyc_es = JRequest::getVar('tyc_es');
            $tyc_en = JRequest::getVar('tyc_en');
            $task = JRequest::getVar('task');
            if ($tyc_es) {
                $terminosycondiciones_ES = new terminosycondiciones();
                if ($idtyc_es) {
                    $terminosycondiciones_ES->ter_id = $idtyc_es;
                }
                $terminosycondiciones_ES->ter_ret_id = $ret_id;
                $terminosycondiciones_ES->ter_fecha = date('Y-m-d H:i:s');
                $terminosycondiciones_ES->ter_idioma = 'es';
                $terminosycondiciones_ES->ter_texto = $tyc_es;
                $terminosycondiciones_ES->store();
            }
            if ($tyc_en) {
                $terminosycondiciones_EN = new terminosycondiciones();
                if ($idtyc_en) {
                    $terminosycondiciones_EN->ter_id = $idtyc_en;
                }
                $terminosycondiciones_EN->ter_ret_id = $ret_id;
                $terminosycondiciones_EN->ter_fecha = date('Y-m-d H:i:s');
                $terminosycondiciones_EN->ter_idioma = 'en';
                $terminosycondiciones_EN->ter_texto = $tyc_en;
            }
            ?>
            <script>
                parent.location.reload();
            </script>
            <?php

            exit;
        } elseif ($task == 'guardarAnexos') {
            $ret_id = JRequest::getVar('idRet');
            $reto = new reto($ret_id);
            for ($i = 1; $i <= 5; $i++) {
                if (isset($_FILES["ret_anexar$i"]['name'])) {
                   // exit('222222222222222222222');
                    if ($_FILES["ret_anexar$i"]['name'] && $_FILES["ret_anexar$i"]['name'] != '') {
                        $anexoxreto = new anexoxreto();
                        $anexoxreto->axr_ret_id = $reto->ret_id;
                        $uploads_dir = '../images/retos/anexos/';
                        $tamanoMax = 5000000;
                        $ext = explode('.', $_FILES["ret_anexar$i"]['name']);
                        $num = count($ext) - 1;
                        if ($_FILES["ret_anexar$i"]['size'] > $tamanoMax)
                        {
                            echo "El archivo excede el tamaño permitido";
                            ?>

                            <script>
                                parent.location.reload();
                            </script>
                            <?php
                            ?>
                            <script>
                                alert('Error al cargar el archivo, debe verificar el tamaño o el formato.');
                                parent.location.reload();
                            </script>
                        <?php
                            exit;
                            //return false;
                        }

                        move_uploaded_file($_FILES["ret_anexar$i"]['tmp_name'], $uploads_dir . $ext[0] . '_' . $reto->ret_id . '.' . $ext[1]);
                        $anexoxreto->axr_archivo = $ext[0] . '_' . $reto->ret_id . '.' . $ext[1];

                        // Store the data.
                        if (!$anexoxreto->store()) {
                            $anexoxreto->setError($anexoxreto->getError());
                            ?>
                            <script>
                                alert('Error al cargar el archivo, debe verificar el tamaño o el formato.');
                                parent.location.reload();
                            </script>
                            <?php
                            return false;
                        }
                    }
                }
            }
            ?>
            <script>
                alert('El anexo ha sido cargado satisfactoriamente.');
               parent.location.reload();
            </script>
            <?php

            exit;
        } elseif ($task == 'alerta') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'alerta';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $alerta = new alerta();
            $alerta->cargarDatos($reto->ret_id);
            $newView->alerta = $alerta;
            $newView->reto = $reto;
            $newView->display($tpl);
        } elseif ($task == 'guardarAlerta') {
            $ret_id = JRequest::getVar('idRet');
            $reto = new reto($ret_id);
            $ale_directores = JRequest::getVar('ale_directores');
            $ale_diasdirectores = JRequest::getVar('ale_diasdirectores');
            $ale_director = JRequest::getVar('ale_director');
            $ale_diasdirector = JRequest::getVar('ale_diasdirector');
            $ale_presidente = JRequest::getVar('ale_presidente');
            $ale_diaspresidente = JRequest::getVar('ale_diaspresidente');
            $alerta = new alerta();
            $alerta->cargarDatos($reto->ret_id);
            $alerta->ale_ret_id = $reto->ret_id;
            $alerta->ale_directores = $ale_directores;
            $alerta->ale_diasdirectores = $ale_diasdirectores;
            $alerta->ale_director = $ale_director;
            $alerta->ale_diasdirector = $ale_diasdirector;
            $alerta->ale_presidente = $ale_presidente;
            $alerta->ale_diaspresidente = $ale_diaspresidente;
            $alerta->store();
            ?>
            <script>
                parent.location.reload();
            </script>
            <?php

            exit;
        } elseif ($task == 'participantes') {
            $newView = $this->getView('reto', 'html');
            $tpl = 'participantes';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $participantes = $reto->listarParticipantes();
            $newView->participantes = $participantes;
            $newView->reto = $reto;
            $newView->display($tpl);
        }elseif($task == 'soluciones'){
            $newView = $this->getView('reto', 'html');
            $tpl = 'soluciones';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $soluciones = $reto->cargarSoluciones($ret_id);
            $newView->soluciones = $soluciones;
            $newView->reto = $reto;
            $newView->display($tpl);
        }elseif($task == 'notificarVencimientoSeleccion'){
            $newView = $this->getView('reto', 'html');
            $tpl = 'notificacion';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $newView->reto = $reto;
            $newView->display($tpl);
        }elseif($task == 'configurarVencimientoAT'){
            $newView = $this->getView('reto', 'html');
            $tpl = 'vencimientoAT';
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $notificarretovencido = new notificarretovencido();
            $notificacion = $notificarretovencido->cargarNotificaion($reto->ret_id);
            $newView->reto = $reto;
            $newView->notificacion = $notificacion;
            $newView->display($tpl);
        }elseif($task == 'guardarMensajeVencimiento'){
            $ret_id = JRequest::getVar('idRet');
            $reto = new reto($ret_id);
            $notificar = new notificarretovencido();
            $objNtv = $notificar->cargarNotificaion($reto->ret_id);
            if($objNtv->ntv_id){
                $notificarretovencido = new notificarretovencido($objNtv->ntv_id);
                $notificarretovencido->ntv_fechaModificacionAT = date('Y-m-d h:i');
            }else{
                $notificarretovencido = new notificarretovencido();
                $notificarretovencido->ntv_fechaCreacionAT = date('Y-m-d h:i');
            }
            $notificarretovencido->ntv_ret_id = $reto->ret_id;
            $notificarretovencido->ntv_msjAutomatico = JRequest::getVar('ntv_msjAutomatico');
            $notificarretovencido->ntv_diasantes = JRequest::getVar('ntv_diasantes');
            $notificarretovencido->store();
            ?><script>parent.location.reload();</script><?php exit;
        }elseif($task == 'configurarVencimientoMN'){
            $newView = $this->getView('reto', 'html');
            $ret_id = JRequest::getVar('Item');
            $tpl = 'vencimientoMN';
            $reto = new reto($ret_id);
            $notificarretovencido = new notificarretovencido();
            $notificacion = $notificarretovencido->cargarNotificaion($reto->ret_id);
            $newView->reto = $reto;
            $newView->notificacion = $notificacion;
            $newView->display($tpl);
        }elseif($task == 'guardarMensajeVencimientoMN'){
            $ret_id = JRequest::getVar('idRet');
            $enviarNotificacion = JRequest::getVar('ntv_enviarNTF');
            $reto = new reto($ret_id);
            $notificar = new notificarretovencido();
            $objNtv = $notificar->cargarNotificaion($reto->ret_id);
            if($objNtv->ntv_id > 0){
                $notificarretovencido = new notificarretovencido($objNtv->ntv_id);
                $notificarretovencido->ntv_fechaModificacionMN = date('Y-m-d h:i');
            }else{
                $notificarretovencido = new notificarretovencido();
                $notificarretovencido->ntv_fechaCreacionMN = date('Y-m-d h:i');
            }
            $notificarretovencido->ntv_ret_id = $reto->ret_id;
            $notificarretovencido->ntv_msjManual = JRequest::getVar('ntv_msjManual');
            $notificarretovencido->store();
            if($enviarNotificacion=='SI'){
                $participantes = $reto->listarParticipantes();
                $notificarretovencido->enviarNotificacionMN($participantes,$reto);
            }
            ?><script>parent.location.reload();</script><?php exit;
        }elseif ($task == 'retoActualizado') {
            $ret_id = JRequest::getVar('Item');
            $reto = new reto($ret_id);
            $participantes = $reto->listarParticipantes();

            foreach ($participantes as $participante) {
                $cuerpo = "<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.=" Hola, {$participante->usu_nombre}.</br></br>";
                $cuerpo.="Se ha actualizado la información del reto {$reto->ret_nombre}. te recomendamos que ";
                $cuerpo.="visites la página y conozcas la nueva información que te será de ayuda para resolverlo.</br></br>";
                $cuerpo.="Soluciones Innovadoras</br>";
                //$cuerpo.="<a href='http://www.solucionesinnovadoras.com.co'>www.solucionesinnovadoras.com.co</a></br>";
                $configMail =JFactory::getConfig();
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $sitename = $configMail->get('mailfrom');
                $correos = "$participante->usu_email";
                $subject = "información retos";
                JFactory::getMailer()->sendMail($from, $fromname, $correos, $subject, $cuerpo, true);
            }
            echo 'La notificación ha finalizado';
        } elseif ($task == 'verformulario') {
            $ret_id = JRequest::getVar('idreto');
            $user = JFactory::getUser();
            $reto = new reto($ret_id);
            $newView = $this->getView('retos', 'html');
            $tpl = 'form';
            $campos = new propiedad();
            $cargarCampos = $campos->cargar($reto->ret_id);
            foreach ($cargarCampos as $carga) {
                if ($carga->pro_tipo == 'select') {
                    $carga->pro_option = $campos->cargarOption($carga->pro_id);
                } elseif ($carga->pro_tipo == 'check') {
                    $carga->pro_optionchek = $campos->cargarOption($carga->pro_id);
                } else {
                    continue;
                }
            }

            $form = new formulario();
            if ($reto) {

                $cargaForm = $form->cargarFormReto($reto->ret_id);

                if (isset($cargaForm->frm_id)) {

                    $form = new formulario($cargaForm->frm_id);
                } else {
                    $form = new formulario();
                }
            }

            $newView->campos = $cargarCampos;
            $newView->forname = 'form' . $ret_id;
            $newView->reto = $reto;
            $newView->user = $user;
            $newView->display($tpl);
        } elseif ($task == 'crearForm') {
            $arrdatos = JRequest::getVar('elemento');
            $ret_id = JRequest::getVar('ret_id');
            $reto = new reto($ret_id);
            $form = new formulario();
            if ($reto) {

                $cargaForm = $form->cargarFormReto($reto->ret_id);

                if (isset($cargaForm->frm_id)) {

                    $form = new formulario($cargaForm->frm_id);
                } else {

                    $form = new formulario();
                }

                $form->frm_nombre = 'form' . $ret_id;
                $form->frm_fechaCreado = date('Y-m-d h:i');
                $form->frm_estado = '1';
                $form->frm_ret_id = $ret_id;
                $form->store();



                foreach ($arrdatos as $variablesForm) {
                    if(isset($variablesForm['tipo'])){
                    if ($variablesForm['tipo'] != 'select' && $variablesForm['tipo'] != 'check') {
                        $this->validarDatosForm($variablesForm, $form);
                    } elseif ($variablesForm['tipo'] == 'select' | $variablesForm['tipo'] == 'check') {
                        $dato = $this->validarDatosForm($variablesForm, $form);
                        if (isset($variablesForm['option'])) {
                             foreach ($variablesForm['option']as $k => $opciones) {
                                $this->validarDatosOption($variablesForm,$dato, $k, $opciones);
                            }
                        } elseif (isset($variablesForm['optionchek'])) {
                           foreach ($variablesForm['optionchek']as $k => $opciones) {
                                $this->validarDatosOption($variablesForm,$dato, $k, $opciones);
                            }
                        }
                        //haga lo que tenga que hacer
                    }
                }
                }




                $user = JFactory::getUser();
                $newView = $this->getView('retos', 'html');
                $tpl = 'form';
                $campos = new propiedad();
                $cargarCampos = $campos->cargar($reto->ret_id);
                foreach ($cargarCampos as $carga) {
                    if ($carga->pro_tipo == 'select') {
                        $carga->pro_option = $campos->cargarOption($carga->pro_id);
                    } elseif ($carga->pro_tipo == 'check') {
                        $carga->pro_optionchek = $campos->cargarOption($carga->pro_id);
                    } else {
                        continue;
                    }
                }
            }
            echo "<script language='JavaScript'>
              alert('Formulario Actualizado')
              </script>";

            $newView->campos = $cargarCampos;
            $newView->reto = $reto;
            $newView->forname = $form->frm_nombre;
            $newView->user = $user;
            $newView->display($tpl);
        } else {
            //$task = JRequest::getVar('task');
            if ($task == 'eliminarReto') {
                $idRet = JRequest::getVar('idRet');
                $reto = new reto($idRet);
                if ($reto->ret_estado == 'Anulado') {
                    if ($reto->delete()) {
                        JError::raiseWarning(null, "el reto $reto->ret_nombre ha sido borrado");
                    } else {
                        JError::raiseWarning(null, "No fue posible borrar el reto $reto->ret_nombre");
                    }
                } else {
                    JError::raiseWarning(null, "El reto debe de estar en estado ANULADO");
                }
            }
            // set default view if not set
            JRequest::setVar('view', JRequest::getCmd('view', 'retos'));
            // call parent behavior
            parent::display($cachable);
        }

    }

    public function validarDatosForm($variblesForm, $form) {
        $db = JFactory::getDBO();
        $sql = "SELECT * FROM propiedad where pro_nombre='{$variblesForm['nombre']}' and pro_frm_id = '$form->frm_id'";
        $db->retos->setQuery($sql);
        $variableBD = $db->retos->loadObject();
        if ($variableBD) {
            $propiedad = new propiedad($variableBD->pro_id);
            $propiedad->store();

        } else {
            $propiedad = new propiedad();
            $propiedad->pro_nombre = $variblesForm['nombre'];
        }
        $propiedad->pro_frm_id = $form->frm_id;
        $propiedad->pro_tipo = $variblesForm['tipo'];
        $propiedad->pro_descripcion = $variblesForm['descripcion'];
        $propiedad->pro_valor = $variblesForm['valor'];
        $propiedad->pro_texto = $variblesForm['texto'];
        $propiedad->pro_ancho = $variblesForm['ancho'];
        $propiedad->pro_alto = $variblesForm['alto'];
        $propiedad->pro_Requerido = $variblesForm['Requerido'];
        $propiedad->pro_caracteres = $variblesForm['caracteres'];
        $propiedad->pro_estado = $variblesForm['estado'];
        $propiedad->pro_posicion = $variblesForm['posicion'];
        $propiedad->store();

        return $propiedad;

    }

    public function validarDatosOption($arrVariable,$dato, $val, $texto) {


        $db = JFactory::getDBO();
        $sql = "SELECT * FROM propiedad inner join formulario on frm_id=pro_frm_id where pro_pro_id='$dato->pro_id'and pro_valor='$val'";
        $db->retos->setQuery($sql);
        $variableBD = $db->retos->loadObject();

        if ($variableBD) {
            $propiedad = new propiedad($variableBD->pro_id);
            $propiedad->store();
        } else {
            $propiedad = new propiedad();
        }


        if ($dato->pro_tipo == 'select') {
            $propiedad->pro_tipo = 'option';
            $propiedad->pro_estado = $arrVariable['estadoption'][$val];
        } elseif ($dato->pro_tipo == 'check') {
            $propiedad->pro_estado = $arrVariable['estadochek'][$val];
            $propiedad->pro_nombre = $arrVariable['nombre'].'option'.$val;
        }
        $propiedad->pro_frm_id = $dato->pro_frm_id;
        $propiedad->pro_pro_id = $dato->pro_id;
        $propiedad->pro_valor = $val;
        $propiedad->pro_texto = $texto;


        $propiedad->store();


    }

}
