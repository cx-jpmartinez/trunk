<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');
JHtml::_('behavior.framework', true);
JHtml::_('behavior.modal');
/**
 * retos View
 */
class RetosViewRetos extends JViewLegacy
{
	/**
	 * Retos view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$db = JFactory::getDbo();
		$filter_estado = JRequest::getVar( 'filter_estado' );
                $filter_negocio = JRequest::getVar( 'filter_negocio' );
                $where = '';
                $inner = '';
                if($filter_estado){
                     $where = " WHERE ret_estado = '$filter_estado'";
                }
                if($filter_negocio){
                    $inner = " INNER JOIN retoxnegocio ON (ret_id=rxn_ret_id) ";
                    $where = " WHERE  rxn_com_id = '$filter_negocio' ";
                }

            $query = "
            SELECT
                ret_id
                , ret_cat_id
                , ret_nombre
                , ret_imagen
                , ret_resumen
                , ret_descripcion
                , ret_resultadoEsperado
                , ret_nosebusca
                , ret_estado
                , ret_fechaCreacion
                , ret_fechaPublicacion
                , ret_fechaVigencia
                , ret_anexos
                ,not_id
                ,(SELECT group_concat(com_nombre) FROM compania
                   inner join retoxnegocio on (rxn_com_id=com_id)
                    where rxn_ret_id=ret_id
                    group by rxn_ret_id) as negocio
            FROM reto
            $inner
            left join notificacion on (ret_id=not_ret_id)
            $where
            GROUP BY ret_id
            order by ret_fechaVigencia desc
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
//		$items = $this->get('Items');

		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	* Setting the toolbar
	*/
	protected function addToolBar()
	{
			$task = JRequest::getVar( 'task' );
			JToolBarHelper::title(JText::_('COM_FORO_TITLE_RETOS'), 'thumbs-up');
			if($task=='')
			{
				$canDo = RetosHelpersRetos::getActions();
				$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/reto';

				if (file_exists($formPath))
				{
					if ($canDo->get('core.create'))
					{
						JToolBarHelper::addNew('reto.add', 'JTOOLBAR_NEW');
					}


					if ($canDo->get('core.edit') && isset($this->items[0]))
					{
						JToolBarHelper::editList('reto.edit', 'JTOOLBAR_EDIT');
					}
				}


				if ($canDo->get('core.admin'))
				{
					JToolBarHelper::preferences('com_retos');
				}


			}
	}
}
