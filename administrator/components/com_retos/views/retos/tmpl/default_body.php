<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo $item->ret_id; ?>
		</td>
		<td class="center" >
			<?php echo JHtml::_('grid.id', $i, $item->ret_id); ?>
		</td>
		<td class="has-context">
			<a href="<?php echo JRoute::_('index.php?option=com_retos&task=reto.edit&ret_id=' . $item->ret_id); ?>">
				<?php echo $item->ret_nombre; ?>
			</a>
		</td>
	    <td class="small">
            <?php echo $item->ret_estado; ?>
        </td>
        <td align="small">
            <?php echo $item->negocio; ?>
        </td>
           
		<td class="center">
			<a class="btn btn-small hasTooltip"  data-original-title="Visualizar" href="../index.php?option=com_retos&view=reto&idreto=<?php echo $item->ret_id; ?>&Itemid=444&admin=visualizar" target="_blank">
	           <span class="icon-zoom-in"></span>
	        </a>
        </td>
		<!--td align="center">
            <?php echo $item->ret_fechaCreacion; ?>
        </td-->
		<td class="small">
            <?php echo $item->ret_fechaPublicacion; ?>
		</td>
		<td class="small">
            <?php echo $item->ret_fechaVigencia; ?>
		</td>
		
        <!--td align="center">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=participantes&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            <?php //echo cargarNumeroParticipantes($item->ret_id); ?>
           </a>
        </td--> 
        <td class="center">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=soluciones&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            <?php echo cargarNumeroSolucionesReto($item->ret_id); ?>
           </a>
        </td>
        <td class="center">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=anexos&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            Anexos
           </a>
        </td> 
        <!--td align="center">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=traduccion&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            Inglés
           </a>
        </td-->
        
        <td class="small">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=notificarVencimientoSeleccion&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            Notificar Vencimiento
           </a>
        <td class="small">
            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=notificarExperto&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            <div class="small center">Notificar a Experto</div>
            <?php if($item->not_id){ ?>
                <div class="small center hasTooltip">
                   <span class="icon-checkmark" data-original-title="Notificado"></span>
                </div>
            <?php } ?> 
            
           </a>
        </td>
        <td class="small">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=retoActualizado&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
            Notificación de Reto Actualizado
           </a>
        </td> 
        <td class="small">
           <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=alerta&Item=<?php echo $item->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
              Configuración de Alerta en Preguntas          
           </a>
        </td>
        <td class="small">
            <?php if($item->ret_estado=='Anulado'){ ?>
                <a class="btn btn-small hasTooltip" data-original-title="Eliminar" onclick="confirBorrado(<?php echo $item->ret_id; ?>);">
                    <span class="icon-unpublish"></span><span>Eliminar</span>
                </a>
            <?php }else{ ?>
            &nbsp;
              <a class="btn btn-small hasTooltip" data-original-title="Formulario de soluci&oacute;n" href="index.php?option=com_retos&task=verformulario&idreto=<?php echo $item->ret_id; ?>">
                  <span class="icon-file-2"></span><span>Solución</span>
              </a>
            <?php } ?>
        </td>
        </tr>
<?php endforeach; ?>

