<?php
/**
 * default template file for HelloWorlds view of HelloWorld component
 *
 * @version		$Id: default.php 46 2010-11-21 17:27:33Z chdemko $
 * @package		Joomla16.Tutorials
 * @subpackage	Components
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @author		Christophe Demko
 * @link		http://joomlacode.org/gf/project/helloworld_1_6/
 * @license		License GNU General Public License version 2 or later
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('bootstrap.tooltip');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_retos/css/form.css');
?>

<script type="text/javascript">
function confirBorrado(id){
        form = document.forms.adminForm;
        if(confirm('Estas seguro de borrar este reto')){
            document.getElementById('task').value='eliminarReto';
            document.getElementById('idRet').value=id;
            form.submit();
            return true;
	}else{
		return false;
	}

}
</script>
<thead><?php echo $this->loadTemplate('buscar');?></thead>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" id="adminForm" name="adminForm">
	<table id="adminlist" class="table table-striped">
        <thead><?php echo $this->loadTemplate('head');?></thead>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
	</table>
	<div>
        <input type="hidden" id="task" name="task" value="" />
        <input type="hidden" id="idRet" name="idRet" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
