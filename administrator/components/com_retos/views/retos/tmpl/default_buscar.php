<?php 
$filter_estado = JRequest::getVar( 'filter_estado' );
$filter_negocio = JRequest::getVar( 'filter_negocio' );
?>

<form action="<?php echo JRoute::_('index.php?option=com_retos');?>" method="post" name="adminForm1" id="adminForm1">
    <div id="filter-bar" class="row-fluid">
        <div class="filter-search span12">
            <label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filtrar Por: '); ?></label>
            <div class="form-inline">
                <select name="filter_estado" class="inputbox" onchange="this.form.submit()">
                    <option value=""><?php echo JText::_('Seleccione Estado -');?></option>
                    <?php echo cargarListaEstadoRetos($filter_estado); ?>
                </select>

                <select name="filter_negocio" class="inputbox" onchange="this.form.submit()">
                    <option value=""><?php echo JText::_('Selecciones Negocio - ');?></option>
                    <?php echo cargarListaNegocios($filter_negocio); ?>
                </select>

                <a href="index.php?option=com_retos">
                    <button class="btn btn-default" type="button"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
                </a>
            </div>
        </div>
    </div>

</form>
