<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
?>
<script type="text/javascript"  src='../_objects/scripts/jquery.js' ></script>
<script type="text/javascript">
 function validarForm(){
    form = document.forms.retosform;
    if($("#ntv_msjManual").val() == "") {        
        alert("El campo mensaje es obligatorio"); 
        $("#ntv_msjManual").focus();
        return false;  
    }  
    if($("#ntv_enviarNTF").val().length <= 0) {        
        alert("El campo enviar es obligatorio");  
        $("#ntv_msjManual").focus();
        return false;  
    }  
    form.submit();
}   
</script>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="retosform" id="retosform">
      <h1><?php echo $this->reto->ret_nombre; ?></h1>	
      <h2>&#9658; Enviar notificación de vencimiento.</h2>


    <div class="form-horizontal">

        <div class="control-group">

            <div class="control-label">Mensaje</div>
            <div class="controls">
                <?php if(isset($this->notificacion->ntv_msjManual)){ ?>
                    <textarea rows="10" cols="70" name="ntv_msjManual" id='ntv_msjManual'><?php echo $this->notificacion->ntv_msjManual; ?></textarea>
                <?php }else{ ?>
                    <textarea rows="10" cols="70" name="ntv_msjManual" id='ntv_msjManual'></textarea>
                <?php } ?>
            </div>
        </div>
        <h3></h3>

        <div class="control-group">
            <div class="control-label">Enviar</div>
            <div class="controls">
                <select name="ntv_enviarNTF" id="ntv_enviarNTF">
                    <option value="NO">NO</option>
                    <option value="SI">SI</option>
                </select>
            </div>
        </div>
    </div>

	<p align="center">
            <input type="button" value="Aceptar" class="button" src="" onclick="return validarForm();"/>
            &nbsp;&nbsp;
            <input type="hidden" name="task" value="guardarMensajeVencimientoMN" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="idRet" value="<?php echo $this->reto->ret_id; ?>"/>
        </p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>


