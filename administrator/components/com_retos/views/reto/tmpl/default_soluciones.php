<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$reto = $this->reto;
$soluciones = $this->soluciones;
//$sitio = 'http://www.solucionesinnovadoras.com.co';
//$sitio = 'http://us.localhost.com/htdocs/retos';
$sitio = JURI::root();
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="adminForm" id="retos-form">
	<h2>Soluciones del Reto: <?php echo $reto->ret_nombre; ?></h2>
        <table border="1" width="100%">
            <tr style="font-size: 15px;">
                <th>Solución</th>
                <th>Estado</th>
                <th>Enlace</th>
             </tr>
	<?php
	
	 foreach ($soluciones as $solucion){ ?>
            <tr>
                <td><?php echo $solucion->sol_titulo ?></td>
                <td>
                    <?php 
                    if(!$solucion->sol_estado || $solucion->sol_estado=='Pendiente'){
                        echo 'BORRADOR';
                    }elseif($solucion->sol_estado=='Finalizado'){
                        echo 'PENDIENTE DE APROBACIÓN';
                    }elseif($solucion->sol_estado=='Aprobada'){
                        echo 'SOLUCIÓN GANADORA';
                    }elseif($solucion->sol_estado=='Rechazada'){
                        echo 'RECHAZADA';
                    }else{
                        echo 'BORRADOR';
                    }
                    ?>
                </td>
                <th>
                  <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" target="blank" href="<?php echo "$sitio"."index.php?option=com_solucion&view=verSolucion&idSol=$solucion->sol_id&&origen=8fd715a4a8bac9113c7486147a444da5&amp;component=com_retos&amp;path=&amp;tmpl=component"?>">Ver solución</a> </th>
            </tr>
     <?php
        
        } 
     ?>
	        
	        
	</table>
	        
	        
	        
	        
	        
	        
	       
       
       
	    
	   
	
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>