<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
$idRet = JRequest::getVar('Item');
$reto = $this->reto;
$anexos = $this->anexos;
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="adminForm" id="retos-form" enctype="multipart/form-data">

	<h2><?php echo $reto->ret_nombre; ?></h2>

    <div class="form-horizontal">
        <div class="control-group">
            <div class="control-label">Anexo 1</div>
            <div class="controls">
                <?php
                if(isset($anexos[0]->axr_archivo))
                {
                ?>
                    <a href="../images/retos/anexos/<?php echo $anexos[0]->axr_archivo; ?>" target="blank"><?php echo $anexos[0]->axr_archivo; ?></a>
                    &nbsp; | &nbsp;
                    <a href="index.php?option=com_retos&task=eliminarAnexo&anex=<?php echo $anexos[0]->axr_id; ?>">
                        Eliminar
                    </a>

                <?php
                }else{ ?>
                    <input type="file" size="60" name="ret_anexar1" id="ret_anexar1">
                <?php
                }
                ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">Anexo 2</div>
            <div class="controls">
                <?php
                if(isset($anexos[1]->axr_archivo)){ ?>
                    <a href="../images/retos/anexos/<?php echo $anexos[1]->axr_archivo; ?>" target="blank"><?php echo $anexos[1]->axr_archivo; ?></a>
                    &nbsp; | &nbsp;
                    <a href="index.php?option=com_retos&task=eliminarAnexo&anex=<?php echo $anexos[1]->axr_id; ?>">
                        Eliminar
                    </a>
                    <?php
                }else{ ?>
                    <input type="file" size="60" name="ret_anexar2" id="ret_anexar2">
                <?php
                }
                ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">Anexo 3</div>
            <div class="controls">
                <?php
                if(isset($anexos[2]->axr_archivo)){?>

                    <a href="../images/retos/anexos/<?php echo $anexos[2]->axr_archivo; ?>" target="blank"><?php echo $anexos[2]->axr_archivo; ?></a>
                    &nbsp; | &nbsp;
                    <a href="index.php?option=com_retos&task=eliminarAnexo&anex=<?php echo $anexos[2]->axr_id; ?>">
                        Eliminar
                    </a>
                    <?php
                    //}
                }else{ ?>
                    <input type="file" size="60" name="ret_anexar3" id="ret_anexar3">
                <?php
                }
                ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">Anexo 4</div>
            <div class="controls">
                <?php
                if(isset($anexos[3]->axr_archivo)){?>

                    <a href="../images/retos/anexos/<?php echo $anexos[3]->axr_archivo; ?>" target="blank"><?php echo $anexos[3]->axr_archivo; ?></a>
                    &nbsp; | &nbsp;
                    <a href="index.php?option=com_retos&task=eliminarAnexo&anex=<?php echo $anexos[3]->axr_id; ?>">
                        Eliminar
                    </a>
                    <?php
                }else{ ?>
                    <input type="file" size="60" name="ret_anexar4" id="ret_anexar4">
                <?php
                }
                ?>

            </div>
        </div>
        <div class="control-group">
            <div class="control-label">Anexo 5</div>
            <div class="controls">
                <?php
                if(isset($anexos[4]->axr_archivo)){
                    //if($anexos[4]->axr_archivo &&  file_exists("../images/retos/anexos/{$anexos[4]->axr_archivo}")){ ?>
                    <a href="../images/retos/anexos/<?php echo $anexos[4]->axr_archivo; ?>" target="blank"><?php echo $anexos[4]->axr_archivo; ?></a>
                    &nbsp; | &nbsp;
                    <a href="index.php?option=com_retos&task=eliminarAnexo&anex=<?php echo $anexos[4]->axr_id; ?>">
                        Eliminar
                    </a>
                    <?php
                    //}
                }else{ ?>
                    <input type="file" size="60" name="ret_anexar5" id="ret_anexar5">
                <?php
                }
                ?>
            </div>
        </div>
    </div>

	<p align="center">
	    <input type="submit" value="Aceptar" class="button" src=""/>
	    <input type="hidden" name="task" value="guardarAnexos" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>"/>
	</p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>