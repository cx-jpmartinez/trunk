<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
$idRet = JRequest::getVar('Item');
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="adminForm" id="retos-form"">
	<h2><?php echo $this->reto->ret_nombre; ?></h2>
	<div class="form">

		<h3>Notificar al Experto</h3>
				<div style="text-align: justify">
				   Hola, <?php echo $this->experto->exp_nombre; ?>.<br><br>  
                   Te damos la bienvenida a Soluciones Innovadoras.<br> 
                   Has sido seleccionado como experto del reto " <b><?php echo $this->reto->ret_nombre; ?></b> ". Como <br> 
                   miembro del equipo tienes como responsabilidades:<br><br>
                   -   Dar respuesta oportuna a las inquietudes de quienes quieran participar en tu reto. <br><br>
                   -   Tener disposición para ayudar a quienes necesiten de tu asesoría. <br><br>
                   -   Disfrutar del proceso de encontrar soluciones para el reto. <br><br>
				</div>
	</div>
	<p align="center">
	    <input type="submit" value="Enviar Notificación" class="button" src=""/>
	    <input type="hidden" name="task" value="enviarNotificacion" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="Item" value="<?php echo $this->reto->ret_id; ?>"/>
	</p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>