<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
$idRet = JRequest::getVar('Item');
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="adminForm" id="retos-form"">
	<h2><?php echo $this->tituloReto; ?></h2>
	<div class="width-100 fltlft">
		<fieldset class="adminform">
		
		    <legend>Términos y Condiciones</legend>
		       <ul class="adminformlist">
				<li>
				    <b>ESPAÑOL</b>
				    <textarea rows="15" cols="70" name="tyc_es" id='tyc_es'>
				    <?php 
				    if(isset($this->tyc[0]->ter_texto)){
				        echo $this->tyc[0]->ter_texto;
				    }
				    ?>
				    </textarea>
				</li>
		      </ul>
	    </fieldset>
	</div>
	<div class="width-100 fltlft">
        <fieldset class="adminform">
            <legend>Términos y Condiciones</legend>
               <ul class="adminformlist">
                <li>
                    <b>INGLES</b>
                    <textarea rows="15" cols="70" name="tyc_en" id='tyc_en'>
                    <?php 
                    if(isset($this->tyc[1]->ter_texto)){
                        echo $this->tyc[1]->ter_texto;
                    }
                    ?>
                    </textarea>
                </li>
              </ul>
        </fieldset>
    </div>
    <p align="center">
	    <input type="submit" value="Aceptar" class="button" src=""/>
	    <input type="hidden" name="task" value="guardarTYC" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="idRet" value="<?php echo $idRet; ?>"/>
	    <?php if(isset($this->tyc[0]->ter_id)){ ?>
	       <input type="hidden" name="idtyc_es" value="<?php echo $this->tyc[0]->ter_id; ?>"/>
	    <?php } ?>
	     <?php if(isset($this->tyc[1]->ter_id)){ ?>
	    <input type="hidden" name="idtyc_en" value="<?php echo $this->tyc[1]->ter_id; ?>"/>
	    <?php } ?>
	    
    </p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>