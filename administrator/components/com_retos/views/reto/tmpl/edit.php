<?php

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_retos/css/form.css');

?>

<style>
    .ui-widget { font-size: 0.8em !important; }
</style>

<link rel="stylesheet" type="text/css" href="../_objects/css/redmond/jquery-ui-1.8.16.custom.css" />

<script src="../_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="../_objects/scripts/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript">

js = jQuery.noConflict();

js(function(){
    js('#jform_ret_nombrecomite1').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite1').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite1').attr('readonly',true);
            }

        }
      });
});

js(function(){
    js('#jform_ret_nombrecomite2').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite2').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite2').attr('readonly',true);
            }

        }
      });
});

js(function(){
    js('#jform_ret_nombrecomite3').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite3').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite3').attr('readonly',true);
            }

        }
      });
});

js(function(){
    js('#jform_ret_nombrecomite4').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite4').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite4').attr('readonly',true);
            }

        }
      });
});
js(function(){
    js('#jform_ret_nombrecomite5').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite5').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite5').attr('readonly',true);
            }

        }
    });
});

js(function(){
    js('#jform_ret_nombrecomite6').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_mailcomite6').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_mailcomite6').attr('readonly',true);
            }

        }
    });
});



js(function(){
    js('#jform_ret_nombredirector1').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_maildirector1').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_maildirector1').attr('readonly',true);
            }

        }
      });
});

js(function(){
    js('#jform_ret_nombredirector2').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_maildirector2').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_maildirector2').attr('readonly',true);
            }

        }
      });
});

js(function(){
    js('#jform_ret_nombredirector3').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            js('#jform_ret_maildirector3').val(arr[1]);
            if(arr[1]!=''){
                js('#jform_ret_maildirector3').attr('readonly',true);
            }

        }
      });
});


js(function(){
js('#jform_ret_compuntos1').change(function(){
        var suma = 0;
        suma = Number($("input[id='jform_ret_compuntos1']").val());
        suma += Number($("input[id='jform_ret_compuntos2']").val());
        suma += Number($("input[id='jform_ret_compuntos3']").val());
        suma += Number($("input[id='jform_ret_compuntos4']").val());
        suma += Number($("input[id='jform_ret_compuntos5']").val());
        suma += Number($("input[id='jform_ret_compuntos6']").val());
        suma += Number($("input[id='jform_ret_compuntos7']").val());
        suma += Number($("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos2').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos3').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos4').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        $("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos5').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});

});

js(function(){
js('#jform_ret_compuntos6').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos7').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

js(function(){
js('#jform_ret_compuntos8').change(function(){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
});
});

function sumaPuntos (){
        var suma = 0;
        suma = Number(js("input[id='jform_ret_compuntos1']").val());
        suma += Number(js("input[id='jform_ret_compuntos2']").val());
        suma += Number(js("input[id='jform_ret_compuntos3']").val());
        suma += Number(js("input[id='jform_ret_compuntos4']").val());
        suma += Number(js("input[id='jform_ret_compuntos5']").val());
        suma += Number(js("input[id='jform_ret_compuntos6']").val());
        suma += Number(js("input[id='jform_ret_compuntos7']").val());
        suma += Number(js("input[id='jform_ret_compuntos8']").val());
        js("#jform_ret_puntosP").val(suma);
}

Joomla.submitbutton = function (task) {
  if (task == 'reto.cancel')
  {
    Joomla.submitform(task, document.getElementById('retos-form'));
  }
  else
  {

        if (task != 'reto.cancel')
        {
            Joomla.submitform(task, document.getElementById('retos-form'));
        }
        else
        {
              alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
        }
  }
}

</script>


<?php


$idRet = JRequest::getVar('ret_id');
define('PUBLICAR_CAMPO',"<span style='font-size: 9px; color: red;'>Pubicar Campo</span>");
$_reto = new _reto();
if($this->item->ret_id){
	$_reto = $_reto->cargarcomposReto($this->item->ret_id);
	if(!isset($_reto->rte_id)){
		$_reto = new _reto();
	}
	$comite = new comite();
	$evaluadores = $comite->cargarComiteReto($this->item->ret_id);
	$director = new director();
	$directores = $director->cargarDirectorReto($this->item->ret_id);
        $retoxcompania = new retoxcompania();
        $companias =  $retoxcompania->cargarCompaniasReto($this->item->ret_id);
}
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos&layout=edit&ret_id='.(int) $this->item->ret_id); ?>"
      method="post"
      name="adminForm"
      id="retos-form"
      enctype="multipart/form-data"
    >
    <div class="row-fluid">
        <div class="span6 formula">
            <div class="form">
                <fieldset >
                    <legend><?php echo JText::_( 'Información del Reto' ); ?></legend>

                    <div class="control-group">
                        <div class="control-field">
                            <?php echo $this->form->getLabel('ret_nombre'); ?>
                        </div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombre'); ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_cat_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_cat_id'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_resumen'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_resumen'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_descripcion'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_descripcion'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_antecedente'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_antecedente'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_resultadoEsperado'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_resultadoEsperado'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_nosebusca'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_nosebusca'); ?></div>
                    </div>

                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_criterioEvaluacion'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_criterioEvaluacion'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-field"><?php echo $this->form->getLabel('ret_objetivo'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_objetivo'); ?></div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="span5 formula">
            <div class="form">
                <fieldset>
                    <legend>Publicación</legend>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_estado'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_estado'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_fechaPublicacion'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_fechaPublicacion'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_fechaVigencia'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_fechaVigencia'); ?></div>
                    </div>
                </fieldset>
            </div>


            <div class="form">
                <fieldset >

                    <legend>Asociado A:</legend>

                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_coms_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_coms_id'); ?></div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_ares_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_ares_id'); ?></div>
                    </div>

                </fieldset>
            </div>
            <div>
                <fieldset >
                    <legend>Anexos</legend>
                    <div class="form-inline">
                        <?php echo $this->form->getLabel('ret_imagen1'); ?>
                        <?php echo $this->form->getInput('ret_imagen1'); ?>
                        <?php if($this->item->ret_imagen &&  file_exists("../images/retos/{$this->item->ret_imagen}")){ ?>
                            <img src="../images/retos/<?php echo $this->item->ret_imagen; ?>" width="74" height="50" alt="imagen reto" border="0">
                                <div>
                                    <a title="Elimianr imagen grande" onclick="return confirm('Esta seguro de eliminar la Imagen?');" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 600}}" id="modal" href="index.php?option=com_retos&task=eliminImagenGrande&ret_id=<?php echo $this->item->ret_id; ?>">
                                        Eliminar imagen grande
                                    </a>
                                </div>
                            <?php } ?>

                    </div>
                    <div class="form-inline">
                        <?php echo $this->form->getLabel('ret_imagen2'); ?>
                        <?php echo $this->form->getInput('ret_imagen2'); ?>
                    </div>
                    <div>
                        <div class="form-inline">
                            <?php echo $this->form->getLabel('ret_galeria'); ?>
                            <?php echo $this->form->getInput('ret_galeria'); ?>
                        </div>
                    </div>

                </fieldset>
            </div>
            <div class="form">
                <fieldset>
                    <legend>Experto</legend>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_exp_id'); ?></div>
                        <div class="controls"> <?php echo $this->form->getInput('ret_exp_id'); ?></div>
                    </div>
                </fieldset>
            </div>

            <div>
                <fieldset>
                    <legend>Comité Evaluador</legend>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite1'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite1'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite1'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite2'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite2'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite2'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite3'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite3'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite3'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite4'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite4'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite4'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite5'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite5'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite5'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombrecomite6'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombrecomite6'); ?>
                            <?php echo $this->form->getInput('ret_mailcomite6'); ?>
                        </div>
                    </div>
                </fieldset>
            </div>


            <div>
                <fieldset>
                    <legend>Director de Innovación</legend>

                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombredirector1'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombredirector1'); ?>
                            <?php echo $this->form->getInput('ret_maildirector1'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombredirector2'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombredirector2'); ?>
                            <?php echo $this->form->getInput('ret_maildirector2'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_nombredirector3'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_nombredirector3'); ?>
                            <?php echo $this->form->getInput('ret_maildirector3'); ?>
                        </div>
                    </div>

                </fieldset>
            </div>

            <div>
                <fieldset>
                    <legend>Compañias y Puntos</legend>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_puntos'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('ret_puntos'); ?></div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com1'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com1'); ?>
                            <?php echo $this->form->getInput('ret_compuntos1'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com2'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com2'); ?>
                            <?php echo $this->form->getInput('ret_compuntos2'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com3'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com3'); ?>
                            <?php echo $this->form->getInput('ret_compuntos3'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com4'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com4'); ?>
                            <?php echo $this->form->getInput('ret_compuntos4'); ?>
                        </div>
                    </div>

                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com5'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com5'); ?>
                            <?php echo $this->form->getInput('ret_compuntos5'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com6'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com6'); ?>
                            <?php echo $this->form->getInput('ret_compuntos6'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com7'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com7'); ?>
                            <?php echo $this->form->getInput('ret_compuntos7'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"><?php echo $this->form->getLabel('ret_com8'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_com8'); ?>
                            <?php echo $this->form->getInput('ret_compuntos8'); ?>
                        </div>
                    </div>
                    <div class="control-form">
                        <div class="control-label"> <?php echo $this->form->getLabel('ret_puntosP'); ?></div>
                        <div class="controls">
                            <?php echo $this->form->getInput('ret_puntosP'); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>

    <div>
        <input type="hidden" name="task" value="reto.edit" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
<?php if($this->item->ret_id){
	if(count($evaluadores)>0){

?>
	<script type="text/javascript">
	    <?php if(isset($evaluadores[0]['cim_nombre'])){ ?>
            js("#jform_ret_nombrecomite1").val('<?php echo $evaluadores[0]['cim_nombre']; ?>');
			js("#jform_ret_mailcomite1").val('<?php echo $evaluadores[0]['cim_email']; ?>');
		<?php } ?>
		<?php if(isset($evaluadores[1]['cim_nombre'])){ ?>
			js("#jform_ret_nombrecomite2").val('<?php echo $evaluadores[1]['cim_nombre']; ?>');
			js("#jform_ret_mailcomite2").val('<?php echo $evaluadores[1]['cim_email']; ?>');
		<?php } ?>
		<?php if(isset($evaluadores[2]['cim_nombre'])){ ?>
			js("#jform_ret_nombrecomite3").val('<?php echo $evaluadores[2]['cim_nombre']; ?>');
			js("#jform_ret_mailcomite3").val('<?php echo $evaluadores[2]['cim_email']; ?>');
		<?php } ?>
                <?php if(isset($evaluadores[3]['cim_nombre'])){ ?>
                        js("#jform_ret_nombrecomite4").val('<?php echo $evaluadores[3]['cim_nombre']; ?>');
                        js("#jform_ret_mailcomite4").val('<?php echo $evaluadores[3]['cim_email']; ?>');
		<?php } ?>
        <?php if(isset($evaluadores[4]['cim_nombre'])){ ?>
			js("#jform_ret_nombrecomite5").val('<?php echo $evaluadores[4]['cim_nombre']; ?>');
			js("#jform_ret_mailcomite5").val('<?php echo $evaluadores[4]['cim_email']; ?>');
		<?php } ?>
        <?php if(isset($evaluadores[5]['cim_nombre'])){ ?>
            js("#jform_ret_nombrecomite6").val('<?php echo $evaluadores[5]['cim_nombre']; ?>');
            js("#jform_ret_mailcomite6").val('<?php echo $evaluadores[5]['cim_email']; ?>');
        <?php } ?>


        <?php if(isset($directores[0]['dir_nombre'])){ ?>
            js("#jform_ret_nombredirector1").val('<?php echo $directores[0]['dir_nombre']; ?>');
            js("#jform_ret_maildirector1").val('<?php echo $directores[0]['dir_email']; ?>');
        <?php } ?>
        <?php if(isset($directores[1]['dir_nombre'])){ ?>
           js("#jform_ret_nombredirector2").val('<?php echo $directores[1]['dir_nombre']; ?>');
           js("#jform_ret_maildirector2").val('<?php echo $directores[1]['dir_email']; ?>');
        <?php } ?>
        <?php if(isset($directores[2]['dir_nombre'])){ ?>
           js("#jform_ret_nombredirector3").val('<?php echo $directores[2]['dir_nombre']; ?>');
           js("#jform_ret_maildirector3").val('<?php echo $directores[2]['dir_email']; ?>');
        <?php } ?>


        <?php if(isset($companias[0]['neg_id'])){ ?>
            js("#jform_ret_com1").val('<?php echo $companias[0]['neg_id']; ?>');
            js("#jform_ret_compuntos1").val('<?php echo $companias[0]['rxc_puntos']; ?>');
        <?php } ?>

         <?php if(isset($companias[1]['neg_id'])){ ?>
            js("#jform_ret_com2").val('<?php echo $companias[1]['neg_id']; ?>');
            js("#jform_ret_compuntos2").val('<?php echo $companias[1]['rxc_puntos']; ?>');
        <?php } ?>

          <?php if(isset($companias[2]['neg_id'])){ ?>
            js("#jform_ret_com3").val('<?php echo $companias[2]['neg_id']; ?>');
            js("#jform_ret_compuntos3").val('<?php echo $companias[2]['rxc_puntos']; ?>');
        <?php } ?>

            <?php if(isset($companias[3]['neg_id'])){ ?>
            js("#jform_ret_com4").val('<?php echo $companias[3]['neg_id']; ?>');
            js("#jform_ret_compuntos4").val('<?php echo $companias[3]['rxc_puntos']; ?>');
        <?php } ?>

            <?php if(isset($companias[4]['neg_id'])){ ?>
            js("#jform_ret_com5").val('<?php echo $companias[4]['neg_id']; ?>');
            js("#jform_ret_compuntos5").val('<?php echo $companias[4]['rxc_puntos']; ?>');
        <?php } ?>

            <?php if(isset($companias[5]['neg_id'])){ ?>
            js("#jform_ret_com6").val('<?php echo $companias[5]['neg_id']; ?>');
            js("#jform_ret_compuntos6").val('<?php echo $companias[5]['rxc_puntos']; ?>');
        <?php } ?>

            <?php if(isset($companias[6]['neg_id'])){ ?>
            js("#jform_ret_com7").val('<?php echo $companias[6]['neg_id']; ?>');
            js("#jform_ret_compuntos7").val('<?php echo $companias[6]['rxc_puntos']; ?>');
        <?php } ?>

            <?php if(isset($companias[7]['neg_id'])){ ?>
            js("#jform_ret_com8").val('<?php echo $companias[7]['neg_id']; ?>');
            js("#jform_ret_compuntos8").val('<?php echo $companias[7]['rxc_puntos']; ?>');
        <?php } ?>
        sumaPuntos();
        </script>
<?php
	}
  }
?>
