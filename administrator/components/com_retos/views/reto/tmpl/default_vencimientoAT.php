<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
?>
<script type="text/javascript"  src='../_objects/scripts/jquery.js' ></script>
<script type="text/javascript">
 function validarForm(){
    form = document.forms.retosform;
    if($("#ntv_msjAutomatico").val() == "") {        
        alert("El campo mensaje es obligatorio"); 
        $("#ntv_msjAutomatico").focus();
        return false;  
    }  
    if($("#ntv_diasantes").val().length <= 0) {        
        alert("El campo Días antes del cierre es obligatorio");  
        $("#ntv_diasantes").focus();
        return false;  
    }  
    form.submit();
}   
</script>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="retosform" id="retosform">
      <h1><?php echo $this->reto->ret_nombre; ?></h1>	
      <h2>&#9658; Programar envio de vencimiento.</h2>
    
	<div class="form-horizontal">

        <div class="control-group">

            <div class="control-label">Mensaje</div>
            <div class="controls"><textarea rows="10" cols="70" name="ntv_msjAutomatico" id='ntv_msjAutomatico'><?php echo $this->notificacion->ntv_msjAutomatico;?></textarea></div>
        </div>

        <div class="control-group">
            <div class="control-label">Días antes del cierre</div>
            <div class="controls"><textarea rows="1" cols="2" name="ntv_diasantes" id='ntv_diasantes'><?php echo $this->notificacion->ntv_diasantes;?></textarea></div>
        </div>
    </div>

	<p align="center">
        <input type="button" value="Aceptar" class="button" src="" onclick="return validarForm();"/>
	    <input type="hidden" name="task" value="guardarMensajeVencimiento" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="idRet" value="<?php echo $this->reto->ret_id; ?>"/>
        </p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>