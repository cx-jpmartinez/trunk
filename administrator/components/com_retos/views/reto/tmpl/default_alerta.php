<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<script type="text/javascript"  src='../_objects/scripts/jquery.js' ></script>
<link rel="stylesheet" type="text/css" href="../_objects/css/redmond/jquery-ui-1.8.16.custom.css" />
<script src="../_objects/scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="../_objects/scripts/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('#ale_filtro1').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#ale_directores').val($("#ale_directores").get(0).value +',\n'+ arr[1]);
        }
      });
});
$(function(){
    $('#ale_filtro2').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#ale_director').val($("#ale_director").get(0).value +',\n'+ arr[1]);
        }
      });
});
$(function(){
    $('#ale_filtro3').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#ale_presidente').val($("#ale_presidente").get(0).value +',\n'+ arr[1]);
        }
      });
});
</script>




<?php
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
$idRet = JRequest::getVar('Item');
$reto = $this->reto;
$alerta = $this->alerta;
?>
<form action="<?php echo JRoute::_('index.php?option=com_retos'); ?>" method="post" name="adminForm" id="retos-form" enctype="multipart/form-data">
	<h2>Configuración de alerta (<?php echo $reto->ret_nombre; ?>) </h2>


    <div class="form-horizontal">
        <h3>Nivel # 1</h3>
        <div class="control-group">
            <div class="control-label">Buscar: </div>
            <div class="controls">
                <input type="text" size="60" value="" name="ale_filtro1" id="ale_filtro1">
            </div>
        </div>
        <div class="control-group">
            <div class="control-label">Lista: </div>
            <div class="controls">
                <textarea rows="3" cols="40" name="ale_directores" id="ale_directores"><?php echo $alerta->ale_directores; ?></textarea>
            </div>
        </div>
        <div class="control-group">
            <div class="control-label">Días:</div>
            <div class="controls">
                <input type="text" size="2" value="<?php echo $alerta->ale_diasdirectores; ?>" name="ale_diasdirectores" id="ale_diasdirectores">
            </div>
        </div>
    </div>


    <div class="form-horizontal">
        <h3>Nivel # 2</h3>
        <div class="control-group">
            <div class="control-label">Buscar :</div>
            <div class="controls"><input type="text" size="60" value="" name="ale_filtro2" id="ale_filtro2"></div>
        </div>
        <div class="control-group">
            <div class="control-label">Lista :</div>
            <div class="controls"><textarea rows="3" cols="40" name="ale_directores" id="ale_directores"><?php echo $alerta->ale_directores; ?></textarea></div>
        </div>
        <div class="control-group">
            <div class="control-label">Días:</div>
            <div class="controls"><input type="text" size="2" value="<?php echo $alerta->ale_diasdirector; ?>" name="ale_diasdirector" id="ale_diasdirector"></div>
        </div>
    </div>

    <div class="form-horizontal">
        <h3>Nivel # 3</h3>
        <div class="control-group">
            <div class="control-label">Buscar:</div>
            <div class="controls"><input type="text" size="60" value="" name="ale_filtro3" id="ale_filtro3"></div>
        </div>
        <div class="control-group">
            <div class="control-label">Lista :</div>
            <div class="controls">
                <textarea rows="3" cols="40" name="ale_presidente" id="ale_presidente"><?php echo $alerta->ale_presidente; ?></textarea>
            </div>
        </div>
        <div class="control-group">
            <div class="control-label">Días:</div>
            <div class="controls"><input type="text" size="2" value="<?php echo $alerta->ale_diaspresidente; ?>" name="ale_diaspresidente" id="ale_diaspresidente"></div>
        </div>
    </div>

	<p align="center">
	    <input type="submit" value="Aceptar" class="button" src=""/>
	    <input type="hidden" name="task" value="guardarAlerta" />
	    <input type="hidden" name="option" value="com_retos" />
	    <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>"/>
	</p>
    <div>
        <?php echo JHtml::_('form.token'); ?>
    </div>
    </div>
</form>