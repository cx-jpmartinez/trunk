<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class RetosViewReto extends JViewLegacy
{
	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null)
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;

		$this->item = $item;

		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		$task = JRequest::getVar('task');
		if(
                        $task!='terminos'
                        && $task!='traduccion'
                        && $task!='notificarExperto'
                        && $task!='anexos'
                        && $task!='alerta'
                        && $task!='participantes'
                        && $task!='soluciones'
                        && $task!='configurarVencimientoMN'
                        && $task!='configurarVencimientoAT'
                        && $task!='notificarVencimientoSeleccion'
        )
		{
			JRequest::setVar('hidemainmenu', true);
			$isNew = ($this->item->ret_id == 0);
			JToolBarHelper::title($isNew ? JText::_('COM_RETOS_ADMIN_RETOS') : JText::_('COM_RETOS_ADMIN_RETOS'),  'thumbs-up');

			JToolBarHelper::save('reto.save');
			JToolBarHelper::cancel('reto.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
		}

	}
}
