<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');


class PreguntaControllerPregunta extends JControllerForm
{
	
   /**
     * Overrides parent save method to check the submitted passwords match.
     *
     * @return  mixed   Boolean or JError.
     * @since   1.6
     */
    public function save($key = null, $urlVar = null)
    {
        return parent::save();
    }
}
