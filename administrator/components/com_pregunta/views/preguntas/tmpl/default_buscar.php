<?php 
$filter_estado = JRequest::getVar( 'filter_estado' );
$filter_reto = JRequest::getVar( 'filter_reto' );
?>

<form action="<?php echo JRoute::_('index.php?option=com_pregunta');?>" method="post" name="adminForm" id="adminForm1">
    <fieldset id="filter-bar">
        <div class="filter-search fltlft">
            <label class="filter-search-lbl" for="filter_search"><?php echo JText::_('Filtrar Por: '); ?></label>

                    <select name="filter_reto" class="inputbox" onchange="this.form.submit()">
                                    <option value=""><?php echo JText::_('Seleccione Reto - ');?></option>
                                    <?php echo cargarListaRetos($filter_reto); ?>
                            </select>
                    <select name="filter_estado" class="inputbox" onchange="this.form.submit()">
                                    <option value=""><?php echo JText::_('Seleccione Estado -');?></option>
                                    <?php echo cargarListaEstadoRetos($filter_estado); ?>
                            </select>

                    <!-- select name="filter_negocio" class="inputbox" onchange="this.form.submit()">
                                    <option value=""><?php echo JText::_('Selecciones Negocio - ');?></option>
                                    <?php //echo cargarListaNegocios($filter_negocio); ?>
                            </select-->
                    <a href="index.php?option=com_pregunta">
                            <button type="button"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
                    </a>
            
</div>
                       </fieldset>
	<div class="clr"> </div>
        <br><br>
</form>
