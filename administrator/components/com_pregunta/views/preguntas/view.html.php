<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');


class PreguntaViewPreguntas extends JViewLegacy
{

	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$db 						= JFactory::getDbo();
		$filter_estado 	= JFactory::getApplication()->input->get( 'filter_estado' );
    $filter_reto 		= JFactory::getApplication()->input->get( 'filter_reto' );
    $where = '';
		if($filter_estado){
			$where = " WHERE ret_estado = '$filter_estado'";
		}
		if($filter_reto){
			$where = " WHERE ret_id = '$filter_reto'";
		}
		$query = "
            SELECT
                ret_nombre,
                pre_id,
                pre_titulo,
                pre_descipcion,
                pre_respuesta,
                pre_tipo,
                pre_ret_id,
                pre_usu_id,
                pre_fechaCreacion,
                pre_fechaEstado,
                pre_estado,
                exp_nombre,
                usu_nombre
            FROM pregunta
            inner join reto on (ret_id=pre_ret_id)
            inner join experto on (exp_id=ret_exp_id)
            inner join usuario on (usu_id=pre_usu_id)
            $where
            order by ret_id
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
    $pagination = $this->get('Pagination');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{

		JToolBarHelper::title(JText::_('TIT_LISTA_PREGUNTAS'));


		$canDo = PreguntaHelpersPregunta::getActions();

		JToolBarHelper::title(JText::_('COM_PREGUNTA_TITLE_PREGUNTAS'), 'question-circle');
		//JToolBarHelper::ExportarPregunta('Exportar Excel','http://www.solucionesinnovadoras.com.co/administrator/index.php?option=com_reportes&view=seleccionarReto&idrep=3&tipo=excel');
		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/preguntas';

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_pregunta');
		}




	}
}
