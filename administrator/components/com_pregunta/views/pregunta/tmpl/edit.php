<?php
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

	});

	Joomla.submitbutton = function (task) {
		if (task == 'pregunta.cancel')
    {
			Joomla.submitform(task, document.getElementById('pregunta-form'));
		}
		else
		{

					if (task != 'pregunta.cancel' && document.formvalidator.isValid(document.id('pregunta-form')))
		      {
							Joomla.submitform(task, document.getElementById('pregunta-form'));
					}
					else
					{
								alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					}
		}
	}

</script>
<form action="<?php echo JRoute::_('index.php?option=com_pregunta&layout=edit&pre_id='.(int) $this->item->pre_id); ?>" method="post" name="pregunta-form" id="pregunta-form">


    <div class="form-horizontal">
        <legend><?php echo JText::_( 'Pregunta' ); ?></legend>
        <div class="control-group">
            <?php foreach($this->form->getFieldset() as $field): ?>
                <div class="control-label"><?php echo $field->label;?></div>
                <div class="controls"><?php echo $field->input; ?></div>
            <?php endforeach; ?>
        </div>
        <div class="control-group">
            <input type="hidden" name="task" value="compania.edit" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </div>

</form>
