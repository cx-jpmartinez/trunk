<?php


// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla table library
jimport('joomla.database.table');

class PreguntaTablePregunta extends JTable
{
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db)
	{
		JObserverMapper::addObserverClassToClass('JTableObserverContenthistory', 'PreguntaTablePregunta', array('typeAlias' => 'com_pregunta.pregunta'));
		parent::__construct('pregunta', 'pre_id', $db->retos);
	}
}
