<?php
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

	});

	Joomla.submitbutton = function (task) {
		if (task == 'foro.cancel')
    {
			Joomla.submitform(task, document.getElementById('form-foro'));
		}
		else
		{

					if (task != 'foro.cancel' && document.formvalidator.isValid(document.id('form-foro')))
		      {
							Joomla.submitform(task, document.getElementById('form-foro'));
					}
					else
					{
								alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					}
		}
	}

</script>

<form action="<?php echo JRoute::_('index.php?option=com_foro&layout=edit&for_id='.(int) $this->item->for_id); ?>"
	method ="post"
	name	 ="adminForm"
	id		 ="form-foro"
>

    <div class="form-horizontal">
        <div class="span12">
            <legend><?php echo JText::_( 'COM_FORO_LEGEND_FORO' ); ?></legend>
            <?php foreach($this->form->getFieldset() as $field): ?>
                <div class="control-group">
                    <div class="control-label">
                        <?php echo $field->label; ?>
                    </div>
                    <div class ="controls">
                        <?php echo $field->input; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div>
            <input type="hidden" name="task" value="foro.edit" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </div>

</form>
