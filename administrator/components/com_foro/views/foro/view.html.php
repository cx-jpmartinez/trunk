<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HelloWorld View
 */
class ForoViewForo extends JViewLegacy
{

	protected $state;

	protected $item;

	protected $form;

	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null)
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;

		$this->item = $item;
        //echo '<pre>' . print_r($this,true) . '</pre>'; exit;
		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	 protected function addToolbar()
 	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$isNew = ($this->item->for_id == 0);
		JToolBarHelper::title(JText::_('COM_FORO_TITLE_FORO'), 'book');

		JToolBarHelper::save('foro.save');
		JToolBarHelper::cancel('foro.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');

 	}

}
