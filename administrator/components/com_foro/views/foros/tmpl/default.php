<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('bootstrap.tooltip');
?>
<script type="text/javascript">
function confirBorrado(id)
{
    form = document.forms.adminForm;
    if(confirm('Estas seguro de borrar este foro'))
    {
      document.getElementById('task').value  ='eliminarForo';
      document.getElementById('idFor').value =id;
      form.submit();
      return true;
    }else
    {
      return false;
    }

}

</script>

<form action="<?php echo JRoute::_('index.php?option=com_foro&view=foros'); ?>" method="post" id="adminForm" name="adminForm">
	<table id="adminlist" class="table table-striped">
		<thead><?php echo $this->loadTemplate('head');?></thead>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
	</table>
	<div>
		<input type="hidden" id="task" name="task" value="" />
    <input type="hidden" id="idFor" name="idFor" value="" />
		<input type="hidden" name="boxchecked" value="0" />

		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
