<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo $item->for_id; ?>
		</td>
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->for_id); ?>
		</td>
		<td class="has-context">
			<a href="<?php echo JRoute::_('index.php?option=com_foro&task=foro.edit&for_id=' . $item->for_id); ?>">
				<?php echo $item->for_tema; ?>
			</a>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->for_fecha; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->for_estado; ?>
		</td>
                <td class="center">
                    <a class="btn btn-small hasTooltip" data-original-title="Eliminar" onclick="confirBorrado(<?php echo $item->for_id; ?>);">
                        <span class="icon-unpublish"></span><span>Eliminar</span>
                    </a>
                </td>
		
	</tr>
<?php endforeach; ?>

