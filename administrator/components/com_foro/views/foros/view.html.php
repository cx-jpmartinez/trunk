<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Foro View
 */
class ForoViewForos extends JViewLegacy
{

	protected $items;

	protected $pagination;

	protected $state;


	function display($tpl = null)
	{

		$this->state = $this->get('State');

		$this->items = $this->get('Items');

		$this->pagination = $this->get('Pagination');

		$db 		= JFactory::getDbo();
		$query 	= "SELECT for_id,  for_tema,  for_fecha,  for_estado,  for_ret_id FROM foro";

		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}


		$this->items = $items;
		$this->pagination = $pagination;

		$this->addToolbar();

		parent::display($tpl);
	}

	/**
	* Setting the toolbar
	*/
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('TIT_LISTA_AREAS_INTERES'));
		$canDo = ForoHelpersForo::getActions();

		JToolBarHelper::title(JText::_('COM_FORO_TITLE_FOROS'), 'book');
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/foro';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('foro.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('foro.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_foro');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_foro&view=foros');

		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.are_nombre'), true)

		);

	}
}
