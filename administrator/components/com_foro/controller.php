<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of Foro component
 */
class ForoController extends JControllerLegacy
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false, $urlparams = null)
	{
		$task 		= JFactory::getApplication()->input->get('task');

		if($task=='eliminarForo')
		{
			$idFor 	= JFactory::getApplication()->input->get('idFor');
			$foro 	= new foro($idFor);


			try {

				if ($foro->delete())
				{
					JFactory::getApplication()->enqueueMessage("El foro $foro->for_tema ha sido borrado");
				} else
				{
					JError::raiseWarning(null, "No fue posible borrar el foro $foro->for_tema");
				}

			} catch (Exception $e)
			{
				JError::raiseWarning(null, "No fue posible borrar el foro $foro->for_tema");
			}

		}

		JRequest::setVar('view', JRequest::getCmd('view', 'foros'));
        parent::display($cachable, $urlparams);
	}
}
