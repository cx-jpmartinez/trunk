<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla table library
jimport('joomla.database.table');

/**
 * Hello Table class
 */
class ForoTableForo extends JTable
{


	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db)
	{

		JObserverMapper::addObserverClassToClass('JTableObserverContenthistory', 'ForoTableforo', array('typeAlias' => 'com_foro.foro'));
		parent::__construct('foro', 'for_id', $db->retos);
	}
}
