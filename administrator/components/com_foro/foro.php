<?php
// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_foro'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Foro', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Foro');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
