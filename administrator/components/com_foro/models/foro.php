<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * HelloWorld Model
 */
class ForoModelForo extends JModelAdmin
{



	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_FORO';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_foro.foro';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Foro', $prefix = 'ForoTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}


	 public function getForm($data = array(), $loadData = true)
 	{
 		// Initialise variables.
 		$app = JFactory::getApplication();

 		$form = $this->loadForm(
 			'com_foro.foro', 'foro',
 			array('control' => 'jform',
 				'load_data' => $loadData
 			)
 		);

 		if (empty($form))
 		{
 			return false;
 		}

 		return $form;
 	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_foro.edit.foro.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}

	public function save($data)
  {
    	$table     = $this->getTable();

    	if( !$data['for_id'] ){
            $data['for_fecha'] = date( 'Y-m-d H:i:s' );
    	}
            // Bind the data.
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }
        // Store the data.
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }
    	return true;
  }

	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->retos->setQuery('SELECT MAX(ordering) FROM retos.foro');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}


}
