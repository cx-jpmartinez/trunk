<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Areainteres controller class.
 *
 * @since  1.6
 */
class AreainteresControllerAreainteres extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'areaintereses';
		parent::__construct();
	}
}
