<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Areainteres records.
 *
 * @since  1.6
 */
class AreainteresModelAreaintereses extends JModelList
{

    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'ma_id', 'a.`ma_id`',
                'ma_texto', 'a.`ma_texto`',
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param   string  $ordering   Elements order
     * @param   string  $direction  Order direction
     *
     * @return void
     *
     * @throws Exception
     */
    protected function populateState($ordering = null, $direction = null)
    {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.ma_texto', 'filter_published', '', 'string');
        $this->setState('filter.ma_texto', $published);

        // Load the parameters.
        $params = JComponentHelper::getParams('com_areainteres');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('ma_id', 'asc');
    }


	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.ma_texto');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{


		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// Select some fields
		$query->select('ma_id, ma_texto');

		// From the hello table
		$query->from('#__maestro');


        // Filter by published state
        $published = $this->getState('filter.ma_texto');

        if (is_numeric($published))
        {
            $query->where('ma_texto = ' . (int) $published);
        }
        elseif ($published === '')
        {
            $query->where('(ma_texto IN (0, 1))');
        }

        // Filter by search in title
        $search = $this->getState('filter.search');



        if (!empty($search))
        {
            if (stripos($search, 'id:') === 0)
            {
                $query->where('ma_id = ' . (int) substr($search, 3));
            }
            else
            {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');

            }
        }

        // Add the list ordering clause.
        $orderCol  = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');

        if ($orderCol && $orderDirn)
        {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }



		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}
}
