<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Areainteres.
 *
 * @since  1.6
 */
class AreainteresViewAreaintereses extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{

        $this->state = $this->get('State');

        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');


        $db = JFactory::getDbo();

		$query = "
            SELECT
				are_id
				, are_nombre
				, are_descripcion
				, are_estado
				, are_fecha
            FROM areainteres
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		//AreainteresHelpersAreainteres::addSubmenu('areaintereses');

		$this->addToolbar();

//		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{

		JToolBarHelper::title(JText::_('TIT_LISTA_AREAS_INTERES'));


		$canDo = AreainteresHelpersAreainteres::getActions();

		JToolBarHelper::title(JText::_('COM_AREAINTERES_TITLE_AREAINTERESES'), 'circle');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/areainteres';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('areainteres.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('areainteres.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_areainteres');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_areainteres&view=areaintereses');

		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',
            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.are_nombre'), true)

		);


	}

	/**
	 * Method to order fields
	 *
	 * @return void
	 */
	/**protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}**/
}
