<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<th class="nowrap center" width="5">
		<?php echo JText::_('LAB_ID'); ?>
	</th>
	<th class="nowrap center" width="20">
		<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
	</th>
	<th class="nowrap center">
		<?php echo JText::_('LAB_NOMBRE_AREA_INTERES'); ?>
	</th>
	<th class="nowrap center">
		<?php echo JText::_('LAB_DESCRIPCION'); ?>
	</th>
	<th class="nowrap hidden-phone">
		<?php echo JText::_('LAB_ESTADO'); ?>
	</th>
	<th class="nowrap hidden-phone">
		<?php echo JText::_('LAB_FECHA_CREACION'); ?>
	</th>
</tr>
