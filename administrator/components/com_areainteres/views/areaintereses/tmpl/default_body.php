<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo $item->are_id; ?>
		</td>
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->are_id); ?>
		</td>
		<td class="has-context" >
			<a href="<?php echo JRoute::_('index.php?option==com_areainteres&task=areainteres.edit&are_id=' . $item->are_id); ?>">
				<?php echo $item->are_nombre; ?>
			</a>
		</td>
		<td class="has-context">
            <?php echo $item->are_descripcion; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->are_estado; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->are_fecha; ?>
		</td>
	</tr>
<?php endforeach; ?>

