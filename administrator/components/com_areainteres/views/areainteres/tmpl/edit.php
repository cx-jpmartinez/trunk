<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
//$document = JFactory::getDocument();
//$document->addStyleSheet(JUri::root() . 'media/com_areainteres/css/form.css');
?>
<script type="text/javascript">


	Joomla.submitbutton = function (task) {
		if (task == 'areainteres.cancel')
        {
			Joomla.submitform(task, document.getElementById('areainteres-form'));
		}
		else {
			
			if (task != 'areainteres.cancel' && document.formvalidator.isValid(document.id('areainteres-form')))
            {
				
				Joomla.submitform(task, document.getElementById('areainteres-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_areainteres&layout=edit&are_id='.(int) $this->item->are_id); ?>"
      method="post"
      name="'areainteres-form"
      id="areainteres-form"
      class = "form-validate"
    >
    <div class="form-horizontal">
        <div class="span12">
            <legend><?php echo JText::_( 'LAB_DATOS_AREA_INTERES' ); ?></legend>

            <?php foreach($this->form->getFieldset() as $field): ?>
                <div class="control-group">
                    <div class="control-label col-xs-2"><?php echo $field->label; ?></div>
                    <div class="controls"><?php echo $field->input; ?></div>
                </div>
            <?php endforeach; ?>

            <div class="control-group">
                <div class="controls">
                    <input type="hidden" name="task" value="areainteres.edit" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </div>
        </div>
    </div>

</form>
