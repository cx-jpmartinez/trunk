<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class AreainteresViewAreainteres extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            throw new Exception(implode("\n", $errors));
        }

		// Assign the Data
		$this->form = $form;

		$this->item = $item;
        //echo '<pre>' . print_r($this->item) . '</pre>'; exit;
		// Set the toolbar
		$this->addToolBar();
        // Display the template
        parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function addToolbar()
	{

        JFactory::getApplication()->input->set('hidemainmenu', true);

		$isNew = ($this->item->are_id == 0);
        JToolBarHelper::title(JText::_('COM_AREAINTERES_TITLE_AREAINTERES'), 'areainteres.png');

		JToolBarHelper::save('areainteres.save');
		JToolBarHelper::cancel('areainteres.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');

	}
}
