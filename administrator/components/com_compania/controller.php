<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class CompaniaController extends JControllerLegacy
{

	 /**
 	 * Method to display a view.
 	 *
 	 * @param   boolean  $cachable   If true, the view output will be cached
 	 * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
 	 *
 	 * @return   JController This object to support chaining.
 	 *
 	 * @since    1.5
 	 */
 	public function display($cachable = false, $urlparams = false)
 	{
 		$view = JFactory::getApplication()->input->getCmd('view', 'companias');
 		JFactory::getApplication()->input->set('view', $view);

        $task = JFactory::getApplication()->input->get('task');

        if($task=='eliminarCom')
        {
            $idcom      = JFactory::getApplication()->input->get('idcom');

            if( !empty($idcom) )
            {
                $compania   = new compania($idcom);

                if($compania->delete())
                {
                    JFactory::getApplication()->enqueueMessage("La compañia $compania->neg_nombre ha sido borrada");
                }else
                {

                    JError::raiseError(null, "No fue posible borrar la compañia $compania->neg_nombre");
                }
            }



        }


 		parent::display($cachable, $urlparams);

 		return $this;
 	}


}
