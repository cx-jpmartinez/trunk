<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Compania
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

?>
<script type="text/javascript">
	js = jQuery.noConflict();

	Joomla.submitbutton = function (task) {
		if (task == 'compania.cancel')
        {
			Joomla.submitform(task, document.getElementById('compania-form'));
		}
		else {

			if (task != 'compania.cancel' && document.formvalidator.isValid(document.id('compania-form')))
            {

				Joomla.submitform(task, document.getElementById('compania-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_compania&layout=edit&neg_id='.(int) $this->item->neg_id); ?>"
	method="post"
	name="adminForm"
	id="compania-form"
>
    <div class="form-horizontal">
        <class class="span12">

            <legend><?php echo JText::_( 'COM_COMPANIA_LEGEND_COMPANIA' ); ?></legend>

            <?php foreach($this->form->getFieldset() as $field): ?>
                <div class="control-group">
                    <div class="control-label"><?php echo $field->label;?></div>
                    <div class="controls"><?php echo $field->input; ?></div>
                </div>
            <?php endforeach; ?>

            <div class="control-group">
                <input type="hidden" name="task" value="compania.edit" />
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </class>
    </div>



</form>
