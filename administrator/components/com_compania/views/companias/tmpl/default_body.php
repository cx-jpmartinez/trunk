<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo $item->neg_id; ?>
		</td>
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->neg_id); ?>
		</td>
		<td class="has-context">
			<a href="<?php echo JRoute::_('index.php?option=com_compania&task=compania.edit&neg_id=' . $item->neg_id); ?>">
				<?php echo $item->neg_nombre; ?>
			</a>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->neg_descripcion; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->neg_estado; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->neg_fecha; ?>
		</td class="center">
                 <td>
                     <a class="btn btn-small hasTooltip" data-original-title="Eliminar" onclick="confirBorrado(<?php echo $item->neg_id; ?>);">
                         <span class="icon-unpublish"></span><span>Eliminar</span>
                     </a>
                </td>
	</tr>
<?php endforeach; ?>
