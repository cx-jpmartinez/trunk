<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('bootstrap.tooltip');
?>
<script type="text/javascript">
function confirBorrado(id){
        form = document.forms.adminForm;
        if(confirm('Estas seguro de borrar esta compañia')){
            document.getElementById('task').value='eliminarCom';
            document.getElementById('idcom').value=id;
            form.submit();
            return true;
	}else{
		return false;
	}

}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_compania&view=companias'); ?>" method="post" id="adminForm" name="adminForm">
	<table id="adminlist" class="table table-striped">
		<thead><?php echo $this->loadTemplate('head');?></thead>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
	</table>
	<div>
		<input type="hidden" id="task" name="task" value="" />
        <input type="hidden" id="idcom" name="idcom" value="" />



		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
