<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_compania
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');


class CompaniaViewCompanias extends JViewLegacy
{

	function display($tpl = null)
	{
		$this->state = $this->get('State');

		$this->items = $this->get('Items');

		$this->pagination = $this->get('Pagination');

		$db = JFactory::getDbo();

		$query = "
            SELECT
                neg_id, neg_nombre, neg_descripcion, neg_estado, neg_fecha
            FROM negocio
		";

		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_COMPANIA'));


		$canDo = CompaniaHelpersCompania::getActions();

		JToolBarHelper::title(JText::_('COM_COMPANIA_ADMIN_COMPANIA'), 'briefcase');

		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/compania';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('compania.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('compania.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_compania');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_compania&view=companias');

        $this->extra_sidebar = '';
        JHtmlSidebar::addFilter(

            JText::_('JOPTION_SELECT_PUBLISHED'),

            'filter_published',
            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.neg_nombre'), true)

        );

	}
}
