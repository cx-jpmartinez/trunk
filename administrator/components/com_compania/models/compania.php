<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_compania
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

class CompaniaModelCompania extends JModelAdmin
{


	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_COMPANIA';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_compania.compania';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;



	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Compania', $prefix = 'CompaniaTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_compania.compania', 'compania',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_compania.edit.compania.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}

	public function save($data)
  {
    	$table     = $this->getTable();

    	if( !$data['neg_id'] ){
            $data['neg_fecha'] = date( 'Y-m-d H:i:s' );
    	}
            // Bind the data.
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }
        // Store the data.
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }
    	return true;
    }

		/**
		 * Prepare and sanitise the table prior to saving.
		 *
		 * @param   JTable  $table  Table Object
		 *
		 * @return void
		 *
		 * @since    1.6
		 */
		protected function prepareTable($table)
		{
			jimport('joomla.filter.output');

			if (empty($table->id))
			{

				if (@$table->ordering === '')
				{
					$db = JFactory::getDbo();
					$db->retos->setQuery('SELECT MAX(ordering) FROM retos.negocio');
					$max             = $db->loadResult();
					$table->ordering = $max + 1;
				}

			}

		}

}
