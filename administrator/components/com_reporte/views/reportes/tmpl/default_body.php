<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
    <tr class="row<?php echo $i % 2; ?>">
        <td class="has-context">
            <?php echo $item->rep_nombre; ?>
        </td>
        <td class="center">
            <?php if($item->rep_excel && $item->rep_orden<>8){ ?>
            <a href="index.php?option=com_reporte&view=seleccionarReto&idrep=<?php echo $item->rep_id; ?>&tipo=excel" target="_blank">
                <img alt="Archivo Excel" title="Visualizar" src="templates/bluestork/images/header/icon-48-stats.png">
            </a>
            <?php }elseif($item->rep_orden==8){ ?>
            <a href="index.php?option=com_reporte&view=seleccionarRetoNegocio&idrep=<?php echo $item->rep_id; ?>&tipo=excel" target="_blank"">
                <img alt="Archivo Excel" title="Visualizar" src="templates/bluestork/images/header/icon-48-stats.png">
            </a>
            <?php } ?>
        </td>    
        <td class="center">
            <?php if($item->rep_grafica && $item->rep_orden<>8){ ?>
            <a href="index.php?option=com_reporte&view=seleccionarReto&idrep=<?php echo $item->rep_id; ?>&tipo=grafica" target="_blank">
                <img alt="Gráfica" title="Visualizar" src="templates/bluestork/images/header/icon-48-levels.png">
            </a>
            <?php }elseif($item->rep_orden==8){ ?>
            <a href="index.php?option=com_reporte&view=seleccionarRetoNegocio&idrep=<?php echo $item->rep_id; ?>&tipo=grafica" target="_blank">
                <img alt="Gráfica" title="Visualizar" src="templates/bluestork/images/header/icon-48-levels.png">
            </a>
            <?php } ?>
        </td>
    </tr>
<?php endforeach; ?>

