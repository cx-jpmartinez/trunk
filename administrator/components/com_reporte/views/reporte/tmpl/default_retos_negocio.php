<script type="text/javascript">
function checkAll(a,b){b||(b="cb");if(a.form){for(var c=0,d=0,f=a.form.elements.length;d<f;d++){var e=a.form.elements[d];if(e.type==a.type&&(b&&e.id.indexOf(b)==0||!b))e.checked=a.checked,c+=e.checked==!0?1:0}if(a.form.boxchecked)a.form.boxchecked.value=c;return!0}else{for(var e=document.adminForm,c=e.toggle.checked,f=a,g=0,d=0;d<f;d++){var h=e[b+""+d];if(h)h.checked=c,g++}document.adminForm.boxchecked.value=c?g:0}}
</script>        
<form action="<?php echo JRoute::_('index.php?option=com_reporte'); ?>" method="post" name="adminForm">
    <table style="text-align: center;">
         <thead>
            <tr class="row<?php echo $i % 2; ?>">
                <input type="submit" value="ACEPTAR" class="button" style="font-size: 20px;color:#0055BB;text;font-weight: extra-light;" src=""/>
            </tr>
        </thead>
    </table>	
    <br><br>
    
    
    <table width="48%" align="left">
        <thead>
            <tr>
                <th width="2"><?php echo JText::_('#'); ?></th>
                <th width="3"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->retos); ?>);" /></th>
                <th><?php echo JText::_('RETO'); ?></th>			
                <th><?php echo JText::_('ESTADO'); ?></th>			
            </tr>
        </thead>
        <tbody>
            <?php foreach($this->retos as $i => $item){ ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td><?php echo $item->ret_id; ?></td>
                    <td><?php echo JHtml::_('grid.id', $i, $item->ret_id); ?></td>
                    <td><?php echo $item->ret_nombre; ?></td>
                    <td><?php echo $item->ret_estado; ?></td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
    
    <table width="48%" align="right">
            <thead>
                <tr>
                    <th width="3"></th>
                    <th><?php echo JText::_('NEGOCIO DEL PROPONENTE'); ?></th>			
                </tr>
            </thead>
            <tbody>
                <?php foreach($this->negocios as $j => $itemneg){ ?>
                    <tr class="row<?php echo $j % 2; ?>">
                       <td>
                           <input type="checkbox" name="neg[]" value="<?php echo $itemneg->com_descripcion; ?>" id="<?php echo 'c'.$j; ?>" selected="true"/>
                       </td>
                        <td><?php echo $itemneg->com_nombre; ?></td>
                    </tr>
                <?php } ?>
                    
            </tbody>
	</table>
	<div>
                <input type="hidden" id="task" name="task" value="" />
                <input type="hidden" id="task" name="view" value="graficabarra" />
                <input type="hidden" id="task" name="tipo" value="<?php echo $this->tipo; ?>" />
                <input type="hidden" id="idRet" name="idrep" value="<?php echo $this->idrep;?>" />
                <input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>