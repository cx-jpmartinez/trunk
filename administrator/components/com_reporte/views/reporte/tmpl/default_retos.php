<script type="text/javascript">
function checkAll(a,b){b||(b="cb");if(a.form){for(var c=0,d=0,f=a.form.elements.length;d<f;d++){var e=a.form.elements[d];if(e.type==a.type&&(b&&e.id.indexOf(b)==0||!b))e.checked=a.checked,c+=e.checked==!0?1:0}if(a.form.boxchecked)a.form.boxchecked.value=c;return!0}else{for(var e=document.adminForm,c=e.toggle.checked,f=a,g=0,d=0;d<f;d++){var h=e[b+""+d];if(h)h.checked=c,g++}document.adminForm.boxchecked.value=c?g:0}}
</script>       
<h3 style="text-align: center;color:#146295;"><?php echo $this->reporte->rep_nombre; ?></h3>
<form action="<?php echo JRoute::_('index.php?option=com_reporte'); ?>" method="post" name="adminForm">
	<table class="adminlist">
            <thead>
                <tr class="row">
                    <th colspan="6"><input type="submit" value="ACEPTAR" class="button" style="font-size: 20px;color:#0055BB;text;font-weight: extra-light;" src=""/></th>
                </tr>
                <tr>
                    <th width="2" style="color:#146295;"><?php echo JText::_('#'); ?></th>
                    <th width="3" style="color:#146295;"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->retos); ?>);" /></th>
                    <th style="color:#146295;"><?php echo JText::_('RETO'); ?></th>			
                    <th style="color:#146295;"><?php echo JText::_('FECHA LANZAMIENTO'); ?></th>			
                    <th style="color:#146295;"><?php echo JText::_('FECHA CIERRE'); ?></th>			
                    <th style="color:#146295;"><?php echo JText::_('ESTADO'); ?></th>			
                </tr>
            </thead>
            <tbody>
                <?php foreach($this->retos as $i => $item){ ?>
                    <tr class="row">
                        <td><?php echo $item->ret_id; ?></td>
                        <td><?php echo JHtml::_('grid.id', $i, $item->ret_id); ?></td>
                        <td><?php echo $item->ret_nombre; ?></td>
                        <td><?php echo $item->ret_fechaPublicacion; ?></td>
                        <td><?php echo $item->ret_fechaVigencia; ?></td>
                        <td><?php echo $item->ret_estado; ?></td>
                    </tr>
                <?php } ?>
                    
            </tbody>
	</table>
	<div>
                <input type="hidden" id="task" name="task" value="" />
                <input type="hidden" id="task" name="view" value="<?php echo $this->tipo;?>" />
                <input type="hidden" id="task" name="tipo" value="<?php echo $this->tipo;?>" />
                <input type="hidden" id="idRet" name="idrep" value="<?php echo $this->idrep;?>" />
                <input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>