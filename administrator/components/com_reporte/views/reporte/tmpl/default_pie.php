<?php 
$datos = $this->result;
$reporte = $this->reporte;
$filas = count($datos);
$eje='';
$i=1;
foreach ($datos as $dato){
    if($i<$filas){
        $eje.="['Reto $dato->negocio', $dato->cantidad ".'],';
    }else{
        $eje.="['Reto $dato->negocio', {$dato->cantidad}]";
       
    }
   $i++;
}
?>

<script type="text/javascript" src="../_objects/scripts/jsgrafica/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: '<?php echo $reporte->rep_nombre; ?> <?php echo $this->compania; ?>',
                style: {
                        fontSize: '20px',
                        fontFamily: 'Verdana, sans-serif',
                        color:'#0B1907',
                        weight:'bold'
                    }
            },
            tooltip: {
        	//pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>, '+
                            Highcharts.numberFormat(this.y, 0);
                        }
                    }
                    
                    
                }
            },
            series: [{
                type: 'pie',
                name: '# Partcipantes',
                data: [
                    <?php echo $eje; ?>
                ]
            }]
        });
    });
    
});
		</script>
	</head>
	<body>
<script src="../_objects/scripts/jsgrafica/highcharts/highcharts.js"></script>
<script src="../_objects/scripts/jsgrafica/highcharts/exporting.js"></script>
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>


