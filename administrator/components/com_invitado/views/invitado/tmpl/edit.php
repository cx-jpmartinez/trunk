<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');


?>
<script type="text/javascript">
//JOMMLA BUTTONS
Joomla.submitbutton = function (task)
{
    if (task == 'invitado.cancel')
    {
      Joomla.submitform(task, document.getElementById('invitado-form'));
    }
    else
    {
      if (task != 'invitado.cancel' && document.formvalidator.isValid(document.id('invitado-form')))
      {
        Joomla.submitform(task, document.getElementById('invitado-form'));
      }
      else
      {
        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
      }
    }
  }

</script>

<form action="<?php echo JRoute::_('index.php?option=com_invitado&layout=edit&usl_id='.(int) $this->item->usl_id); ?>" method="post" name="adminForm" class="form-validate" id="invitado-form">
    <div class="form-horizontal">
        <h3><?php echo JText::_( 'Invitado' ); ?></h3>
        <?php foreach($this->form->getFieldset() as $field): ?>
        <div class="control-group">
            <div class="control-label"> <?php echo $field->label ?></div>
            <div class="controls"><?php echo $field->input ?></div>
        </div>
        <?php endforeach; ?>
    </div>
	<div>
            <input type="hidden" name="task" value="invitado.edit" />
            <?php echo JHtml::_('form.token'); ?>
	</div>
</form>
