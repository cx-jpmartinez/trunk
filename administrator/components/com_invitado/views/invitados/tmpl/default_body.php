<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="small hidden-phone">
			<?php echo JHtml::_('grid.id', $i, $item->usl_id); ?>
		</td>
		<td class="has-context">
            <a href="<?php echo JRoute::_('index.php?option=com_invitado&task=invitado.edit&usl_id=' . $item->usl_id); ?>">
                <?php echo $item->usl_nombre; ?>
            </a>
        </td>
		<td class="small">
            <?php echo $item->usl_email; ?>
		</td>
	</tr>
<?php endforeach; ?>

