<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class InvitadoViewInvitados extends JViewLegacy
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$db = JFactory::getDbo();
		$query = "SELECT * FROM usuariolotus order by 1 desc limit 20";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();

		$pagination = $this->get('Pagination');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{

		JToolBarHelper::title(JText::_('COM_INVITADO'));

		$canDo = InvitadoHelpersInvitado::getActions();

		JToolBarHelper::title(JText::_('COM_INVITADO_ADMIN_INVITADO'), 'users');

		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/invitado';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('invitado.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('invitado.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_invitado');
		}




	}
}
