<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_invitado'))
{
    throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// import joomla controller library
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Invitado', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Invitado');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
