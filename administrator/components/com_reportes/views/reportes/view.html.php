<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');
JHtml::_('behavior.framework', true);
JHtml::_('behavior.modal');
/**
 * retos View
 */
class ReportesViewReportes extends JView
{
	/**
	 * Retos view display method
	 * @return void
	 */
	function display($tpl = null) 
	{
		// Get data from the model
		$db =& JFactory::getDbo();
		$query = "SELECT * FROM reporte where rep_estado='Activo' order by rep_orden";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
//		$items = $this->get('Items');
		
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
            $task = JRequest::getVar( 'task' );
            JToolBarHelper::title(JText::_('Lista de Reportes'));
        }
}
