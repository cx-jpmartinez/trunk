<?php 
$datos = $this->result;
$reporte = $this->reporte;
$filas = count($datos);
$x='';
$y='';
$i=1;
foreach ($datos as $dato){
    if($i<$filas){
        $x.="'$dato->negocio'".',';
        $y.=$dato->cantidad.',';
    }else{
        $x.="'$dato->negocio'";
        $y.=$dato->cantidad;
    }
   $i++;
}
?>

<script type="text/javascript" src="../_objects/scripts/jsgrafica/jquery.min.js"></script><script type="text/javascript" src="<?php echo JUri::root() . 'media/com_retos/js/scrollspy.js' ?>" ></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/com_retos/js/scrollspy.js' ?>" ></script>

<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column',
                margin: [ 70, 70, 140, 90]
                
            },
            title: {
                text: '<?php echo $reporte->rep_nombre; ?>',
                style: {
                        fontSize: '15px',
                        fontFamily: 'Verdana, sans-serif',
                        color:'#0B1907',
                        weight:'bold'
                    }
            },
            xAxis: {
                min: 0,
                title: {
                    text: ' <?php echo $reporte->rep_textox; ?>'
                     
                },
                
                categories: [
                    <?php echo $x; ?>
                ],
                labels: {
                    rotation: -30,
                    align: 'right',
                    
                    style: {
                        fontSize: '14px',
                        fontFamily: 'Verdana, sans-serif',
                        color:'#0B1907',
                        weight:'bold'
                    }
                    
                }
                
                
                
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $reporte->rep_nombre; ?>'
                },
                
                labels: {
                   style: {
                        fontSize: '15px',
                        fontFamily: 'Verdana, sans-serif',
                        color:'#0B1907',
                        weight:'bold'
                    }
                    
                }
                
                
            },
            legend: {
                enabled: false
                
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                         Highcharts.numberFormat(this.y, 0) + ' <?php echo $reporte->rep_nombre; ?>';
                }
            },
            series: [{
                name: 'Population',
                data: [<?php echo $y; ?>],
                color: '<?php echo $reporte->rep_color; ?>',
                dataLabels: {
                    enabled: true,
                    rotation: -360,
                    color: '#000000',
                    align: 'right',
                    x: 15,
                    y: -5,
                    style: {
                        fontSize: '20px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
    
});
		</script>
	</head>
	<body>
<script src="../_objects/scripts/jsgrafica/highcharts/highcharts.js"></script>
<script src="../_objects/scripts/jsgrafica/highcharts/exporting.js"></script>

<div id="container" style="min-width: 600px; height: 600px; margin: 0 auto"></div>