<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

class ReportesController extends JController {

    /**
     * display task
     *
     * @return void
     */
    function display($cachable = false) {
        $task = JRequest::getVar('task');
        $view = JRequest::getVar('view');
        $idrep = JRequest::getVar('idrep');
        $tipo = JRequest::getVar('tipo');
        $idretos = JRequest::getVar('cid');
        $idneg = JRequest::getVar('neg');
        $retosreporte = 'in(';
        if($idretos){
           foreach ($idretos as $j => $idreto){
                $retosreporte.= "$idreto,";
           }
            $retosreporte.=')';
            $retosreporte=str_replace(',)',')',$retosreporte);
        }else{
            $idretos = '';
        }
        
       
        
        $negociosreporte = 'in(';
         if($idneg){
           foreach ($idneg as $j => $idnegocio){
                $negociosreporte.= "'$idnegocio',";
           }
            $negociosreporte.=')';
            $negociosreporte=str_replace(',)',')',$negociosreporte);
        }else{
            $idneg = '';
        }
        
        if($view=='seleccionarReto'){
          $grafica = new grafica();
          $retos = $grafica->cargarRetosGrafica();
          $reporteMostrar = new grafica($idrep);
          $tpl= 'retos';
          $newView = & $this->getView('reporte', 'html');
          $newView->retos =  $retos;
          $newView->idrep =  $idrep;
          $newView->tipo =  $tipo;
          $newView->reporte =  $reporteMostrar;
          $newView->display($tpl); 
        }elseif($view=='seleccionarRetoNegocio'){
          $grafica = new grafica();
          $retos = $grafica->cargarRetosGrafica();
          $negocios = $grafica->cargarNegocios();
          $tpl= 'retos_negocio';
          $newView = & $this->getView('reporte', 'html');
          $newView->retos =  $retos;
          $newView->negocios =  $negocios;
          $newView->idrep =  $idrep;
          $newView->tipo =  $tipo;
          $newView->display($tpl); 
        }elseif($view=='grafica' && $tipo=='grafica'){
          if($idretos){
                $grafica = new grafica($idrep);
                //$result = $grafica->grafica1();
                $result = $grafica->cargarGrafica($grafica->rep_grafica, $retosreporte);
                $tpl= 'grafica';
                $newView = & $this->getView('reporte', 'html');
                $newView->result =  $result;
                $newView->reporte = $grafica;
                $newView->display($tpl); 
          }else{
                JError::raiseWarning(null, JText::_('Por favor, primero haga una selección en la lista'));
                $grafica = new grafica();
                $retos = $grafica->cargarRetosGrafica();
                $tpl= 'retos';
                $newView = & $this->getView('reporte', 'html');
                $newView->retos =  $retos;
                $newView->idrep =  $idrep;
                $newView->tipo =  $tipo;
                $newView->display($tpl); 
          }
        }elseif($view=='excel' && $tipo=='excel'){
            if($idretos){
                $grafica = new grafica($idrep);
                $consolidado = false;
                if($grafica->rep_orden==1){
                    $consolidado = true;
                }
                $result = $grafica->exportarExcel($grafica,$grafica->rep_excel, $retosreporte, $consolidado);
            }else{
                JError::raiseWarning(null, JText::_('Por favor, primero haga una selección en la lista'));
                $grafica = new grafica();
                $retos = $grafica->cargarRetosGrafica();
                $tpl= 'retos';
                $newView = & $this->getView('reporte', 'html');
                $newView->retos =  $retos;
                $newView->idrep =  $idrep;
                $newView->tipo =  $tipo;
                $newView->display($tpl);
            }
        }elseif($view=='graficabarra' && $idrep==8){
           if($idretos && $idneg){
                $grafica = new grafica($idrep);
                if($tipo=='excel'){
                    $result = $grafica->exportarPieExcel($grafica->rep_nombre, $grafica->rep_excel ,$retosreporte, $negociosreporte );
                }else{
                    $result = $grafica->cargarPie($grafica->rep_grafica, $retosreporte, $negociosreporte );
                    $compania = $grafica->cargarNombreNegocio($negociosreporte);
                    $tpl= 'pie';
                    $newView = & $this->getView('reporte', 'html');
                    $newView->result =  $result;
                    $newView->compania =  $compania;
                    $newView->reporte = $grafica;
                    $newView->display($tpl); 
                }
            }else{
                JError::raiseWarning(null, JText::_('Por favor, primero haga una selección del reto y el negocio'));
                $grafica = new grafica();
                $retos = $grafica->cargarRetosGrafica();
                $negocios = $grafica->cargarNegocios();
                $tpl= 'retos_negocio';
                $newView = & $this->getView('reporte', 'html');
                $newView->retos =  $retos;
                $newView->negocios =  $negocios;
                $newView->idrep =  $idrep;
                $newView->tipo =  $tipo;
                $newView->display($tpl); 
            }
        }else{
         $newView = & $this->getView('reportes', 'html');
         $newView->display();
        }
    }
    
   

    

   

}
