<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * Retos Controller
 */
class ReportesControllerReportes extends JControllerAdmin {

    /**
     * Proxy for getModel.
     * @since	1.6
     */
    public function getModel($name = 'Reportes', $prefix = 'ReportesModel') {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }

    public function exportarExcel() {

        $idsResiduo = JRequest::getVar('cid', array());
        $where = "";
        if (count($idsResiduo) > 0) {
            $idsResiduo = implode(",", $idsResiduo);
            $where = 'WHERE ret_id IN (' . $idsResiduo . ')';
        }

        $config = JFactory::getConfig();
        $rutaReporte = $config->get('root_report');
        $nombreArchivo = 'ReporteRetos' . '_' . date('Ymd_His') . '.xls';

        $db = JFactory::getDbo();
       $sql = "
            SELECT
                CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                CONCAT_WS('</th><th>',
                                   'NOMBRE RETO'
                                   ,'NEGOCIO RETO'
                                   ,'ESTADO RETO'
                                   ,'NOMBRE USUARIO'
                                   ,'EMAIL'
                                   ,'NEGOCIO PROPONENTE'
                                   ,'CODIGO DE LA SOLUCION'
                                   ,'INVITADO A LA SOLUCION'
                                   ,'ESTADO SOLUCION'
                                   ,'FECHA SOLUCI0N'
                                ), '</th></tr>')
             UNION ALL
            (
                SELECT
                    CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                    CONCAT_WS('</td><td>',
                    ret_nombre,
                    (SELECT group_concat(com_nombre) FROM compania
                      inner join retoxnegocio on (rxn_com_id=com_id)
                      where rxn_ret_id=ret_id
                      group by rxn_ret_id),
                    ret_estado,
                    usu_nombre,
                    usu_email,
                    if(usu_compania<>'',usu_compania,'0'),
                    if(sol_titulo<>'',sol_titulo,'0') ,
                    (SELECT if(count(sl.sol_id)>0,
                    if(sl.sol_titulo<>'',sl.sol_titulo,'0'),'0'
                    ) from solucion sl where sl.sol_id=p.sol_sol_id),
                    if(sol_estado='Finalizado','PENDIENTE DE APROBACION',if(sol_estado='Aprobada','SOLUCION GANADORA',if(sol_estado='Rechazada','RECHAZADA','BORRADOR'))),              
                    sol_fechaEstado
                    ), '</td></tr>')
                FROM reto
                    inner join solucion p on (sol_ret_id=ret_id)
                    inner join usuario on (usu_id=sol_usu_id),
                    (SELECT @rn:=false) num
                {$where}
                ORDER BY usu_nombre
                LIMIT 100000000
            )
            UNION ALL
                SELECT '</table>'
                INTO OUTFILE '{$rutaReporte}{$nombreArchivo}'
            ";
         //exit($sql);   

        $db->retos->setQuery($sql);
        $db->retos->query();

//        $nombreArchivo = "ReporteRetos_20120718_111755.xls"; // esta línea se quita cuando se monte en produccion
        $rutaReporte = $config->get('root_report_download');
        $archivo = $rutaReporte . $nombreArchivo;
        
        //echo $archivo;
        //exit;

        echo "<script> window.location = 'index.php?option=com_retos'; </script>";
        header("Location: " . $rutaReporte . $nombreArchivo);
    }
}
