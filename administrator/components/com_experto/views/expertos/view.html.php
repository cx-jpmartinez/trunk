<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');


class ExpertoViewExpertos extends JViewLegacy
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null)
	{

		$this->state 			= $this->get('State');
		$this->items 			= $this->get('Items');
		$this->pagination = $this->get('Pagination');

		$db = JFactory::getDbo();

		$query = "
							SELECT
									exp_id
									, exp_nombre
									, exp_mail
									, exp_tipo
									, exp_estado
									, exp_fecha
							FROM experto
					WHERE exp_nombre is not null;
		";

		$db->retos->setQuery( $query );

		$items 			= $db->retos->loadObjectList();
    $pagination = $this->get('Pagination');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$this->items = $items;
		$this->pagination = $pagination;

		$this->addToolBar();
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_EXPERTO'));

		$canDo = ExpertoHelpersExperto::getActions();

		JToolBarHelper::title(JText::_('COM_EXPERTO_ADMIN_EXPERTO'), 'user');

		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/experto';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('experto.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('experto.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_experto');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_experto&view=expertos');

        $this->extra_sidebar = '';
        JHtmlSidebar::addFilter(

            JText::_('JOPTION_SELECT_PUBLISHED'),

            'filter_published',
            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.neg_nombre'), true)

        );

	}
}
