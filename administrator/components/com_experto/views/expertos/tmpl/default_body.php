<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
$j=1;
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php
          echo $j;
      ?>
		</td>
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->exp_id); ?>
		</td>
		<td class="has-context">
			<a href="<?php echo JRoute::_('index.php?option=com_experto&task=experto.edit&exp_id=' . $item->exp_id); ?>">
				<?php echo $item->exp_nombre; ?>
			</a>
		</td>
		<td  class="small hidden-phone">
      	<?php echo $item->exp_mail; ?>
		</td>
		<td  class="small hidden-phone">
        <?php echo $item->exp_tipo; ?>
    </td>
		<td class="small hidden-phone">
        <?php echo $item->exp_estado; ?>
		</td>
		<td  class="small hidden-phone">
        <?php echo $item->exp_fecha; ?>
		</td>
	</tr>

<?php $j++; endforeach; ?>
