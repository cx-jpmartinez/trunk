<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');


class ExpertoViewExperto extends JViewLegacy
{

	protected $state;

	protected $item;

	protected $form;


	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null)
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;

		$this->item = $item;
        // Set the toolbar
		$this->addToolBar();
		// Display the template



		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{

		JFactory::getApplication()->input->set('hidemainmenu', true);

		$isNew = ($this->item->exp_id == 0);

		JToolBarHelper::title(JText::_('COM_EXPERTO_TITLE_EXPERTO'), 'user');
		JToolBarHelper::save('experto.save');
		JToolBarHelper::cancel('experto.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');

	}
}
