<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_compania/css/form.css');

?>

<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<script src="../_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    js = jQuery.noConflict();

    js(document).ready(function () {

        js('#jform_exp_nombre').autocomplete({
          source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
          , param: { text: '' }
          , options: { scroll: true }
          , select: function (event, ui){
              var arr = ui.item.value.split(' | ');
              ui.item.value = ui.item.text;
                js('#jform_exp_mail').val(arr[1]);
              if(arr[1]!=''){
                  $('#jform_exp_mail').attr('readonly',true);
              }

          }
        });

    if(  js( "#jform_exp_tipo0:checked").length > 0  )
    {

        js("#jform_exp_companiaExte").val("");

        js("#interno").css('display','');
        js("#externo").css('display','none');

    }
    else
    {
        if( js( "#jform_exp_tipo1:checked").length > 0)
        {
            js("#jform_exp_companiaInter").val("");

            js("#interno").css('display','none');
            js("#externo").css('display','');

        }
    }

  });
</script>

<script type="text/javascript">
// ultima modificacion en  20016 para joomla 3.4
function capaInterno(){
	var exter = document.getElementById('externo');
	var inter = document.getElementById('interno');

    var inputExter = document.getElementById('jform_exp_companiaExte');
    var inputInter = document.getElementById('jform_exp_companiaInter');

    noRequerido(inputExter);
    requerido(inputInter);

    exter.style.display = "none";
    inter.style.display = "";

}

function capaExterno(){
    var exter = document.getElementById('externo');
    var inter = document.getElementById('interno');

    var inputExter = document.getElementById('jform_exp_companiaExte');
    var inputInter = document.getElementById('jform_exp_companiaInter');

    noRequerido(inputInter);
    requerido(inputExter);

    inter.style.display = "none";
    exter.style.display = "";
}

/**
 *
 * @param field  :  Campo a poner como requerido
 */
function requerido(field)
{
    field.classList.add("required");
    field.setAttribute('aria-required','true');
}


/**
 *
 * @param field  :  Campo a poner como no requerido
 */
function noRequerido(field)
{
    field.classList.remove( "required" );
    field.setAttribute('aria-required','false');
}

//JOMMLA BUTTONS
Joomla.submitbutton = function (task)
{
    if (task == 'experto.cancel')
    {
      Joomla.submitform(task, document.getElementById('experto-form'));
    }
    else
    {
      if (task != 'experto.cancel' && document.formvalidator.isValid(document.id('experto-form')))
      {
        Joomla.submitform(task, document.getElementById('experto-form'));
      }
      else
      {
        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
      }
    }
  }

</script>
<form action="<?php echo JRoute::_('index.php?option=com_experto&layout=edit&exp_id='.(int) $this->item->exp_id); ?>" method="post" name="experto_form" id="experto-form">
    <div class="form-horizontal">
        <legend><?php echo JText::_('COM_EXPERTO_LEGEND_EXPERTO') ?></legend>
        <div class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_tipo'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_tipo'); ?>
            </div>
        </div>
        <div class="control-group"  id="externo" style="display: none;">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_companiaExte'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_companiaExte'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_nombre'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_nombre'); ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_mail'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_mail'); ?>
            </div>
        </div>

        <div id="interno" class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_companiaInter'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_companiaInter'); ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_telefono'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_telefono'); ?>
            </div>
        </div>

        <div class="control-group">
            <div class="control-label">
                <?php echo $this->form->getLabel('exp_estado'); ?>
            </div>
            <div class="controls">
                <?php echo $this->form->getInput('exp_estado'); ?>
            </div>
        </div>

        <div class="control-group">
            <input type="hidden" name="task" value="experto.edit" />
            <?php echo JHtml::_('form.token'); ?>
        </div>

    </div>

</form>
