<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Experto
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * negocio helper.
 *
 * @since  1.6
 */
class ExpertoHelpersExperto
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_EXPERTO_TITLE_EXPERTOS'),
			'index.php?option=com_experto&view=expertos',
			$vName == 'expertos'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_experto';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit',  'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
