<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HelloWorlds View
 */
class AreaViewAreas extends JViewLegacy
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$db =& JFactory::getDbo();

		$query = "
            SELECT
				are_id
				, are_nombre
				, are_descripcion
				, are_estado
				, are_fecha
            FROM areaInteres
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('TIT_LISTA_AREAS_INTERES'));
		//JToolBarHelper::deleteListX('', 'areas.delete');
		JToolBarHelper::editListX('areainteres.edit');
		JToolBarHelper::addNewX('areainteres.add');
		JToolBarHelper::cancel('cancel');
	}
}
