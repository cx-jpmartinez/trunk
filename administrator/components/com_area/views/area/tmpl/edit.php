<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_area&layout=edit&are_id='.(int) $this->item->are_id); ?>" method="post" name="adminForm" id="area-form">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'LAB_DATOS_AREA_INTERES' ); ?></legend>
		<ul class="adminformlist">
<?php foreach($this->form->getFieldset() as $field): ?>
			<li><?php echo $field->label;echo $field->input;?></li>
<?php endforeach; ?>
		</ul>
	</fieldset>
	<div>
		<input type="hidden" name="task" value="areaInteres.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

