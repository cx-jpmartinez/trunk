<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td>
			<?php echo $item->cat_id; ?>
		</td>
		<td>
			<?php echo JHtml::_('grid.id', $i, $item->cat_id); ?>
		</td>
		<td>
			<a href="<?php echo JRoute::_('index.php?option=com_categoria&task=categoria.edit&cat_id=' . $item->cat_id); ?>">
				<?php echo $item->cat_nombre; ?>
			</a>
		</td>
		<td>
            <?php echo $item->cat_descripcion; ?>
		</td>
		<td align="center">
            <?php echo $item->cat_estado; ?>
		</td>
		<td align="center">
            <?php echo $item->cat_fecha; ?>
		</td>
	</tr>
<?php endforeach; ?>

