<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class CategoriaViewCategorias extends JView
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null) 
	{
		// Get data from the model
		$db =& JFactory::getDbo();
		
		$query = "
            SELECT
                cat_id
                , cat_nombre
                , cat_descripcion
                , cat_estado
                , cat_fecha
            FROM categoria
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JToolBarHelper::title(JText::_('TIT_LISTA_CATEGORIA'));
		//JToolBarHelper::deleteListX('', 'categorias.delete');
		JToolBarHelper::editListX('categoria.edit');
		JToolBarHelper::addNewX('categoria.add');
		JToolBarHelper::cancel('cancel');
	}
}
