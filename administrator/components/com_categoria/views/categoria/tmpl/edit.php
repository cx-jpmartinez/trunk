<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_categoria&layout=edit&cat_id='.(int) $this->item->cat_id); ?>" method="post" name="adminForm" id="categoria-form">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'LAB_DATOS_CATEGORIA' ); ?></legend>
		<ul class="adminformlist">
<?php foreach($this->form->getFieldset() as $field): ?>
			<li><?php echo $field->label;echo $field->input;?></li>
<?php endforeach; ?>
		</ul>
	</fieldset>
	<div>
		<input type="hidden" name="task" value="categoria.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

