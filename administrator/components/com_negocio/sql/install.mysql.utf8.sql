CREATE TABLE IF NOT EXISTS `#_compania` (
	`com_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`com_nombre` VARCHAR(20) NOT NULL,
	`com_descripcion` TEXT NOT NULL,
	`com_estado` ENUM('Activa','Inactiva') NOT NULL,
	`com_fecha` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`com_codigo` VARCHAR(10) NULL DEFAULT NULL,
	PRIMARY KEY (`com_id`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=68
;
