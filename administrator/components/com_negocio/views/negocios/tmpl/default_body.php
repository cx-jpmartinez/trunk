<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
$j=1;
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php
                        echo $j;
      ?>
		</td>
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->com_id); ?>
		</td>
		<td class="has-context">
			<a href="<?php echo JRoute::_('index.php?option=com_negocio&task=negocio.edit&com_id=' . $item->com_id); ?>">
				<?php echo $item->com_nombre; ?>
			</a>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->com_descripcion; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->com_estado; ?>
		</td>
		<td class="small hidden-phone">
            <?php echo $item->com_fecha; ?>
		</td>
	</tr>
<?php $j++; endforeach; ?>
