<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class NegocioViewNegocios extends JViewLegacy
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		$this->state = $this->get('State');

		$this->items = $this->get('Items');

		$this->pagination = $this->get('Pagination');


		$db = JFactory::getDbo();

		$query = "
            SELECT
                com_id
                , com_nombre
                , com_descripcion
                , com_estado
                , com_fecha
            FROM compania
		";
		$db->retos->setQuery( $query );
		$items = $db->retos->loadObjectList();
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;



		$this->addToolbar();


		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{

		JToolBarHelper::title(JText::_('LAB_NOMBRE_NEGOCIO'));

		$canDo = NegocioHelpersNegocio::getActions();

		JToolBarHelper::title(JText::_('COM_NEGOCIO_ADMIN_NEGOCIOS'), 'briefcase');

        // Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/negocio';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('negocio.add', 'JTOOLBAR_NEW');
			}


			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('negocio.edit', 'JTOOLBAR_EDIT');
			}
		}


		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_negocio');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_negocio&view=negocios');

		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',
            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.com_negocio'), true)
		);

	}

}
