<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_negocio
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_negocio/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		jQuery("#jform_com_coms_id").addClass("required");

	});

	Joomla.submitbutton = function (task) {
		if (task == 'negocio.cancel')
        {
			Joomla.submitform(task, document.getElementById('negocio-form'));
		}
		else {

			if (task != 'negocio.cancel' && document.formvalidator.isValid(document.id('negocio-form')))
            {
				var comp	= jQuery("#jform_com_coms_id").val();

				if( comp[0] == ""  )
				{
					alert("opcion por defecto no valida, seleccione una distinta");
				}
				else
				{
					Joomla.submitform(task, document.getElementById('negocio-form'));
				}

			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_negocio&layout=edit&com_id='.(int) $this->item->com_id); ?>"
		method="post"
		name="negocio-form"
		id="negocio-form"
		class = "form-validate"
>

    <div class="form-horizontal">
        <legend><?php echo JText::_( 'COM_NEGOCIO_TITLE_NEGOCIOS' ); ?></legend>
        <?php foreach($this->form->getFieldset() as $field): ?>
            <div class="control-group">
                <div class="control-label"><?php  echo $field->label; ?></div>
                <div class="controls"><?php  echo $field->input; ?></div>
            </div>
        <?php endforeach; ?>
        <div class="control-group">
            <input type="hidden" name="task" value="negocio.edit" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </div>

</form>
