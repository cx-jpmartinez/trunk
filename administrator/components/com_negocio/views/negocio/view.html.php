<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class NegocioViewNegocio extends JViewLegacy
{
	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null)
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
				throw new Exception(implode("\n", $errors));
		}

		// Assign the Data
		$this->form = $form;

		$this->item = $item;


		$this->addToolBar();
		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$isNew = ($this->item->com_id == 0);
		JToolBarHelper::title(JText::_('COM_NEGOCIO_TITLE_NEGOCIOS'), 'negocio.png');

		JToolBarHelper::save('negocio.save');
		JToolBarHelper::cancel('negocio.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
	}
}
