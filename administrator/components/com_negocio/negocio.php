<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');




// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_negocio'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::registerPrefix('Negocio', JPATH_COMPONENT_ADMINISTRATOR);

// import joomla controller library
jimport('joomla.application.component.controller');

$controller = JControllerLegacy::getInstance('Negocio');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
