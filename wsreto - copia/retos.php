<?php
//set_time_limit(90);
//define( '_VALID_MOS', 1 );

define('USER', 'cognox');
define('PASS', '46948e5f');
error_reporting(E_ALL);

ini_set('display_errors', 1);

require '../configuration.php';

class Retos {

	/**
	 * Variable para almacenar la conexión a la BD
	 *
	 * @var PDO
	 */
	private $conexion;

	public function __construct(){
		$config = new JConfig();
		$config->host = explode(':', $config->host . ':3306');
		$this->conexion = new PDO("mysql:dbname=retos;host={$config->host[0]};port={$config->host[1]}", $config->user, $config->password);
	}

	/**
	 * Actualizar el estado de una solución
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idsol
	 * @param string  $estado
	 * @param string  $codigo
	 * @param string  $comentario
	 * @return string
	 */
	public function estadoSolucion($wsUser, $wsClave, $idsol, $estado, $codigo, $comentario){
	     if($wsUser==USER && $wsClave==PASS ){
			if($codigo=='2'){
				$estado='Pendiente';
			}
			$sql ="SELECT sol_id FROM solucion where sol_titulo=?";
			$prepare = $this->conexion->prepare($sql);
			$prepare->execute(array($idsol));
			
			$objResult = $prepare->fetch( PDO::FETCH_OBJ );
			if($objResult && $objResult->sol_id){
				if($codigo=='2'){
					$sql = "INSERT INTO comentario (cm_sol_id,cmt_texto) VALUES (:cm_sol_id,:cmt_texto)";
					$insertar = $this->conexion->prepare($sql);
					$insertar->execute(array(':cm_sol_id'=>$objResult->sol_id,':cmt_texto'=>$comentario));
				}
				$sql="UPDATE solucion SET sol_estado=?
				WHERE sol_titulo=? and sol_id=?";
				$prepare = $this->conexion->prepare($sql);
	            $result = $prepare->execute(array($estado, $idsol, $objResult->sol_id));
	            if($result){
					$sql="UPDATE solucion SET sol_estado=?
					WHERE sol_sol_id=? and sol_sol_id is not null";
	                $prepare = $this->conexion->prepare($sql);
	                $result = $prepare->execute(array($estado, $objResult->sol_id));
                    if($result){
                    	return 'OK';	            	
                    } else {
                    	return 'ERROR';
                    }
	            } else {
					return 'ERROR';
	            }
			}else{
				return 'ERROR';
			}
		} else {
			return 'ERROR';
		}
	}
        
        
        /**
	 * Pregunta del comite
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idSolucion
	 * @param string  $remitente
	 * @param string  $mensaje
	 * @return string
	 */
	public function datosPregunta($wsUser, $wsClave, $idSolucion, $remitente, $mensaje){
	     if($wsUser==USER && $wsClave==PASS ){
                 $sql ="SELECT sol_id FROM solucion where sol_titulo=?";
			$prepare = $this->conexion->prepare($sql);
			$prepare->execute(array($idSolucion));
			
			$objResult = $prepare->fetch( PDO::FETCH_OBJ );
			if($objResult && $objResult->sol_id){
                            $fecha = date('Y-m-d H:i');
                            $sql = "INSERT INTO preguntacomite (pco_sol_id,pco_pregunta,pco_fechapregunta,pco_remitente) VALUES (:pco_sol_id,:pco_pregunta,:pco_fechapregunta,:pco_remitente)";
                            $insertar = $this->conexion->prepare($sql);
                            $insertar->execute(array(':pco_sol_id'=>$objResult->sol_id,':pco_pregunta'=>$mensaje,':pco_fechapregunta'=>$fecha,':pco_remitente'=>$remitente));
                            return 'OK';	
                        }else{
                            return 'ERROR';    
                        }
             }else{
                 return 'ERROR';
             }
        
        }
        
        
}
/*
$reto = new Retos();
echo $ff = $reto->estadoSolucion(USER,PASS,'6-SOL-57-201203231146','Solución pendiente falta info','2','este es el comentario sddd');

*/
