<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.cms.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
JFormHelper::loadFieldClass('empresa');

/**
 * Form Field class for the Joomla Platform.
 * Provides a list of access levels. Access levels control what users in specific
 * groups can see.
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 * @see         JAccess
 * jfdelrio
 */
class JFormFieldEmpresa extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $type = 'Empresa';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string   The field input markup.
	 * @since   11.1
	 */
	protected function getInput()
	{
		// Initialize variables.
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="'.(string) $this->element['class'].'"' : '';
		$attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
		$attr .= $this->element['size'] ? ' size="'.(int) $this->element['size'].'"' : '';
		$attr .= $this->multiple ? ' multiple="multiple"' : '';

		// Initialize JavaScript field attributes.
		$attr .= $this->element['onchange'] ? ' onchange="'.(string) $this->element['onchange'].'"' : '';
		
		// Get the field options.
		$options = $this->getOptions();
	    $objs = null;
        $com_id=JRequest::getVar( 'com_id' );
        //echo $ret_com_id=JRequest::getVar( 'ret_com_id' );exit;
        if($com_id){
            $db =& JFactory::getDBO();
            $sql="SELECT neg_id as value FROM negocio
                inner join companiaxnegocio on (neg_id=cxn_neg_id)
                where cxn_com_id='$com_id'";
            $db->retos->setQuery( $sql );
            $objs  = $db->retos->loadObjectList(); 
            return JHtml::_('empresa.empresa', $this->name, $objs, $attr, $options, $this->id);
        }else{
            return JHtml::_('empresa.empresa', $this->name, $this->value, $attr, $options, $this->id);	
        }
	}
}