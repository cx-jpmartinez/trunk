<?php

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Pregunta', JPATH_COMPONENT);

// Execute the task.
$controller = JControllerLegacy::getInstance('Pregunta');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
