<?php
defined('_JEXEC') or die('Restricted access');
$preguntas = $this->preguntas;
$reto = $this->reto;
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<div id="cont_reto_prof">
    <p style="padding-left: 10px;font-size: 13px;">
        <b>
            <?php echo $this->reto->ret_nombre; ?>
        </b>
    </p>
</div>
<?php if($this->preguntas){?>
	<div id="cont_crear_solucion">
        <div id="crear_solucion">
         <?php foreach ($this->preguntas as $pregunta){ ?>
            <h4>&#9658; <?php echo $pregunta->pre_titulo; ?> 
                <?php if($pregunta->pre_respuesta){ ?>
                    <img src="./images/acepto.png" align="right" title="Respuesta enviada">
                <?php }else{ ?>
                    <img src="./images/eliminar.png" align="right" title="Respuesta pendiente">
                <?php } ?>
            </h4><br>
            <p style="margin-left: 10px;">
	            <span style="font-weight: bold;  font-family: '';"> 
	                <?php echo JText::_('COM_PREGUNTAS_PREGUNTA');?>:
	            </span> 
                <?php echo $pregunta->pre_descipcion; ?>
            </p>
            <p>
            <span style="font-weight: bold;margin-left: 10px;font-family: '';">  
                <?php echo JText::_('Fecha');?>:
            </span> 
            <?php echo $pregunta->pre_fechaCreacion; ?></p><br>
            
	            <div id="acepto" align="left" style="width: 100%;">
		            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_preguntas&amp;view=experto&amp;task=formRespuesta&amp;idRet=<?php echo $reto->ret_id; ?>&amp;idPre=<?php echo $pregunta->pre_id;  ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
		                <u style="font-size: 14px;font-weight: bold;">Dar una nueva respuesta </u>
		            </a>   
	            </div>
            
            
            <?php  if($pregunta->pre_respuesta){
            	 $respuesta = new respuesta();
            	 $respuestas = $respuesta->cargarRespuestas($pregunta->pre_id);
            	 foreach ($respuestas as $rest){
            ?>
                 <p style="margin-left: 10px;">
	                <span style="font-weight: bold;  font-family: '';"> 
	                    <?php echo JText::_('COM_PREGUNTAS_RESPUESTA');?>: 
	                </span>
                    <?php echo $rest->res_respuesta; ?>
                </p>
                <p> 
                    <span style="font-weight: bold;margin-left: 10px; font-family: '';"> 
                        <?php echo JText::_('Fecha');?>: 
                    </span>    
                        <?php echo $rest->res_fecha; ?>
                    
                </p>
                <br>
            <?php 
            	 }
                }  
            }
         ?>
        </div>
    </div>
<?php } ?>
                