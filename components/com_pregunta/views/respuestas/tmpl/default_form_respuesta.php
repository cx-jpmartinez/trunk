<?php defined('_JEXEC') or die('Restricted access');?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<script type="text/javascript">
function validarPreg (){
    form = document.forms.preguntaFrm;
    if($("#pre_respuesta").val().length <= 0) {  
        alert("El campo respuesta es requerido");  
        return false;  
    }  
    if($("#pre_tipo").val().length <= 0) {  
        alert("El campo tipo es requerido");  
        return false;  
    }  
    form.submit();
}
</script>
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="font-size:13px !important;padding-top: 1px !important;"><?php echo $this->reto->ret_nombre; ?></h3>
    </div>
</div>

<div id="cont_crear_solucion">
    <div id="crear_solucion">
       <h4>&#9658; <?php echo $this->pregunta->pre_titulo; ?></h4>
        <form action="index.php" method="post" name="preguntaFrm" id="preguntaFrm" class="preguntaFrm"> 
        <div class="cont_campos">
            <p>
                 <span style="font-weight: bold;  font-family: '';">
                    <?php echo JText::_('COM_PREGUNTAS_DESCRIPCION');?>
                 </span>
                 <?php echo $this->pregunta->pre_descipcion; ?><br>
	            <span style="font-weight: bold;  font-family: '';">
	                <?php echo JText::_('Respuesta:');?>
	            </span>
            </p>
            <textarea  name="pre_respuesta"  id="pre_respuesta"></textarea>
        </div>
        <div class="cont_campos">
                <p>
             <span style="font-weight: bold;  font-family: '';">
                   <?php echo JText::_('Tipo:');?>
                </span>
                <select name="pre_tipo" id="pre_tipo">
                    <option value="Privada">Privada</option>
                    <option value="Publica">Pública</option>
                </select>
                </p>
        </div>
          <div id="acepto">
            <p>
	            <input type="button" value="Enviar Respuesta" class="button" src="" onclick="validarPreg();"/>
	            <input type="hidden" name="task" value="responder" />
	            <input type="hidden" name="option" value="com_preguntas" />
	            <input type="hidden" name="idRet" value="<?php echo $this->reto->ret_id; ?>" />
	            <input type="hidden" name="idPre" value="<?php echo $this->pregunta->pre_id; ?>" />
	            <input type="hidden" name="idUsu" value="<?php echo $this->usuario->usu_id; ?>" />
	            <input type="hidden" name="pregunt" value="1"/>
            </p>
            </div>
        </form>
    </div>
</div>
                