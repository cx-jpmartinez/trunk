<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');



class ForosController extends JControllerLegacy
{ 
	function display($cachable = false, $urlparams = false)
    {
            $task = JRequest::getVar('task');
            if($task=='verForo'){
                $this->execute('verForo');
            }elseif($task=='nuevoComentario'){
                $this->execute('nuevoComentario');
            }elseif($task=='enviarComentario'){
                $this->execute('enviarComentario');
            }else{
               $this->execute('mostrarForos');
            }
        }
        
        function mostrarForos (){
            $user = JFactory::getUser();
            $foroestado = JRequest::getVar('foroestado');
            if($user->id){
                $usuario = new usuario($user->id);
                $foro = new foro();
                $listaforos = $foro->listarForos($foroestado);
                $newView = & $this->getView('foros','html');
                $newView->objs =& $listaforos;
                $newView->usuario =& $usuario;
                $newView->display();
            }else{
                echo '<br>';
                    echo 'Es necesario iniciar sesión';
                echo '</p><br>';
            }
        }


        function verForo(){
            $idFor = JRequest::getVar('idfor');
            $user = JFactory::getUser();
            $usuario = new usuario($user->id);
            $foro = new foro($idFor);
            $objLista = $foro->cargarRespuestas();
            if($foro->for_id){
                $newView = & $this->getView('foros','html');
                $tmp = 'verForo';
                $newView->foro =& $foro;
                $newView->usuario =& $usuario;
                $newView->objLista =& $objLista;
                $newView->display($tmp);
            }else{
                $this->execute('mostrarForos');
            }
        }
        
        
        function nuevoComentario(){
            $idFor = JRequest::getVar('idfor');
            $idcom = JRequest::getVar('idcom');
            $user = JFactory::getUser();
            $usuario = new usuario($user->id);
            $foro = new foro($idFor);
            if($foro->for_id){
                $newView = & $this->getView('foros','html');
                if($idcom){
                    $respuestaforo = new respuestaforo($idcom);
                    $newView->respuestaforo =& $respuestaforo;
                }else{
                     $newView->respuestaforo = '';
                }
                $tmp = 'form';
                $newView->foro =& $foro;
                $newView->usuario =& $usuario;
                $newView->display($tmp);
            }else{
                $this->execute('mostrarForos');
            }
        }
        
        function enviarComentario (){
            $idFor = JRequest::getVar('idfor');
            $rpf_texto = JRequest::getVar('rpf_texto');
            $idcom = JRequest::getVar('idcom');
            $user = JFactory::getUser();
            $foro = new foro($idFor);
            if($foro->for_id){
                $respuestaforo = new respuestaforo();
                $respuestaforo->rpf_for_id = $idFor;
                $respuestaforo->rpf_usu_id = $user->id;
                $respuestaforo->rpf_texto = $rpf_texto;
                if($idcom){
                     $respuestaforoPadre = new respuestaforo($idcom);
                     $respuestaforo->rpf_rpf_id = $respuestaforoPadre->rpf_id;
                     $respuestaforoPadre->rpf_cantidad = $respuestaforoPadre->rpf_cantidad + 1;
                     $respuestaforoPadre->store();
                }
                $respuestaforo->store();
                echo "<script type=\"text/javascript\">parent.location.reload();</script>";
                exit;
            }else{
                $this->execute('mostrarForos');
            }
    }
}

