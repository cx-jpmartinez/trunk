<?php
 defined('_JEXEC') or die('Restricted access');
$objs = $this->objs;
$usuario = $this->usuario;
?>
<div id="cont_crear_solucion">
    <div id="titulo_retos">
        <h3 class="titulo_reto">Foros</h3>
        <a href="index.php?option=com_home&amp;id=2&amp;Itemid=530" class="button">Atrás</a>
    </div>
    <?php $foroestado = JRequest::getVar('foroestado');?>
        <div id="crear_solucion" style="background:none;border: 0px;">
                <div id="acepto" style="width: 100%;text-align: left;padding-left: 10px;">
                     <form action="index.php" method="post" name="filtrarForo" id="filtrarForo"> 
                        <span style="font-weight:bold;"> Estado:</span>
                        <select name="foroestado" id="foroestado" style="width: 170px;" onchange="document.filtrarForo.submit();">
                            <option value="Vigente" <?php if($foroestado=='Vigente'){echo 'selected';} ?>>Vigente</option>
                            <option value="Cerrado" <?php if($foroestado=='Cerrado'){echo 'selected';} ?>>Cerrado</option>
                        </select>   
                        <input type="hidden" name="Itemid" value="572" />
                        <input type="hidden" name="option" value="com_foros" />
                    </form>
                </div>
        </div>
    
    
    
    <table id="background-contac" style="width: 100%;margin: 0 auto;text-align: left;">
        <thead>
                <tr>
                    <th style="background-color: #9A4D9E;" id="tableOrdering" class="list-title">
                        <a title="Haga clic para ordenar en esta columna." href="javascript:tableOrdering('a.title','asc','');">Seleccionar Foro</a>                                    
                    </th>
                    <th style="background-color: #9A4D9E;" id="tableOrdering" class="list-title">
                        <a>Fecha Creación</a>                                    
                    </th>
                </tr>
            </thead>
           <?php if(count($objs)){ 
               foreach($objs as $obj){
           ?>
            <tbody>
                <tr class="cat-list-row0">
                    <td class="list-title">
                        <a href="index.php?option=com_foros&task=verForo&idfor=<?php echo $obj->for_id; ?>&Itemid=572">
                        <?php echo $obj->for_tema; ?>
                        </a>
                    </td>
                    <td class="list-author">
                        <?php echo $obj->for_fecha; ?>                                                                                              
                    </td>
                </tr>
            </tbody>
          <?php } 
           }else{
          ?>
            <tbody>
                <tr class="cat-list-row0">
                    <td class="list-title" colspan="2">
                       No hay foros
                    </td>
                </tr>
            </tbody>   
          <?php } ?>
    </table>
</div>