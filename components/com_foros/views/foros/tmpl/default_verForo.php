<?php
 defined('_JEXEC') or die('Restricted access');
$foro = $this->foro;
$usuario = $this->usuario;
$objLista = $this->objLista;
?>
<div id="cont_crear_solucion">
    <div id="titulo_retos">
        <h3 class="titulo_reto">Foro</h3>
        <a href="index.php?option=com_foros" class="button">Atrás</a>
    </div>
    
    
     <div id="crear_solucion" style="background:none;border: 0px;">
                <div id="acepto" style="width: 100%;text-align: left;padding-left: 10px;">
                   <span style="font-weight:bold;"> <?php echo $foro->for_tema; ?></span>
                    <p style="margin-left: 5px;margin-right:15px;text-align:justify;">
                       <?php echo $foro->for_texto; ?>
                        
                    </p>
                     <?php if($foro->for_estado=='Vigente'){ ?>  
                        <p style="margin-left: 5px;margin-right:15px;text-align:justify;">
                        <a class="modal" style="text-align:center; text-decoration: none;" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_foros&amp;task=nuevoComentario&amp;idfor=<?php echo $foro->for_id; ?>&amp;&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <u style="font-size: 14px;font-weight: bold;">Agregar Comentario </u>
                        </a>
                       </p>
                     <?php } ?>   
                </div>
        </div>
    <div id="crear_solucion">
        <?php 
        if($objLista){
        foreach ($objLista as $respuesta){ ?>
        <h4 style="font-weight:bold;">&#9658; Por: <?php echo $respuesta->usu_nombre; ?></h4><br>
                     <p style="margin-right:10px;font-weight:bold;text-align: right">
                       Este comentario tiene <?php echo $respuesta->rpf_cantidad; ?> respuestas
                    </p>
                    <p style="margin-left: 10px;margin-right:10px;text-align:justify;">
                        Fecha: <?php echo $respuesta->rpf_fecha; ?><br>
                        <?php echo $respuesta->rpf_texto; ?>  
                    </p>
                   <?php if($foro->for_estado=='Vigente'){ ?>   
                        <p style="margin-right:10px;font-weight:bold;text-align: right">
                            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_foros&amp;task=nuevoComentario&amp;idfor=<?php echo $foro->for_id; ?>&idcom=<?php echo $respuesta->rpf_id; ?>&amp;&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                <u style="font-weight: bold;">Responder a este comentario</u>
                            </a>
                        </p>
                    <?php } ?>
                    
                    
                    <?php 
                    $respuestaforo = new respuestaforo($respuesta->rpf_id); 
                    $hijos =  $respuestaforo->cargarRespuestaHijo();
                    if($hijos){
                        foreach ($hijos as $hijo){
                    ?>
                        <h5 id="interno" style="font-weight:bold;">&#9658; Por: <?php echo $hijo->usu_nombre; ?></h5>
                        <div class="cont_campos" style="width: 85%;margin-left: 10px; text-align:justify;">
                            <p style="margin-right:10px;font-weight:bold;text-align: right">
                        Este comentario tiene <?php echo $hijo->rpf_cantidad; ?> respuestas
                        </p>
                            <p>Fecha: <?php echo $hijo->rpf_fecha; ?><br>
                                <?php echo $hijo->rpf_texto; ?>
                            </p>
                            <?php if($foro->for_estado=='Vigente'){ ?>   
                            <p style="margin-right:10px;font-weight:bold;text-align: right">
                                <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_foros&amp;task=nuevoComentario&amp;idfor=<?php echo $foro->for_id; ?>&idcom=<?php echo $hijo->rpf_id; ?>&amp;&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                    <u style="font-weight: bold;">Responder a este comentario</u>
                                </a>
                            </p>
                        <?php } ?>
                        </div>
                    <?php
                        $respuestanieto = new respuestaforo($hijo->rpf_id); 
                        $nietos= $respuestanieto->cargarRespuestaHijo();
                       
                        if($nietos){
                             foreach ($nietos as $nieto){
                    ?>
                                <h5 id="interno" style="font-weight:bold;background:#0088BB;width: 480px;">&#9658; Por: <?php echo $nieto->usu_nombre; ?></h5>
                                <div class="cont_campos" style="width: 75%;margin-left: 10px; text-align:justify;">
                                    <p>Fecha: <?php echo $nieto->rpf_fecha; ?><br>
                                    <?php echo $nieto->rpf_texto; ?>
                                    </p>
                                </div>
                    <?php
                                 
                             }
                        }
                    
                    
                        }
                    }
                    ?>
                    
                    
                    
                
           
            <?php  
            }
        }else{ ?>
            <p style="margin-right:10px;font-weight:bold;text-align: center;">
            No hay comentarios
            </p>
            <?php  
        }
        ?>
    </div>
    </div>


