<?php 
defined('_JEXEC') or die('Restricted access');
$foro = $this->foro;
$usuario = $this->usuario;
$respuestaforo= $this->respuestaforo;
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<script type="text/javascript">
function validarPreg (){
    form = document.forms.preguntaFrm;
    if($("#rpf_texto").val().length <= 0) {  
        alert("El campo Opinión es requerido");  
        return false;  
    }  
    $("#aceptar").attr('disabled','disabled');
    form.submit();
}
</script>

     <div id="crear_solucion" style="background:none;border: 0px;">
                <div class="cont_campos">
                    <p style="padding-right:20px;text-align:justify;font-weight:bold;"> <?php echo $foro->for_tema; ?></p>
                    <p style="padding-right:20px;text-align:justify;">
                       <?php echo $foro->for_texto; ?>
                    </p>
                </div>
            
        </div>

<div id="cont_crear_solucion">
    
    <div id="crear_solucion">
       <h4>&#9658; Adicione su comentario</h4>
        <form action="index.php" method="post" name="preguntaFrm" id="preguntaFrm" class="preguntaFrm"> 
        <div class="cont_campos">
            <p>
                <span style="font-weight: bold;  font-family: '';">
                    <?php echo JText::_('Opinión:');?>
                </span>
            </p>
            <textarea  name="rpf_texto" id="rpf_texto"></textarea>
        </div>
          <div id="acepto">
            <p>
	            <input type="button" value="Enviar" name="aceptar" id="aceptar" class="button" src="" onclick="validarPreg();"/>
	            <input type="hidden" name="task" value="enviarComentario" />
	            <input type="hidden" name="option" value="com_foros" />
	            <input type="hidden" name="idfor" value="<?php echo $foro->for_id;  ?>"/>
                    <?php if(isset($respuestaforo->rpf_id)){ 
                        if($respuestaforo->rpf_id){ ?>
                        <input type="hidden" name="idcom" value="<?php echo $respuestaforo->rpf_id;  ?>"/>
                    <?php }
                    
                    }
                    ?>
            </p>
            </div>
        </form>
    </div>
</div>