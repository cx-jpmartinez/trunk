<?php

/**
 * @version     1.0.0
 * @package     com_graphic
 * @copyright   Copyright (C) 2013. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      afperez <afperez@cognox.com> - http://www.cognox.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * View to edit
 */
class GraphicViewGraphic extends JViewLegacy {



    /**
     * Display the view
     */
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = $this->get('Msg');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Display the view
		parent::display($tpl);
	}




}
