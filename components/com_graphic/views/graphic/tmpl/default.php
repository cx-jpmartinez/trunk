<?php
defined('_JEXEC') or die;
$lang = JFactory::getLanguage();
$task = JRequest::getVar('task');
$rep_id = JRequest::getVar('rep_id');
$Itemid = JRequest::getVar('Itemid', '629');
$accion = JRequest::getVar('accion', '');
 
/* echo '<pre>';
  print_r($this->grafica); */

JHtml::_('behavior.framework', true);
JHtml::_('behavior.modal');
JHtml::_('behavior.tooltip');

$verRetos = false;
$estados = array();
$AnosSolucion = array();
$AnosLanzamiento = array();
$negociosReto = array();
$AnosInscripcion = array();
$IntRetos = array();

if ($this->flEstadosReto || $this->flAnosSolucion || $this->flAnosLanzamiento || $this->flNegociosReto || $this->flAnosInscripcion || $this->flIntRetos) {
    $estadosReto = explode(",", $this->flEstadosReto);
    foreach ($estadosReto as $key => $value) {
        $estados["$value"] = $value;
    }

    $flAnosSolucion = explode(",", $this->flAnosSolucion);
    foreach ($flAnosSolucion as $key => $value) {
        $AnosSolucion["$value"] = $value;
    }

    $flAnosLanzamiento = explode(",", $this->flAnosLanzamiento);
    foreach ($flAnosLanzamiento as $key => $value) {
        $AnosLanzamiento["$value"] = $value;
    }

    $flNegociosReto = explode(",", $this->flNegociosReto);
    foreach ($flNegociosReto as $key => $value) {
        $negociosReto["$value"] = $value;
    }

    $flAnosInscripcion = explode(",", $this->flAnosInscripcion);
    foreach ($flAnosInscripcion as $key => $value) {
        $AnosInscripcion["$value"] = $value;
    }
    
    $flIntRetos = explode(",", $this->flIntRetos);
    foreach ($flIntRetos as $key => $value) {
        $IntRetos["$value"] = $value;
    }
}
if($accion){
    $verRetos = true;
}
//echo '<pre>';
//print_r($_REQUEST);
//echo '<pre>';
//print_r($AnosLanzamiento);
?>
<script src="<?php echo $this->baseurl ?>/templates/graphic/js/jquery-1.9.1.js"></script>

<table  width="100%" border="0"  >
    <tr>
        <td>
            <div class="cabecera" style="position:relative; left: 190px;"/><image style="position:relative;left: 45px;top:5px;"src="<?php echo JURI::root(); ?>templates/graphic/images/<?php echo $this->grafica->rep_color; ?>" width="40" height="40"/>
            <span class="textcab"  style="position:relative; top:-25px"><?php echo $this->grafica->rep_nombre; ?></span></div>
        </td>
    </tr>
</table>

<form action="index.php?option=com_graphic" method="post" name="viltrosReporte" id="viltrosReporte">
    <div id="filtrosBuscador" style="">
        <table align="center" style="width: 68%; ">
            <tr>
                <td class="column" ><span class="titulos">Reto</span></td>
                <td class="column"><span class="titulos">Negocio <?php if($this->grafica->rep_id==23){ echo 'del reto'; }else{ echo 'del proponente'; } ?> </span></td>
                <?php if($this->grafica->rep_id!=23){ ?> 
                    <?php if($this->grafica->rep_id!=25 && $this->grafica->rep_id!=21 && $this->grafica->rep_id!=27){ ?> 
                        <td class="column"><span class="titulos">Año envío solución</span></td>
                    <?php } ?> 
                    <?php if($this->grafica->rep_id!=22 && $this->grafica->rep_id!=26 ){ ?>     
                        <td class="column"><span class="titulos">Año Inscripción </span></td>
                    <?php } ?>
                <?php } ?> 
                <td class="column4"><span class="titulos">Año Lanzamiento </span></td>
            </tr>
            <tr>
                <td class="column2" valign="top" style="padding-left: 10px;">
                    <label class="lbl"><input type="checkbox" name="estadoReto[]" id="checkRetos" value="'DefectValue'"/> Todos</label>
                    <label class="lbl"><input type="checkbox" name="estadoReto[]" id="chsckboxVigente"  value="'Vigente'" <?php
if (array_key_exists("'Vigente'", $estados)) {
    echo "checked";
}
?> /> 
                        Vigente 
                    </label>
                    <label class="lbl"><input type="checkbox" name="estadoReto[]"  id="checkboxCerrada" class="retos" value="'Cerrada'" <?php
                                              if (array_key_exists("'Cerrada'", $estados)) {
                                                  echo "checked";
                                              }
?>  /> Cerrada </label>
                </td>
                <td class="column2" valign="top" style="padding-left: 10px;">
                    <label class="lbl"><input type="checkbox" name="negocioReto[]" id="negocioRetoTodos" value="'DefectValue'"/> Todos</label>
                    <?php foreach ($this->negocios as $negocio) { ?>
                        <label class="lbl"><input type="checkbox" name="negocioReto[]" id="checkbox<?php echo $negocio->neg_descripcion ?>" value="'<?php echo $negocio->neg_descripcion ?>'" <?php
                        if (array_key_exists("'$negocio->neg_descripcion'", $negociosReto)) {
                            echo "checked";
                        }
                            ?> /> <?php echo $negocio->neg_descripcion; ?></label>
                                <?php
                        }
                    ?>  
                </td>
                <?php if($this->grafica->rep_id!=23){ ?> 
                    <?php if($this->grafica->rep_id!=25 && $this->grafica->rep_id!=21 && $this->grafica->rep_id!=27 ){ ?> 
                    <td class="column2" valign="top" style="padding-left: 10px;">
                        <label class="lbl"><input type="checkbox" name="anoSolucion[]" id="anoSolucionTodos" value="'DefectValue'"/> Todos</label>
                        <?php foreach ($this->anoSolucion as $anoSol) { ?>
                            <label class="lbl"><input type="checkbox" name="anoSolucion[]" id="checkboxS<?php echo $anoSol->anoSolucion ?>" value="'<?php echo $anoSol->anoSolucion ?>'" <?php
                        if (array_key_exists("'$anoSol->anoSolucion'", $AnosSolucion)) {
                            echo "checked";
                        }
                            ?> /> <?php echo $anoSol->anoSolucion; ?></label>

                            <?php
                        }
                    }
                        ?>   
                    </td>
               
                    <?php if($this->grafica->rep_id!=22 && $this->grafica->rep_id!=26){ ?>   
                    <td class="column2" valign="top" style="padding-left: 10px;">
                        <label class="lbl"><input type="checkbox" name="anoInscripcion[]" id="anoInscripcionTodos" value="'DefectValue'"/> Todos</label>
                        <?php foreach ($this->anoInscripcion as $anoIns) { ?>
                            <label class="lbl"><input type="checkbox" name="anoInscripcion[]" id="checkboxS<?php echo $anoIns->anoInscripcion ?>" value="'<?php echo $anoIns->anoInscripcion ?>'" <?php
                        if (array_key_exists("'$anoIns->anoInscripcion'", $AnosInscripcion)) {
                            echo "checked";
                        }
                            ?> /> <?php echo $anoIns->anoInscripcion; ?></label>

                            <?php
                        }
                        ?>   
                    </td>
                  <?php } ?>   
                    
                 <?php } ?>

                <td class="column3" valign="top" style="padding-left: 10px;">
                    <label class="lbl"><input type="checkbox" name="anoReto[]" id="anoRetoTodos" value="'DefectValue'"/> Todos</label>
                    <?php foreach ($this->anoPublicacion as $ano) { ?>
                        <label class="lbl"><input type="checkbox" name="anoReto[]" id="checkboxR<?php echo $ano->anoPublicacion ?>" value="'<?php echo $ano->anoPublicacion ?>'" <?php
                    if (array_key_exists("'$ano->anoPublicacion'", $AnosLanzamiento)) {
                        echo "checked";
                    }
                        ?> /> <?php echo $ano->anoPublicacion; ?>
                        </label>
                        <?php
                    }
                    ?>   
            </tr>
        </table> 
    </div>
    <div id="listaRetos" style="">
        <?php if ($verRetos) { ?>

            <table  style="width: 75%; position: relative; left: 35px;" id="customers" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">

                <tr align="center">
                    <td colspan="2" ><span style="font-size: 18px; font-weight:bold;color:#87C137; position: relative; right: 5px;">#</span>&nbsp;&nbsp;&nbsp;<label><input type="checkbox" name="retid[]" id="retosTodos" value="'DefectValue'"></label></td>
                    <td id="cab_table1" style=" border-right-style: none;"  >Reto</td>
                    <td id="cab_table2" style=" border-right-style: none;" >Fecha Lanzamiento</td>
                    <td id="cab_table3" style=" border-right-style: none;" >Fecha Cierre</td>
                    <td id="cab_table4" style=" border-right-style: none;">Estado</td>
                </tr>
                <?php
                $i = 0;
                $class = '';
                foreach ($this->obj as $reto) {
                    if ($i % 2 == 0) {
                        $class = '';
                    } else {
                        $class = 'alt';
                    }
                    ?>
                    <tr class="<?php echo $class; ?>"> 
                        <td><center><?php echo $reto->ret_id; ?></center> </td>
                    <td >
                        <label>
                            <input 
                                    type="checkbox" name="retid[]" id="checkbox<?php echo $reto->ret_id; ?>"  value="<?php echo $reto->ret_id; ?>" 
                                    <?php if (array_key_exists("$reto->ret_id", $IntRetos)) {
                                        echo "checked";
                                    } ?>
                            />
                        </label>
                    </td>
                    <td style="width: 40%;"><?php echo $reto->ret_nombre; ?></td>
                    <td><center><?php echo $reto->ret_fechaPublicacion; ?></center> </td>
                    <td><center><?php echo $reto->ret_fechaVigencia; ?></center> </td>
                    <td style="border-right-style: none;">
                    <center> 
                        <?php if ($reto->ret_estado == 'Vigente') { ?>
                            <img src="<?php echo $this->baseurl . '/templates/graphic/images/acepto2.jpg'; ?>" title="Vigente" align="center">
                        <?php } elseif ($reto->ret_estado == 'Cerrada') { ?>
                            <img src="<?php echo $this->baseurl . '/templates/graphic/images/eliminar2.png'; ?>" title="Cerrada" align="center">
                        <?php } ?>
                    </center> 
                    </td>
                    </tr>

                    <?php
                    $i++;
                }
                ?>
            </table>


        <?php } ?>
    </div>

    <?php if(!$verRetos){ ?>
    <span style="display: none;">
        <input type="hidden" name="retid[]" value="<?php echo $this->flIntRetos;?>" style="">
    </span>
    <?php } ?>
    <input type="hidden" name="task" id="task" value="<?php echo $task; ?>">
    <input type="hidden" name="Itemid" value="629">
    <input type="hidden" name="accion" id="accion" value="">
    <input type="hidden" name="rep_id" value="<?php echo $rep_id; ?>">
    <table   border="0" cellpadding="0" cellspacing="2" width="85%" style="position:relative;left:180px;">
        <tr>
            <td style="word-spacing: 47px;">
                <br>
                <?php if (!$verRetos) { ?>
                    <input type="button" onclick="buscarRetos();"  value="Seleccionar reto" class="boton1">
                <?php }else{ ?>
                    <input type="button" onclick="filtrosRetos();"  value="Continuar Filtros" class="boton1" style="width: 154px;">
                <?php }?>
                <?php
                /*
                  location.href = 'index.php?option=com_graphic&task=excel&rep_id=<?php echo $rep_id; ?>&Itemid=629'
                  location.href = 'index.php?option=com_graphic&task=generarGrafica&rep_id=<?php echo $rep_id; ?>&Itemid=629'
                 */
                ?>    
                <?php if ($this->grafica->rep_excel) { ?>
                    <input type="button" class="boton1" id="btnGenerarReporte" name="btnGenerarReporte" onclick="generarReporte();" value="Generar Reporte" >
                <?php } ?>
                <?php if ($this->grafica->rep_grafica) { ?>    
                    <input type="button" class="boton1" id="btnGenerarGrafica" name="btnGenerarGrafica" onclick="generarGrafica();" value="Generar Gráfica" >
                <?php } ?>

                <input type="button" class="boton1" onclick="location.href = 'index.php?option=com_graphic&task=filtros&rep_id=<?php echo $rep_id; ?>&Itemid=629'" value="Limpiar"  id="reset">
                <input type="button" class="boton1" onclick="location.href = 'index.php?option=com_graphic&view=graphic&Itemid=629'" value="Inicio" >
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
<?php if ($verRetos) { ?>
            document.getElementById('filtrosBuscador').style.display='none';    
            document.getElementById('listaRetos').style.display='';    
            
<?php } else { ?>
            document.getElementById('filtrosBuscador').style.display='';    
            document.getElementById('listaRetos').style.display='none';    
<?php } ?>
</script>


<script type="text/javascript">

    function customCheckbox(checkboxName){
        var clase = 'custom-checkbox';
        if(checkboxName=='retid[]')
        {
            clase = 'custom-checkboxBlue';  
        }
        var checkBox = $('input[name="'+ checkboxName +'"]');
        $(checkBox).each(function(){
            $(this).wrap( "<span class='"+clase+"'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
        });
        $(checkBox).click(function(){
            $(this).parent().toggleClass("selected");
        });
    }
    $(document).ready(function (){
        customCheckbox("estadoReto[]");
        customCheckbox("negocioReto[]");
        customCheckbox("anoSolucion[]");
        customCheckbox("anoReto[]");
        customCheckbox("retid[]");
        customCheckbox("anoInscripcion[]");
    })
</script>


<script type="text/javascript">

    function checkAll (checkboxName, estado){

        var checkBox = $('input[name="'+ checkboxName +'"]');
        $(checkBox).each(function(){

            if($( estado ).is(':checked')){
                $( this ).prop( "checked", true );
                $(this).parent().addClass("selected"); 
            }else{
                $( this ).prop( "checked", false );
                $(this).parent().removeClass("selected");
            }
        });
    }
    $(document).ready(function (){
        $('#checkRetos').click(function() {
            checkAll('estadoReto[]', $(this)); 
        });
    
        $('#negocioRetoTodos').click(function() {
            checkAll('negocioReto[]', $(this)); 
        });
    
        $('#anoSolucionTodos').click(function() {
            checkAll('anoSolucion[]', $(this)); 
        });
    
        $('#anoRetoTodos').click(function() {
            checkAll('anoReto[]', $(this)); 
        });
    
        $('#retosTodos').click(function() {
            checkAll('retid[]', $(this)); 
        });
     
        $('#anoInscripcionTodos').click(function() {
            checkAll('anoInscripcion[]', $(this)); 
        });
    })
</script>
<script type="text/javascript">
    
    function generarGrafica(){
        $( "#task" ).val( "generarGrafica" );
        $( "#viltrosReporte" ).submit();
    }
    
    function buscarRetos(){
        $( "#task" ).val( "filtros" );
        $( "#accion" ).val( "cargaretos" );
        $( "#viltrosReporte" ).submit();
    }
    
    function filtrosRetos(){
        $( "#task" ).val( "filtros" );
        $( "#accion" ).val( "" );
        $( "#viltrosReporte" ).submit();
    }
    
    
    
    function generarReporte(){
        $( "#task" ).val( "generarReporte" );
        $( "#viltrosReporte" ).submit();
    }
    
    
</script>


