<?php
defined('_JEXEC') or die;
$lang = JFactory::getLanguage();

$nav = $_SERVER['HTTP_USER_AGENT'];
$count = 0;

if($this->datosGrafica){
    $total = 0;
    foreach ($this->datosGrafica as $dato) {
       $count++;
       $total = $total + $dato->cantidad;
    }
}
$sizeGraph = 1000;
if(preg_match('/Chrome/i',$nav)&& $count >20){
   $sizeGraph = 1300;
}

function showData($datosGrafica) {
    $data = '';
    foreach ($datosGrafica as $dato) {
            
        $titulo = wordwrap($dato->label, 190, '<br>');
        $data .= "{ name: '$titulo',
                    data: [" . $dato->cantidad . "],
                    dataLabels: {
                    enabled: true,
                    style:{
                    color: 'black',
                    fontWeight: 'bold',
                    fontSize: '16px'
                    }}},";
    }
    return substr($data, 0, -1);
}




?>
<script src="<?php echo $this->baseurl ?>/templates/graphic/js/jquery.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/graphic/js/highcharts.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/graphic/js/exporting.js"></script>



 
<?php if($this->datosGrafica){ ?>
<center>
<div id="container" >
   
    <table  width="60%" border="0"  style="position: relative; top: 50px; z-index: 1;">
    <tr>
        <td>
            <div class="cabecera" /><image style="position:relative;left: 45px;top:5px;"src="<?php echo JURI::root(); ?>templates/graphic/images/<?php echo $this->grafica->rep_color; ?>" width="40" height="40"/>
            <span class="textcab"  style="position:relative; top:-25px"><?php echo $this->grafica->rep_nombre; ?></span></div>
        </td>
    </tr>
</table>
      <div id="sel" align="center"></div>
</div> 
  <input type="button" class="boton1" onclick="location.href = 'index.php?option=com_graphic&task=filtros&rep_id=<?php echo $this->grafica->rep_id;?>&Itemid=629'" value="Inicio" >
</center>
<script>
  $(function () {
        $('#container #sel').highcharts({
            chart: {
                
   
                type: 'column',
                width: 1200,
                height:<?php echo $sizeGraph;?>,
                margin: [ 90]

            },
            credits:{
              enabled: false  
            },
            legend: {
            width: 1150,
            layout: 'vertical',
            itemWidth: 550,
            itemStyle: {
                padding: '8px'
            }
             
        },
            
            title: {
                text: '<?php echo $this->grafica->rep_nombre;?>',
                style:{
                    top: '10px',
                    color: '#A4A4A4',
                    fontSize:'21px',
                    fontWeight: 'bold'
                }
           
            },
            
           /*subtitle:{
               text: 'Total: <?php //echo $total;?>',
               floating: true,
               x:525,
               y: 105,
               style:{
                   color: '#0099FF',
                   fontSize: '21px'
               }
           },*/
         
            xAxis: {
                categories: [
                    '<?php echo " {$this->grafica->rep_textox} (Total: {$total})" ; ?>'
                    
                ],
                labels: {
                verticalAlign: 'top',
                y: 20,
                style: {
                    top: '10px',
                    color: '#A4A4A4',
                    fontSize:'21px',
                    fontWeight: 'bold'
                }
            }
            },
           yAxis: {
           lineWidth: 5,
           min:0,
           title: {
           text: '<?php echo $this->grafica->rep_nombre; ?>',
           style:{
               color: '#A4A4A4',
               fontSize:'22px'
           }
           
         }
       },
          
            plotOptions: {
                column: {
                    pointPadding: 0.1,
                    borderWidth: 0,
                    groupPadding: 0
                }
            },
            
           series: [<?php echo showData($this->datosGrafica);?>],
        
        navigation: {
            buttonOptions: {
            
                height: 40,
                width: 48,
                symbolSize: 24,
                symbolX: 23,
                symbolY: 21,
                symbolStrokeWidth: 2,
                verticalAlign: 'top',
                y: -10,
                horizontalAlign: 'left',
                x: -40
            }
        }
           
        });
    });
</script>
<?php }else{ ?>

<center>
<div id="container" >
    <br><div id="sel" align="center" style="color: red;"><b>No hay datos</b></div><br>
</div> 
  <input type="button" class="boton1" onclick="location.href = 'index.php?option=com_graphic&task=filtros&rep_id=<?php echo $this->grafica->rep_id;?>&Itemid=629'" value="Inicio" >
</center>

<?php } ?>


   
  



