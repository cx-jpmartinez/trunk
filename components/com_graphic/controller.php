<?php
/**
 * @version     1.0.0
 * @package     com_graphic
 * @copyright   Copyright (C) 2013. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      afperez <afperez@cognox.com> - http://www.cognox.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class GraphicController extends JControllerLegacy
{
    function display($cachable = false, $urlparams = false) {
        $user = JFactory::getUser();
        $usuario = new usuario($user->id);
        if(!validarIngresoGraficas($usuario->usu_email)){
            $app = JFactory::getApplication();
            $app->redirect("index.php", '', '');
        }

        //$app = JFactory::getApplication();
        //$app->redirect("index.php?option=com_graphic&view=graphic&Itemid=629", '', '');
        $db = JFactory::getDBO();
        $view = JRequest::getVar( 'view' );
        $task = JRequest::getVar('task');
        $Itemid = JRequest::getVar('Itemid','629');
        $rep_id = JRequest::getVar('rep_id');
        $grafica = new grafica($rep_id);
        if($task=='generarGrafica'){
            self::registraLog($usuario->usu_id,$grafica->rep_nombre,'grafica');
            $newView = $this->getView('graphic','html');
             /** recupera los filtros **/
            $strEstadoRetos = self::getFiltroEstadoReto();
            $strNegociosReto = self::getFiltroNegocioReto();
            $strAnosSolucion = self::getFiltroAnoSolucion();
            $strAnosInscripcion = self::getFiltroAnoInscripcion();
            $strAnosReto = self::getFiltranoReto();
            $strIntRetos = self::getFiltroReto();

            /** fin recupera los filtros **/
            if($grafica->rep_id){
                $newView->grafica = $grafica;
                if($grafica->rep_id==21){ /* Participantes por negocio */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                    $sql = "SELECT IF(a.neg_descripcion is null,'Externos',a.neg_descripcion)  label
                        ,COUNT(0) AS cantidad FROM
                        (
                        SELECT count(usu_compania),usu_compania,neg_descripcion FROM usuario
                        LEFT JOIN negocio ON (usu_compania=neg_nombre)
                        INNER JOIN solucion ON (usu_id=sol_usu_id)
                        INNER JOIN reto ON (ret_id=sol_ret_id)
                        INNER JOIN tyc ON (tyc_ret_id=ret_id and tyc_usu_id=sol_usu_id)
                        WHERE ret_id
                        $andEstado
                        $andNegocio
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos
                        GROUP BY ret_id,usu_compania,usu_nombre
                        )
                        a GROUP BY a.neg_descripcion ORDER BY cantidad DESC";
                    /* fin Participantes por negocio */
                }elseif($grafica->rep_id==22){
                    /*Personas por negocio que han participado en solución */

                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        //$andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                        $andAnoInscripcion =  " ";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }


                    $sql="select
                        count(0) as cantidad ,
                        if(a.neg_descripcion is null,'Externos',a.neg_descripcion ) as 'label'
                        from
                        (
                        SELECT count(usu_compania) as 'cantidad',neg_descripcion FROM solucion
                        inner join reto on (ret_id=sol_ret_id)
                        inner join usuario on (usu_id=sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre )
                        where (sol_estado<>'' and sol_estado not in ('Pendiente','BORRADOR'))
                        -- and (sol_sol_id is null or sol_sol_id='')
                        $andNegocio
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos

                        group by
                        sol_id,ret_id

                        order by cantidad desc
                        ) a
                        group by a.neg_descripcion  order by cantidad desc";


                    /*Fin Personas por negocio que han participado en solución */
                }elseif($grafica->rep_id==23){
                    /* Preguntas realizadas al experto por reto */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND com_descripcion IN ($strNegociosReto) ";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                   $sql = "select a.ret_nombre label ,
                        count(0) cantidad from (
                        select   ret_nombre  , count(ret_id) cantidad  FROM experto
                        inner join reto  on (ret_exp_id=exp_id)
                        inner join pregunta on (pre_ret_id=ret_id)
                        left join usuario on (usu_id=pre_usu_id)
                        inner join retoxnegocio on (rxn_ret_id=ret_id)
                        inner join compania on (rxn_com_id=com_id)
                        where pre_ret_id<>'' and pre_ret_id is not null
                        $andRetos
                        $andAnosReto
                        $andNegocio
                        $andEstado
                        GROUP BY pre_id,ret_id
                        ORDER BY cantidad DESC  ) as a group by label order by cantidad desc
                    " ;
                /* Fin Preguntas realizadas al experto por reto */
                }elseif($grafica->rep_id==25){
                    /* Personas inscritas por reto */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ) ";
                          $andNegocio.= $orNegocio;
                        }else{
                        $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }

                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }


                   $sql ="SELECT a.ret_nombre  label ,COUNT(0) AS cantidad FROM (
                        SELECT
                        ret_nombre,count(0)
                        FROM usuario
                        inner join solucion on (sol_usu_id=usu_id)
                        inner join reto on (ret_id=sol_ret_id)
                        INNER JOIN tyc ON (tyc_ret_id=ret_id and tyc_usu_id=sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre )
                        WHERE ret_id
                        $andEstado
                        $andNegocio
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos
                        group by  sol_usu_id,sol_ret_id )  a GROUP BY a.ret_nombre ORDER BY cantidad DESC";


                    /* Fin Personas inscritas por reto */
                }elseif($grafica->rep_id==26){
                    /* Soluciones enviadas por negocio */

                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       //$andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                       $andAnoSolucion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosSolucion)";

                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                       // $andAnoInscripcion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosInscripcion)";
                       $andAnoInscripcion =  " ";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }


                    $consulta="create temporary table tmpsolucionesNegocio
                        SELECT
                        if(neg_descripcion<>'',neg_descripcion,'Externo') as NEGOCIO_PARTICIPANTE,
                        ret_nombre as RETO,
                        usu_nombre as PARTICIPANTE ,
                        usu_email as EMAIL,
                        p.sol_estado as 'ESTADO',
                        if(p.sol_sol_id,(select sl.sol_titulo from solucion sl where sl.sol_id=p.sol_sol_id),sol_titulo) as SOLUCION
                        FROM solucion p
                        inner join reto on (ret_id=p.sol_ret_id)
                        inner join usuario on (usu_id=p.sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre )
                        where (p.sol_estado<>''
                        and p.sol_estado NOT IN ('Pendiente','BORRADOR'))
                        -- and (sol_sol_id is null or sol_sol_id='')
                        $andNegocio
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos

                    ;";
                    $db->retos->setQuery($consulta);
                    $db->retos->query();
                    $sql =" select
                    count(0) as cantidad ,
                    if(a.negocio is null,'Externos',a.negocio ) as 'label'
                    from
                    (
                    select count(NEGOCIO_PARTICIPANTE) as cantidad,NEGOCIO_PARTICIPANTE as negocio
                    FROM tmpsolucionesNegocio
                    WHERE ESTADO NOT IN ('Pendiente','BORRADOR')
                    group by SOLUCION,NEGOCIO_PARTICIPANTE
                    order by cantidad desc
                    ) a
                    group by a.negocio  order by cantidad desc";


                    /* Fin Soluciones enviadas por negocio */
                }elseif($grafica->rep_id==27){
                    /* Detalle participación por necogico */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       //$andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                       $andAnoSolucion =  "  ";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }

                  $sql="select concat('Retos ', a.companiareto) as label, count(0) as cantidad from ( SELECT
                      usu_id
                    ret_nombre,neg_descripcion,
                    (SELECT group_concat(com_nombre) FROM compania
                      inner join retoxnegocio on (rxn_com_id=com_id)
                      where rxn_ret_id=ret_id
                      group by rxn_ret_id) as companiareto,
                    ret_estado,
                    usu_nombre,
                    usu_email,
                    if(usu_compania<>'',usu_compania,'0') as campaniauser,
                    if(sol_titulo<>'',sol_titulo,'0') as titulo ,
                    (SELECT if(count(sl.sol_id)>0 ,
                    if(sl.sol_titulo<>'',sl.sol_titulo,'0'),'0'
                    ) from solucion sl where sl.sol_id=p.sol_sol_id) as invitado,
                    if(sol_estado='Finalizado','PENDIENTE DE APROBACION',if(sol_estado='Aprobada','SOLUCION GANADORA',if(sol_estado='Rechazada','RECHAZADA','BORRADOR'))) as estado,
                    sol_fechaEstado

                FROM reto
                    inner join solucion p on (sol_ret_id=ret_id)
                    inner join usuario on (usu_id=sol_usu_id)
                    left join negocio on (usu_compania=neg_nombre)
                    INNER JOIN tyc ON (tyc_ret_id=ret_id and tyc_usu_id=p.sol_usu_id)

                WHERE ret_id
                $andEstado
                $andNegocio
                $andAnoSolucion
                $andAnoInscripcion
                $andAnosReto
                $andRetos
                group by sol_usu_id,sol_ret_id
                ORDER BY usu_nombre
   ) a group by a.companiareto  order by cantidad desc";
                    /* Fin Detalle participación por necogico */
                }



                $db->retos->setQuery( $sql );
                $datosGrafica = $db->retos->loadObjectList();
                $newView->datosGrafica = $datosGrafica;
            }
            $tpl = 'grafica';
            $newView->display($tpl);
        }elseif($task=='generarReporte'){
            self::registraLog($usuario->usu_id,$grafica->rep_nombre,'excel');
            /** recupera los filtros **/
            $strEstadoRetos = self::getFiltroEstadoReto();
            $strNegociosReto = self::getFiltroNegocioReto();
            $strAnosSolucion = self::getFiltroAnoSolucion();
            $strAnosInscripcion = self::getFiltroAnoInscripcion();
            $strAnosReto = self::getFiltranoReto();
            $strIntRetos = self::getFiltroReto();
            /** fin recupera los filtros **/
            if($grafica->rep_id){
                $newView->grafica = $grafica;

                if($grafica->rep_id==21){ /* Participantes por negocio */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                    $consulta ="SELECT
                            CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                            CONCAT_WS('</th><th>',
                            'PARTICIPANTE'
                            ,'EMPRESA'
                            ,'NEGOCIO'
                            ,'EMAIL'
                            ,'RETO'
                            ,'FECHA_INSCRIPCION'


                            ), '</th></tr>')
                            UNION ALL
                            (
                            SELECT
                            CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                                                CONCAT_WS('</td><td>',
                            usu_nombre,if(usu_compania<>'',usu_compania,0),IF(neg_descripcion<>'',neg_descripcion,'externo'),usu_email,ret_nombre,tyc_fecha
                            ), '</td></tr>')
                            FROM usuario
                            left join negocio on (usu_compania=neg_nombre)
                            inner join solucion on (usu_id=sol_usu_id)
                            INNER JOIN reto on (ret_id=sol_ret_id)
                            INNER JOIN tyc ON (tyc_ret_id=ret_id and tyc_usu_id=sol_usu_id)
                            WHERE sol_ret_id
                            $andNegocio
                            $andEstado
                            $andAnoSolucion
                            $andAnoInscripcion
                            $andAnosReto
                            $andRetos
                            group by ret_id,usu_compania,usu_nombre  LIMIT 100000000
                            )
                            UNION ALL
                            SELECT '</table>'";

                    /* fin Participantes por negocio */
                }elseif($grafica->rep_id==22){
                    /*Personas por negocio que han participado en solución */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND ( neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        //$andAnoInscripcion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosInscripcion)";
                        $andAnoInscripcion =  " ";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                    $consulta = "SELECT
                        CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                        CONCAT_WS('</th><th>',
                                        'NEGOCIO_PARTICIPANTE'
                                        ,'RETO'
                                        , 'PARTICIPANTE'
                                        , 'EMAIL'
                                        , 'SOLUCION'
                                        , 'FECHA_SOLUCION'


                                        ), '</th></tr>')
                        UNION ALL
                        (
                        SELECT
                        CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                        CONCAT_WS('</td><td>',

                        if(neg_descripcion<>'',neg_descripcion,'Externo'),ret_nombre,usu_nombre,usu_email,

                        if(p.sol_sol_id,(select sl.sol_titulo from solucion sl where sl.sol_id=p.sol_sol_id),sol_titulo),p.sol_fechaCreacion


                        ), '</td></tr>')
                        FROM solucion p
                        inner join reto on (ret_id=p.sol_ret_id)
                        inner join usuario on (usu_id=p.sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre  )
                        where (p.sol_estado<>'' and p.sol_estado not in ('Pendiente','BORRADOR')) -- and (sol_sol_id is null or sol_sol_id='')
                        $andNegocio
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos
                        group by
                        p.sol_id,ret_id

                        LIMIT 100000000
                        )
                        UNION ALL
                        SELECT '</table>'";

                    $grafica->rep_nombre = str_replace('solución', 'solucion', $grafica->rep_nombre);

                    /*Fin Personas por negocio que han participado en solución */
                }elseif($grafica->rep_id==23){
                    /* Preguntas realizadas al experto por reto */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND com_descripcion IN ($strNegociosReto) ";
                    }

                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }

                   $consulta="SELECT
                                    CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                                    CONCAT_WS('</th><th>',
                                    'NEGOCIO_EXPERTO'
                                    ,'TITULO'
                                    , 'DESCRIPCION'
                                    , 'EXPERTO'
                                    , 'EMAIL'
                                    ,'PARTICIPANTE'
                                    ,'EMAIL PARTICIPANTE'
                                    ,'RETO'
                                    ,'FECHA'
                                    ,'NEGOCIO_RETO'
                                    , 'RESPUESTA'
                                    ), '</th></tr>')
                                    UNION ALL
                                    (
                                    SELECT
                                    CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                                    CONCAT_WS('</td><td>',
                                    if(exp_companiaExte<>'',exp_companiaExte,'Externo'),
                                    pre_titulo,
                                    pre_descipcion,
                                    exp_nombre,
                                    exp_mail,
                                    if(usu_nombre<>'',usu_nombre,0),
                                    if(usu_email<>'',usu_email, 0),
                                    ret_nombre,
                                    pre_fechaCreacion,
                                    (SELECT group_concat(com_nombre) FROM compania
                                        inner join retoxnegocio on (rxn_com_id=com_id)
                                        where rxn_ret_id=ret_id
                                        group by rxn_ret_id) ,
                                    (SELECT group_concat(res_fecha,res_respuesta) FROM respuesta
                                                where res_pre_id=pre_id
                                                group by res_pre_id)
                                    ), '</td></tr>')
                                    FROM experto
                                    inner join reto on (ret_exp_id=exp_id)
                                    inner join pregunta on (pre_ret_id=ret_id)
                                    left join usuario on (usu_id=pre_usu_id)
                                    inner join retoxnegocio on (rxn_ret_id=ret_id)
                                    inner join compania on (rxn_com_id=com_id)
                                    where pre_ret_id<>'' and pre_ret_id is not null

                                    $andRetos
                                    $andAnosReto
                                    $andNegocio
                                    $andEstado
                                    group by pre_id
                                    order by ret_id

                                    LIMIT 100000000
                                    )
                                    UNION ALL
                                    SELECT '</table>'";
                    /* Fin Preguntas realizadas al experto por reto */
                }elseif($grafica->rep_id==24){
                    /* Reporte consolidado */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                    $consulta =" SELECT
                            CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                            CONCAT_WS('</th><th>',
                            'NOMBRE RETO'
                            ,'NEGOCIO RETO'
                            ,'ESTADO RETO'
                            ,'NOMBRE USUARIO'
                            ,'EMAIL'
                            ,'CEDULA'
                            ,'COMPANIA DEL PROPONENTE'
                            ,'NEGOCIO DEL PROPONENTE'
                            ,'CODIGO DE LA SOLUCION'
                            ,'INVITADO A LA SOLUCION'
                            ,'ESTADO SOLUCION'
                            ,'FECHA SOLUCION'
                            ,'FECHA MODIFICACION'
                        ), '</th></tr>')
                            UNION ALL
                            (
                            SELECT
                                CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                                CONCAT_WS('</td><td>',
                                ret_nombre,
                                (SELECT group_concat(com_nombre) FROM compania
                                    inner join retoxnegocio on (rxn_com_id=com_id)
                                    where rxn_ret_id=ret_id
                                    group by rxn_ret_id),
                                ret_estado,
                                usu_nombre,
                                usu_email,
                                if(usu_cedula<>'',usu_cedula,'0'),
                                if(usu_compania<>'',usu_compania,'0'),
                                (Select if(COUNT(neg_descripcion)> 0,neg_descripcion ,'externo') from negocio  where neg_nombre=usu_compania limit 1),
                                if(sol_titulo<>'',sol_titulo,(SELECT if(count(sl.sol_id)>0,
                                if(sl.sol_titulo<>'',sl.sol_titulo,'0'),'0'
                                ) from solucion sl where sl.sol_id=p.sol_sol_id)) ,
                                (SELECT if(count(sl.sol_id)>0,
                                if(sl.sol_titulo<>'',sl.sol_titulo,'0'),'0'
                                ) from solucion sl where sl.sol_id=p.sol_sol_id),
                                if(
                                    sol_estado='Finalizado','PENDIENTE DE APROBACION',
                                    if(sol_estado='Aprobada','SOLUCION GANADORA',
                                    if(sol_estado='Rechazada','RECHAZADA',
                                        if(sol_titulo is null and sol_estado in ('Pendiente') and sol_sol_id is null,'INSCRITO', 'BORRADOR')
                                        )
                                    )
                                ),

                                sol_fechaCreacion
                                ,sol_fechaEstado
                                ), '</td></tr>')
                            FROM reto
                                inner join solucion p on (sol_ret_id=ret_id)
                                inner join usuario on (usu_id=sol_usu_id),
                                (SELECT @rn:=false) num
                            WHERE ret_id
                            $andEstado
                            $andAnoSolucion
                            $andAnoInscripcion
                            $andAnosReto
                            $andRetos

                            ORDER BY usu_nombre
                            LIMIT 100000000
                            )
                            UNION ALL
                            SELECT '</table>'";
                    /* fin Reporte consolidado */
                }elseif($grafica->rep_id==25){
                    /* Personas inscritas por reto */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL ) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = "  ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(tyc_fecha) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                   $consulta="SELECT
                        CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                        CONCAT_WS('</th><th>',
                                  'RETO'
                                  ,'NEGOCIO_RETO'
                                  , 'FECHA_DE-APERTURA'
                                  ,'FECHA_DE_CIERRE'
                                  ,'DURACION'
                                  ,'ESTADO'
                                  ,'PUNTOS'
                                  ,'NOMBRE_PERSONA_INSCRITA'
                                  ,'COMPAÑIA_DEL_INSCRITO'
                                  ,'NEGOCIO_DEL_INSCRITO'
                                  ,'EXTERNO/INTERNO'
                                  ,'CEDULA'
                                  ,'REGIONAL'
                                  ,'EMAIL'
                                  ,'FECHA_ACEPTO_TYC'
                                  ,'FECHA_INSCRIPCION'

                        ), '</th></tr>')
                            UNION ALL
                        (
                        SELECT
                        CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                        CONCAT_WS('</td><td>',
                        -- com_nombre,
                        ret_nombre,
                        (SELECT group_concat(com_nombre) FROM compania
                        inner join retoxnegocio on (rxn_com_id=com_id)
                        where rxn_ret_id=ret_id
                        group by rxn_ret_id),
                        ret_fechaPublicacion,
                        ret_fechaVigencia,
                        ROUND(DATEDIFF(ret_fechaVigencia, ret_fechaPublicacion)/30),
                        ret_estado,
                        ret_puntos,
                        usu_nombre,
                        if(usu_compania<>'',usu_compania,0),
                        (Select if(COUNT(neg_descripcion)> 0,neg_descripcion ,'externo') from negocio  where neg_nombre=usu_compania limit 1),
                        (Select IF(COUNT(neg_descripcion)> 0 ,'Interno','Externo') from negocio  where neg_nombre=usu_compania limit 1),
                        if(usu_cedula>0,usu_cedula,0),
                        if(usu_regional is not null,if(usu_regional<>'',usu_regional,0),0),
                        usu_email,
                        (SELECT if(tyc_fecha is not null,tyc_fecha,sol_fechaCreacion) FROM tyc where tyc_usu_id=usu_id and tyc_ret_id=ret_id LIMIT 1),
                        sol_fechaCreacion

                        ), '</td></tr>')
                        FROM usuario
                        inner join solucion on (sol_usu_id=usu_id)
                        inner join reto on (ret_id=sol_ret_id)
                        INNER JOIN tyc ON (tyc_ret_id=ret_id and tyc_usu_id=sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre $andNegocio )

                        -- inner join retoxnegocio on (ret_id=rxn_ret_id)
                        -- inner join compania on (rxn_com_id=com_id $andNegocio)
                        WHERE ret_id
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos

                        group by sol_usu_id, sol_ret_id order by ret_id LIMIT 100000000
                        )
                        UNION ALL
                        SELECT '</table>' ";

                    /* Personas inscritas por reto */

                }elseif($grafica->rep_id==26){
                    /* Soluciones enviadas por negocio */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       //$andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                       $andAnoSolucion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        //$andAnoInscripcion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosInscripcion)";
                        $andAnoInscripcion =  " ";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }

                   $sql="create temporary table tmpsolucionesNegocioD
                        SELECT
                        if(usu_compania<>'',usu_compania,0) as COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        if(neg_descripcion<>'',neg_descripcion,'Externo') as NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        if(p.sol_sol_id,(select sl.sol_titulo from solucion sl where sl.sol_id=p.sol_sol_id),sol_titulo) as CODIGO_DE_LA_SOLUCION,
                        if(sol_estado='Finalizado','PENDIENTE DE APROBACION',if(sol_estado='Aprobada','SOLUCION GANADORA',if(sol_estado='Rechazada','RECHAZADA','BORRADOR'))) as ESTADO_DE_LA_SOLUCION,
                        sol_fechaCreacion as  FECHA_DE_CREACION_DE_LA_SOLUCION,
                        sol_fechaEstado as  FECHA_DE_ENVIO_DE_LA_SOLUCION,
                        usu_nombre as NOMBRE_PERSONA_QUE_ENVIO_SOLUCION,
                        ret_nombre as RETO_AL_QUE_ENVIO_LA_SOLUCION,
                        (SELECT group_concat(com_nombre) FROM compania
                        inner join retoxnegocio on (rxn_com_id=com_id)
                        where rxn_ret_id=ret_id
                        group by rxn_ret_id) as NEGOCIO_DEL_RETO
                        FROM solucion p
                        inner join reto on (ret_id=p.sol_ret_id)
                        inner join usuario on (usu_id=p.sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre  )
                        where (p.sol_estado<>''
                        and p.sol_estado NOT IN ('Pendiente','BORRADOR'))
                        -- and (sol_sol_id is null or sol_sol_id='')
                        $andNegocio
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos
                        ;";
                    $db->retos->setQuery($sql);
                    $db->retos->query();
                    //exit($sql);
                    $consulta="SELECT
                        CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \charset=UTF-8 \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                        CONCAT_WS('</th><th>',
                                    'COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION',
                                    'NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION',
                                    'CODIGO_DE_LA_SOLUCION',
                                    'ESTADO_DE_LA_SOLUCION',
                                    'FECHA_DE_CREACION_DE_LA_SOLUCION',
                                    'FECHA_DE_ENVIO_DE_LA_SOLUCION',
                                    'NOMBRE_PERSONA_QUE_ENVIO_SOLUCION',
                                    'RETO_AL_QUE_ENVIO_LA_SOLUCION',
                                    'NEGOCIO_DEL_RETO'

                                    ), '</th></tr>')
                                UNION ALL
                                (
                    SELECT
                    CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                    CONCAT_WS('</td><td>',

                    COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                    NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                    CODIGO_DE_LA_SOLUCION,
                    ESTADO_DE_LA_SOLUCION,
                    FECHA_DE_CREACION_DE_LA_SOLUCION,
                    FECHA_DE_CREACION_DE_LA_SOLUCION,
                    -- FECHA_DE_ENVIO_DE_LA_SOLUCION,
                    NOMBRE_PERSONA_QUE_ENVIO_SOLUCION,
                    RETO_AL_QUE_ENVIO_LA_SOLUCION,
                    NEGOCIO_DEL_RETO
                    ), '</td></tr>')
                    FROM tmpsolucionesNegocioD WHERE ESTADO_DE_LA_SOLUCION NOT IN ('BORRADOR')
                    group by CODIGO_DE_LA_SOLUCION,NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION order by CODIGO_DE_LA_SOLUCION
                    LIMIT 100000000
                    )
                    UNION ALL
                    SELECT '</table>'";
                    /* fin Soluciones enviadas por negocio*/
                }elseif($grafica->rep_id==27){
                    /* Detalle participación por negocio */
                    $andEstado = '';
                    if($strEstadoRetos){
                        $andEstado = " AND ret_estado IN ($strEstadoRetos)";
                    }
                    $andNegocio = '';
                    if($strNegociosReto){
                        $andNegocio = " AND (neg_descripcion IN ($strNegociosReto) ";
                        $arrNeg =  explode(",", $strNegociosReto);
                        $externo = array_search("'Externo'", $arrNeg);
                        if($externo!==false){
                          $orNegocio = " OR neg_descripcion IS NULL) ";
                          $andNegocio.= $orNegocio;
                        }else{
                          $orNegocio = " ) ";
                          $andNegocio.= $orNegocio;
                        }
                    }
                    $andAnoSolucion = '';
                    if($strAnosSolucion){
                       $andAnoSolucion =  " AND YEAR(sol_fechaEstado) IN ($strAnosSolucion)";
                    }
                    $andAnoInscripcion = '';
                    if($strAnosInscripcion){
                        $andAnoInscripcion =  " AND YEAR(sol_fechaCreacion) IN ($strAnosInscripcion)";
                    }
                    $andAnosReto = '';
                    if($strAnosReto){
                        $andAnosReto = " AND YEAR(ret_fechaPublicacion) IN ($strAnosReto)" ;
                    }
                    $andRetos = '';
                    if($strIntRetos){
                        $strIntRetos = str_replace("'DefectValue'", '0', $strIntRetos);
                        $andRetos = " AND ret_id IN ($strIntRetos)" ;
                    }
                    $consulta="SELECT CONCAT('<meta http-equiv=\"Content-Type\" content=\"text/html; \" /><style type=\"text/css\">body{font-family: sans-serif;}table{border: 1px solid black;empty-cells: show;font-size: 12px;}th{background-color: #BCBCBC;border: 1px solid black;white-space:nowrap;}.titulo{background-color: #BCBCBC;}td{border: 1px solid black;white-space:nowrap;}.total{background-color: #A9D0F5;}.trImpar{background-color: #EEEEEE;}</style><table><tr><th>',
                    CONCAT_WS('</th><th>',
                    'NEGOCIO_DEL_RETO'
                    , 'RETO'
                    ,'PARTICIPANTE'
                    ,'EMAIL'
                    , 'COMPAÑIA'
                    ), '</th></tr>')
                    UNION ALL
                    (
                    SELECT
                    CONCAT('<tr', IF(@rn:=!@rn, ' class=\"trImpar\"', ''), '><td>',
                    CONCAT_WS('</td><td>',
                    (SELECT group_concat(com_nombre) FROM compania
                    inner join retoxnegocio on (rxn_com_id=com_id)
                    where rxn_ret_id=ret_id
                    group by rxn_ret_id),
                    ret_nombre,usu_nombre,usu_email,if(usu_compania<>'',usu_compania,'0')
                    ), '</td></tr>')


                    FROM reto
                        inner join solucion p on (sol_ret_id=ret_id)
                        inner join usuario on (usu_id=sol_usu_id)
                        left join negocio on (usu_compania=neg_nombre)
                        WHERE ret_id
                        $andEstado
                        $andAnoSolucion
                        $andAnoInscripcion
                        $andAnosReto
                        $andRetos
                        $andNegocio

                        group by sol_usu_id,sol_ret_id
                    ORDER BY ret_id
                    LIMIT 100000000
                    )
                    UNION ALL
                    SELECT '</table>'
";
                 $grafica->rep_nombre = str_replace('participación', 'participacion', $grafica->rep_nombre);
                    /* Fin Detalle participación por negocio */
                }

                $config = JFactory::getConfig();
                $rutaReporte = $config->get('root_report');
                $nombreArchivo = "$grafica->rep_nombre". '_' . date('Ymd_His') . '.xls';
                $consulta.=" INTO OUTFILE '{$rutaReporte}{$nombreArchivo}'";
                //echo $consulta;exit;
                $db->retos->setQuery($consulta);
                $db->retos->query();
                $rutaReporte = $config->get('root_report_download');
                $archivo = $rutaReporte . $nombreArchivo;
                echo "<script> window.location = 'index.php?option=com_graphic&view=graphic&Itemid=629'; </script>";
                header("Location: " . $rutaReporte . $nombreArchivo);




            }



        }elseif($task=='excel'){
            $rutaReporte = 'http://us.orb.cognox.com/htdocs/retos/Reportes/';
            $nombreArchivo = 'prueba.'.'xls';
            echo "<script> window.location = 'index.php?option=com_graphic&task=filtros&rep_id=$rep_id&Itemid=629'; </script>";
            header("Location: " . $rutaReporte . $nombreArchivo);
        }else{
            if(!$grafica->rep_id){
                $filtos = 'home';
                $grafica->rep_nombre = 'HOME';
            }else{
                $filtos = 'filtros';
            }
            self::registraLog($usuario->usu_id,$grafica->rep_nombre,$filtos);
            $newView = $this->getView('graphic','html');
            $rep_id  = JRequest::getVar('rep_id');
            if($rep_id){
                $grafica = new grafica($rep_id);
            }
            /** recupera los filtros **/
            $strEstadoRetos = self::getFiltroEstadoReto();
            //$strEstadoRetos = '';
            $strNegociosReto = self::getFiltroNegocioReto();
            $strAnosSolucion = self::getFiltroAnoSolucion();
            $strAnosInscripcion = self::getFiltroAnoInscripcion();
            $strAnosReto = self::getFiltranoReto();
            $strIntRetos = self::getFiltroReto();
            /** fin recupera los filtros **/

            $retos = new reto();
            $anoPublicacion=$retos->cargarAnoPublicacion();
            $anoSolucion=$retos->cargarAnoSolucion();
            $externo = true;
            $negocios=$retos->cargarNegocios($externo);
            $anoInscripcion = $retos->cargarAnoInscripcion();
            $obj = $retos->listarRetosReporte($strEstadoRetos, $strNegociosReto, $strAnosSolucion, $strAnosReto);
            $newView->obj = $obj;
            $newView->anoPublicacion = $anoPublicacion;
            $newView->anoSolucion = $anoSolucion;
            $newView->anoInscripcion = $anoInscripcion;

            $newView->negocios = $negocios;
            if($grafica->rep_id){
                $newView->grafica = $grafica;
            }
            /** pasa los filtros **/
            $newView->flEstadosReto = $strEstadoRetos;
            $newView->flNegociosReto = $strNegociosReto;
            $newView->flAnosSolucion = $strAnosSolucion ;
            $newView->flAnosLanzamiento =   $strAnosReto;
            $newView->flAnosInscripcion=   $strAnosInscripcion;
            $newView->flIntRetos =   $strIntRetos;
            /** fin pasa los filtros **/
            $newView->display();
        }


    }

    function getFiltroEstadoReto(){
        $strEstadoRetos= JRequest::getVar('estadoReto');
        if($strEstadoRetos){
            $strEstadoRetos = implode(",",$strEstadoRetos);
        }else{
            //$strEstadoRetos ="'Vigente','Cerrada'";
            $strEstadoRetos = '';

        }
        return $strEstadoRetos;
    }

    function getFiltroNegocioReto(){
        $negociosReto= JRequest::getVar('negocioReto');
        if($negociosReto){
            $strNegociosReto = implode(",",$negociosReto);
        }else{
            $strNegociosReto = '';
        }
        return $strNegociosReto;
    }

    function getFiltroAnoSolucion(){
        $anosSolucion= JRequest::getVar('anoSolucion');
        if($anosSolucion){
            $strAnosSolucion = implode(",",$anosSolucion);
        }else{
            $strAnosSolucion = '';
        }
        return $strAnosSolucion;
    }

    function getFiltroAnoInscripcion(){
        $anosInscripcion= JRequest::getVar('anoInscripcion');
        if($anosInscripcion){
            $strAnosInscripcion = implode(",",$anosInscripcion);
        }else{
            $strAnosInscripcion = '';
        }
        return $strAnosInscripcion;
    }

    function getFiltranoReto(){
        $anosReto= JRequest::getVar('anoReto');
        if($anosReto){
            $strAnosReto = implode(",",$anosReto);
        }else{
            $strAnosReto = '';
        }
        return $strAnosReto;
    }

    function getFiltroReto(){
        $intRetos= JRequest::getVar('retid');
        if($intRetos){
            $strIntRetos = implode(",",$intRetos);
        }else{
            $strIntRetos = '';
        }
        return $strIntRetos;
    }

    function registraLog ($usu_id,$reporte,$tipo){
        $db = JFactory::getDBO();
        $logreporte = new stdClass();
        $logreporte->lrp_usu_id = $usu_id;
        $logreporte->lrp_reporte = $reporte;
        $logreporte->lrp_tipo = $tipo;
        $db->retos->insertObject('logreporte',$logreporte,'lrp_id');
        return true;
    }



}
