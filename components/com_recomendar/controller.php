<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

class RecomendarController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false) {
		$view = JFactory::getApplication()->input->get( 'view' );
		$task = JFactory::getApplication()->input->get( 'task' );

		if($task=='recomendarAmigo')
		{
	        $this->execute('recomendarAmigo');
	    }else
			{
                $user 						= JFactory::getUser();
	        $usuario 					= new usuario($user->id);
	        $ret_id 					= JFactory::getApplication()->input->get( 'idRet' );
	        $reto 						= new reto($ret_id);
	        $newView 					=  $this->getView('recomendar','html');
	        $newView->obj 		= $reto;
	        $newView->usuario = $usuario;
	        $newView->display();
	    }
	}


    function recomendarAmigo(){
      $user 					= JFactory::getUser();
      $usuario 				= new usuario($user->id);
      $rec_nombre 		= JFactory::getApplication()->input->get( 'rec_nombre' );
      $rec_mail 			= JRequest::getVar( 'rec_mail' );
      $ret_comentario	= JFactory::getApplication()->input->get( 'ret_comentario' );
      $idRet					= JFactory::getApplication()->input->get( 'idRet' );
      $reto 					= new reto($idRet);

      if($rec_nombre && $rec_mail && $idRet)
			{
      	  $configMail = JFactory::getConfig();
          $sitio= JURI::base() ;
          $cuerpo			= "<img src='$sitio/images/cabezote_correo.png' width='100%'>";
      	  $cuerpo			.= "Hola, $rec_nombre. </br></br>";
          $cuerpo			.= "$user->name te  ha  recomendado visitar la página de Soluciones</br>";
          $cuerpo			.= "Innovadoras para que conozcas el reto ";
          $cuerpo			.= "<a href='$sitio/index.php?option=com_retos&view=reto&idreto=$idRet&Itemid=444'>$reto->ret_nombre</a>.</br></br>";

          if($ret_comentario)
					{
            $cuerpo.= "Comentario: $ret_comentario</br></br>";
          }
          $cuerpo.= "Esperamos que muy pronto seas uno de nuestros solucionadores.</br></br>";
          $cuerpo.="Soluciones Innovadoras</br>";

          $from 		= $configMail->get('mailfrom');
          $fromname = $configMail->get('fromname');
          $correos 	= "$rec_mail";


          $subject 	= JText::_('Te han recomendado un reto.');
          // JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            $mail = JFactory::getMailer();
            $mail->addRecipient($from);
            $mail->addRecipient($correos);
            $mail->setSender(array($from, $fromname));
            $mail->setSubject($subject);
            $mail->setBody($cuerpo);
            $mail->IsHTML(true);

            $sent = $mail->Send();

            if ( $sent !== true ) {
                echo 'Error sending email: ' . $sent->__toString();
            } else {
                echo 'Mail sent';
            }
                JError::raiseNotice(null, JText::_('Te han recomendado un reto.'));

          $js="<script>";
          //$js.="alert('Recomendación enviada satisfactoriamente');";
          $js.="parent.location.reload();";
          $js.="</script>";
          echo $js;
          exit;
      }else{
          $js="<script>";
          //$js.="alert('No fue posible enviar la recomendación');";
          $js.="parent.location.reload();";
          $js.="</script>";
          echo $js;
          exit;
      }
  }
}
