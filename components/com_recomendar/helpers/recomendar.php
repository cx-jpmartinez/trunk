<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Class AreainteresFrontendHelper
 *
 * @since  1.6
 */
class RecomendarFrontendHelper
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_recomendar/models/' . strtolower($name) . '.php'))
		{
			$model = JModelLegacy::getInstance($name, 'RecomendarModel');
		}

		return $model;
	}
}
