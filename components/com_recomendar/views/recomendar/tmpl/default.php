<?php
    defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<link rel="stylesheet" type="text/css" href="_objects/css/redmond/jquery-ui-1.8.16.custom.css" />
<script src="_objects/scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('#rec_nombre').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#rec_mail').val(arr[1]);
            if(arr[1]!=''){
                $('#rec_mail').attr('readonly',true);    
            }
            
        }
      });
});

function validarRec (){
    form = document.forms.recomendarFrm;
    if($("#rec_nombre").val().length <= 0) {  
        alert("El campo nombre es requerido");  
        return false;  
    }  
    if($("#rec_mail").val().length <= 0) {  
        alert("El campo E-mail es requerido");  
        return false;  
    }  
    if($("#rec_mail").val().indexOf('@', 0) == -1 || $("#rec_mail").val().indexOf('.', 0) == -1) {  
        alert("El E-mail es incorrecto");  
        return false;  
    }  
    form.submit();
}


</script>
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="padding-top: 1px !important;">Recomendar a un Amigo</h3>
    </div>
</div>
<div id="cont_crear_solucion">
	<div id="crear_solucion">
	    <h4>&#9658;<?php echo $reto->ret_nombre; ?></h4>
		<form action="index.php" method="post" name="recomendarFrm" id="recomendarFrm" class="recomendarFrm"> 
			<h5 id="compartida"><?php echo JText::_('Recomendar a un Amigo');?></h5>
			<div class="cont_campos">
			    <label><?php echo JText::_('COM_RECOMENDAR_LABEL_NOMBRE');?>:</label>
			    <input type="text" name="rec_nombre" id="rec_nombre"/>
			</div>
			<div class="cont_campos">
			    <label><?php echo JText::_('COM_RECOMENDAR_LABEL_EMAIL');?>:</label>
			    <input type="text" name="rec_mail" id="rec_mail"/>
			</div>
			<div class="cont_campos">
			   <label><?php echo JText::_('COM_RECOMENDAR_LABEL_COMENTARIO');?>:</label>
			   <textarea rows="5" cols="50" name="ret_comentario"></textarea>
			</div>
			<div id="acepto">
			    <input type="button" value="Enviar" id="Aceptar" onclick="validarRec();">
			    <input type="hidden" name="task" value="recomendarAmigo" />
			    <input type="hidden" name="option" value="com_recomendar" />
			    <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>" />
			    <input type="hidden" name="enviarAmigo" value="1"/>
			</div>
		</form>
	</div>
</div>