<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Retos
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Retos', JPATH_COMPONENT);

// Execute the task.
$controller = JControllerLegacy::getInstance('Retos');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
