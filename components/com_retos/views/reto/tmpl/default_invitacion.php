<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$solCompartida = new solucioncompartida();
$objCompartidas = $solCompartida->cargarSolucionesCompartidas($usuario->usu_email,$reto->ret_id);
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<link rel="stylesheet" type="text/css" href="_objects/css/redmond/jquery-ui-1.8.16.custom.css" />
<script src="_objects/scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery-ui.js" type="text/javascript"></script>

<div id="cont_crear_solucion">
	<div id="crear_solucion">
	    <h4>&#9658;<?php echo $reto->ret_nombre; ?></h4>
            <form action="index.php" method="GET" name="aceptarInvitacionFrm" id="aceptarInvitacionFrm" class="aceptarInvitacionFrm" target="ifrSuplantacion"> 
			<h5 id="compartida"><?php echo JText::_('Invitaciones Pendientes');?></h5>
                        </br>
                        <table id="background-contac" style="width: 100%;  padding: 0px; border-collapse: separate;margin: 0;">
                            <tr style="background-color: #9A4D9E;">
                                <th style="text-align: center;font-size: 13px;border-spacing: 0;color: #fff;">Código Solución</th>
                                <th style="text-align: center;font-size: 13px;border-spacing: 0;color: #fff;">Nombre</th>
                                <th style="text-align: center;font-size: 13px;border-spacing: 0;color: #fff;">E-mail</th>
                                <th style="text-align: center;font-size: 13px;border-spacing: 0;color: #fff;">Opciones</th>
                            </tr>
                            <?php 
                                foreach ($objCompartidas as $objSol) { 
                                   $usuarioInivita =  getUsuarioSolucion($objSol->slc_sol_id);
                                    
                            ?>
                                <tr>
                                    <td style="text-align: center;border-spacing: 0;"><?php echo $objSol->sol_titulo; ?></td>
                                    <td style="text-align: center;border-spacing: 0;" ><?php echo $usuarioInivita->usu_nombre; ?></td>
                                    <td style="text-align: center;border-spacing: 0;"><?php echo $usuarioInivita->usu_email; ?></td>
                                    <td style="text-align: center;border-spacing: 0;">
                                        <select name="estado[<?php echo $objSol->slc_id ?>]" id="estado[<?php echo $objSol->slc_id ?>]" style="width: 100px;">
                                            <option value="">Seleccione....</option>
                                            <option value="Aceptar">Aceptar</option>
                                            <option value="Rechazar">Rechazar</option>
                                            
                                        </select>    
                                    </td>
                                   
                                </tr>
                            <?php } ?>
                        </table>
                        <br>
                        <div id="acepto">
                            <input type="submit" value="Aceptar" id="Aceptar">
			    <input type="hidden" name="task" value="aceptarInvitacion" />
			    <input type="hidden" name="option" value="com_retos" />
			    <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>" />
			</div>
		</form>
	</div>
</div>