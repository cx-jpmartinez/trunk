<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
//$order   = array("\r\n", "\n", "\r");
//$p = array("<p>", "</p>");

$usuario = $this->usuario;
$msFavorito = 'Agregar a Favoritos';
if($reto->_favorito){
    $msFavorito = 'Ver Favoritos';
}
if($reto->ret_galeria){
    $GLOBALS['galeriaRetos'] = $reto->ret_galeria;  
}
if(isset($this->solucion)){
    $solucion= $this->solucion;
}
$fechaActual = date('Y-m-d H:i');
$editarReto = false;
if(($fechaActual >= $reto->ret_fechaCreacion && $fechaActual<=$reto->ret_fechaVigencia) &&  $reto->ret_estado='Vigente'){
    $editarReto = true;
}
?>
<div id="breadcrumb" class="pathway">
   <a href="index.php?option=com_home&id=2&Itemid=530">Home</a> - 
   <a href="index.php?option=com_retos&view=retos&id=2&Itemid=444">Lista de retos</a> -
   <strong><?php  echo $reto->ret_nombre; ?></strong>
</div>
<div id="cont_reto_detalle" style="margin-top: 0px !important;">
   <div id="titulo_retos">
    <h3 class="titulo_reto" style="font-size:12px !important;font-weight: bold;">
        <?php  echo $reto->ret_nombre; ?></h3>
    <a class="button" href="index.php?option=com_retos&view=retos&id=2&Itemid=444"><?php echo JText::_('Atrás');?></a>
   </div>
   <?php if($reto->ret_imagen){
        if (file_exists("./images/retos/$reto->ret_imagen")) {
    ?>
        <img src="images/retos/<?php echo $reto->ret_imagen; ?>" alt="<?php  echo $reto->ret_nombre; ?>" width="200px" style="float: left;">
    <?php 
        }
    } 
   ?>
   <p>
      <?php 
        if($usuario->usu_id>0){
            if($reto->_solucion && $reto->_tycusuario>0){ ?>
               <?php 
                   if(isset($reto->_categoria)){ ?>
                        <br /><br /><span style="font-weight: bold;  font-family: 'Arial';">Categoría:</span> 
                        <?php echo $reto->_categoria; 
                    }
                    if(isset($reto->_negocios)){ ?><br> 
                       <span style="font-weight: bold;  font-family: 'Arial';">Negocio:</span>
                       <?php  echo "$reto->_negocios"; 
                    }
                    if($reto->_areasInteres){?><br>
                       <span style="font-weight: bold;  font-family: 'Arial';">Área de interés: </span>
                       <?php echo $reto->_areasInteres; ?> <br><br>
                   <?php
                   }
               }
           }
        ?>
        <?php
           if((!isset($reto->_negocios) || !isset($reto->_areasInteres)) && isset($reto->_categoria)){
               echo '<br><br>'; 
           } 
           echo $reto->ret_resumen; 
         ?>
   </p>
    <?php 
       if($usuario->usu_id>0){
          if($reto->_solucion && $reto->_tycusuario>0){ ?>
            <p>
                 <span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('COM_RETOS_DESCRIPCION');  ?></span>
               <?php echo $reto->ret_descripcion; ?>
               <?php 
               /*
                if($reto->ret_imgDetalle){
                    if (file_exists("./images/retos/$reto->ret_imgDetalle")) {
                ?>
                        <img src="images/retos/<?php echo $reto->ret_imgDetalle; ?>" alt="<?php  echo $reto->ret_imgDetalle; ?>">
                <?php 
                    }
                } */
               ?>
            </p>
               <?php if($reto->ret_antecedente  &&  strlen($reto->ret_antecedente)>8){ ?>
                    <p>
                       <span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('Antecedentes:');  ?></span>
                       <br/>
                       <?php  echo $reto->ret_antecedente;
                       ?></p>
               <?php } ?>
               <?php if($reto->ret_resultadoEsperado  &&  strlen($reto->ret_resultadoEsperado)>8){ ?>
                   <p><span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('COM_RETOS_RESULTADO_ESPERADO'); ?></span>
                   <br/><?php echo $reto->ret_resultadoEsperado; ?></p>
               <?php } ?>
               <?php if($reto->ret_nosebusca &&  strlen($reto->ret_nosebusca)>8){ ?>
                   <p><span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('Información Complementaria:'); ?></span>
                   <br/><?php  echo $reto->ret_nosebusca;?></p>
               <?php } ?>
               <?php if($reto->ret_criterioEvaluacion  &&  strlen($reto->ret_criterioEvaluacion)>8){ ?>
                   <p><span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('Criterios de evaluación:'); ?></span>
                   <br/><?php  echo $reto->ret_criterioEvaluacion;?></p>
               <?php } ?>
               <?php if($reto->ret_objetivo  &&  strlen($reto->ret_objetivo)>8){ ?>
                   <p><span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('Objetivos Estratégicos:'); ?></span>
                   <br/><?php  echo $reto->ret_objetivo;?></p>
               <?php } ?>
               <?php /* if($reto->ret_anexos){ ?> 
                   <p><span style="font-weight: bold;  font-family: '';"><?php echo JText::_('Descarga el formato para ingresar tu solución'); ?></span> 
                   <a target="blank" href="images/retos/anexos/<?php echo $reto->ret_anexos; ?>"><?php  echo $reto->ret_anexos; ?></a>
                   </p>
               <?php }else{ ?>
                   <p>
                       <span style="font-weight: bold;  font-family: '';"><?php echo JText::_('Descarga el formato para ingresar tu solución '); ?></span> 
                       <a target="blank" href="images/retos/anexos/Solución.doc">Solución.doc</a>
                   </p>
               <?php  
               }*/
               if($this->retoAnexos){ ?>
                   <p>
                       <span style="font-weight: bold;  font-family: 'Arial';"><?php echo JText::_('Anexos:'); ?></span><br>
                       <?php  foreach ($this->retoAnexos as $retoAnexos){
                            //if($retoAnexos->axr_archivo &&  file_exists("images/retos/anexos/{$retoAnexos->axr_archivo}")){
                       ?>
                           <a href="images/retos/anexos/<?php echo $retoAnexos->axr_archivo; ?>" target="blank"><?php echo $retoAnexos->axr_archivo; ?></a>
                           <br>
                       <?php  //} 
                       } ?>
                   </p>   
              <?php  }
          } 
         ?>       
           <p>
           <?php 
           if($reto->ret_fechaVigencia){ ?>
            <span style="font-weight: bold;  font-family: 'Arial';">
                <?php echo JText::_('RETO_FECHA_LIMITE'); ?>
            </span>
           <?php 
               $fechaVigencia = explode(' ', $reto->ret_fechaVigencia);
               echo $fechaVigencia[0]; 
           } 
           ?>
           <br/>
           <?php if($reto->ret_puntos){ ?>
                <span style="font-weight: bold;  font-family: 'Arial';">
                <?php  echo JText::_('RETO_PUNTOS'); ?></span> <?php echo $reto->ret_puntos;?> 
                <br/> 
                <span style="font-weight: bold;  font-family: 'Arial';">
                <?php echo JText::_('RETO_SOLUCIONADORES'); ?></span>
                <?php echo $reto->_postulaciones;
           } 
           ?>
           </p>
           <div id="btns_reto_prof">
               <?php if((!$reto->_solucion || !$reto->_tycusuario) && $editarReto){ ?>
                   <a class="modal" rel="{handler: 'iframe', size: {x: 650, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=tyc&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                    <input type="button" value="Inscribirme" class="button" />
                   </a>
               <?php  }else{
                    ?>
                        <a  class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 680, y: 800}}" id="modal" href="index.php?option=com_solucion&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                          <input type="button" value="Solución" class="button" border="0" />
                       </a>
                <?php 
                    }  if($reto->_solucion && $reto->_tycusuario>0){ ?>
                    <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 500}}" id="modal" href="index.php?option=com_pregunta&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <input type="button" value="Pregunta al experto" class="button"   border="0"/>
                    </a>
                <?php } ?>
                
               <?php if($editarReto){ ?> 
                   <a class="modal" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_recomendar&amp;view=recomendar&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                       <input type="button" value="Recomendar a un amigo" class="button" />
                   </a>
              <?php } ?>
           </div>
       <?php 
       }else{?>
           <p>
           <?php if($reto->ret_fechaVigencia){ ?>
           <span style="font-weight: bold;  font-family: 'Arial';">      
               <?php     echo JText::_('RETO_FECHA_LIMITE'); ?>
           </span>
           <?php   
               $fechaVigencia = explode(' ', $reto->ret_fechaVigencia);
               echo $fechaVigencia[0];
                
           }
           ?>
           <br/>
           <?php if($reto->ret_puntos){ ?>
           <span style="font-weight: bold;  font-family: 'Arial';">
                <?php echo JText::_('RETO_PUNTOS'); ?>
            </span>
            <?php echo $reto->ret_puntos.'<br/>'; ?>
               <span style="font-weight: bold;  font-family: 'Arial';">
                   <?php echo JText::_('RETO_SOLUCIONADORES'); ?>
              </span>
              <?php echo $reto->_postulaciones; 
            }
           ?>
           </p>
           <br/><br/><br/>
           
            <div style="float: left;">
                <div style="vertical-align: middle; display: table-cell; height: 33px;">
                    <div><?php echo JText::_('COM_RETOS_MENSAJE_POSTULACION'); ?></div>
                </div>
            </div>
            <div style="float: right;">
                <div style="vertical-align: middle; display: table-cell;">
                <a class="modal" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="index.php?option=com_retos&amp;view=loginretos&amp;component=com_retos&amp;path=&amp;tmpl=component">

            <img style="margin-top: 2px;" src="images/retos/retoSesion.png">                
                </a>
                </div>
            </div>
       <?php  
       } 
       ?>
</div>
   

   
