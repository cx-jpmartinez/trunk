<?php defined('_JEXEC') or die('Restricted access'); ?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<div id="cont_crear_solucion">
    <div id="crear_solucion">
	    <h4>&#9658; Iniciar sesión </h4>
	    <form action="index.php" method="post" id="login-form" >
		    <div class="cont_campos">
		         <label>Usuario:</label>
		         <input id="modlgn-username" type="text" name="username" value=""  size="18" />
		    </div>
		    
		     <div class="cont_campos">
		         <label>Contraseña:</label>
		         <input id="modlgn-passwd" type="password" name="password" value="" size="18"  />
		    </div>
		    
		    <div id="acepto" style="width: 300px !important;">
		        <input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN') ?>" />
		        <input type="hidden" name="option" value="com_users" />
		        <input type="hidden" name="task" value="user.login" />
		        <input type="hidden" name="recargar" value="1" />
		        <?php echo JHtml::_('form.token'); ?>
		    </div>
	    </form>
    </div>
</div>