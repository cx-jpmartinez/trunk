<?php
    defined('_JEXEC') or die('Restricted access');
    $objreto = new reto();
?>
<div id="breadcrumb" class="pathway">
    > <a href="index.php?option=com_home&id=2&Itemid=530"><?php echo JText::_('COM_RETOS_TITULO_HOME');?></a> - <strong><?php echo JText::_('COM_RETOS_TITULO_MIS_RETOS');?></strong>
 </div>
<div id="cont_lista_retos">
<div id="titulo_retos">
    <h3><?php echo JText::_('COM_RETOS_TITULO_MIS_RETOS');?></h3>
    <form></form>
    <p><?php echo JText::_('COM_RETOS_MENSAJE_PARTICIPANTES');?></p>
</div>

<?php if($this->obj){ ?>
	<div id="lista_retos">
		<?php foreach ($this->obj as $reto){
		$tamTiutlo = '';	    
		$solucion = new solucion();
		$solucion->cargarSolucion($reto->ret_id,$this->usuario->usu_id);
		$editarReto = false;
        $fechaActual = date('Y-m-d H:i');
        if(($fechaActual >= $reto->ret_fechaCreacion && $fechaActual<=$reto->ret_fechaVigencia) && ($reto->ret_estado=='Vigente') ){
            $editarReto = true;
        } 
        //if(strlen($reto->ret_nombre)>140){
        	$tamTiutlo = "style='font-size: 12px;'";
       // } 
		?>
		<div class="reto">
		    <div>
				<a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
					<?php if($reto->ret_imagenPeq){
						if (file_exists("./images/retos/$reto->ret_imagenPeq")) {
						?>
					   <img src="images/retos/<?php echo $reto->ret_imagenPeq; ?>" width="74" height="50" alt="nutresa" border="0">
					<?php }
					} 
					?>
				</a>  
		        <p>
					<span>
						<?php echo JText::_('RETO_PUNTOS'); ?>  <?php echo $reto->ret_puntos; ?> 
						| 
						<?php echo  JText::_('RETO_FECHA_LIMITE'); ?><?php echo $reto->_fechaVigencia; ?>
					</span>
					<br>
		              <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444"><b <?php echo $tamTiutlo; ?>><?php  echo $reto->ret_nombre; ?></b></a>
		            <br><br>
		            <?php 
                        /*if($reto->sol_estado=='Finalizado'){
		            	$sitio = SITIO;
                        $enlace = "index.php?option=com_solucion&view=verSolucion&idSol=$solucion->sol_id";
                    ?>   
                        <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 650, y: 480}}" id="modal" href="<?php echo $enlace; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <input type="button" value="Solución" class="button" border="0" onclick=""/>
                        </a>
	                   <?php }else{  */
	                      // if($editarReto){
                    ?>
	                   <a  class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 680, y: 800}}" id="modal" href="index.php?option=com_solucion&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
	                      <input type="button" value="Solución" class="button" border="0" />
	                   </a>
	                <?php //}
	               // } 
	                ?>
                    <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 500}}" id="modal" href="index.php?option=com_preguntas&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <input type="button" value="Pregunta al experto" class="button"   border="0"/>
                    </a>
		        </p>
		    </div>
		    <p class="participantes">
		        <?php 
                    //$reto->_ret_postulacion = cargarNumeroParticipantes($reto->ret_id);
                    $reto->_ret_postulacion = $objreto->numeroPostulaciones($reto->ret_id);
                    if($reto->_ret_postulacion){
                        echo $reto->_ret_postulacion ; 
                    }else{
                        echo 0;
                    }
                ?>
		    </p>
		</div>
		<?php } ?>                         
	</div>
<?php }else{ ?> 
    <div id="lista_retos">
	<div class="reto">
	    <p align="center">
	        <?php echo JText::_('No hay Retos'); ?>
	    </p>
	</div>   
    </div> 
<?php } ?>  
</div>