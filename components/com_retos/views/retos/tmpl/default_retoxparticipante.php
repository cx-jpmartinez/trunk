<?php
    defined('_JEXEC') or die('Restricted access');
?>
<div id="breadcrumb" class="pathway">
    > <a href="index.php?option=com_home&id=2&Itemid=530"><?php echo JText::_('COM_RETOS_TITULO_HOME');?></a> - <strong><?php echo JText::_('COM_RETOS_MENSAJE_LISTA_RETOS');?></strong>
 </div>
<div id="cont_lista_retos">
<div id="titulo_retos">
    <h3>Participantes</h3>
    <form></form>
    <p>#</p>
</div>
<?php 
if($this->obj){
    ?>
<div id="lista_retos">
    <?php 
        $objreto = new reto();
        foreach ($this->obj as $reto){ 
        $paticipantes = $objreto->numeroPostulaciones($reto->ret_id);
        ?>
                        <div class="reto">
                            <div>
                                <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
                                    <?php if($reto->ret_imagenPeq){ 
                                    if (file_exists("./images/retos/$reto->ret_imagenPeq")) { ?>
                                    <img src="images/retos/<?php echo $reto->ret_imagenPeq; ?>" width="74" height="50" alt="nutresa" border="0">
                                    <?php }
                                        } 
                                    ?>
                                </a>
                                <p>
                                    <span>
                                        <?php echo JText::_('RETO_PUNTOS'); ?>  <?php echo $reto->ret_puntos; ?> 
                                        | 
                                        <?php echo  JText::_('RETO_FECHA_LIMITE'); ?><?php echo $reto->_fechaVigencia; ?>
                                    </span>
                                    <br>
                                    <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444"><b style="font-size:12px !important;"><?php  echo $reto->ret_nombre; ?></b></a>
                                    <br>
                                     <?php  
                                        $reto->ret_resumen = str_replace('<p>', '', $reto->ret_resumen);
                                        $reto->ret_resumen = str_replace('</p>', '', $reto->ret_resumen);
                                        echo substr($reto->ret_resumen,0,200);
                                     ?>
                                     <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">... +</a>
                                </p>
                            </div>
                            <p class="participantes">
                                <?php 
                                    //$ret_postulacion = cargarNumeroParticipantes($reto->ret_id);
                                    $ret_postulacion = $paticipantes;
                                    if($ret_postulacion){
                                    ?>
                                        <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 700, y: 500}}" id="modal" href="index.php?option=com_retos&amp;view=verparticipantes&amp;id=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                                            <?php echo $ret_postulacion ; ?>
                                        </a>
                                            <?php    
                                    }else{
                                        echo 0;
                                    }
                                ?>
                            </p>
                        </div>
                   
        <?php } ?>
         </div>
        
        <?php }else{ ?>        
            <div id="lista_retos">
                <div class="reto">
                    <p align="center">
                        <?php echo JText::_('MOD_BUSCAR_NO_BUSQUEDA'); ?>
                    </p>
                </div>   
            </div>
        <?php } ?>
</div>        