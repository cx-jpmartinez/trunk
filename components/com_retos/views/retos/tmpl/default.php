<?php
    defined('_JEXEC') or die('Restricted access');
    $objreto = new reto();
?>
<div id="breadcrumb" class="pathway">
    > <a href="index.php?option=com_home&id=2&Itemid=530"><?php echo JText::_('COM_RETOS_TITULO_HOME');?></a> - <strong><?php echo JText::_('COM_RETOS_MENSAJE_LISTA_RETOS');?></strong>
 </div>
<div id="cont_lista_retos">
<div id="titulo_retos">
    <h3><?php echo JText::_('COM_RETOS_MENSAJE_RETOS');?></h3>
    <form></form>
    <p><?php echo JText::_('COM_RETOS_MENSAJE_PARTICIPANTES');?></p>
</div>
<?php if($this->obj){ ?>
<div id="lista_retos">
    <?php foreach ($this->obj as $reto){ ?>
                        <div class="reto">
                            <div>
                                <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">
                                    <?php if($reto->ret_imagenPeq){ 
                                    if (file_exists("./images/retos/$reto->ret_imagenPeq")) { ?>
                                    <img src="images/retos/<?php echo $reto->ret_imagenPeq; ?>" width="74" height="50" alt="nutresa" border="0">
                                    <?php }
                                        } 
                                    ?>
                                </a>
                                <p>
                                    <span>
                                        <?php echo JText::_('RETO_PUNTOS'); ?>  <?php echo $reto->ret_puntos; ?> 
                                        | 
                                        <?php echo  JText::_('RETO_FECHA_LIMITE'); ?><?php echo $reto->_fechaVigencia; ?>
                                    </span>
                                    <br>
                                    <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444"><b style="font-size:12px !important;"><?php  echo $reto->ret_nombre; ?></b></a>
                                    <br>
                                     <?php  
                                        $reto->ret_resumen = str_replace('<p>', '', $reto->ret_resumen);
                                        $reto->ret_resumen = str_replace('</p>', '', $reto->ret_resumen);
                                        echo substr($reto->ret_resumen,0,200);
                                     ?>
                                     <a href="index.php?option=com_retos&amp;view=reto&amp;idreto=<?php echo $reto->ret_id; ?>&Itemid=444">... +</a>
                                </p>
                            </div>
                            <p class="participantes">
                                <?php 
	                                //$ret_postulacion = cargarNumeroParticipantes($reto->ret_id);
                                        $ret_postulacion = $objreto->numeroPostulaciones($reto->ret_id);
	                                if($ret_postulacion){
	                                    echo $ret_postulacion;
                                    }else{
	                                    echo 0;
	                                }
                                ?>
                            </p>
                        </div>
                   
        <?php } ?>
         </div>
		<!--div id="paginador_retos">
		<ul>
		    <li id="pag_ant">
		      <a href="#"><?php echo JText::_('COM_RETOS_ANTERIOR'); ?></a>
		    </li>
		<li>
		    <a href="#">1</a>
		</li>
			<li id="current">
			    <a href="#">2</a>
			</li>
		    <li id="pag_sig">
		      <a href="#"><?php echo JText::_('COM_RETOS_SIGUIENTE'); ?></a>
            </li>
		</ul>
		</div-->
		<?php }else{ ?>        
		    <div id="lista_retos">
				<div class="reto">
				    <p align="center">
				        <?php echo JText::_('MOD_BUSCAR_NO_BUSQUEDA'); ?>
				    </p>
				</div>   
		    </div>
		<?php } ?>
</div>        