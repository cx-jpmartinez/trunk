<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');
?>
<?php
class RetosController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false)
    {

        $db = JFactory::getDBO();
	   $view = JRequest::getVar( 'view' );
	   $task = JRequest::getVar('task');

           if($task=='aceptarInvitacion'){
                $ret_id = JRequest::getVar( 'idRet' );
                $reto = new reto($ret_id);
                $arrSol = JRequest::getVar( 'estado', array());
                $user = JFactory::getUser();
                $usuario = new usuario($user->id);
               if(count($arrSol)>0){
                   $configMail = JFactory::getConfig();
                   $sitio = JURI::base();
                   foreach ($arrSol as $sol=>$key) {
                       $objSolCompartida = new solucioncompartida($sol);
                       $amigo = new usuario($objSolCompartida->slc_usu_id);
                       if($key=='Aceptar'){
                            $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                            $subject = "Invitación aceptada.";
                            $cuerpo.=" Hola, {$amigo->usu_nombre}.</br></br>";
                            $cuerpo.=" $usuario->usu_nombre aceptó tu invitación para participar";
                            $cuerpo.=" contigo en la solución  del reto   '$reto->ret_nombre'.</br></br>";
                            $cuerpo.=" Muchas Gracias por participar</br></br>";
                            $cuerpo.=" Estaremos atento a tu solución";
                            $objSolCompartida->slc_estado = 'Aceptada';
                       }elseif($key=='Rechazar'){
                           $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                            $subject = "Invitación rechazada.";
                            $cuerpo.=" Hola, {$amigo->usu_nombre}.</br></br>";
                            $cuerpo.=" $usuario->usu_nombre no aceptó tu invitación para participar";
                            $cuerpo.=" contigo en la solución  del reto   '$reto->ret_nombre'.</br></br>";
                            $cuerpo.=" Te invitamos a que sigas participando en la búsqueda de soluciones para este reto.";
                            $objSolCompartida->slc_estado = 'Rechazada';
                       }
                        $objSolCompartida->store();
                        $from = $configMail->get('mailfrom');
                        $fromname = $configMail->get('fromname');
                        $correos = "$amigo->usu_email";
                        JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
                   }
               }
               echo "<script type=\"text/javascript\">window.parent.location='index.php?option=com_retos&view=reto&idreto={$reto->ret_id}&Itemid=444';</script>";
               exit;
           }elseif($task=='aceptarParticipar'){
                $user = JFactory::getUser();
		$usuario = new usuario($user->id);
                $idsol = JRequest::getVar( 'idsol');
                $usu_id = JRequest::getVar( 'usu_id');
                if($idsol){
                    $solucion = new solucion($idsol);
                    if($solucion->sol_ret_id){
                        $reto = new reto($solucion->sol_ret_id);
                    }
                }
                $idamigo = JRequest::getVar( 'idamigo');
                $amigo = new usuario($idamigo);
                $sql="UPDATE solucioncompartida SET slc_estado='Aceptada'
                WHERE slc_sol_id='$idsol' AND slc_usu_id='$usuario->usu_id'";
                $db->retos->setQuery( $sql );
                $db->retos->query();

                $configMail = JFactory::getConfig();
               $sitio= JURI::base() ;
               $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.=" Hola, {$amigo->usu_nombre}.</br></br>";
                $cuerpo.=" $usuario->usu_nombre aceptó tu invitación para participar";
                $cuerpo.=" contigo en la solución  del reto   '$reto->ret_nombre'.</br></br>";
                $cuerpo.=" Muchas Gracias por participar</br></br>";
                $cuerpo.=" Estaremos atento a tu solución";
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $correos = "$amigo->usu_email";
                $subject = "Invitación aceptada.";
                JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            }elseif($task=='rechazarParticipar'){
                $user = JFactory::getUser();
		$usuario = new usuario($user->id);
                $idsol = JRequest::getVar( 'idsol');
                if($idsol){
                    $solucion = new solucion($idsol);
                    if($solucion->sol_ret_id){
                        $reto = new reto($solucion->sol_ret_id);
                    }
                }
                $usu_id = JRequest::getVar( 'usu_id');
                $idamigo = JRequest::getVar( 'idamigo');
                $amigo = new usuario($idamigo);
                if($amigo->usu_id && $usuario->usu_id){
                    $sql="UPDATE solucioncompartida SET slc_estado='Rechazada'
                    WHERE slc_sol_id='$idsol' AND slc_usu_id='$usuario->usu_id'";
                    $db->retos->setQuery( $sql );
                    $db->retos->query();
                   /* $sql="DELETE FROM solucioncompartida
                        WHERE
                        slc_sol_id='$idsol'
                        AND slc_usu_id='$usuario->usu_id'";
                     $db->retos->setQuery( $sql );
                     $db->retos->query();
                    $sql="DELETE FROM solucion
                        WHERE
                        sol_sol_id='$idsol'
                        AND sol_usu_id='$usuario->usu_id'";
                    $db->retos->setQuery( $sql );
                    $db->retos->query();*/
                    $configMail = &JFactory::getConfig();
                    $sitio= JURI::base() ;
                    $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                    $cuerpo.=" Hola, {$amigo->usu_nombre}.</br></br>";
                    $cuerpo.=" $usuario->usu_nombre no aceptó tu invitación para participar";
                    $cuerpo.=" contigo en la solución  del reto   '$reto->ret_nombre'.</br></br>";
                    $cuerpo.=" Te invitamos a que sigas participando en la búsqueda de soluciones para este reto.";
                    $from = $configMail->get('mailfrom');
                    $fromname = $configMail->get('fromname');
                    $correos = "$amigo->usu_email";
                    $subject = "Invitación rechazada.";
                    JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
                    echo "<script type=\"text/javascript\">parent.location.reload();</script>";
                    exit;
                }
            }elseif($task=='cargarAjax'){
                $buscar= JRequest::getVar('term');
                getAjaxUsuariosLotus($buscar);
            }else{
                if(!$view){
                    $view ='retos';
	        }
		   if($view=='retos'){

		   	   $buscar = JRequest::getVar( 'buscar' ); if(!$buscar){$buscar='';}
		   	   $area = JRequest::getVar( 'area' ); if(!$area){$area='';}
		   	   $vigente = JRequest::getVar( 'vigente' ); if(!$vigente){$vigente='';}
		   	   $cerrada = JRequest::getVar( 'cerrada' ); if(!$cerrada){$cerrada='';}
		   	   $resuelta = JRequest::getVar( 'resuelta' ); if(!$resuelta){$resuelta='';}
                           $arrEstado = array('1'=>$vigente,'2'=>$cerrada,'3'=>$resuelta);
		   	   $retos = new reto();
		   	   if($buscar || $area || $vigente || $resuelta || $cerrada){
                                $obj = $retos->listarRetosBusqueda($buscar, $area, $arrEstado);
                            }else{
                                $obj = $retos->listarRetosVigentes();
		   	   }
		       $newView =  $this->getView('retos','html');
			   $newView->obj = $obj;
			   $newView->display();
		   }
		   if($view=='reto')
           {




                $user = JFactory::getUser();
                $usuario = new usuario($user->id);
                $ret_id = JRequest::getVar( 'idreto' );
                $reto = new reto($ret_id);
                $reto->_favorito = $reto->cargarFavorito($ret_id,$usuario->usu_id);
                $reto->_solucion = $reto->cargarSolucion($ret_id,$usuario->usu_id);
                $reto->_tycusuario = $reto->cargartycSolucion($ret_id,$usuario->usu_id);
                $reto->_postulaciones = $reto->numeroPostulaciones($ret_id);
                $reto-> cargarDatosExperto();
                $reto->cargarAreasInteresReto($reto->ret_id);
                $reto->cargarCategoriaReto($reto->ret_id);
                $reto->cargarCompaniaReto($reto->ret_id);
                $reto->cargarNegocioReto($reto->ret_id);

                $solucion = new solucion();

                $solucion->cargarSolucion($reto->ret_id,$usuario->usu_id);

                $anexoreto = new anexoxreto();
                $retoAnexos = $anexoreto->cargarAnexos($ret_id);

                $newView = $this->getView('reto','html');
                $newView->obj = $reto;
                $newView->usuario = $usuario;
                $newView->solucion = $solucion;
                $newView->retoAnexos = $retoAnexos;
                $admin = JRequest::getVar( 'admin' );

		       if(($admin && $admin=='visualizar') || $reto->ret_estado=='Cerrada'){
		          $newView->display('visualizar');
		       }else{
                           $solCompatidas = getSolucionesCompatidas($reto->ret_id ,$usuario->usu_email);
                           $userTyC = validarUserTYC($usuario->usu_id,$reto->ret_id);
                           if($solCompatidas && $userTyC){
                               $newView->display('invitacion');
                           }else{
                              $newView->display();
                           }
                       }
		   }

		  if($view=='misretos'){
	           $user    = JFactory::getUser();
	           $usuario = new usuario($user->id);
	           $reto    = new reto();
	           $obj     = $reto->listarRetosUsuario($usuario->usu_id);
	           $newView = $this->getView('misretos','html');
	           $newView->obj = $obj;
              $newView->usuario = $usuario;
	           $newView->display();
	       }
	       if($view=='loginretos'){
	       	    $newView = $this->getView('reto','html');
                $newView->display('login');
	       }

	       if($view=='participantes'){
	       	   $mostrar = validarVerParticipantes();
	       	   if($mostrar){
	             $retos = new reto();
                 $obj = $retos->listarRetosTodos();
                 $newView = $this->getView('retos','html');
                 $newView->obj = $obj;
                 $newView->display('retoxparticipante');
	       	   }
	       }

	       if($view=='verparticipantes'){
	           $ret_id = JRequest::getVar('id');
               $reto = new reto($ret_id);
               $participantes = $reto->listarParticipantes();
               $newView = $this->getView('retos','html');
               $newView->participantes = $participantes;
               $newView->reto = $reto;
               $newView->display('listap');
	       }


	   }
	}
}
