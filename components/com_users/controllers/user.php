<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Registration controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerUser extends UsersController
{
	/**
	 * Method to log in a user.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function login()
	{
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		$app    = JFactory::getApplication();
		$input  = $app->input;
		$method = $input->getMethod();
        $recargar = JRequest::getVar('recargar');

		// Populate the data array:
		$data = array();

		$data['return']    = base64_decode($app->input->post->get('return', '', 'BASE64'));
		$data['username']  = $input->$method->get('username', '', 'USERNAME');
		$data['password']  = $input->$method->get('password', '', 'RAW');
		$data['secretkey'] = $input->$method->get('secretkey', '', 'RAW');

		// Don't redirect to an external URL.
		if (!JUri::isInternal($data['return']))
		{
			$data['return'] = '';
		}

		// Set the return URL if empty.
		if (empty($data['return']))
		{
			$data['return'] = 'index.php?option=com_users&view=profile';
		}

		// Set the return URL in the user state to allow modification by plugins
		$app->setUserState('users.login.form.return', $data['return']);

		// Get the log in options.
		$options = array();
		$options['remember'] = $this->input->getBool('remember', false);
		$options['return']   = $data['return'];

		// Get the log in credentials.
		$credentials = array();
		$credentials['username']  = $data['username'];
		$credentials['password']  = $data['password'];
		$credentials['secretkey'] = $data['secretkey'];


        try {
            $lotus = new Lotus();
            $users = $lotus->datosPersona($credentials['username'], $credentials['password']);

        } catch (Exception $e)
        {
            $users = null;
            echo 'Lotus Error: ',  $e->getMessage(), "\n";
        }

        if ($users['email'] || $users['nombre'])
        {
            $credentials['name'] = $users['nombre'];
            $db =  JFactory::getDBO();
            $sql = "SELECT usu_nombreUsuario FROM usuario
                    where usu_nombreUsuario='{$credentials['username']}'
                    and usu_nombre='{$users['nombre']}'";
            $db->retos->setQuery($sql);
            $exite = $db->retos->loadResult(); //exit;


            if (!$exite)
            {
                $model = $this->getModel('Registration', 'UsersModel');
                $requestData = JRequest::getVar('jform', $users, 'post', 'array');
                $dataReg = array();
                if (!$users['email'])
                {
                    $requestData['email'] = 'usuario' . $credentials['username'] . '@UsuarioSinCorreo.com';

                }
                //  SET DE DATOS

                $dataReg['name'] = $users['nombre'];
                $dataReg['username'] = $data['username'];
                $dataReg['email1'] = $requestData['email'];
                $dataReg['password1'] = $data['password'];
                $dataReg['telefono'] = $users['telefono'];
                $dataReg['cedula'] = $users['cedula'];
                //nuevos
                $dataReg['direccion'] = $users['direccion'];
                $dataReg['area'] = $users['area'];
                $dataReg['cargo'] = $users['cargo'];
                $dataReg['compania'] = $users['compania'];
                $dataReg['regional'] = $users['regional'];

                $return = $model->register($dataReg);
                if (!$return)
                {
                    $app->setUserState('users.login.form.data', array());
                    $app->redirect(JRoute::_($data['return'], false));
                }
                }
                else
                {
                // Generate the new password hash.
                jimport('joomla.user.helper');
                $salt = JUserHelper::genRandomPassword(32);
                $crypted = JUserHelper::getCryptedPassword($data['password'], $salt);
                $password = $crypted . ':' . $salt;
                $db = JFactory::getDBO();
                $sql = "UPDATE retos_rts.rts_users SET password='$password'
                        WHERE
                        username='$exite' and name ='{$users['nombre']}'";

                $db->retos->setQuery($sql);
                if ($db->retos->query())
                {
                    $set = "";
                    if ($users['cedula']) {
                        $users['cedula'] = preg_replace('/[^0-9]/', '', $users['cedula']);
                        $set .= ", usu_cedula = '{$users['cedula']}'";
                    }
                    if ($users['telefono']) {
                        $set .= ", usu_telefono = '{$users['telefono']}'";
                    }
                    if ($users['direccion']) {
                        $set .= ", usu_direccion = '{$users['direccion']}'";
                    }
                    if ($users['cargo']) {
                        $set .= ", usu_cargo = '{$users['cargo']}',";
                    }
                    if ($users['compania']) {
                        $set .= ", usu_compania = '{$users['compania']}'";
                    }
                    if ($users['area']) {
                        $set .= ", usu_area = '{$users['area']}'";
                    }
                    if ($users['regional']) {
                        $set .= ", usu_regional = '{$users['regional']}'";
                    }
                    $sql = "UPDATE usuario SET
                            usu_clave='{$data['password']}'
                            $set
                            WHERE
                            usu_nombreUsuario='$exite'
                            and usu_nombre='{$users['nombre']}'";
                    $db->retos->setQuery($sql);
                    $db->retos->query();
                }
            }

            $error = $app->login($credentials, $options);
            if (!JError::isError($error))
            {
                $user = JFactory::getUser();
                $usuario = new usuario($user->id);
                if ($user->id > 0) {
                    $db =& JFactory::getDBO();
                    $sql = "UPDATE usuario SET
                            usu_clave='{$data['password']}'
                            WHERE
                            usu_nombreUsuario='{$usuario->usu_nombreUsuario}'
                            and usu_nombre='{$user->name}'";
                    $db->retos->setQuery($sql);
                    $db->retos->query();
                    $domain = strrchr($usuario->usu_email, '@UsuarioSinCorreo.com');
                    if (!$usuario->usu_email
                        || $domain == '@UsuarioSinCorreo.com'
                        || !$usuario->usu_cedula
                        || !$usuario->usu_telefono
                    ) {
                        $app->redirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));
                    }
                }


                $app->setUserState('users.login.form.data', array());
                if ($recargar)
                {
                    echo "<script type=\"text/javascript\">parent.location.reload();</script>";
                    exit;
                } else
                {
                    $app->redirect(JRoute::_($data['return'], false));
                }
            } else {
                $data['remember'] = (int)$options['remember'];
                $app->setUserState('users.login.form.data', $data);
                $app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
            }
        } else
        {
            //Perform the log in.
            $error = $app->login($credentials, $options);

            if (!JError::isError($error)) {
                $app->setUserState('users.login.form.data', array());
                if ($recargar) {
                    echo "<script type=\"text/javascript\">parent.location.reload();</script>";
                    exit;
                }
                else
                {
                    $user = JFactory::getUser();
                    $usuario = new usuario($user->id);
                    if ($user->id > 0) {
                        $db = JFactory::getDBO();
                        $sql = "UPDATE usuario SET
                                    usu_clave='{$data['password']}'
                                    WHERE
                                    usu_nombreUsuario='{$usuario->usu_nombreUsuario}'
                                    and usu_nombre='{$user->name}'";
                        $db->retos->setQuery($sql);
                        $db->retos->query();
                        $domain = strrchr($usuario->usu_email, '@UsuarioSinCorreo.com');
                        if (!$usuario->usu_email
                            || $domain == '@UsuarioSinCorreo.com'
                            || !$usuario->usu_cedula
                            || !$usuario->usu_telefono
                        ) {
                            $app->redirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));
                        }
                    }
                    $app->redirect(JRoute::_($data['return'], false));
                }
            }
            else
            {
                $data['remember'] = (int)$options['remember'];
                $app->setUserState('users.login.form.data', $data);
                $app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
            }
        }
	}

	/**
	 * Method to log out a user.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function logout()
	{
		JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));

		$app = JFactory::getApplication();

		// Perform the log out.
		$error  = $app->logout();
		$input  = $app->input;
		$method = $input->getMethod();

		// Check if the log out succeeded.
		if (!($error instanceof Exception))
		{
			// Get the return url from the request and validate that it is internal.
			$return = $input->$method->get('return', '', 'BASE64');
			$return = base64_decode($return);

			if (!JUri::isInternal($return))
			{
				$return = '';
			}

			// Redirect the user.
			$app->redirect(JRoute::_($return, false));
		}
		else
		{
			$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
		}
	}

	/**
	 * Method to register a user.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function register()
	{
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		// Get the application
		$app = JFactory::getApplication();

		// Get the form data.
		$data = $this->input->post->get('user', array(), 'array');

		// Get the model and validate the data.
		$model  = $this->getModel('Registration', 'UsersModel');

		$form = $model->getForm();

		if (!$form)
		{
			JError::raiseError(500, $model->getError());

			return false;
		}

		$return = $model->validate($form, $data);

		// Check for errors.
		if ($return === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'notice');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'notice');
				}
			}

			// Save the data in the session.
			$app->setUserState('users.registration.form.data', $data);

			// Redirect back to the registration form.
			$this->setRedirect('index.php?option=com_users&view=registration');

			return false;
		}

		// Finish the registration.
		$return = $model->register($data);

		// Check for errors.
		if ($return === false)
		{
			// Save the data in the session.
			$app->setUserState('users.registration.form.data', $data);

			// Redirect back to the registration form.
			$message = JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_users&view=registration', $message, 'error');

			return false;
		}

		// Flush the data from the session.
		$app->setUserState('users.registration.form.data', null);

		return true;
	}

	/**
	 * Method to login a user.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function remind()
	{
		// Check the request token.
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('User', 'UsersModel');
		$data  = $this->input->post->get('jform', array(), 'array');

		// Submit the username remind request.
		$return = $model->processRemindRequest($data);

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->get('error_reporting'))
			{
				$message = $return->getMessage();
			}
			else
			{
				$message = JText::_('COM_USERS_REMIND_REQUEST_ERROR');
			}

			// Get the route to the next page.
			$itemid = UsersHelperRoute::getRemindRoute();
			$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
			$route  = 'index.php?option=com_users&view=remind' . $itemid;

			// Go back to the complete form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');

			return false;
		}
		elseif ($return === false)
		{
			// Complete failed.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getRemindRoute();
			$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
			$route  = 'index.php?option=com_users&view=remind' . $itemid;

			// Go back to the complete form.
			$message = JText::sprintf('COM_USERS_REMIND_REQUEST_FAILED', $model->getError());
			$this->setRedirect(JRoute::_($route, false), $message, 'notice');

			return false;
		}
		else
		{
			// Complete succeeded.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getLoginRoute();
			$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
			$route  = 'index.php?option=com_users&view=login' . $itemid;

			// Proceed to the login form.
			$message = JText::_('COM_USERS_REMIND_REQUEST_SUCCESS');
			$this->setRedirect(JRoute::_($route, false), $message);

			return true;
		}
	}

	/**
	 * Method to login a user.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function resend()
	{
		// Check for request forgeries
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));
	}
}
