<?php
/**
 * @version		$Id: default_core.php 21020 2011-03-27 06:52:01Z infograf768 $
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
$user = JFactory::getUser();
$usuario = new usuario($user->id);
$areas   = $this->areas;
if($usuario->usu_id <= 0)
    return;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');

?>
<script>
    function confor(){
        var answer= confirm('<?php echo JText::_('COM_USERS_REGISTER_CONFIRMACION');?>?');
        if(answer){
            return true;
        }else{
            return false;
        }
    }
</script>
<div id="contenido">
    <div class="centrado">
        <div id="breadcrumb" class="pathway">
            > <a href="index.php?option=com_users">Home</a> - <strong>Mi perfil</strong>
        </div>
        <div id="cont_crear_solucion">
            <div id="titulo_retos">
                <h3 class="titulo_reto">Mi Perfil</h3>
                <a class="button" href="index.php?option=com_home&id=2&Itemid=530">Atrás</a>
            </div>
            <div style="padding: 20px;width: 100%;text-align: center;">

                <?php if(!$usuario->usu_foto) { ?>
                    <img src="<?php echo 'images/retos/perfiles/perfilblanco.jpg';?>" width="200px;"/>
                <?php }else{?>
                    <?php /*list($width, $height)*/ @ $arrSize = getimagesize(JPATH_BASE . "/images/retos/perfiles/" . $usuario->usu_foto);?>
                    <img style="text-align: center;" src="<?php echo JURI::base( true ) . '/images/retos/perfiles/' . $usuario->usu_foto;?>" width="<?php echo $arrSize[ 0 ] > 200 ? '200px' : $arrSize[ 0 ] . 'px';?>" />
                <?php }?>
            </div>
            <div id="crear_solucion">
                <h4> &#9658; Datos Personales : </h4>
                <form action="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>" method="POST" name="formFotoPerfil">
                    <table class="mi_perfil_table" cellspacing="0" cellpadding="6" style=" margin: 30px !important; color: #59585D;" align="center" >
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th align="left"><?php echo JText::_('COM_USERS_REMIND_EMAIL_LABEL'); ?>:</th>
                            <td><?php echo $usuario->usu_email; ?></td>
                        </tr>
                        <tr>
                            <th align="left"> <?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></th>
                            <td><?php echo $usuario->usu_nombre; ?></td>
                        </tr>
                        <tr>
                            <th align="left"> <?php echo JText::_('Cédula:'); ?></th>
                            <td><?php echo $usuario->usu_cedula; ?></td>
                        </tr>
                        <tr>
                            <th align="left"> <?php echo JText::_('Télefono:'); ?></th>
                            <td><?php echo $usuario->usu_telefono; ?></td>
                        </tr>
                        <tr>
                            <th align="left"> <?php echo JText::_('Compañia:'); ?></th>
                            <td><?php echo $usuario->usu_compania; ?></td>
                        </tr>

                        <tr>
                            <th align="left"> <?php echo JText::_('Área:'); ?></th>
                            <td><?php echo $usuario->usu_area; ?></td>
                        </tr>

                        <tr>
                            <th align="left"> <?php echo JText::_('Cargo:'); ?></th>
                            <td><?php echo $usuario->usu_cargo; ?></td>
                        </tr>

                        <tr>
                            <th align="left"> <?php echo JText::_('Dirección  de residencia:'); ?></th>
                            <td><?php echo $usuario->usu_direccion; ?></td>
                        </tr>


                        <?php if($user->id  == 96 || $user->id  == 97 || $user->id  == 98 || $user->id  == 99){ ?>
                            <!-- tr>	
                                <th align="left"><?php echo JText::_('COM_USERS_REGISTER_APELLIDO_LABEL:'); ?>:</th>
                                <td><?php echo $usuario->usu_apellido; ?></td>
                            </tr-->
                            <tr>
                                <th align="left"><?php echo JText::_('COM_USERS_REGISTER_PAIS_LABEL:'); ?>:</th>
                                <td><?php echo $usuario->usu_pais; ?></td>
                            </tr>
                            <?php if($usuario->usu_ciudad) : ?>
                                <tr>
                                    <th align="left"><?php echo JText::_('LAB_CIUDAD'); ?>:</th>
                                    <td><?php echo $usuario->usu_ciudad; ?></td>
                                </tr>
                            <?php endif;?>
                            <tr>
                                <th align="left"><?php echo JText::_('COM_USERS_REGISTER_DIRECCION_LABEL'); ?>:</th>
                                <td><?php echo $usuario->usu_direccion; ?></td>
                            </tr>
                            <tr>
                                <th align="left"><?php echo JText::_('COM_USERS_REGISTER_TELEFONO_LABEL'); ?>:</th>
                                <td><?php echo $usuario->usu_telefono; ?></td>
                            </tr>
                            <!--                            <tr>
                                                            <th align="left">Empresa:</th>
                                                            <td>Zénu</td>
                                                        </tr>-->
                        <?php } ?>

                        <tr>
                            <th align="left"><?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?></th>
                            <td><?php echo JHtml::_('date',$this->data->registerDate); ?></td>
                        </tr>
                        <tr>
                            <th align="left"><?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?></th>
                            <?php if ($this->data->lastvisitDate != '0000-00-00 00:00:00'){?>
                                <td>
                                    <?php echo JHtml::_('date',$this->data->lastvisitDate); ?>
                                </td>
                            <?php }
                            else {?>
                                <td>
                                    <?php echo JText::_('COM_USERS_PROFILE_NEVER_VISITED'); ?>
                                </td>
                            <?php } ?>

                        </tr>
                    </table>
                    <div id="acepto">
                        <input type="submit" value="Editar">
                        <input type="hidden" name="option" value="com_users" />
                        <input type="hidden" name="view" value="profile.edit" />
                    </div>
                </form>
                <h5 id="compartida">&#9658;<?php echo JText::_('LAB DATOS ADICIONALES'); ?></h5>
                <table class="mi_perfil_table" cellspacing="0" cellpadding="6" style=" margin: 30px !important; color: #59585D;" align="center" >
                    <?php /*if( $usuario->usu_nivelEducacion || $usuario->usu_experiencia):?>
                            <tr><td>&nbsp;</td></tr>
                            <?php if($usuario->usu_nivelEducacion) : ?>
                                <tr>
                                    <th align="left"><?php echo JText::_('LAB_NIVEL_EDUCACION'); ?></th>
                                    <td><?php echo $usuario->usu_nivelEducacion; ?></td>
                                </tr>
                            <?php endif;?>
                            <?php if($usuario->usu_experiencia) : ?>
                                <tr>
                                    <th align="left"><?php echo JText::_('LAB_EXPERIENCIA'); ?></th>
                                    <td><?php echo $usuario->usu_experiencia; ?></td>
                                </tr>
                            <?php endif;?>
                        <?php endif;*/?>

                </table>
                <form  enctype='multipart/form-data' action="<?php echo JRoute::_('index.php?option=com_users&task=profile.subirImagen'); ?>" method="POST" name="formFotoPerfil">
                    <table class="mi_perfil_table" cellspacing="0" cellpadding="2" style=" margin: 20px !important; color: #59585D;" align="center" >

                        <tr>

                            <td>

                                <?php echo JText::_('LAB CAMBIAR IMAGEN'); ?>:
                                <input type="file" name="usu_foto" si>
                                <div id="acepto">
                                    <input type="submit" value="Aceptar">
                                    <input type="hidden" name="formFotoPerfil[usu_id]" value="<?php echo $usuario->usu_id;?>" >
                                    <input type="hidden" name="option" value="com_users" >
                                    <input type="hidden" name="task" value="profile.subirImagen" >
                                    <?php echo JHtml::_('form.token'); ?>
                                </div>
                            </td>

                        </tr>

                    </table>
                </form>


                <h5 id="compartida">&#9658;<?php echo JText::_('JAREAINTERES_TITULO_MIS_INTERESES');?></h5>
                <div class="leading-0" style="border-color: red; border: 1px;margin-right:10px !important;">
                    <div style="width: 100%; float: center; color: #59585D;">
                        <p></p>
                        <form action="index.php?option=com_users" method="post" name="retoFavorito" id="retoFavorito" onSubmit="return confor();">
                            <table cellspacing="0" cellpadding="6" style=" margin: 10px !important; " align="center" >
                                <tr align="left">
                                    <th colspan="2" style="padding: 5px !important;"><?php echo JText::_('JAREAINTERES_FAVOR_MARQUE');?></th>
                                </tr>
                                <tr align="center">
                                    <th style="padding: 5px !important;"><?php echo JText::_('JAREAINTERES_TITULO');?></th>
                                    <th style="padding: 5px !important;"><?php echo JText::_('JAREAINTERES_ME_INTERESA');?></th>
                                </tr>
                                <?php
                                if( isset($areas) ){
                                    foreach ($areas as $area){
                                        $check  = '';
                                        if( isset($this->areasxusuario) && in_array($area->are_id,$this->areasxusuario)){
                                            $check = 'checked';
                                        }else{
                                            $check = '';
                                        }
                                        ?>
                                        <tr>
                                            <td style="padding: 5px !important;" ><?php echo $area->are_nombre; ?></td>
                                            <td style="padding: 5px !important; " align="center"><input type="checkbox" name="areaInteres[<?php echo $area->are_id; ?>]" value="<?php echo $area->are_id; ?>" <?php echo $check; ?>></td>
                                        </tr>
                                    <?php }
                                }else{
                                    ?>
                                    <tr align="center">
                                        <td><?php echo JText::_('JAREAINTERES_NO_REGISTROS');?></td>
                                    </tr>
                                <?php } ?>
                                <tr><td>&nbsp;</td></tr>
                            </table>
                            <br/>
                            <?php
                            if( isset($this->datosUsuario->usu_envioInfo)){
                                if($this->datosUsuario->usu_envioInfo == 'email'){
                                    $checkemail = 'checked';
                                    $checkrss = '';
                                }elseif($this->datosUsuario->usu_envioInfo == 'rss'){
                                    $checkemail = '';
                                    $checkrss = 'checked';
                                }else{
                                    $checkemail = '';
                                    $checkrss = '';
                                }
                            }else{
                                $checkemail = '';
                                $checkrss = '';
                            }?>
                            <b><?php echo JText::_('COM_USERS_REGISTER_INFORMACION'); ?>:</b>
                            E-mail:<input type="radio" name="sendinfo" value="email" <?php echo $checkemail; ?>>
                            <!--                                                RSS:<input type="radio" name="sendinfo" value="rss" <?php echo $checkrss; ?>>-->
                            <div id="acepto">
                                <input type="submit" value="Guardar Cambios">
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="view" value="profile" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



