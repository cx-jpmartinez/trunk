<?php
/**
 * @version		$Id: edit.php 21321 2011-05-11 01:05:59Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load( 'plg_user_profile', JPATH_ADMINISTRATOR );
?>

<div class="profile-edit<?php echo $this->pageclass_sfx?>">
    <?php if ($this->params->get('show_page_heading')){ ?>
        <h1><?php echo $this->escape($this->params->get('page_heading'));?></h1>
    <?php } ?>

    <form id="member-profile" action="<?php // echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="form-validate">
        <?php foreach ($this->form->getFieldsets() as $group => $fieldset){// Iterate through the form fieldsets and display each one.?>
        <?php $fields = $this->form->getFieldset($group);?>
        <?php if (count($fields) && $group != 'params'){?>
        <div id="contenido">
            <div class="centrado">
                <div id="breadcrumb" class="pathway">
                    > <a href="index.php?option=com_users">Home</a> - <a href="index.php?option=com_users&view=profile">Mi perfil</a> - <strong>Editar perfil</strong>
                </div>
                <div id="breadcrumb" class="pathway">
                    <div id="cont_crear_solucion">
                        <fieldset>
                            <?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.?>
                                <div id="titulo_retos">
                                    <h3 class="titulo_reto"><?php echo JText::_($fieldset->label); ?></h3>
                                    <a class="button" href="index.php?option=com_users&view=profile">Atrás</a>
                                </div>
                            <?php endif;?>
                            <br>
                            <div id="crear_solucion">
                                <h4> &#9658; Datos Personales : </h4>

                                <dl style="margin-left: 20px;">
                                    <?php foreach ($fields as $field){// Iterate through the fields in the set and display them.?>

                                        <?php

                                        try {

                                            if( isset($this->datosUsuario->{$field->fieldname}))
                                            {
                                              $this->form->setValue($field->fieldname, null, $this->datosUsuario->{$field->fieldname});
                                            }

                                            $fieldInput = $this->form->getInput($field->fieldname);

                                        } catch (Exception $e)
                                        {
                                            $fieldInput =  'Error en : '.$field->fieldname. " > ".  $e->getMessage(). "\n";
                                        }
                                        ?>
                                        <br>
                                        <?php if ($field->type == "Hidden"){// If the field is hidden, just display the input.?>
                                            <?php  echo $fieldInput ?>
                                        <?php }else{?>
                                            <dt>
                                                <b><?php  echo $field->label; ?></b>
                                                <?php if (!$field->required && $field->type!='Spacer'){ ?>
                                                   <span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
                                                <?php } ?>
                                            </dt>
                                            <br>
                                            <dd>
                                                <?php echo $fieldInput; ?>
                                            </dd><br>
                                        <?php } ?>
                                    <?php } ?>
                                </dl>
                                <?php    }?>
                                <?php } ?>
                                <div id="acepto">
                                    <input type="submit" class="validate" value="Aceptar">
                                    <!--                <?php echo JText::_('COM_USERS_OR'); ?>
                        <a href="<?php echo JRoute::_('index.php?option=com_users&view=profile'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>-->

                                    <input type="hidden" name="option" value="com_users" />
                                    <input type="hidden" name="task" value="profile.save" />
                                    <?php echo JHtml::_('form.token'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
