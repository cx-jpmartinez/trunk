<?php
defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Home', JPATH_COMPONENT);

// Execute the task.
$controller = JControllerLegacy::getInstance('Home');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();

