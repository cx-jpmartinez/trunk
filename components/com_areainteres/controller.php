<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class AreainteresController
 *
 * @since  1.6
 */
class AreainteresController extends JControllerLegacy
{
    /**
     * Method to display a view.
     *
     * @param null $view
     * @return JController This object to support chaining.
     *
     * @internal param bool $cachable If true, the view output will be cached
     * @internal param mixed $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}..
     *
     * @since    1.5
     */
	public function display($view = null)
	{

		$view = JRequest::getVar( 'view' );
		$ret_id = JRequest::getVar( 'idRet' );
		$user = JFactory::getUser();
		$usuario = new usuario($user->id);
		$db =& JFactory::getDBO();

		$sql = "SELECT  are_id, are_nombre, are_descripcion, are_estado, are_fecha FROM areainteres WHERE  LOWER(are_estado) = 'activa'" ;

		$db->retos->setQuery( $sql );
		$obj  = $db->retos->loadObjectList();

		$view->areas =& $obj;
		$view->usuario =& $usuario;

		$view = JFactory::getApplication()->input->getCmd('view', 'areainteres');
		JFactory::getApplication()->input->set('view', $view);

        parent::display();

        return $this;
	}
}
