<?php
defined('_JEXEC') or die('Restricted access');
$areas = $this->areas;
$usuario = $this->usuario;
?>
<div id="breadcrumb" class="pathway">
    > <a href="index.php?option=com_home&id=2&Itemid=530"><?php echo JText::_('COM_AREAINTERES_TITULO_HOME');?></a> -
    <strong><?php echo JText::_('COM_AREAINTERES_TITULO');?></strong>
</div>
<div class="items-leading">
    <h1 align="center"><?php echo JText::_('COM_AREAINTERES_TITULO_MIS_INTERESES');?></h1>
    <div class="leading-0" style="border-color: red; border: 1px;margin-right:10px !important;">
        <div style="width: 100%; float: center;">
            <p></p>
            <table cellspacing="0" cellpadding="6" border="" width="100%">
                <tr align="left">
                    <th colspan="2"><?php echo JText::_('COM_AREAINTERES_FAVOR_MARQUE');?></th>
                </tr>
                <tr align="center">
                    <th><?php echo JText::_('COM_AREAINTERES_TITULO');?></th>
                    <th><?php echo JText::_('COM_AREAINTERES_ME_INTERESA');?></th>
                </tr>
                <?php
                if($areas){
                    foreach ($areas as $area){ ?>
                        <tr>
                            <td width="40%"><?php echo $area->are_nombre; ?></td>
                            <td align="center"><input type="checkbox" name="area" checked="checked"></td>
                        </tr>
                    <?php }
                }else{
                    ?>
                    <tr align="center">
                        <td><?php echo JText::_('COM_AREAINTERES_NO_REGISTROS');?></td>
                    </tr>
                <?php } ?>
            </table>
            <p></p>

        </div>
        <p></p>
        <form action="index.php" method="post" name="retoFavorito" id="retoFavorito">
            <div align="center" >
                <?php
                $buttonFavoritos = '<input type="image" value="Guardar Cambios" class="button" src=""/>';
                echo $buttonFavoritos;
                ?>
                <input type="hidden" name="option" value="com_areainteres" />
                <input type="hidden" name="view" value="areainteres" />
            </div>
        </form>
    </div>
</div>
