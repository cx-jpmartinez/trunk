<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Areainteres
 * @author     CognoX <jpmartinez@cognox.com>
 * @copyright  2016 Cognox
 * @license    Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Areainteres records.
 *
 * @since  1.6
 */
class AreainteresModelAreaintereses extends JModelItem
{
    /**
     * @var array messages
     */
    protected $messages;

    /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $type    The table name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   1.6
     */
    public function getTable($type = 'areainteres', $prefix = 'areainteres', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Get the message
     *
     * @param   integer  $id  Greeting Id
     *
     * @return  string        Fetched String from Table for relevant Id
     */
    public function getMsg($id = 1)
    {
        if (!is_array($this->messages))
        {
            $this->messages = array();
        }

        if (!isset($this->messages[$id]))
        {
            // Request the selected id
            $jinput = JFactory::getApplication()->input;
            $are_id     = $jinput->get('are_id', 'INT');

            // Get a TableHelloWorld instance
            $table = $this->getTable();

            // Load the message
            $table->load($are_id);

            // Assign the message
            $this->messages[$are_id] = $table->greeting;
        }

        return $this->messages[$id];
    }

}
