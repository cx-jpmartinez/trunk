<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');


class ForosModelForos extends JModelItem
{

	public function getTable($type = 'foro', $prefix = 'foro', $config = array())
	{
			return JTable::getInstance($type, $prefix, $config);
	}


}
