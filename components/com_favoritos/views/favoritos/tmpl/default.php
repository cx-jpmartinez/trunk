<?php
 defined('_JEXEC') or die('Restricted access');
$favoritos = $this->favoritos;
$usuario = $this->usuario;

?>
<div id="breadcrumb" class="pathway">
   > <a href="index.php?option=com_home&id=2&Itemid=530"><?php echo JText::_('COM_FAVORITOS_HOME');?></a> - 
   <strong><?php echo JText::_('COM_FAVORITOS_TITULO');?></strong>
</div>
<div class="items-leading">
    <h1 align="center"><?php echo JText::_('COM_FAVORITOS_LISTADO_FAVORITOS');?></h1>
	   <div class="leading-0" style="border-color: red; border: 1px;margin-right:10px !important;">
				<div style="width: 100%; float: center;">
					<table cellspacing="0" cellpadding="6" border="" width="100%">
                            <tr align="center">
                                <th><?php echo JText::_('COM_FAVORITOS_TITULO_RETO');?></th>
                                <th><?php echo JText::_('COM_FAVORITOS_TITULO_ESTADO');?></th>
                                <th><?php echo JText::_('COM_FAVORITOS_TITULO_FAVORITO');?></th>
                                <th><?php echo JText::_('COM_FAVORITOS_TITULO_POSTULADO');?></th>
                            </tr>
                            <?php 
                             if($favoritos){ 
                                foreach ($favoritos as $favorito){ ?>
                             <tr align="center">
                                <td><?php echo $favorito->ret_nombre; ?></td>
                                <td><?php echo $favorito->ret_estado; ?></td>
                                <td><?php echo JText::_('COM_FAVORITOS_SI');?></td>
                                <td>
                                <?php
                                if(isset($favorito->sol_ret_id)){
                                    echo JText::_('COM_FAVORITOS_SI');
                                }else{
                                	echo JText::_('COM_FAVORITOS_NO');
                                }
                                ?>
                                </td>
                            </tr>
                            <?php }
                             }else{
                            ?>
                                <tr align="center">
                                    <td><?php echo JText::_('COM_FAVORITOS_MENSAJE');?></td>
                                </tr>
                            <?php } ?>
                    </table>
                    <p></p><p></p>
                    
					</div>
					<p></p><p></p>
				<div class="item-separator"></div>
	        </div>
</div>
