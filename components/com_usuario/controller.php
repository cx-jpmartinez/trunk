<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

class UsuarioController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false)
  {
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $view = JRequest::getVar('view' , 'usuario');
    $newView =  $this->getView($view,'html');
    $newView->display();
  }

	function verRegistro($clear = false)
  {
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $newView = $this->getView('registro','html');
    $newView->objcompania = $objUsuario->cargarArrCompania();
    if(!$clear){
      $newView->arrData = $_REQUEST;
    }else{
      $newView->arrData = array();
    }
    $newView->display();
  }

  function guardarRegistro(){
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $result = $objUsuario->validarCamposRegistro();
    if(!$result['status']){
      JError::raiseWarning( null, $result['message'] );
      $this->verRegistro();
      return;
    }

    $objUsuario->usu_clave = uniqid();
    $dataReg['name'] = $objUsuario->usu_nombre;
    $dataReg['username'] = $objUsuario->usu_nombreUsuario;
    $dataReg['email1'] = $objUsuario->usu_email;
    $dataReg['password1'] = $objUsuario->usu_clave;
    $dataReg['telefono'] = $objUsuario->usu_telefono;
    $dataReg['cedula'] = $objUsuario->usu_cedula;
    //nuevos
    $dataReg['direccion'] = $objUsuario->usu_direccion;
    $dataReg['area'] = $objUsuario->usu_area;
    $dataReg['cargo'] = $objUsuario->usu_cargo;
    $dataReg['compania'] = $objUsuario->usu_compania;
    $dataReg['regional'] = $objUsuario->usu_regional;

    include_once './components/com_users/models/registration.php';
    $objReg = new UsersModelRegistration();
    $result = $objReg->register($dataReg);

    if(!$result){
      JError::raiseWarning( null, JText::_('COM_USUARIO_REGISTRO_USUARIO_NO_CREADO') );
      $this->verRegistro();
      return;
    }else{
      $db = JFactory::getDBO();
      $objlotus = new stdClass ( );
      $objlotus->usl_fullname = $objUsuario->usu_nombre;
      $objlotus->usl_nombre =  $objUsuario->usu_nombre;
      $objlotus->usl_email = $objUsuario->usu_email ;
      $objlotus->usl_shortname =  $objUsuario->usu_nombre;
      $db->retos->insertObject('usuariolotus', $objlotus);
    }
    JFactory::getApplication()->enqueueMessage(JText::_('COM_USUARIO_REGISTRO_USUARIO_CREADO'));
    $this->verRegistro(true);
    return;
  }

  function buscarUsuario(){
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $strBuscar = JRequest::getVar('strBuscar' , false);
    $filter = JRequest::getVar('filtro' , false);
    if(!$strBuscar || !$filter){
      JError::raiseWarning( null, JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_REQUERIDO'));
      return;
    }
    $view = JRequest::getVar('view' , 'usuario');
    $newView = $this->getView($view,'html');
    $newView->usuarios = $objUsuario->buscarUsuario($strBuscar, $filter);
    $newView->display('tablaUsuarios');
  }

  function suplantarUsuario(){
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $usuId = JRequest::getVar('usu_id', false);
    if(!$usuId)
    {
      JError::raiseWarning( null, JText::_('COM_USUARIO_FALLO_SUPLANTACION') );
      return;
    }
    $session = JFactory::getSession();
    $user = JFactory::getUser();
    $session->set('idSuplantacion', $usuId);
    $idAdmin = $session->get('idUsuarioAdmin', false);
    if(!$idAdmin)
    {
      $session->set('idUsuarioAdmin', $user->id);
    }
    $user->load($usuId);
    $newView = $this->getView('Suplantacion','html');
    $newView->display();
  }

  function dejarSuplantacion(){
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $session = JFactory::getSession();
    $idAdmin = $session->get('idUsuarioAdmin', false);
    if($idAdmin){
      $user = JFactory::getUser();
      $user->load($idAdmin);
    }
    $session->clear('idSuplantacion');
    $session->clear('idUsuarioAdmin');
    $this->display();
  }

  function dejarSuplantacionAjax(){
    $objUsuario = new usuario();
    $objUsuario->validarAccesoAdministracionUsuarios();
    $session = JFactory::getSession();
    $idAdmin = $session->get('idUsuarioAdmin', false);
    if(!$idAdmin){
      return;
    }
    $user = JFactory::getUser();
    $user->load($idAdmin);
    $session->clear('idSuplantacion');
    $session->clear('idUsuarioAdmin');
  }
}
