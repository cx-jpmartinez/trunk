<?php
defined('_JEXEC') or die('Restricted access');

if(count((array)$this->usuarios) == 0){
   echo "<span style='color: red;'>".JText::_('COM_USUARIO_USUARIO_NO_DATA')."</span>";
}else{ ?>
<style>
    #box-table-a {
        border-collapse: collapse;
        font-family: "Lucida Sans Unicode","Lucida Grande",Sans-Serif;
        font-size: 12px;
        margin: 0;
        text-align: left;
        width: 100%;
    }
    #box-table-a th {
        background: none repeat scroll 0 0 #7F7E84;
        border-bottom: 1px solid #FFFFFF;
        border-top: 4px solid #C8C9CD;
        color: #FFFFFF;
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
    }
    #box-table-a td {
        background: none repeat scroll 0 0 #F0F0F0;
        border-bottom: 1px solid #FFFFFF;
        border-top: 1px solid rgba(0, 0, 0, 0);
        color: #666699;
        padding: 8px;
    }
    #box-table-a tr:hover td {
        background: none repeat scroll 0 0 #C8C9CD;
        color: #333399;
    }
    #box-table-b {
        border-bottom: 7px solid #9BAFF1;
        border-collapse: collapse;
        border-top: 7px solid #9BAFF1;
        font-family: "Lucida Sans Unicode","Lucida Grande",Sans-Serif;
        font-size: 12px;
        margin: 20px;
        text-align: center;
        width: 480px;
    }
    #box-table-b th {
        background: none repeat scroll 0 0 #E8EDFF;
        border-left: 1px solid #9BAFF1;
        border-right: 1px solid #9BAFF1;
        color: #003399;
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
    }
    #box-table-b td {
        background: none repeat scroll 0 0 #E8EDFF;
        border-left: 1px solid #AABCFE;
        border-right: 1px solid #AABCFE;
        color: #666699;
        padding: 8px;
    }
</style>
<table id="box-table-a">
    <thead>
        <tr>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_NOMBRE'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_NOMBREUSUARIO'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_EMAIL'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_TELEFONO'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_DIRECCION'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_CEDULA'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_COMPANIA'); ?></th>
            <th scope="col"><?php echo JText::_('COM_USUARIO_USUARIO_TIT_ACCION'); ?></th>
        </tr>
    </thead>
    <tbody>
      <?php foreach($this->usuarios as $usuario){ ?>
                <tr>
                    <td><?php echo $usuario->usu_nombre; ?></td>
                    <td><?php echo $usuario->usu_nombreUsuario; ?></td>
                    <td><?php echo $usuario->usu_email; ?></td>
                    <td><?php echo $usuario->usu_telefono; ?></td>
                    <td><?php echo $usuario->usu_direccion; ?></td>
                    <td><?php echo $usuario->usu_cedula; ?></td>
                    <td><?php echo $usuario->usu_compania; ?></td>
                    <td><a href="javascript:;" onclick="suplantar('<?php echo $usuario->usu_id; ?>')"><?php echo JText::_('COM_USUARIO_USUARIO_SUPLANTAR'); ?></a></td>
                </tr>
      <?php } ?>
    </tbody>
</table>

<script>
    function suplantar(id){
        if(typeof id != 'undefined'){
            window.location.href = "index.php?option=com_usuario&task=suplantarUsuario&usu_id="+id;
        }
    }
</script>
<?php }