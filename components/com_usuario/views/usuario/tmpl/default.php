<?php
defined('_JEXEC') or die('Restricted access');

?>
<style>
    h1{
        text-align: center;
        padding: 10px;
        font-size: 12pt;
        color: #9A4D9E;
    }
    .formBuscar{
/*        margin: 0 auto;*/
    }
    .formBuscar tr td{
        padding: 7px;
    }

    .formBuscar input[type='text'],
    .formBuscar input[type='password'],
    .formBuscar input[type='email']
    {
        padding: 5px;
        background: none repeat scroll 0 0 #E6E7E8;
        border: 1px solid #D2D2D2;
        color: #7F7E84;
        height: 24px;
        margin: 0 7px;
        text-align: center;
    }

    #tdButton{
        text-align: center;
    }
    .symbol-required{
        color: red;
    }
    select[name='filtro']{
        width: 100px;
    }
    input[name='btnCrear']{
        float: right;
    }
    #content_locker{
        display: none;
        background-color: rgba(255, 255, 255, 0.6);
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 999;
    }
    
    #content_locker img{
        top: 50%;
        left: 45%;
        position: fixed;
    }
</style>
<h1><?php echo 'Ingresar Solución'; //JText::_('COM_USUARIO_TIT_USUARIO_ADMIN_USUARIOS'); ?></h1>
<input type="button" class="button" name="btnCrear" value="<?php echo JText::_('COM_USUARIO_BTN_CREAR'); ?>" onclick="javascript: window.location.href = 'index.php?option=com_usuario&task=verRegistro';"/>
<div id="divFormBuscar">
    <table class="formBuscar">
        <tr>
            <td><span><?php echo JText::_('COM_USUARIO_LAB_BUSCAR_USUARIO'); ?>:</span></td>
            <td><input type="text" name="strBuscar" requerid="required"/></td>
            <td>
                <select name="filtro">
                    <option value="usu_nombre"><?php echo JText::_('COM_USUARIO_OPTION_NOMBRE');?></option>
                    <option value="usu_email"><?php echo JText::_('COM_USUARIO_OPTION_EMAIL');?></option>
                    <option value="usu_cedula"><?php echo JText::_('COM_USUARIO_OPTION_CEDULA');?></option>
                </select>
            </td>
            <td id="tdButton"><input type="button" class="button" value="<?php echo JText::_('COM_USUARIO_BTN_BUSCAR'); ?>" onclick="ajaxBuscarUsuarios();"/></td>
        </tr>
    </table>
</div>
<div id="resultadoBusqueda"></div>
<div id="content_locker">
    <img src="<?php echo $this->baseurl; ?>/images/loadingAnimation.gif"/>
</div>
<script type="text/javascript"  src='<?php echo $this->baseurl; ?>/_objects/scripts/jquery-ui-1.9.2.custom.js' ></script>
<script>
    
    function ajaxBuscarUsuarios(){
        jQuery("#content_locker").show();
        var filtro = jQuery('select[name="filtro"]').val();
        var criterio = jQuery('input[name="strBuscar"]').val();
        jQuery.ajax({
            url: 'index.php?option=com_usuario &task=buscarUsuario&format=raw',
            type: 'post',
            data:{'strBuscar': criterio, 'filtro': filtro},
            dataType: "html",
            success: function(html){ 
                        jQuery("#resultadoBusqueda").html(html);
                        jQuery("#content_locker").hide();
                    },
            error: function(){
                    jQuery("#content_locker").hide();
                }
        });
    }

</script>