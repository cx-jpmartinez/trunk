<?php
    defined('_JEXEC') or die('Restricted access');

    $session = JFactory::getSession();
    $idSpl = $session->get('idSuplantacion', false);
    if(!$idSpl){
        return;
    }
    $user = JFactory::getUser();
    $usuario = usuario::getUsuario($user->id);

?>
<style>
    #ifrSuplantacion{
        width: 100%;
        height: 800px;
/*        position: absolute;*/
/*        left:3px;*/
        border: 2px solid #7F7E84;
    }
    
    #suplantacion{
        margin-top: 60px;
        margin-bottom: 20px;
        background-color: #C9E84C;
        padding: 5px;
    }
    
    h1{
        text-align: center;
        padding: 10px;
        font-size: 20pt;
        color: #9A4D9E;
        font-weight: bold;
    }
    
    #datosUsuario span{
        font-size: 15pt;
        color: #336B14;
        vertical-align: middle !important;
    }
    
   #btnNoSuplantar{
        position: inherit;
        float: right;
        clear: right;
    }
    
    #tableIfr{
       text-align: center;
       width: 100%;
       height: 800px;
    }
</style>
<div id="suplantacion">
    <h1><?php echo JText::_('COM_USUARIO_TIT_SUPLANTACION'); ?></h1>
    <div id="datosUsuario">
        <span><strong><?php echo JText::_('COM_USUARIO_LAB_NOMBRE');?>:</strong></span>
        <span><?php echo $usuario->usu_nombre;?></span>
        <input type="button" class="button" value="<?php echo JText::_('COM_USUARIO_BTN_DEJAR_SUPLANTACION'); ?>" id="btnNoSuplantar" onclick="dejardesuplantar()"/>
    </div>
    <br/>
</div>

<table id="tableIfr">
    <tr>
        <td>

            <iframe id="ifrSuplantacion" name="ifrSuplantacion" src="<?php echo JURI::base(); ?>"></iframe>
        </td>
    </tr>
</table>
<script>
    function dejardesuplantar(){
        window.location.href = 'index.php?option=com_usuario&task=dejarSuplantacion';
    }
</script>
    