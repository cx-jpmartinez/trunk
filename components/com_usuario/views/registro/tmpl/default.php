<?php
    defined('_JEXEC') or die('Restricted access');
?>
<div id="content">
    <style>
        h1{
            text-align: center;
            padding: 10px;
            font-size: 12pt;
            color: #9A4D9E;
        }

        .formRegister{
            margin: 0 auto;
        }
        .formRegister tr td{
            padding: 7px;
        }

        .formRegister input[type='text'],
        .formRegister input[type='password'],
        .formRegister input[type='email']
        {
            padding: 5px;
            background: none repeat scroll 0 0 #E6E7E8;
            border: 1px solid #D2D2D2;
            color: #7F7E84;
            height: 24px;
            margin: 0 7px;
            text-align: center;
        }

        #tdButton{
            text-align: center;
        }
        .symbol-required{
            color: red;
        }
    </style>
    <form method="post" action="index.php?option=com_usuario&task=guardarRegistro">
        <h1><?php  echo JText::_('COM_USUARIO_REGISTRO_TIT_REGISTRO');?></h1>
        <table class="formRegister">
            <tr>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_NOMBRE'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="text" maxlength="50" name="usu_nombre" id="nombre" required="required" value="<?php echo array_key_exists('usu_nombre', $this->arrData) ?  $this->arrData['usu_nombre'] : ''; ?>"/></td>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_NOMBREUSUARIO'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="text" maxlength="50"  name="usu_nombreUsuario" id="nombreUsuario" required="required" value="<?php echo array_key_exists('usu_nombreUsuario', $this->arrData) ?  $this->arrData['usu_nombreUsuario'] : ''; ?>"/></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_EMAIL'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="email"  maxlength="50" name="usu_email" id="email"  required="required" value="<?php echo array_key_exists('usu_email', $this->arrData) ?  $this->arrData['usu_email'] : ''; ?>"/></td>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_TELEFONO'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="text"  maxlength="20" name="usu_telefono" id="telefono" required="required" pattern="^(0|[1-9][0-9]*)$" title="<?php echo JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_NUMERICO');?>" value="<?php echo array_key_exists('usu_telefono', $this->arrData) ?  $this->arrData['usu_telefono'] : ''; ?>"/></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_DIRECCION'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="text" maxlength="50"  name="usu_direccion" id="direccion" required="required" value="<?php echo array_key_exists('usu_direccion', $this->arrData) ?  $this->arrData['usu_direccion'] : ''; ?>"/></td>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_CEDULA'); ?></span><span class="symbol-required">*</span></td>
                <td><input type="text" maxlength="20"  name="usu_cedula" id="cedula" required="required" pattern="^(0|[1-9][0-9]*)$" title="<?php echo JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_NUMERICO');?>" value="<?php echo array_key_exists('usu_cedula', $this->arrData) ?  $this->arrData['usu_cedula'] : ''; ?>"/></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_USUARIO_REGISTRO_LAB_COMPAÑIA'); ?></span><span class="symbol-required">*</span></td>
                <td>
                    <select name="usu_compania" id="compania" required="required">
                    <?php
                            $valSelect = array_key_exists('usu_compania', $this->arrData) ? $this->arrData['usu_compania'] : '';
                            foreach ($this->objcompania as $row){
                                ?>
                                <option value="<?php echo $row->neg_nombre; ?>" <?php if($valSelect == $row->neg_nombre){echo "selected='selected' ";} ?> ><?php echo $row->neg_nombre?></option>
                                <?php
                            }
                    ?>
                    </select>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4" id="tdButton">
                    <input type="submit" class="button" value="<?php echo JText::_('COM_USUARIO_REGISTRO_BTN_SUBMIT');?>" id="btnRegistrar"/>
                </td>
            </tr>
        </table>
        <input type="hidden" name="option" value="com_usuario"/>
        <input type="hidden" name="task" value="guardarRegistro"/>
    </form>
</div>
