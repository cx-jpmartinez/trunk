<?php
defined('_JEXEC') or die('Restricted access');
$reto= $this->reto;    
$preguntas = $this->preguntas;
$editarReto = false;
$fechaActual = date('Y-m-d H:i');
if($fechaActual >= $reto->ret_fechaCreacion && $fechaActual<=$reto->ret_fechaVigencia && $reto->ret_estado='Vigente'){
    $editarReto = true;
}
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<?php if($editarReto){ ?>
<script type="text/javascript">
function validarPreg (){
    form = document.forms.preguntaFrm;
    if($("#pre_nombre").val().length <= 0) {  
        alert("El campo titulo es requerido");  
        return false;  
    }  
    if($("#pre_comentario").val().length <= 0) {  
        alert("El campo pregunta es requerido");  
        return false;  
    } 
    $("#Enviar").attr('disabled','disabled'); 
    form.submit();
}
</script>
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="padding-top: 1px !important;">Pregunta al experto</h3>
    </div>
</div>
<div id="cont_crear_solucion">
	<div id="crear_solucion">
	   <h4>&#9658; <?php echo $this->reto->ret_nombre; ?></h4>
		<form action="index.php" method="post" name="preguntaFrm" id="preguntaFrm" class="preguntaFrm"> 
		    <div class="cont_campos">
		        <label><?php echo JText::_('COM_PREGUNTAS_TITULO');?></label>
		    <input type="text" name="pre_nombre" id="pre_nombre"/>
		</div>
		<div class="cont_campos">
		    <label><?php echo JText::_('COM_PREGUNTAS_DESCRIPCION');?></label>
		    <textarea  name="pre_comentario"  id="pre_comentario"></textarea>
		</div>
		  <div id="acepto">
		    <p>
		    <input type="button" value="Enviar" id="Enviar" class="button" src="" onclick="validarPreg();"/>
		<input type="hidden" name="task" value="preguntar" />
		<input type="hidden" name="option" value="com_preguntas" />
		<input type="hidden" name="idRet" value="<?php echo $this->ret_id; ?>" />
		<input type="hidden" name="idUsu" value="<?php echo $this->usuario->usu_id; ?>" />
		        <input type="hidden" name="pregunt" value="1"/>
		        </p>
		    </div>
		</form>
	</div>
</div>
<?php } ?>
<?php if($this->preguntas){?>
	<div id="cont_reto_prof">
		<div id="titulo_retos">
		    <h3 class="titulo_reto" style="padding-top: 1px !important;"><?php echo JText::_('COM_PREGUNTAS_HISTORIAL');?></h3>
		</div>
	</div>
   <div id="cont_crear_solucion">
        <div id="crear_solucion">
         <?php foreach ($this->preguntas as $pregunta){ ?>
            <h4>&#9658; <?php echo $pregunta->pre_titulo; ?></h4><br>
            <p style="margin-left: 10px;">
                <span style="margin-left: 10px;font-weight:bold;">Innovador:</span> <?php echo $pregunta->usu_nombre;?><br> 
                <span style="margin-left: 10px;font-weight:bold;"><?php echo JText::_('COM_PREGUNTAS_PREGUNTA');?>:</span> <?php echo $pregunta->pre_descipcion; ?><br>
                <span style="margin-left: 10px;font-weight:bold;"><?php echo JText::_('Fecha');?>:</span> <?php echo $pregunta->pre_fechaCreacion; ?><br>
            </p>
            <?php if($pregunta->pre_respuesta){
            	 $respuesta = new respuesta();
                 $respuestas = $respuesta->cargarRespuestas($pregunta->pre_id);
                 foreach ($respuestas as $rest){	
            	
            ?>
                <p style="margin-left: 10px;">
                <span style="margin-left: 10px;font-weight:bold;">
                    <?php echo JText::_('COM_PREGUNTAS_RESPUESTA');?>
                </span>
                  <?php echo $rest->res_respuesta; ?><br>
                  <span style="margin-left: 10px;font-weight:bold;">
                    <?php echo JText::_('Fecha');?>:
                  </span> 
                   <?php echo $rest->res_fecha; ?>
                  
                </p>
            <?php } 
            }
            }
         ?>
        </div>
    </div>
<?php } ?>
                