<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

class PreguntasController extends JControllerLegacy
{ 
	function display() {
		$task = JRequest::getVar( 'task' );
		if($task=='preguntar'){
            $this->execute('preguntar');
		}elseif($task=='respuestas'){
			$this->execute('respuestas');
		}elseif($task=='experto'){
			$this->execute('experto');
        }elseif($task=='formRespuesta'){
        	$this->execute('formRespuesta');
        }elseif($task=='responder'){
            $this->execute('responder');        	        	
        }else{
            $this->execute('mostrarHacerPreguntas');
	    }
	}
	
	function responder(){
		$user = JFactory::getUser();
        
		$ret_id = JRequest::getVar( 'idRet' );
        $pre_id = JRequest::getVar( 'idPre' );
        $pre_tipo = JRequest::getVar( 'Publica','Publica' );
        $reto = new reto($ret_id);
        $pre_respuesta = JRequest::getVar( 'pre_respuesta' );
        $pregunta = new pregunta($pre_id);
        $respuesta = new respuesta();
        $respuesta->res_pre_id = $pregunta->pre_id;
        $respuesta->res_tipo = $pre_tipo;
        $respuesta->res_respuesta = $pre_respuesta;
        if($respuesta->store()){
        	$pregunta->pre_respuesta = 'enviada';
            $pregunta->store();
            if($pregunta){
                $usuario = new usuario($pregunta->pre_usu_id);
            }
          $sitio= JURI::base() ;
          $enlace= "<a href='$sitio/index.php?option=com_retos&view=reto&idreto=$reto->ret_id&Itemid=444'>$reto->ret_nombre</a>";
          $configMail = &JFactory::getConfig();
          $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
          $cuerpo.= "Hola, $usuario->usu_nombre $usuario->usu_apellido. </br></br>";
          $cuerpo.= "Has recibido una respuesta a la pregunta que le hiciste al experto del reto $enlace</br></br>";
          $cuerpo.= "'$pregunta->pre_descipcion:</br></br>";
          $cuerpo.= "$respuesta->res_respuesta'</br></br></br>";
          $cuerpo.= "Esperamos que la respuesta sea de gran utilidad para que soluciones el reto.</br></br></br>";
          $cuerpo.="Soluciones Innovadoras</br>";
          //$cuerpo.="<a href='$sitio'>$sitio</a></br>";
          $from = $configMail->get('mailfrom');
          $fromname = $configMail->get('fromname');
          $subject = JText::_('Respuesta del experto.');
          $correos = "$usuario->usu_email";
          JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
          $js="<script>";
		  //$js.="alert('Respuesta enviada satisfactoriamente');";
		  $js.="parent.location.reload();";
		  $js.="</script>";
		  echo $js;
		  exit;
        }else{
        	$js="<script>";
           // $js.="alert('No fue posible enviar la respuesta');";
            $js.="parent.location.reload();";
            $js.="</script>";
            echo $js;
            exit;
        }
    }
	
	function formRespuesta(){
		$user = JFactory::getUser();
        $usuario = new usuario($user->id);
        $ret_id = JRequest::getVar( 'idRet' );
        $pre_id = JRequest::getVar( 'idPre' );
        $reto = new reto($ret_id);
        $pregunta = new pregunta($pre_id);
        $newView = & $this->getView('respuestas','html');
        $newView->pregunta =& $pregunta;
        $newView->reto =& $reto;
        $newView->usuario =& $usuario;
        $newView->display('form_respuesta');
    }
	
    function experto(){
        $user = JFactory::getUser();
        $usuario = new usuario($user->id);
        $reto = new reto();
        $obj = $reto->listarRetosExperto($usuario->usu_email);
        $newView = & $this->getView('experto','html');
        $newView->obj =& $obj;
        $newView->usuario =& $usuario;
        $newView->display();
        
    }
	
	function respuestas(){
		$db =& JFactory::getDBO();
		$ret_id = JRequest::getVar( 'idRet' );
		$user = JFactory::getUser();
        $usuario = new usuario($user->id);
        $reto = new reto($ret_id);
        $sql= "SELECT pregunta.*
            FROM usuario
            inner join experto on (exp_mail=usu_email)
            inner join reto  on (ret_exp_id=exp_id)
            inner join pregunta on (ret_id=pre_ret_id)
            where usu_email = '$usuario->usu_email' and ret_id='$reto->ret_id'
            group by pre_id order by pre_id";
        $db->retos->setQuery( $sql );
        $preguntas = $db->retos->loadObjectList();
        $newView = & $this->getView('respuestas','html');
        $newView->preguntas =& $preguntas;
        $newView->reto =& $reto;
        $newView->usuario =& $usuario;
        $newView->display();
    }
	
	function mostrarHacerPreguntas(){
		$db =& JFactory::getDBO();
		$ret_id = JRequest::getVar( 'idRet' );
        $user = JFactory::getUser();
        $usuario = new usuario($user->id);
        $sql= "SELECT * FROM pregunta
            INNER JOIN usuario ON (usu_id=pre_usu_id) 
            where (pre_usu_id='$usuario->usu_id' OR pre_tipo='Publica') and pre_ret_id='$ret_id'";
        $db->retos->setQuery( $sql );
        $preguntas = $db->retos->loadObjectList();
        $reto = new reto($ret_id);
        $newView = & $this->getView('preguntas','html');
        $newView->preguntas =& $preguntas;
        $newView->usuario =& $usuario;
        $newView->ret_id =& $ret_id;
        $newView->reto =& $reto;
        $newView->display();
	}
	
	function preguntar(){
	  $user = JFactory::getUser();
	  $pre_nombre = JRequest::getVar( 'pre_nombre' );
      $pre_estado = JRequest::getVar( 'pre_estado','Privada' );
      $pre_comentario = JRequest::getVar( 'pre_comentario' );
      $usu_id = JRequest::getVar( 'idUsu' );
      $ret_id = JRequest::getVar( 'idRet' );
      $reto = new reto($ret_id);
      $experto = $reto->cargarExperto();
      $pregunta = new pregunta();
      $pregunta->pre_titulo = $pre_nombre;
      $pregunta->pre_estado = $pre_estado;
      $pregunta->pre_descipcion = $pre_comentario;
      $pregunta->pre_usu_id = $usu_id;
      $pregunta->pre_ret_id = $ret_id;
      $pregunta->pre_fechaCreacion = date('Y-m-d H:i');
      if($pregunta->store()){
      	  $sitio= SITIO;  
      	  $enlace= "<a href='$sitio/index.php?option=com_retos&view=reto&idreto=$reto->ret_id&Itemid=444'>$reto->ret_nombre</a>";
      	  $configMail = &JFactory::getConfig();
          $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
          $cuerpo.= "Hola, $experto->exp_nombre. </br></br>";
          $cuerpo.= "Como experto del reto $enlace, has recibido una pregunta de $user->name / $user->email:</br></br>";
          //$cuerpo.= "Como experto del reto $enlace, has recibido una pregunta de  Alberson Manunga Chara / amanunga@alimentoscarnicos.com.co:</br></br>";
          $cuerpo.= "'$pre_nombre:</br>";
          $cuerpo.= "$pre_comentario'</br></br></br>";
          $cuerpo.= "Tu oportuna y acertada respuesta  ayudará a crear una solución más.</br></br></br>";
          $cuerpo.="Soluciones Innovadoras</br>";
          //$cuerpo.="<a href='$sitio'>$sitio</a></br>";
          
          $from = $configMail->get('mailfrom');
          $fromname = $configMail->get('fromname');
          $correos = "$experto->exp_mail";
          //$correos = "afperez@cognox.com";
          $subject = JText::_('Duda para el experto.');
          JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
          $director = new director();
          $director->notificarDirectorPregunta($reto->ret_id,$reto->ret_nombre,$pre_nombre,$pre_comentario,$user->name,$user->email);
          $js="<script>";
          $js.="parent.location.reload();";
          $js.="</script>";
          echo $js;
          exit;
      }else{
      	  $js="<script>";
          $js.="parent.location.reload();";
          $js.="</script>";
          echo $js;
          exit;
      }
    }
}

