<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');
?>
<?php
class SolucionController extends JControllerLegacy
{
    function display($cachable = false, $urlparams = false)
    {

       $view        = JFactory::getApplication()->input->get( 'view' );
       $task        = JFactory::getApplication()->input->get( 'task' );
       $ret_id      = JFactory::getApplication()->input->get( 'idRet' );
       $solucionID  = JFactory::getApplication()->input->get( 'solucionID' );
       $origen      = JFactory::getApplication()->input->get( 'origen' );
       $user        = JFactory::getUser();
       $usuario     = new usuario($user->id);
       $domain      = strrchr($usuario->usu_email, '@UsuarioSinCorreo.com');
       /*  echo '<pre>';
        print_r($usuario);
        echo '</pre>';exit;*/

       if(!$usuario->usu_email || $domain=='@UsuarioSinCorreo.com'
        || !$usuario->usu_cedula
        || !$usuario->usu_telefono
        ){
           if($view=='verSolucion' && $origen=='8fd715a4a8bac9113c7486147a444da5'){
               //muestra la solucion
           }else{
                //$app = JFactory::getApplication();
                //$app->redirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&component=com_retos&path=&tmpl=component', false));
                echo "<script type=\"text/javascript\">window.parent.location='index.php?option=com_users&view=profile&layout=edit';</script>";
                exit;
           }
       }



       if($view=='verSolucion'){
           $idSol = JFactory::getApplication()->input->get( 'idSol' );
           $solucion = new solucion($idSol);
           $anexos = $solucion->cargarAnexos($solucion->sol_id);
           $participantes = $solucion->cargarParticipantesReto($solucion->sol_ret_id,$solucion->sol_id);
           //$solucion->cargarSolucionCompartida($solucion->sol_ret_id);
           if($solucion->sol_id){
               $preguntasComite = $solucion->preguntasComite();
               $reto = new reto($solucion->sol_ret_id);
               $formSolucion = $reto->cargarFormSolucion($reto->ret_id,$solucion->sol_id);
               $usuario = new usuario($solucion->sol_usu_id);
               $newView = $this->getView('verSolucion','html');
               $newView->obj = $reto;
               $newView->usuario = $usuario;
               $newView->anexos = $anexos;
               $newView->participantes = $participantes;
               $newView->solucion = $solucion;
               $newView->preguntasComite = $preguntasComite;
               $newView->formSolucion = $formSolucion;
               $newView->display();
           }
       }elseif($task=='solucion'){
            $this->execute('solucion');
       }elseif($task=='InscripcionReto'){

            $this->execute('InscripcionReto');
       }elseif($task=='elimarParticipante'){

       	    $this->execute('elimarParticipante');
       }elseif($task=='elimarAnexo'){

       	    $this->execute('elimarAnexo');
       }elseif($task=='elimarAnexoPrincipal'){

       	    $this->execute('elimarAnexoPrincipal');
       }elseif($task=='nuevaSolucion'){

       	    $this->execute('nuevaSolucion');
       }elseif($task=='preguntacomite'){

            $this->execute('preguntacomite');
       }elseif($task=='formComite'){
           $this->execute('formComite');
       }else{
       	    $cantidadSoluciones = validarCantidadSoluciones($ret_id);

       	    if($cantidadSoluciones>1 && !isset($solucionID))
            {
       	      $this->execute('listarSolucion');
       	    }else{
       	      $this->execute('MostrarFormulario');
       	    }

       }
    }

    function formComite(){
        $user = JFactory::getUser();
        $usuario = new usuario($user->id);
    	$ret_id = JFactory::getApplication()->input->get( 'idRet' );
        $sol_id = JFactory::getApplication()->input->get( 'idSol' );
        $reto = new reto($ret_id);
        $solucion = new solucion();
        $solucion->load($sol_id);
        $tmpl= 'mensajeCO';
        $newView = $this->getView('solucion','html');
        $newView->obj = $reto;
        $newView->usuario = $usuario;
        $newView->solucion = $solucion;
        $newView->display($tmpl);

    }




    function preguntacomite(){
        $db = JFactory::getDBO();
        $user = JFactory::getUser();
        $usuario = new usuario($user->id);
    	$ret_id = JFactory::getApplication()->input->get( 'idRet' );
        $sol_id = JFactory::getApplication()->input->get( 'idSol' );
        $pco_descripcion = JFactory::getApplication()->input->get( 'pco_descripcion' );
        $reto = new reto($ret_id);
        $solucion = new solucion();
        $solucion->load($sol_id);
        $preguntaComite =  new stdClass();
        $preguntaComite->pco_sol_id= $solucion->sol_id;
        $preguntaComite->pco_pregunta = $pco_descripcion;
        $preguntaComite->pco_fechapregunta = date('Y-m-d H:i');
        $preguntaComite->pco_remitente = $user->name;
        $db->retos->insertObject('preguntacomite',$preguntaComite,'pco_id');
        $lotus = new Lotus();
        $lotus->datosRespuesta($solucion->sol_titulo, $user->name, $pco_descripcion);
        echo "<script type=\"text/javascript\">parent.location.reload();</script>";
        exit;
        //$lotus->datosRespuesta('6-SOL-82-201203231009', $user->name, $pco_descripcion);

    }


    function listarSolucion()
    {

        $user       = JFactory::getUser();
        $usuario    = new usuario($user->id);
    	$ret_id     = JFactory::getApplication()->input->get( 'idRet' );
        $reto       = new reto($ret_id);


        $solucion   =  new solucion();
        $soluciones = $solucion->listarSolucionReto($reto->ret_id,$usuario->usu_id);
        $tmpl= 'soluciones';
        $newView            = $this->getView('solucion','html');
        $newView->obj       = $reto;
        $newView->usuario   = $usuario;
        $newView->soluciones = $soluciones;
        $newView->display($tmpl);
        return $this;
    }

    function MostrarFormulario(){

    	$user = JFactory::getUser();
        $usuario = new usuario($user->id);
        $ret_id = JFactory::getApplication()->input->get( 'idRet' );
        $reto = new reto($ret_id);
        $solucionID = JFactory::getApplication()->input->get( 'solucionID' );

        if($solucionID)
        {
        	$solucion = new solucion($solucionID);
        }else
        {
        	$solucion = new solucion();
        	$solucion->cargarSolucion($reto->ret_id,$usuario->usu_id);

        }

        $preguntasComite = $solucion->preguntasComite();
        if($reto->ret_estado!='Vigente' || ($solucion->sol_estado!='' && $solucion->sol_estado!='Pendiente'))
        {
            $anexos = $solucion->cargarAnexos($solucion->sol_id);
            $participantes = $solucion->cargarParticipantesReto($solucion->sol_ret_id,$solucion->sol_id);
            if($solucion->sol_id)
            {
                $formSolucion = $reto->cargarFormSolucion($reto->ret_id,$solucion->sol_id);
                $preguntasComite = $solucion->preguntasComite();
	            $reto = new reto($solucion->sol_ret_id);

	            $usuario = new usuario($solucion->sol_usu_id);
	            $newView = $this->getView('verSolucion','html');
	            $newView->obj = $reto;
	            $newView->usuario = $usuario;
	            $newView->anexos = $anexos;
	            $newView->participantes = $participantes;
	            $newView->solucion = $solucion;
                    $newView->preguntasComite = $preguntasComite;
                    $newView->formSolucion = $formSolucion;
	            $newView->display();
                return $this;
            }
        }
        else
        {

            $formSolucion = $reto->cargarFormSolucion($reto->ret_id,$solucion->sol_id);
            $solucion->cargarSolucionCompartida($reto->ret_id);
	        $participantes =  $solucion->cargarTitularReto($reto->ret_id,$solucion->sol_id);
	        $solucion->validarAceptacion($reto->ret_id);
	        $anexos = $solucion->cargarAnexos($solucion->sol_id);
	        $newView = $this->getView('solucion','html');
	        $newView->obj = $reto;
	        $newView->usuario = $usuario;
	        $newView->anexos = $anexos;
            $newView->preguntasComite = $preguntasComite;
	        $newView->participantes = $participantes;
            $newView->formSolucion = $formSolucion;
            if($solucion)
            {
	            $newView->solucion = $solucion;
	        }else
            {
	            $solucion = '';
	            $newView->solucion = $solucion;
	        }

            $newView->display();
        }


    }

    function elimarParticipante(){
    	 $db = JFactory::getDBO();
    	 $idcsol = JFactory::getApplication()->input->get( 'idcsol' );
    	 $solucionCompartida = new solucioncompartida($idcsol);
    	 if($solucionCompartida->slc_usu_id){
	    	  $sql="DELETE FROM solucion
	    	       where
	               sol_ret_id='$solucionCompartida->slc_ret_id'
	               AND sol_usu_id='$solucionCompartida->slc_usu_id'
	               AND sol_sol_id='$solucionCompartida->slc_sol_id'";
	          $db->retos->setQuery( $sql );
	          $db->retos->query();
         }
         JRequest::setVar( 'solucionID', $solucionCompartida->slc_sol_id );
    	 $solucionCompartida->delete();
         $this->execute('MostrarFormulario');
         //echo "<script type=\"text/javascript\">parent.location.reload();</script>";
         //exit;
    }

    function elimarAnexo(){
         $db = JFactory::getDBO();
    	 $idanx = JFactory::getApplication()->input->get( 'idanx' );
         if($idanx){
	         $anexo = new anexo($idanx);
                 $texto = $anexo->anx_nombre;
                 $valor = $anexo->anx_archivo;
	         $borrarImagen = unlink("./images/retos/soluciones/$anexo->anx_archivo");
	         if($borrarImagen){
                     $anexo->delete();
                     $sql="DELETE FROM detallesolucion WHERE det_texto='$texto' and det_valor='$valor'";
                     $db->retos->setQuery( $sql );
                     $db->retos->query();
	         }
         }
         $this->execute('MostrarFormulario');
       //  echo "<script type=\"text/javascript\">parent.location.reload();</script>";
        // exit;
    }

    function elimarAnexoPrincipal(){
         $idcsol = JFactory::getApplication()->input->get( 'idcsol' );
         if($idcsol){
         	$solucion = new solucion($idcsol);
         	$solucion->sol_anexo = '';
         	$solucion->store();
         	$borrarImagen = unlink("./images/retos/soluciones/$solucion->sol_anexo");
         }
         $this->execute('MostrarFormulario');
         //echo "<script type=\"text/javascript\">parent.location.reload();</script>";
         //exit;
    }


    function tyc(){
           $user = JFactory::getUser();
           $usuario = new usuario($user->id);
           $ret_id = JFactory::getApplication()->input->get( 'idRet' );
           $reto = new reto($ret_id);
           $textoTYC = $reto->cargarTerminosCondiciones();
           $newView = $this->getView('solucion','html');
           $newView->obj = $reto;
           $newView->textoTYC = $textoTYC;
           $newView->usuario = $usuario;
           $newView->display('tyc');
    }

    function InscripcionReto(){
       $user = JFactory::getUser();
       $usuario = new usuario($user->id);
       $idRet= JFactory::getApplication()->input->get( 'idRet' );
       $checkTerminos = JFactory::getApplication()->input->get( 'sol_terminos' );
       $reto = new reto($idRet);
       if($reto->ret_id && $checkTerminos && $reto->ret_estado=='Vigente'){
           $tyc = new tyc();
           $tyc->tyc_ret_id = $reto->ret_id;
           $tyc->tyc_usu_id = $usuario->usu_id;
           $tyc->tyc_fecha = date('Y-m-d H:i');


           if( isset($_FILES['sol_anexoTyc']['name']) && $_FILES['sol_anexoTyc']['name']!=''){
               $uploads_dir = './images/retos/tyc/';
               $tamanoMax = 5000000;
               $ext = explode('.', $_FILES['sol_anexoTyc']['name']);
               $num = count($ext)-1;
               if($_FILES['sol_anexoTyc']['name'] > $tamanoMax){
                   return false;
               }
               $carga = move_uploaded_file($_FILES['sol_anexoTyc']['tmp_name'], $uploads_dir .'anexoTYC-'.$reto->ret_id.'-'.$usuario->usu_id.'.'.$ext[1] );
               if($carga){
                   $tyc->tyc_anexo = 'anexoTYC-'.$reto->ret_id.'-'.$usuario->usu_id.'.'.$ext[1];
               }
           }



           if($tyc->store()){
           	   $solucion = new solucion();
           	   $objsSolucionCompartidas = $solucion->validarSolucionCompartida($usuario->usu_email,$reto->ret_id);
           	   if($objsSolucionCompartidas){
	           	   foreach ($objsSolucionCompartidas as $objSolucionCompartida){
	           	       $solucionCompartida = new solucioncompartida($objSolucionCompartida->slc_id);
                       $solucionCompartida->slc_usu_id=$usuario->usu_id;
                       $solucionCompartida->store();
                       $solucion = new solucion();
                       $solucion->getby($usuario->usu_id,$solucionCompartida->slc_sol_id);
                       $solucion->sol_sol_id =  $solucionCompartida->slc_sol_id;
                       $solucion->sol_usu_id = $usuario->usu_id;
                       $solucion->sol_ret_id = $reto->ret_id;
                       $solucion->sol_estado = 'Pendiente';
                       $solucion->sol_fechaCreacion = date('Y-m-d H:i');
                       $solucion->store();
                   }
           	   }else{
           	   	   $solucion = new solucion();
           	   	   $solucion->sol_usu_id = $usuario->usu_id;
                   $solucion->sol_ret_id = $reto->ret_id;
                   $solucion->sol_estado = 'Pendiente';
                   $solucion->sol_fechaCreacion = date('Y-m-d H:i');
           	   	   $solucion->store();
           	   }
           	   echo "<script type=\"text/javascript\">parent.location.reload();</script>";
               exit;
           }else{
           	    echo "<script type=\"text/javascript\">parent.location.reload();</script>";
           	    exit;
           }
       }else{
       	    echo "<script type=\"text/javascript\">parent.location.reload();</script>";
       	    exit;
       }
    }


     function solucion(){
       $user = JFactory::getUser();
       $usuario = new usuario($user->id);


       $sol_descripcion = JFactory::getApplication()->input->get( 'sol_descripcion' );
       $sol_anexo= JFactory::getApplication()->input->get( 'sol_anexo' );
       $sol_estado= JFactory::getApplication()->input->get( 'sol_estado','Pendiente' );
       $idRet= JFactory::getApplication()->input->get( 'idRet' );
       $idSol= JFactory::getApplication()->input->get( 'idSol' );
       $reto = new reto($idRet);


       if($reto->ret_estado=='Vigente'){
	       //$compania = new compania($reto->ret_com_id);
	       //$titulo = date('YmdHi');
	       $solucion = new solucion($idSol);



	       $titulo = $solucion->cantidadSoluciones($reto->ret_id,$usuario->usu_id);
	       if(!$titulo){
	           $titulo = '';
	       }
	       $solucion->sol_usu_id = $usuario->usu_id;

	       $solucion->sol_ret_id = $idRet;
	       $solucion->sol_estado = $sol_estado;
	       //$solucion->sol_titulo = $reto->ret_id.'-SOL-'.$usuario->usu_id;
	       if(!$solucion->sol_titulo){
	           $solucion->sol_titulo = $reto->ret_id.'-SOL-'.$usuario->usu_id.'-'.$titulo;
	       }
	       $solucion->sol_descripcion = $sol_descripcion;
	       $solucion->sol_anexo = $sol_anexo;
	       $solucion->sol_fechaCreacion = date('Y-m-d H:i');



	       if( isset($_FILES['sol_anexos']['name']) && $_FILES['sol_anexos']['name']!=''){
	            $uploads_dir = './images/retos/soluciones/';
	            $tamanoMax = 5000000;
	            $ext = explode('.', $_FILES['sol_anexos']['name']);
	            $num = count($ext)-1;
	            if($_FILES['sol_anexos']['name'] > $tamanoMax){
	                return false;
	            }
	            $carga = move_uploaded_file($_FILES['sol_anexos']['tmp_name'], $uploads_dir .$solucion->sol_titulo.'.'.$ext[1] );
	            if($carga){
	                $solucion->sol_anexo = $solucion->sol_titulo.'.'.$ext[1];
	                $solucion->sol_nameanexo = $_FILES['sol_anexos']['name'];
	            }
	        }


	        if( isset($_FILES['sol_anexos1']['name']) && $_FILES['sol_anexos1']['name']!='')
            {
	            $uploads_dir = './images/retos/soluciones/';
	            $tamanoMax = 5000000;
	            $ext = explode('.', $_FILES['sol_anexos1']['name']);
	            $num = count($ext)-1;
	            if($_FILES['sol_anexos1']['name'] > $tamanoMax){
	                return false;
	            }

	            $carga1 = move_uploaded_file($_FILES['sol_anexos1']['tmp_name'], $uploads_dir . $solucion->sol_titulo.'anexo1.'.$ext[1] );

	            if($carga1)
                {
	                $anexo1 = $solucion->sol_titulo.'anexo1.'.$ext[1];
	                guardarAnexo($anexo1,$solucion,$_FILES['sol_anexos1']['name']);
	            }
	        }

	        if( isset($_FILES['sol_anexos2']['name']) && $_FILES['sol_anexos2']['name']!=''){
	            $uploads_dir = './images/retos/soluciones/';
	            $tamanoMax = 5000000;
	            $ext = explode('.', $_FILES['sol_anexos2']['name']);
	            $num = count($ext)-1;
	            if($_FILES['sol_anexos2']['name'] > $tamanoMax){
	                return false;
	            }
	            $carga2=move_uploaded_file($_FILES['sol_anexos2']['tmp_name'], $uploads_dir . $solucion->sol_titulo.'anexo2.'.$ext[1] );

	            if($carga2){
	                $anexo2= $solucion->sol_titulo.'anexo2.'.$ext[1];
	                guardarAnexo($anexo2,$solucion,$_FILES['sol_anexos2']['name']);
	            }
	        }

	        if( isset($_FILES['sol_anexos3']['name']) && $_FILES['sol_anexos3']['name']!=''){
		            $uploads_dir = './images/retos/soluciones/';
		            $tamanoMax = 5000000;
		            $ext = explode('.', $_FILES['sol_anexos3']['name']);
		            $num = count($ext)-1;
		            if($_FILES['sol_anexos3']['name'] > $tamanoMax){
		                return false;
		            }
		            $carga3 = move_uploaded_file($_FILES['sol_anexos3']['tmp_name'], $uploads_dir . $solucion->sol_titulo.'anexo3.'.$ext[1] );
		            if($carga3)
                    {
		               $anexo3 = $solucion->sol_titulo.'anexo3.'.$ext[1];
		               guardarAnexo($anexo3,$solucion,$_FILES['sol_anexos3']['name']);
		            }
		      }


	        if( isset($_FILES['sol_anexos4']['name']) && $_FILES['sol_anexos4']['name']!=''){
	                $uploads_dir = './images/retos/soluciones/';
	                $tamanoMax = 5000000;
	                $ext = explode('.', $_FILES['sol_anexos4']['name']);
	                $num = count($ext)-1;
	                if($_FILES['sol_anexos4']['name'] > $tamanoMax)
                    {
	                    return false;
	                }
	                $carga4 = move_uploaded_file($_FILES['sol_anexos4']['tmp_name'], $uploads_dir . $solucion->sol_titulo.'anexo4.'.$ext[1] );
	                if($carga4)
                    {
	                    $anexo4 = $solucion->sol_titulo.'anexo4.'.$ext[1];
	                    guardarAnexo($anexo4,$solucion,$_FILES['sol_anexos4']['name']);
	                }
	          }

	        if($solucion->store())
            {
                    $formSolucion = $reto->cargarFormSolucion($reto->ret_id, $solucion->sol_id);

                    if(!empty($formSolucion))
                    {

                        $db = JFactory::getDBO();

                        $sql ="SELECT * FROM regsolucion WHERE reg_sol_id='$solucion->sol_id'";
                        $db->retos->setQuery( $sql );
                        $reg_id = $db->retos->loadResult();

                        if($reg_id)
                        {
                            $regsolucion = new regsolucion($reg_id);
                        }else{
                            $regsolucion = new regsolucion();
                            $regsolucion->reg_sol_id = $solucion->sol_id;
                            $regsolucion->reg_usu_id = $solucion->sol_usu_id;
                            $regsolucion->reg_frm_id = $formSolucion[0]->pro_frm_id;
                            $regsolucion->reg_fechaCreacion = date ( 'Y-m-d H:i:s' );
                            $regsolucion->store();
                        }
                        foreach ($formSolucion as $form)
                        {
                           $reto->guardarSolucionForm($regsolucion,$form,$solucion);
                        }

                    }

                    $numeroSoluciones = 12;
                    for ($i=0;$i<$numeroSoluciones;$i++)
                    {
                        $nombreParticipante =  JRequest::getVar( "integrante$i" );
                        $mailParticipante = JRequest::getVar( "mailintegrante$i" );


                        if(!empty($nombreParticipante) && !empty($mailParticipante))
                        {
                            guardarSolucionCompartida($solucion,$mailParticipante,$nombreParticipante,$user->name,$reto);
                        }

                    }

	            if( $solucion->sol_estado=='Finalizado')
                {
                    $nombreNegocio  = $reto->cargarNombreNegocio($reto->ret_id);
                    $experto        = $reto->cargarDatosExperto();
                    $evaluadores    = $reto->cargarComite();
                    $lotusStatus    = false;

                    //Lotus Connect
                    try {

                        $lotus       = new Lotus();
                        $lotusStatus = true;

                    } catch (Exception $e)
                    {
                        //lotus fail
                        echo 'Conexion con lotus fallida, Lotus Error: ',  $e->getMessage(), "\n";
                    }

		            $participantes = $solucion->cargarParticipantesReto($reto->ret_id,$solucion->sol_id);
		            $pops = array();
		            $i=0;
		            foreach ($participantes as $participante){
		            	$pop = array('nombre'=>$participante->nombre,'cedula'=>$participante->usu_cedula, 'telefono'=>$participante->usu_telefono,'email'=>$participante->usu_email,'username'=>$participante->usu_nombreUsuario);
		            	$pops[$i] = $pop;
		            	$i++;
		            }
                            /*
                            echo '<pre>';
                            print_r($pops);
                            echo '</pre>';exit;*/

                            $retoxcompania = new retoxcompania();
                            $companias =  $retoxcompania->cargarCompaniasReto($reto->ret_id);
                            $arrcomp = array();
                            $i=0;
                            foreach ($companias as $compania){
                                $arr = array('nombre'=>$compania['neg_nombre'],'nit'=>$compania['neg_nit'], 'valorPuntos'=>$compania['rxc_puntos']);
                                $arrcomp[$i] = $arr;
                                $i++;
                            }
                            /*
                            echo '<pre>';
                            print_r($arrcomp);
                            echo '</pre>';
                            exit;*/
                    $sitio= JURI::base() ;
		            $enlace = "$sitio/index.php?option=com_solucion&view=verSolucion&idSol=$solucion->sol_id";

                    if( $lotusStatus)
                    {
                        $lotus->enviarSolucion("$reto->ret_id", "$reto->ret_nombre", "1", "$nombreNegocio", "$solucion->sol_titulo", "$reto->ret_puntos", "$enlace", $pops, $evaluadores, $experto);
                    }

                    /*
                     echo '<pre>';
                     print_r($ww);
                     echo '</pre>';
                     exit;*/

		            $solucion->actualizarSolucionCompartida($solucion->sol_id);
		            $director = new director();
		            $director->notificarDirector($reto->ret_id,$reto->ret_nombre,$enlace);
                    $solucion->notificarEquipo($reto->ret_nombre,$participantes,$solucion->sol_titulo);

		        }
	            $js="<script>";
	            $js.="parent.location.reload();";
	            $js.="</script>";
	            echo $js;
	            exit;
	       }else{
	            $js="<script>";
	            $js.="parent.location.reload();";
	            $js.="</script>";
	            echo $js;
	            exit;
	       }
       }else{
    	     $js="<script>";
             $js.="parent.location.reload();";
             $js.="</script>";
             echo $js;
             exit;
       }
    }


    function nuevaSolucion(){
       $user = JFactory::getUser();
       $usuario = new usuario($user->id);
       $idRet= JFactory::getApplication()->input->get( 'idRet' );
       $reto = new reto($idRet);
       if($reto->ret_estado=='Vigente'){
	       //$titulo = date('YmdHi');
	       $solucion = new solucion();
	       $titulo = $solucion->cantidadSoluciones($reto->ret_id,$usuario->usu_id);
           if(!$titulo){
               $titulo = '';
           }
	       $solucion->sol_usu_id = $usuario->usu_id;
	       $solucion->sol_ret_id = $reto->ret_id;
	       $solucion->sol_estado = 'Pendiente';
	       $solucion->sol_titulo = $reto->ret_id.'-SOL-'.$usuario->usu_id.'-'.$titulo;
	       $solucion->sol_fechaCreacion = date('Y-m-d H:i');
	       $solucion->store();
	       $ret_id = JRequest::setVar( 'solucionID',$solucion->sol_id );
	       $this->execute('MostrarFormulario');
       }else{
       	    $js="<script>";
            $js.="parent.location.reload();";
            $js.="</script>";
            echo $js;
            exit;
       }
    }

}
