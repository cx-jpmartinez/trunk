<?php

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Solucion', JPATH_COMPONENT);

$controller = JControllerLegacy::getInstance('Solucion');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
