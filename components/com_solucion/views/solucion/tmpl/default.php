<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$solucion = $this->solucion;
$anexos = $this->anexos;

$modificar=true;
//$trueReadOnly="readonly='true'";
$readonly = '';
$noEnviar = '';
if($usuario->usu_id!=$solucion->sol_usu_id){
  $modificar= false;
  $readonly = 'disabled';  
  $estadoCapaForm = 'none';
  $estadoCapaAceptar = '';
  $solCom = validarSolucionCompatida($solucion->sol_id,$usuario->usu_id);
  if($solCom){
      $estadoCapaForm = '';
      $estadoCapaAceptar = 'none';
  }
}else{
    $estadoCapaAceptar = 'none';
    $estadoCapaForm = '';
}
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<link rel="stylesheet" type="text/css" href="_objects/css/redmond/jquery-ui-1.8.16.custom.css" />
<script src="_objects/scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
function mostrarSolucion(idsol,usu_id,idamigo){
    form = document.forms.participarFrm;
    var answer = confirm("Estas seguro que deseas participar de la solución");
    if (answer){
        document.getElementById('crear_solucion').style.display='';
        document.getElementById('aceptarSolucion').style.display='none';
        $.ajax({
            url: 'index.php?option=com_retos&task=aceptarParticipar&idsol='+idsol+'&usu_id='+usu_id+'&idamigo='+idamigo,
            context: document.body
        }).done(function() { 
            $(this).addClass("done");
        });
        return true;
    }else{
        return false;  
    }
} 

function rechazarSolucion(idsol,usu_id,id_amigo){
    form = document.forms.participarFrm;
    var answer = confirm("Estas seguro que deseas rechazar la invitación");
    if (answer){
        form.option.value = 'com_retos';
        form.task.value = 'rechazarParticipar';
        form.idsol.value = idsol;
        form.usu_id.value = usu_id;
        form.idamigo.value = id_amigo;
        $("#Aceptar_par").attr('disabled','disabled');
        $("#Rechazar_par").attr('disabled','disabled');  
        form.submit();
        /*
        $.ajax({
            url: 'index.php?option=com_retos&task=rechazarParticipar&idsol='+idsol+'&usu_id='+usu_id+'&idamigo='+id_amigo,
            context: document.body
        });*/
        //Window.close();
       //parent.location.reload();
       return true;
    }else{
        return false;  
    }
    
}

$(function(){
    $('#integrante0').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante0').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante0').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
           
            
        }
      });
});

$(function(){
    $('#integrante1').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante1').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante1').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
           
            
        }
      });
});

$(function(){
    $('#integrante2').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante2').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante2').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante3').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante3').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante3').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante4').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante4').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante4').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante5').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante5').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante5').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante6').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante6').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante6').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante7').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante7').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante7').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante8').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante8').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante8').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante9').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante9').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante9').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante10').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante10').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante10').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});

$(function(){
    $('#integrante11').autocomplete({
        source:'index.php?option=com_retos&task=cargarAjax&format=ajax'
        , param: { text: '' }
        , options: { scroll: true }
        , select: function (event, ui){
            var arr = ui.item.value.split(' | ');
            ui.item.value = ui.item.text;
            $('#mailintegrante11').val(arr[1]);
            if(arr[1]!=''){
                $('#mailintegrante11').attr('readonly',true);    
            }
            $("#aceptar").attr('disabled','disabled');
            $("#aceptar").css({'background-image':'none','background-color':'gray !important'});
        }
      });
});



function validar (){
	form = document.forms.solucionFrm;
	/*if($("#sol_descripcion").val().length <= 0) {  
        alert("El campo descripción es requerido");  
        return false;  
    }*/
	$("#aceptar").attr('disabled','disabled');
	$("#Guardar").attr('disabled','disabled');  
	form.submit();
	
}

function abrirAnexos (){
    document.getElementById('capaAnexo').style.display='';
    document.getElementById('anexos').style.display='none';
}




function validarMensaje (){
	form = document.forms.preguntaCoFrm;
	if($("#pco_descripcion").val().length <= 0) {  
            alert("El campo mensaje es requerido");  
        return false;  
        }
	$("#Enviar").attr('disabled','disabled');
	form.submit();
}
</script>
<?php 
$NombreAnexo1='';
$Anexo1='';
$IdAnexo1='';
$NombreAnexo2='';
$Anexo2='';
$IdAnexo2='';
$NombreAnexo3='';
$Anexo3='';
$IdAnexo3='';
$NombreAnexo4='';
$Anexo4='';
$IdAnexo4='';
if($anexos){
	if(isset($anexos[0])){
       $NombreAnexo1 = $anexos[0]->anx_archivo;
       $IdAnexo1 = $anexos[0]->anx_id;
       $Anexo1 = $anexos[0]->anx_nombre;
    }
    if(isset($anexos[1])){
       $NombreAnexo2 = $anexos[1]->anx_archivo;
       $IdAnexo2 = $anexos[1]->anx_id;
       $Anexo2 = $anexos[1]->anx_nombre;
    }
    if(isset($anexos[2])){
       $NombreAnexo3 = $anexos[2]->anx_archivo;
       $IdAnexo3 = $anexos[2]->anx_id;
       $Anexo3 = $anexos[2]->anx_nombre;
    }
    if(isset($anexos[3])){
       $NombreAnexo4 = $anexos[3]->anx_archivo;
       $IdAnexo4 = $anexos[3]->anx_id;
       $Anexo4 = $anexos[3]->anx_nombre;
    }
}
?>
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="padding-top: 1px !important;">Solución</h3>
    </div>
</div>
<div id="cont_crear_solucion">
    <div id="aceptarSolucion" style="display:<?php echo $estadoCapaAceptar; ?>;">
        <h4>&#9658; <?php  echo $reto->ret_nombre; ?></h4>
        <form action="index.php" method="post" name="participarFrm" id="participarFrm">
            <p>
                <!-- input type="checkbox" id="sol_terminos" name="sol_terminos"-->
            <b>
            Acepta o rechaza la invitación de <?php echo cargarNombreUsuario ($solucion->sol_usu_id); ?>
            para participar en el equipo de la solución.
            </b>
            <div id="acepto" style="width: 400px !important;">
                <input type="button" value="Aceptar" name="Aceptar_par" class="button" onclick="mostrarSolucion('<?php echo $solucion->sol_id; ?>','<?php echo $usuario->usu_id; ?>','<?php echo $solucion->sol_usu_id; ?>');"/>
                <input type="button" value="Rechazar" name="Rechazar_par" class="button" onclick="rechazarSolucion('<?php echo $solucion->sol_id; ?>','<?php echo $usuario->usu_id; ?>','<?php echo $solucion->sol_usu_id; ?>');"/>
                <input type="hidden" name="option">
                <input type="hidden" name="task">
                <input type="hidden" name="idsol">
                <input type="hidden" name="usu_id">
                <input type="hidden" name="idamigo">
            </div>
            </p>
        </form>
    </div>
    
    
        
 
    
    
    
    
    
    
<div id="crear_solucion" style="display:<?php echo $estadoCapaForm; ?>;">
    <h4>&#9658; <?php  echo $reto->ret_nombre; ?></h4>
    <form action="index.php" method="post" name="solucionFrm" id="solucionFrm"  class="solucionFrm" enctype="multipart/form-data">
    <?php 
        if(!$solucion->sol_titulo){
            $tituloSol= $reto->ret_id.'-SOL-'.$usuario->usu_id;
        }else{
            $tituloSol = $solucion->sol_titulo;
        }
    ?>
    <div class="cont_campos">
      <b style="color: red;text-align:center;">Los campos con * son obligatorios de diligenciar.</b>
    </div>
    <p>
        
        
        
        
        <b><?php echo JText::_('Código de la solución:');?></b><br>
        <input type="text" id="sol_titulo" name="sol_titulo" value="<?php echo $tituloSol;  ?>" disabled readonly="readonly">
    </p>
        
        
     <?php
     echo $validacion='';
     foreach ($this->formSolucion as $input){ ?>
        <div class="cont_campos" style="borde: 1px;">
       <?php $requerido = '';
        if($input->pro_requerido){
             $campoReque = "'#$input->pro_nombre'";
             $msjrequerido = "$input->pro_texto";
             $requerido = 'required';
             $validacion.="if ($($campoReque).length > 0) {";
             $validacion.="if($($campoReque).val().length <= 0){";
             $validacion.="alert('campo requerido');"; 
             $validacion.="$($campoReque).focus();";
             $validacion.="$($campoReque)".'.css("background-color","#FFFFCC");';
             $validacion.='return false;';
             $validacion.='}';
             $validacion.='}';
        }
        $maxlength = '';
        if($input->pro_caracteres>0){
               $maxlength =  $input->pro_caracteres;
        }
        ?>
           
            <p><b> <?php  echo $input->pro_texto;?> &nbsp; 
       <?php if($input->pro_requerido){ echo "<span style='color:red;font-weight: bold;' >*</span>";  } ?>
       </b> 
       <?php 
        if($input->pro_tipo=='select'){ ?>
           <br>
            <select name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" title="<?php echo $input->pro_descripcion ?>">
                <?php foreach ($input->options as $option){ ?>
                    <option value="<?php echo $option->pro_valor; ?>" <?php if($input->det_valor==$option->pro_valor){echo 'selected';} ?>><?php echo $option->pro_texto; ?></option>
                <?php } ?>
            </select>
        <?php }elseif($input->pro_tipo=='textbox'){ 
            $width = '540px;';
            if(!$input->pro_ancho){
                $width = "540px;";
            }else{
                $width = $input->pro_ancho.'px;';
            }
            ?>
           <br>
           <input type="text" name="<?php echo $input->pro_nombre; ?>" class="inputbox" maxlength="<?php echo $maxlength; ?>" size="<?php  echo $maxlength; ?>" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" title="<?php echo $input->pro_descripcion ?>" value="<?php echo $input->det_valor;  ?>" style="width:<?php echo $width;?>">
        <?php }elseif($input->pro_tipo=='textarea'){ ?>
            <br>
            <textarea name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" style="width: 540px;" cols="<?php //echo $input->pro_ancho; ?>"  rows="<?php echo $input->pro_alto; ?>" title="<?php echo $input->pro_descripcion ?>"><?php echo $input->det_valor;  ?></textarea> 
        <?php }elseif($input->pro_tipo=='file'){ 
                if($input->det_valor && $input->det_texto){
                ?>    
                    <a href="./images/retos/soluciones/<?php echo $input->det_valor; ?>" target="_blank"><?php echo $input->det_texto; ?></a>
                <?php }else{
                     echo '<br>';
                  ?>
                    <input type="file" name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" size="70" style="width:540px;">
                <?php } ?>
                <br>
        <?php }elseif($input->pro_tipo=='check'){ echo '<br>'; ?>
             <?php  foreach ($input->options as $option){  ?>
                <span>
                <input type="checkbox" name="<?php echo $option->pro_nombre; ?>" class="inputbox" id="<?php echo $option->pro_nombre; ?>"  value="<?php echo $option->pro_valor; ?>" title="<?php echo $input->pro_descripcion ?>" style="width:15px;height: 15px;" <?php if($option->det_valor==$option->pro_valor){ echo 'checked';} ?>>
                                </span>

                <?php echo $option->pro_texto ?>  <br>
            <?php } ?>
         <?php }elseif($input->pro_tipo=='radio'){ ?>
           <input type="radio" name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" value="<?php echo $input->pro_valor; ?>" title="<?php echo $input->pro_descripcion ?>" style="width:33px;"> 
        <?php }elseif($input->pro_tipo=='fecha'){ ?>
           <br>
           <input type="text" class="inputbox" size="<?php  echo $maxlength; ?>" maxlength="<?php echo $maxlength; ?>" id="jform_ret_fechaPublicacion" name="jform[ret_fechaPublicacion]" title="<?php echo $input->pro_descripcion ?>" value="<?php echo $input->det_valor;  ?>"> 
        <?php }?>
           </p>
        </div>
    <?php } ?>
        
        
        
        
    
        <h5 id="compartida">Solución compartida</h5>
        <?php if($this->participantes && $modificar){ ?>
		<div class="cont_campos">
		    <span style="color: #59585D;font-family:FocoCorpBd;">Nombre:</span> <?php echo $this->participantes->nombre; ?>
		   &nbsp;|&nbsp;
		   <span style="color: #59585D;font-family:FocoCorpBd;">E-mail:</span> <?php echo $this->participantes->usu_email; ?>
                   <?php 
                   $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id,  $this->participantes->usu_id); 
                   if($anexoTYC){
                   ?>
                         &nbsp;|&nbsp;
                        <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                            <img src="./images/anexotyc.png" title="Térmnos y condiciones" border="0">
                        </a>
                   <?php 
                   }
                   ?>
                   
                   
		</div> 
        <?php } ?>
        
        
         <?php
            $numeroSoluciones = 12;
            for ($i=0;$i<$numeroSoluciones;$i++){
                $nombreId = 'integrante'.$i;
                $mailId = 'mailintegrante'.$i;
                $slc_id = '';
                $nombre = '';
                $mail = '';
                $slc_acepto = '';
                $slc_aceptoSol = '';
                $disabled ='';
                $usuID = '';
                if($solucion->_sol_solucionCompartida){
                    if(isset($solucion->_sol_solucionCompartida[$i])){
                        $slc_id = $solucion->_sol_solucionCompartida[$i]->slc_id;
                        $nombre = $solucion->_sol_solucionCompartida[$i]->slc_usu_nombre;
                        $mail = $solucion->_sol_solucionCompartida[$i]->slc_usu_email;
                        $slc_acepto = $solucion->_sol_solucionCompartida[$i]->slc_usu_id;
                        $slc_aceptoSol = $solucion->_sol_solucionCompartida[$i]->slc_estado;
                        $disabled ='disabled';
                        $usuID = $solucion->_sol_solucionCompartida[$i]->slc_usu_id;
                    }else{
                        $slc_id = '';
                        $nombre = '';
                        $mail = '';
                        $slc_acepto = '';
                        $disabled ='';   
                        $slc_aceptoSol = '';
                        $usuID = '';
                    }
                }
            ?>
                <?php if($modificar){ ?>
	                <div class="cont_campos">
	                    <span style="color: #59585D;font-family:FocoCorpBd;">Nombre:</span>
	                    <input type="text" name="<?php echo $nombreId; ?>" id="<?php echo $nombreId; ?>" value="<?php echo $nombre; ?>" <?php echo $disabled; ?> <?php echo $readonly; ?> style="font-size:15px;width:180px"/>
	                    <span style="color: #59585D;font-family:FocoCorpBd;">E-mail:</span>
	                    <input type="text" name="<?php echo $mailId; ?>" id="<?php echo $mailId; ?>" value="<?php echo $mail; ?>" <?php echo $disabled; ?> readonly="readonly" <?php echo $readonly; ?> style="font-size:15px;width:180px"/>
	                    <?php if($slc_acepto && $slc_aceptoSol<>'Rechazada'){ ?>
	                        &nbsp; <!--img src="./images/acepto.png" title="<?php //echo $nombre; ?> acepto tu invitación"> &nbsp; | &nbsp; -->
	                    <?php }elseif($slc_acepto && $slc_aceptoSol=='Rechazada'){
                                $noEnviar = true;
                             ?>
                                <span style="color: red;"> Rechazada</span>  &nbsp; | &nbsp;    
                            <?php } ?>    
	                    <?php if(($nombre && $mail) && $modificar){ ?>
	                    <a title="Eliminar a <?php echo $nombre; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarParticipante&amp;idRet=<?php echo $reto->ret_id; ?>&idcsol=<?php echo $slc_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
	                        <img src="./images/eliminar.png">
	                    </a>
                            <?php } ?>
                            <?php if($slc_aceptoSol=='Aceptada'){ ?>
                                &nbsp; | &nbsp;    
                                <img src="./images/agentes.png" title="Aceptó participar en el reto">
                            <?php }elseif($slc_aceptoSol=='Pendiente'){
                                $noEnviar=true;
                             ?>
                                &nbsp; | &nbsp; 
                                <img src="./images/alerta.gif"  title="Pendiente de respuesta">
                            <?php } ?>
                                
                            <?php 
                            $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id,  $usuID); 
                            if($anexoTYC){
                            ?>
                                |
                                <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                    <img src="./images/anexotyc.png" title="Térmnos y condiciones">
                                </a>
                            <?php 
                            }
                            ?>    
                                
                                
	               </div> 
	            <?php }else{
	                   if($i==0){
                            if($this->participantes){ ?>
                            <div class="cont_campos">
                                <span style="color: #59585D;font-family:FocoCorpBd;">Nombre:</span> <?php echo $this->participantes->nombre; ?>
                                &nbsp;|&nbsp;
                                <span style="color: #59585D;font-family:FocoCorpBd;">E-mail:</span> <?php echo $this->participantes->usu_email; ?>
                                <?php 
                                $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id,  $this->participantes->usu_id); 
                                if($anexoTYC){
                                ?>
                                    &nbsp;|&nbsp;
                                    <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                        <img src="./images/anexotyc.png" title="Térmnos y condiciones" border="0">
                                    </a>
                                <?php 
                                }
                                ?>
                            </div> 
                        <?php } 
	                   }  
	                   if($nombre && $mail){?>
	                     <div class="cont_campos">
	                        <span style="color: #59585D;font-family:FocoCorpBd;">Nombre:</span> <?php echo $nombre; ?>
	                        <input type="hidden" name="<?php echo $nombreId; ?>" id="<?php echo $nombreId; ?>">
	                        &nbsp;|&nbsp;
	                        <span style="color: #59585D;font-family:FocoCorpBd;">E-mail:</span> <?php echo $mail; ?>
	                        <input type="hidden" name="<?php echo $mailId; ?>" id="<?php echo $mailId; ?>">
	                        <?php if($slc_acepto){ ?>
	                            &nbsp; <img src="./images/acepto.png" title="<?php echo $nombre; ?> acepto tu invitación"> &nbsp; | &nbsp;
	                        <?php } ?>
                                <?php 
                                $anexoTYC = cargarAnexoTYC($solucion->sol_ret_id,  $usuID); 
                                if($anexoTYC){
                                ?>
                                <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank">
                                    <img src="./images/anexotyc.png" title="Térmnos y condiciones">
                                </a>
                                <?php 
                                }
                                ?>        
                                    
                                    
	                   </div> 
               <?php 
		               }
	               }
                }
		        ?>
		
		    <h5 id="interno">Anexos complementarios a la solución:</h5>
		    <!--div class="cont_campos">
	            <span style="color: #59585D;font-family:FocoCorpBd;">Archivo Anexo:</span>
	             <?php if($solucion->sol_anexo){ ?>
	                <a href="./images/retos/soluciones/<?php echo $solucion->sol_anexo; ?>" target="_blank">
	                <?php echo $solucion->sol_anexo; 
	                   if($solucion->sol_nameanexo){
	                   	   echo " ($solucion->sol_nameanexo)";
	                   }
	                ?>
	                </a>
	                <?php if($modificar){ ?>
	                &nbsp; | &nbsp;  
	                <a title="Eliminar a <?php echo $solucion->sol_anexo; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarAnexoPrincipal&amp;idRet=<?php echo $reto->ret_id; ?>&idcsol=<?php echo $solucion->sol_id; ?>&solucionID=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
	                    <img src="./images/eliminar.png">
	                </a>
	              <?php }
	                }else{
	                    if($modificar){?>
	                        <input type="file" size="50" name="sol_anexos"  id="sol_anexos" style="width:460px;">  
	                <?php 
	                    }
	                } 
	                ?>
	        </div-->
                    
                      <div class="cont_campos">
			  <?php if($NombreAnexo1 || $modificar){ ?>
			     <span style="color: #59585D;font-family:FocoCorpBd;">Archivo Anexo:</span>  
			   <?php } ?>
			  <?php if($NombreAnexo1){ ?>
			     <a href="./images/retos/soluciones/<?php echo $NombreAnexo1; ?>" target="_blank">
			         <?php 
			             echo $NombreAnexo1;
			             if($Anexo1){
                           echo " ($Anexo1)";
                         }
			         ?>
			     </a>
			     <?php if($modificar){ ?>
			     &nbsp; | &nbsp;  
			     <a title="Eliminar a <?php echo $NombreAnexo1; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo1; ?>&solucionID=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
			         <img src="./images/eliminar.png">
			     </a>
			     <?php }
			     }else{
			     	if($modificar){ ?>
			         <input type="file" size="50" name="sol_anexos1" style="width:460px;">
			  <?php }
			     } 
			  ?>
			</div>
                    
            <?php if($NombreAnexo1 || $NombreAnexo2 || $NombreAnexo3 || $NombreAnexo4){ 
        	   $capa='';
            }else{
        	   $capa='none';    
            ?>
                
                
                <div id="anexos">
                    <?php   if($modificar){  ?>
                        <input type="button" name="btnanexos" id="btnanexos" value="Más Anexos" onclick="abrirAnexos();">
                    <?php } ?>
                </div>
            <?php } ?>
                    
                  
                    
         
        <div id="capaAnexo" style="display: <?php echo $capa ?>;">
			
			
            <div class="cont_campos">
                 <?php if($NombreAnexo2 || $modificar){ ?>
                    <span style="color: #59585D;font-family:FocoCorpBd;">Archivo Anexo:</span>
                <?php } ?>
                 <?php if($NombreAnexo2){ ?>
                    <a href="./images/retos/soluciones/<?php echo $NombreAnexo2; ?>" target="_blank">
                        <?php echo$NombreAnexo2; 
                            if($Anexo2){
                                echo " ($Anexo2)";
                            }
                        ?>
                    </a> 
                    <?php if($modificar){ ?>
                    &nbsp; | &nbsp;  
                    <a title="Eliminar a <?php echo $NombreAnexo2; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo2; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <img src="./images/eliminar.png">
                    </a>
                  <?php } 
                    }else{
                    	if($modificar){ 
                    ?>
                    <input type="file" size="50" name="sol_anexos2" style="width:460px;">  
                  <?php 
                    	}
                    } ?>
	        </div>
            <div class="cont_campos">
                <?php if($NombreAnexo3 || $modificar){ ?>
                <span style="color: #59585D;font-family:FocoCorpBd;">Archivo Anexo:</span>
                <?php } ?>
                <?php if($NombreAnexo3){ ?>
                    <a href="./images/retos/soluciones/<?php echo $NombreAnexo3; ?>" target="_blank">
                        <?php echo $NombreAnexo3;
                            if($Anexo3){
                                echo " ($Anexo3)";
                            } 
                        ?>
                    </a>
                    <?php if($modificar){ ?>
                     &nbsp; | &nbsp;  
                     <a title="Eliminar a <?php echo $NombreAnexo3; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo3; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                        <img src="./images/eliminar.png">
                     </a>
                <?php } 
                    }else{
                    	if($modificar){ 
                    ?>
                    <input type="file" size="50" name="sol_anexos3" style="width:460px;">
                <?php }
                    } ?>
            </div>
        
	         <div class="cont_campos">
	            <?php if($NombreAnexo4 || $modificar){ ?>   
	               <span style="color: #59585D;font-family:FocoCorpBd;">Archivo Anexo:</span>   
	            <?php } ?>
	            <?php if($NombreAnexo4){ ?>
	                   <a href="./images/retos/soluciones/<?php echo $NombreAnexo4; ?>" target="_blank">
	                       <?php echo $NombreAnexo4;
	                        if($Anexo4){
                                echo " ($Anexo4)";
                            }
	                       ?>
	                   </a>
	               <?php if($modificar){ ?>&nbsp; | &nbsp;  
		               <a title="Eliminar a <?php echo $NombreAnexo4; ?>" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=elimarAnexo&amp;idRet=<?php echo $reto->ret_id; ?>&idanx=<?php echo $IdAnexo4; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
		                   <img src="./images/eliminar.png">
		               </a>
	               <?php } 
	                   }else{
	               	if($modificar){ 
	               ?>
	                   <input type="file" size="50" name="sol_anexos4" style="width:460px;">
	             <?php }
	               } 
	             ?>
	        </div>
	        
        </div>
        <h5 id="interno"><?php echo JText::_('COM_SOLUCION_LABEL_ESTADO');?></h5>
        <div class="cont_campos" align="left">
            <h1 style="font-size: 12px;text-align: justify;padding-left: 10px;padding-right: 15px;">
                    Para enviar la solución al comité evaluador debes tener en cuenta que:<br><br>
                    &#9658; Todos los invitados a participar en el reto hayan aceptado los términos y condiciones.  <br><br>
                    &#9658; Si uno de los invitados no aceptó tu invitación, debes eliminarlo haciendo clic en la “x” roja al lado de su nombre.<br><br>
                    &#9658; Sólo puede enviar la solución quien la ingresó.<br>

                    
		        <?php   
		            if($solucion->_sol_aceptarInvitados){
		            	//echo ' '.JText::_('Para enviar al comité evaluador es necesario que todos los invitados a participar en el reto hayan aceptado los términos y condiciones. Sólo puede enviar la solución quien la ingresó ');
		            }elseif(!$solucion->_sol_aceptarInvitados){
		            	//echo ' '.JText::_('Para enviar al comité evaluador es necesario que todos los invitados a participar en el reto hayan aceptado los términos y condiciones. Sólo puede enviar la solución quien la ingresó');
		            	//echo ' '.JText::_('COM_SOLUCION_BORRADOR');
		            }
		        ?>
	        </h1>
        </div>
        <?php if($modificar){ ?>
	        <div id="acepto" style="width: 500px !important;">
		        <?php if(!$solucion->_sol_aceptarInvitados && !$noEnviar){ ?>
		            <input type="button" name="aceptar" id="aceptar" value="<?php echo JText::_('Enviar al comité evaluador');?>" onclick="enviarSolucion();">
		            &nbsp;&nbsp;&nbsp;
		        <?php } ?>
	            <input type="button" name="Guardar" id="Guardar" value="Guardar" onclick="validar();">
	            <input type="hidden" name="task" value="solucion" />
                <input type="hidden" id="sol_estado" name="sol_estado" value=""/>
                <input type="hidden" name="option" value="com_solucion" />
                <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>"/>
                <input type="hidden" name="idSol" value="<?php echo $solucion->sol_id; ?>"/>
	        </div>
	        
		<?php } ?>
        
        
        <div id="acepto" style="width: 400px !important;">
             <a style="font-size: 13px;" title="Nueva Solución" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <span class="enlaceazul">
                   Nueva Solución
                </span>
            </a>
        </div>
      </form>
    
    <?php if(isset($this->preguntasComite) && count($this->preguntasComite)>0){ ?>
      <h4>&#9658; Preguntas y Respuestas Comité</h4>
     <?php 
        foreach($this->preguntasComite as $preguntasComite){
     ?>
        <div class="cont_campos" align="center">
            <p style="padding-left: 20px;">
                 <span style="font-weight: bold;  font-family: '';">Fecha:</span> <?php echo $preguntasComite->pco_fechapregunta; ?> <br>
                 <?php if($preguntasComite->pco_remitente!='Comité'){ ?>
                    <span style="font-weight: bold;  font-family: '';">Innovador:</span> <?php echo $preguntasComite->pco_remitente; ?> <br>
                 <?php }else{  ?>
                    <span style="font-weight: bold;  font-family: '';">Comité</span><br>
                 <?php } ?> 
                 <span style="font-weight: bold;  font-family: '';">Mensaje:</span> <?php echo $preguntasComite->pco_pregunta; ?>
            </p>
        </div>
     <?php } ?>
        <div id="acepto" style="width: 400px !important;">
            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 500, y: 480}}" id="modal" href="index.php?option=com_solucion&amp;view=formComite&amp;task=formComite&amp;idRet=<?php echo $reto->ret_id; ?>&amp;idSol=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <u style="font-size: 14px;font-weight: bold;">Dar una nueva respuesta</u>
            </a>   
        </div>
      
     <?php } ?>
      
   </div>
</div>
<script type="text/javascript">
    function enviarSolucion (){
    $("input, textarea, file").css("background-color","#FFFFFF");
    form = document.forms.solucionFrm;
    /*  if ($("#sol_anexos").length > 0) {
        if($("#sol_anexos").val().length <= 0) {  
            alert("El campo anexo es requerido");  
             $("#sol_anexos").focus();
             $("#sol_anexos").css("background-color","#FFFFCC");
            return false;  
        }
    }  */
    <?php  echo $validacion; ?>
     if(confirm('Esta seguro de enviar la solución')){
         form.sol_estado.value = 'Finalizado';
         $("#aceptar").attr('disabled','disabled');
         $("#Guardar").attr('disabled','disabled');  
    	 form.submit();
    	 
    }else{
        return false;
    }
}
</script>