<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
?>
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="padding-top: 1px !important;">Mis Soluciones</h3>
    </div>
</div>
<div id="cont_crear_solucion">
	<div id="crear_solucion">
	   <h4>&#9658; Plantilla de las soluciones.</h4>
	    <p style="margin-left: 10px;font-weight:bold;text-align:center;" align="center">
           <?php  echo $reto->ret_nombre; ?>
       </p>
	       <?php foreach ($this->soluciones as $solucion){?>
	   <p style="margin-left: 10px;">
            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 680, y: 800}}" id="modal" href="index.php?option=com_solucion&amp;view=solucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;solucionID=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component"> 
	    &#9658; <?php 
                        if($solucion->sol_titulo){
                            echo $solucion->sol_titulo;
                        }else{
                            echo $reto->ret_id.'-SOL-'.$usuario->usu_id;
                        }
                    ?>
           </a> <!-- &nbsp; | &nbsp;  Eliminar -->
       </p>
	   <?php } ?>
	   <div id="acepto" style="width: 500px;">
	        <a style="font-size: 13px;" title="Nueva Solución" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
	           <span class="enlaceazul">
	               Nueva Solución
	            </span>
	        </a>         
       </div>
    </div>
</div>
