<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$solucion = $this->solucion;
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<script type="text/javascript"  src='_objects/scripts/jquery.js' ></script>
<link rel="stylesheet" type="text/css" href="_objects/css/redmond/jquery-ui-1.8.16.custom.css" />
<script src="_objects/scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery.autocomplete.js" type="text/javascript"></script>
<script src="_objects/scripts/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">

function validarMensaje (){
	form = document.forms.preguntaCoFrm;
	if($("#pco_descripcion").val().length <= 0) {  
        alert("El campo mensaje es requerido");  
        return false;  
    }
	$("#Enviar").attr('disabled','disabled');
	form.submit();
}
</script>

<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="padding-top: 1px !important;">Preguntas y Respuestas Comité</h3>
    </div>
</div>
<div id="cont_crear_solucion">
<div id="crear_solucion">
    <h4>&#9658; <?php  echo $reto->ret_nombre; ?></h4>
    <form action="index.php" method="post" name="preguntaCoFrm" id="solucionFrm"  class="preguntaCoFrm">
          <div class="cont_campos">
            <label><?php echo JText::_('Mensaje:');?></label>
            <textarea name="pco_descripcion" id="pco_descripcion"></textarea>
          </div>
          <div id="acepto" style="width: 400px !important;">
            <input type="button" name="Enviar" id="Enviar" value="Guardar" onclick="validarMensaje();">
            <input type="hidden" name="task" value="preguntacomite" />
            <input type="hidden" name="option" value="com_solucion" />
            <input type="hidden" name="idRet" value="<?php echo $reto->ret_id; ?>"/>
            <input type="hidden" name="idSol" value="<?php echo $solucion->sol_id; ?>"/>
          </div>
      </form> 
     
      
   </div>
</div>