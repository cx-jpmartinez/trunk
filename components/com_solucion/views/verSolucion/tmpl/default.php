<?php
defined('_JEXEC') or die('Restricted access');
$reto = $this->obj;
$usuario = $this->usuario;
$solucion = $this->solucion;
$anexos = $this->anexos;
//echo '<pre>' . print_r($solucion,true) . '</pre>'; exit;
?>
<style>
    .ui-widget { font-size: 0.8em !important; }
</style>
<link rel="stylesheet" href="./templates/nutresa/css/template.css" type="text/css" />
<div id="cont_reto_prof">
    <div id="titulo_retos">
        <h3 class="titulo_reto" style="font-size:12px !important;font-weight: bold;padding-top:1px;"> <?php echo JText::_('Plantilla de la Solución');?>:<?php echo $solucion->sol_titulo; ?></h3>
    </div>
</div>
<div id="cont_crear_solucion">

<div id="crear_solucion">
    <h4>&#9658; <?php  echo $reto->ret_nombre; ?></h4>
    <p style="padding-right: 10px;padding-left: 10px;">&nbsp; <b><?php echo JText::_('COM_SOLUCION_LABEL_SOLUCION');?> </b> <?php echo $solucion->sol_titulo; ?></p>
    <?php if($solucion->sol_descripcion){ ?>
        <p style="padding-right: 10px;padding-left: 10px;">&nbsp; <b><?php echo JText::_('COM_SOLUCION_LABEL_DESCRIPCION');?></b> <?php echo $solucion->sol_descripcion;?></p>
        <br>
    <?php } ?>
    <?php foreach ($this->formSolucion as $input){ ?>
        <?php if($input->pro_tipo=='select'){ ?>
            <p style="padding-right: 10px;padding-left: 10px;">&nbsp <b><?php echo $input->pro_texto ?></b>
                <select name="<?php echo $input->pro_nombre; ?>" class="inputbox" id="<?php echo $input->pro_nombre; ?>" required="<?php echo $requerido?>" title="<?php echo $input->pro_descripcion ?>" disabled>
                    <?php foreach ($input->options as $option){ ?>
                        <option value="<?php echo $option->pro_valor; ?>" <?php if($input->det_valor==$option->pro_valor){echo 'selected';} ?>><?php echo $option->pro_texto; ?></option>
                    <?php } ?>
                </select>
            </p>    
        <?php }elseif($input->pro_tipo=='file'){ ?>
            <p style="padding-right: 10px;padding-left: 10px;">&nbsp <b><?php echo $input->pro_texto ?></b> <a href="./images/retos/soluciones/<?php echo $input->det_valor; ?>" target="_blank"><?php echo $input->det_texto; ?></a></p> 
        <?php }elseif($input->pro_tipo=='check'){  ?>
             <p style="padding-right: 10px;padding-left: 10px;">&nbsp <b><?php echo $input->pro_texto ?></b><br>
                <?php  foreach ($input->options as $option){  ?>
                <input type="checkbox" name="<?php echo $option->pro_nombre; ?>" class="inputbox" id="<?php echo $option->pro_nombre; ?>"  value="<?php echo $option->pro_valor; ?>" title="<?php echo $input->pro_descripcion ?>" style="width:33px;" <?php if($option->det_valor==$option->pro_valor){ echo 'checked';} ?> disabled>
                <?php echo $option->pro_texto ?>  <br>
            <?php } ?>
                 
             </p>
         <?php }else{ ?>
            <p style="padding-right: 10px;padding-left: 10px;">&nbsp <b><?php echo $input->pro_texto ?></b> <?php echo $input->det_valor; ?></p>
        <?php } ?>
        <br>
    <?php 
       
    } 
    ?>
    <h4>&#9658;Participantes</h4><br>
    <?php if($this->participantes){ 
        foreach ($this->participantes as $participante){ ?>
        <p style="padding-right: 10px;padding-left: 10px;">&nbsp; 
            <?php echo "$participante->nombre ($participante->usu_email)"; ?>
            &nbsp;
            <?php  
            $valTYC = validarTYC($participante->usu_email, $reto->ret_id);
            if($valTYC ){ ?>
                <img src="./images/acepto.png" title="Aceptó Términos y Condiciones">
            <?php }else{ ?>
                <img src="./images/alerta.gif"  title="No Aceptó Términos y Condiciones">
            <?php } ?>
            <?php 
                $anexoTYC = cargarAnexoTYC($this->solucion->sol_ret_id, $participante->usu_id); 
                if($anexoTYC){
            ?>
                <a href="./images/retos/tyc/<?php echo $anexoTYC; ?>" target="_blank"><?php echo $anexoTYC; ?></a>
                <?php    
            }
            ?>    
                
                
        </p><br>
    <?php } 
    } 
    ?>
     <h4>&#9658;Anexos</h4><br>
    <?php if($solucion->sol_anexo){ ?>
        <p style="padding-right: 10px;padding-left: 10px;">&nbsp;
        <a href="./images/retos/soluciones/<?php echo $solucion->sol_anexo; ?>" target="_blank">
            <?php
                if($solucion->sol_nameanexo){
                	echo $solucion->sol_nameanexo;
                }else{
                    echo $solucion->sol_anexo;
                } 
            ?>
        </a>
        </p><br>
    <?php } ?>    
    <?php 
    if($anexos){
    	foreach ($anexos as $anexo){ 
    ?>
        <p style="padding-right: 10px;padding-left: 10px;">&nbsp;
        <a href="./images/retos/soluciones/<?php echo $anexo->anx_archivo; ?>" target="_blank">
            <?php 
            if($anexo->anx_nombre){
            	echo $anexo->anx_nombre;
            }else{
            	echo $anexo->anx_archivo;
            }
            ?>
        </a> 
        </p><br>
    <?php
    	} 
    } 
    ?>
        
     <?php if(isset($this->preguntasComite) && count($this->preguntasComite)>0){ ?>
      <h4>&#9658; Preguntas y Respuestas Comité</h4>
     <?php 
        foreach($this->preguntasComite as $preguntasComite){
     ?>
        <div class="cont_campos" align="center">
            <p style="padding-left: 20px;">
                 <span style="font-weight: bold;  font-family: '';">Fecha:</span> <?php echo $preguntasComite->pco_fechapregunta; ?> <br>
                 <?php if($preguntasComite->pco_remitente!='Comité'){ ?>
                    <span style="font-weight: bold;  font-family: '';">Innovador:</span> <?php echo $preguntasComite->pco_remitente; ?> <br>
                 <?php }else{  ?>
                    <span style="font-weight: bold;  font-family: '';">Comité</span><br>
                 <?php } ?>   
                 <!--span style="font-weight: bold;  font-family: '';">Mensaje:</span--> <?php echo $preguntasComite->pco_pregunta; ?>
            </p>
        </div>
     <?php } ?>
        <div id="acepto" style="width: 400px !important;">
            <a class="modal" style="text-decoration: none;" rel="{handler: 'iframe', size: {x: 500, y: 480}}" id="modal" href="index.php?option=com_solucion&amp;view=formComite&amp;task=formComite&amp;idRet=<?php echo $reto->ret_id; ?>&amp;idSol=<?php echo $solucion->sol_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <u style="font-size: 14px;font-weight: bold;">Dar una nueva respuesta</u>
            </a>   
        </div>
      
     <?php } ?>  
      
     <h4>&#9658;Estado de la solución</h4><br>
     <p style="padding-right: 10px;padding-left: 10px;text-align: center; color: red;font-weight:">&nbsp; 
        <?php 
        if(!$solucion->sol_estado || $solucion->sol_estado=='Pendiente'){
            echo 'BORRADOR';
        }elseif($solucion->sol_estado=='Finalizado'){
            echo 'PENDIENTE DE APROBACIÓN';
        }elseif($solucion->sol_estado=='Aprobada'){
            echo 'SOLUCIÓN GANADORA';
        }elseif($solucion->sol_estado=='Rechazada'){
            echo 'RECHAZADA';
        }
        ?>
     </p>
   <?php 
       $fechaActual = date('Y-m-d H:i');
       $view = JRequest::getVar('view');
       if($reto->ret_estado=='Vigente' &&  $fechaActual <= $reto->ret_fechaVigencia && $view!='verSolucion'){ ?>
       <div id="acepto" style="width: 400px !important;">
             <a style="font-size: 13px;" title="Nueva Solución" class="modal" rel="{handler: 'iframe', size: {x: 600, y: 500}}" id="modal" href="index.php?option=com_solucion&amp;task=nuevaSolucion&amp;idRet=<?php echo $reto->ret_id; ?>&amp;component=com_retos&amp;path=&amp;tmpl=component">
                <span class="enlaceazul">
                   Nueva Solución
                </span>
            </a>
        </div>
    <?php } ?>  
</div>
   
</div>









