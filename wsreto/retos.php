<?php
//set_time_limit(90);
//define( '_VALID_MOS', 1 );

define('USER', 'cognox');
define('PASS', '46948e5f');
error_reporting(E_ALL);

ini_set('display_errors', 1);

require '../configuration.php';

class Retos {

	/**
	 * Variable para almacenar la conexión a la BD
	 *
	 * @var PDO
	 */
	private $conexion;

	public function __construct(){
                
		$config = new JConfig();
		$config->host = explode(':', $config->host . ':3307');
                $this->conexion = new PDO("mysql:dbname=retos;host={$config->host[0]};port={$config->host[1]}", $config->user, $config->password);
                $this->con = mysql_connect("{$config->host[0]}:{$config->host[1]}","$config->user","$config->password");

                
        }

	/**
	 * Actualizar el estado de una solución
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idsol
	 * @param string  $estado
	 * @param string  $codigo
	 * @param string  $comentario
	 * @return string
	 */
	public function estadoSolucion($wsUser, $wsClave, $idsol, $estado, $codigo, $comentario){
	     if($wsUser==USER && $wsClave==PASS ){
			if($codigo=='2'){
				$estado='Pendiente';
			}
			$sql ="SELECT sol_id FROM solucion where sol_titulo=?";
			$prepare = $this->conexion->prepare($sql);
			$prepare->execute(array($idsol));
			
			$objResult = $prepare->fetch( PDO::FETCH_OBJ );
			if($objResult && $objResult->sol_id){
				if($codigo=='2'){
					$sql = "INSERT INTO comentario (cm_sol_id,cmt_texto) VALUES (:cm_sol_id,:cmt_texto)";
					$insertar = $this->conexion->prepare($sql);
					$insertar->execute(array(':cm_sol_id'=>$objResult->sol_id,':cmt_texto'=>$comentario));
				}
				$sql="UPDATE solucion SET sol_estado=?
				WHERE sol_titulo=? and sol_id=?";
				$prepare = $this->conexion->prepare($sql);
	            $result = $prepare->execute(array($estado, $idsol, $objResult->sol_id));
	            if($result){
					$sql="UPDATE solucion SET sol_estado=?
					WHERE sol_sol_id=? and sol_sol_id is not null";
	                $prepare = $this->conexion->prepare($sql);
	                $result = $prepare->execute(array($estado, $objResult->sol_id));
                    if($result){
                    	return 'OK';	            	
                    } else {
                    	return 'ERROR';
                    }
	            } else {
					return 'ERROR';
	            }
			}else{
				return 'ERROR';
			}
		} else {
			return 'ERROR';
		}
	}
        
        
        /**
	 * Pregunta del comite
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idSolucion
	 * @param string  $remitente
	 * @param string  $mensaje
	 * @return string
	 */
	public function datosPregunta($wsUser, $wsClave, $idSolucion, $remitente, $mensaje){
	     if($wsUser==USER && $wsClave==PASS ){
                $remitente = 'Comité';
                $sql ="SELECT sol_id FROM solucion where sol_titulo=?";
                $prepare = $this->conexion->prepare($sql);
                $prepare->execute(array($idSolucion));
                $objResult = $prepare->fetch( PDO::FETCH_OBJ );
                if($objResult && $objResult->sol_id){
                    $fecha = date('Y-m-d H:i');
                    $sql = "INSERT INTO preguntacomite (pco_sol_id,pco_pregunta,pco_fechapregunta,pco_remitente) VALUES (:pco_sol_id,:pco_pregunta,:pco_fechapregunta,:pco_remitente)";
                    $insertar = $this->conexion->prepare($sql);
                    $insertar->execute(array(':pco_sol_id'=>$objResult->sol_id,':pco_pregunta'=>$mensaje,':pco_fechapregunta'=>$fecha,':pco_remitente'=>$remitente));
                    return 'OK';	
                }else{
                    return 'ERROR';    
                }
             }else{
                 return 'ERROR';
             }
        
        }
        
        /**
	 * Verificar Usuario
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $usuarioRed
	 * @param string  $claveRed
	 * @return string
	 */
        public function verificarUsuario($wsUser, $wsClave, $usuarioRed, $claveRed){
	     if($wsUser==USER && $wsClave==PASS ){
                $sql ="SELECT usu_id FROM usuario where usu_nombreUsuario=?";
                $prepare = $this->conexion->prepare($sql);
                $prepare->execute(array($usuarioRed));
                $objResult = $prepare->fetch( PDO::FETCH_OBJ );
                if($objResult && $objResult->usu_id){
                    require '../_objects/ws/lotus.php';
                    $lotus = new Lotus();
                    $users = $lotus->datosPersona($usuarioRed,$claveRed); 
                    if($users['email'] || $users['nombre']){
                         return 'OK';
                    }else{
                         return 'ERROR';  
                    }
                }else{
                    return 'ERROR';    
                }
             }else{
                 return 'ERROR';
             }
        }
        
        /**
	 * DatosReto
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $solucion
	 * @param string  $reto
	 * @return string
	 */
        public function DatosReto($wsUser, $wsClave, $solucion,$reto){
             if($wsUser==USER && $wsClave==PASS ){
               $sql ="SELECT  
                        ret_puntos
                        ,ret_nombre
                        ,ret_id
                        ,sol_titulo 
                        ,neg_nombre 
                        ,neg_nit
                        ,rxc_puntos
                        FROM reto
                        inner join solucion on (sol_ret_id=ret_id)
                        inner join retoxcompania on (ret_id=rxc_ret_id)
                        inner join negocio on (neg_id=rxc_neg_id)
                        where sol_titulo='$solucion'
                        and ret_id='$reto'
                       ";
                mysql_select_db("retos", $this->con);
                $result = mysql_query($sql);
                $i= 0;
                $puntos=0;
                while($row = mysql_fetch_array($result))
                {
                    //$arr[$i]= array('puntos'=>$row['ret_puntos'],'reto'=>$row['ret_nombre'],'idReto'=>$row['ret_id'],'solucion'=>$row['sol_titulo'],'compania'=>$row['neg_nombre'],'nit'=>$row['neg_nit'],'puntosPorcentaje'=>$row['rxc_puntos']);
                    $arr[$i]= array('puntos'=>$row['ret_puntos'],'compania'=>$row['neg_nombre'],'nit'=>$row['neg_nit'],'puntosPorcentaje'=>$row['rxc_puntos']);
                    //$puntos = $row['ret_puntos'];
                     $i++;
                }
               // $arr['totalPuntos'] = $puntos;
               // $arr['error'] = 'OK';
                
                mysql_close($this->con);
                if($arr>0){
                    //$arr['error'] = 'OK';
                }else{
                    //$arr['error'] = 'ERROR';
                    $arr = null;
                }
                return $arr;
                //return 'OK';
             }else{
                 //return 'ERROR';
                 $arr = null;
                 return $arr;
             }
        }
        
        
      
}
/*
$reto = new Retos();
$ff = $reto->DatosReto(USER,PASS,'6-SOL-82-8','6');
echo '<pre>'; 
print_r($ff);
echo '</pre>'; 
exit;*/


