<?php
//set_time_limit(90);
//define( '_VALID_MOS', 1 );

define('USER', 'cognox');
define('PASS', '46948e5f');
error_reporting(E_ALL);

ini_set('display_errors', 1);

require '../configuration.php';

class retorno {
    /**
     * Respuesta del metodo
     * 
     * @var string
     */
    public $respuesta;    
}


class Retos {

	/**
	 * Variable para almacenar la conexión a la BD
	 *
	 * @var PDO
	 */
	public $conexion;

	public function __construct(){
                
		$config = new JConfig();
		$config->host = explode(':', $config->host . ':3307');
                $this->conexion = new PDO("mysql:dbname=retos;host={$config->host[0]};port={$config->host[1]}", $config->user, $config->password);
                $this->con = mysql_connect("{$config->host[0]}:{$config->host[1]}","$config->user","$config->password");
                

                
        }

	/**
	 * Actualizar el estado de una solución
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idsol
	 * @param string  $estado
	 * @param string  $codigo
	 * @param string  $comentario
	 * @return array
	 */
	public function estadoSolucion($wsUser, $wsClave, $idsol, $estado, $codigo, $comentario){
             $response = new stdClass();
             if($wsUser==USER && $wsClave==PASS ){
                 // nuevo
                    $sql ="SELECT  
                        ret_puntos
                        ,ret_nombre
                        ,ret_id
                        ,sol_titulo 
                        ,neg_nombre 
                        ,neg_nit
                        ,rxc_puntos
                        FROM reto
                    inner join solucion on (sol_ret_id=ret_id)
                    inner join retoxcompania on (ret_id=rxc_ret_id)
                    inner join negocio on (neg_id=rxc_neg_id)
                    where sol_titulo='$idsol'
                    -- and ret_id='$reto'
                    ";
                    mysql_select_db("retos", $this->con);
                    $result = mysql_query($sql);
                    $i= 0;
                    while($row = mysql_fetch_array($result))
                    {
                        $arr[$i]= array('puntos'=>$row['ret_puntos'],'compania'=>$row['neg_nombre'],'nit'=>$row['neg_nit'],'puntosPorcentaje'=>$row['rxc_puntos']);
                        $i++;
                    }
                 //fin nuevo
                        if($codigo=='2'){
				$estado='Pendiente';
			}
			$sql ="SELECT sol_id FROM solucion where sol_titulo=?";
			$prepare = $this->conexion->prepare($sql);
			$prepare->execute(array($idsol));
			
			$objResult = $prepare->fetch( PDO::FETCH_OBJ );
			if($objResult && $objResult->sol_id){
				if($codigo=='2'){
					$sql = "INSERT INTO comentario (cm_sol_id,cmt_texto) VALUES (:cm_sol_id,:cmt_texto)";
					$insertar = $this->conexion->prepare($sql);
					$insertar->execute(array(':cm_sol_id'=>$objResult->sol_id,':cmt_texto'=>$comentario));
				}
				$sql="UPDATE solucion SET sol_estado=?
				WHERE sol_titulo=? and sol_id=?";
				$prepare = $this->conexion->prepare($sql);
                                $result = $prepare->execute(array($estado, $idsol, $objResult->sol_id));
                                if($result){
                                    $sql="UPDATE solucion SET sol_estado=?
                                        WHERE sol_sol_id=? and sol_sol_id is not null";
                                    $prepare = $this->conexion->prepare($sql);
                                    $result = $prepare->execute(array($estado, $objResult->sol_id));
                                    if($result){
                                        //$response->estadoSolucionResult = 'OK';
                                        //return 'OK';    
                                        return $arr;
                                    } else {
                                        $arr = null;
                                        return $arr;   
                                    }
                                }else {
                                    $arr = null;
                                    return $arr;   
                                }
			}else{
                            $arr = null;
                            return $arr;   
			}
		} else {
			$arr = null;
                        return $arr;
		}
	}
        
        
        /**
	 * Pregunta del comite
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $idSolucion
	 * @param string  $remitente
	 * @param string  $mensaje
	 * @return string
	 */
	public function datosPregunta($wsUser, $wsClave, $idSolucion, $remitente, $mensaje){
            $response = new stdClass();
	     if($wsUser->wsUser==USER && $wsClave==PASS ){
                $remitente = 'Comité';
                $sql ="SELECT sol_id FROM solucion where sol_titulo=?";
                $prepare = $this->conexion->prepare($sql);
                $prepare->execute(array($idSolucion));
                $objResult = $prepare->fetch( PDO::FETCH_OBJ );
                if($objResult && $objResult->sol_id){
                    $fecha = date('Y-m-d H:i');
                    $sql = "INSERT INTO preguntacomite (pco_sol_id,pco_pregunta,pco_fechapregunta,pco_remitente) VALUES (:pco_sol_id,:pco_pregunta,:pco_fechapregunta,:pco_remitente)";
                    $insertar = $this->conexion->prepare($sql);
                    $insertar->execute(array(':pco_sol_id'=>$objResult->sol_id,':pco_pregunta'=>$mensaje,':pco_fechapregunta'=>$fecha,':pco_remitente'=>$remitente));
                    //$response->datosPreguntaResult = 'OK';
                    //return $response;	
                    return 'OK'; 
                }else{
                    //$response->datosPreguntaResult = 'ERROR';
                    return 'ERROR'; 
                }
             }else{
                 //$response->datosPreguntaResult = 'ERROR';
                 return 'ERROR'; 
             }
        
        }
        
        
        
        
        /**
	 * Verificar Usuario
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $usuarioRed
	 * @param string  $claveRed
	 * @return string
	 */
        public function verificarUsuario($wsUser, $wsClave, $usuarioRed, $claveRed){
                $response = new stdClass();
                if($wsUser->wsUser==USER && $wsUser->wsClave==PASS ){
                    $sql ="SELECT usu_id FROM usuario where usu_nombreUsuario=?";
                    $prepare = $this->conexion->prepare($sql);
                    $prepare->execute(array($wsUser->usuarioRed));
                    $objResult = $prepare->fetch( PDO::FETCH_OBJ );
                if($objResult && $objResult->usu_id){
                    require '../_objects/ws/lotus.php';
                    $lotus = new Lotus();
                    $users = $lotus->datosPersona($wsUser->usuarioRed,$wsUser->claveRed); 
                    if($users['nombre']){
                        $response->verificarUsuarioResult = 'OK';
                    return $response;
                    }else{
                        $response->verificarUsuarioResult = 'ERROR';
                        return $response;
                    }
                }else{
                    $response->verificarUsuarioResult = 'ERROR';
                    return $response;
                }
             }else{
                $response->verificarUsuarioResult = 'ERROR';
                return $response;
             }
        }
        
        
        /**
	 * DatosReto
	 * WEB SERVICE
	 *
	 * @param string  $wsUser
	 * @param string  $wsClave
	 * @param string  $solucion
	 * @param string  $reto
	 * @return array
	 */
        public function DatosReto($wsUser, $wsClave, $solucion,$reto){
             //$response = new stdClass();
             // return print_r($wsUser);
             if($wsUser==USER && $wsClave==PASS ){
               $sql ="SELECT  
                        ret_puntos
                        ,ret_nombre
                        ,ret_id
                        ,sol_titulo 
                        ,neg_nombre 
                        ,neg_nit
                        ,rxc_puntos
                        FROM reto
                        inner join solucion on (sol_ret_id=ret_id)
                        inner join retoxcompania on (ret_id=rxc_ret_id)
                        inner join negocio on (neg_id=rxc_neg_id)
                        where sol_titulo='$solucion'
                        and ret_id='$reto'
                       ";
                mysql_select_db("retos", $this->con);
                $result = mysql_query($sql);
                $i= 0;
                //$puntos=0;
                while($row = mysql_fetch_array($result))
                {
                   $arr[$i]= array('puntos'=>$row['ret_puntos'],'compania'=>$row['neg_nombre'],'nit'=>$row['neg_nit'],'puntosPorcentaje'=>$row['rxc_puntos']);
                    $i++;
                }
               // $arr['totalPuntos'] = $puntos;
                //$arr['error'] = 'OK';
                
                mysql_close($this->con);
                if($arr>0){
                    //$response->DatosRetoResult = $arr;
                }else{
                     $arr = null;
                     //$response->DatosRetoResult = $arr;
                }
                return $arr;
             }else{
                  $arr = null;
                  //$response->DatosRetoResult = $arr;
                  return $arr;
             }
        }
        
        
      
}
/*
$reto = new Retos();
$ff = $reto->DatosReto(USER,PASS,'6-SOL-82-19','6');
echo '<pre>'; 
print_r($ff);
echo '</pre>'; 
exit;
*/

