<?php

define('DS', DIRECTORY_SEPARATOR);
define('PATH', dirname(__FILE__));
define('WSDLFILE', substr(__FILE__, 0, -3) . 'wsdl');
//define('RUTA_LOGS', 'd:/aplicaciones/logs/retos/ws/');
//define('RUTA_LOGS', 'c:/logs/hp/');
define('RUTA_LOGS', 'd:/logs/hp/');

function friendlyXML(&$xml) {
	if ($xml) {
		$dom = new DOMDocument();
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($xml);
		$xml = trim($dom->saveXML());
	}
	return $xml;
}


// Agregamos la raíz al path para los require_once
set_include_path(get_include_path() .
PATH_SEPARATOR .
realpath(dirname(__FILE__) . "/../"));

// Incluimos nuestra clase que tiene el servicio web
require_once('retosv03_test.php');

ini_set('display_errors', 0);

if ($_SERVER['QUERY_STRING'] == 'wsdl')
{
	// Utilizamos la clase Zend_Soap_AutoDiscover para
	// generar de forma automática el WSDL
	$fromFile = false;
	if (is_file(WSDLFILE)) {
		$timeWsdl = filemtime(WSDLFILE);
		$timeServer = filemtime(__FILE__);
		if ($timeWsdl >= $timeServer) {
			$fromFile = true;
		} else {
                    unlink(WSDLFILE);
                }
	}
	if ($fromFile) {
		header("Content-Type: text/xml");
		echo file_get_contents(WSDLFILE);
	} else {
		require_once 'Zend/Loader/Autoloader.php';
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->setFallbackAutoloader(true);
		$loader->suppressNotFoundWarnings(true);
		$autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeSequence');
		//$autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');

		$autodiscover->setOperationBodyStyle(
		array(
                'encodingStyle' => 'http://schemas.xmlsoap.org/soap/encoding/',
		'use' => 'literal',
                //'use' => 'document',
                'namespace' => 'http://www.orb.cognox.com/htdocs/retos/wsreto/wsretosv03_test.php'
                )
                );
                $autodiscover->setBindingStyle(
                array(
                //'style' => 'rpc',
                'style' => 'document',
                'namespace' => 'http://www.orb.cognox.com/htdocs/retos/wsreto/wsretosv03_test.php'
                )
                );

                $autodiscover->setClass('retos');
                
                $response = $autodiscover->toXml();

                file_put_contents(WSDLFILE, friendlyXML($response));
                header("Content-Type: text/xml");
                echo friendlyXML($response);
	}
	exit;
}
else
{
	// Creamos un servidor que atienda en base al WSDL generado

	$fileLog = date('Ymd-His') . rand(1000, 9999);
	file_put_contents(RUTA_LOGS . 'rq_' . $fileLog . '.log', file_get_contents('php://input'));

//	$server = new SoapServer(WSDLFILE, array('encoding' => 'ISO-8859-1'));
	$server = new SoapServer(WSDLFILE, array(
            'soapVersion' => SOAP_1_2, 
            'encoding' => 'UTF-8',
            'cache_wsdl' => WSDL_CACHE_NONE)
        );
	$server->setClass('Retos');
	ob_start();
	
	$server->handle();
        $response = friendlyXML(ob_get_contents());
        //ob_end_clean();
	file_put_contents(RUTA_LOGS . 'rs_' . $fileLog . '.log', $response);
        //header_remove('Content-Length');
        //header('Content-Length: ' . str_len($response)); 
        //echo $response;
}