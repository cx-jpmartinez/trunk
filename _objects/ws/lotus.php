<?php
set_time_limit(0);
error_reporting(E_ALL);
class Lotus {
   /**
     *
     * @var Config
     */
    private $_config = null;

    /**
     *
     * @var PDO
     */
    private $_connection = null;

    /**
     *
     * @var SoapClient
     */
    private $_soapClient = null;
    /**
     *
     * @ignore
     * @param array $arrConfig 
     */
  
    public function __construct($ws = null)
    {

    	$strPath = realpath( dirname( __FILE__ ) );
    	$this->_config = json_decode( json_encode( require("$strPath/config/service.php") ) );
        $this->_soapClient = new SoapClient(
            $this->_config->soap->url
        );
        
    	 
    }
    
    private function _invoke( $strMethod, array $arrParams = array() )
    {
    	
        array_unshift( 
            $arrParams,
            $this->_config->soap->usuario, 
            $this->_config->soap->clave
          
        );
        //echo '<pre>' . print_r($arrParams,true) . '</pre>'; exit;
        
        $dblInitialTime = microtime( true );
        try {
            $strResult = $this->_soapClient->__soapCall( $strMethod, $arrParams);
        }catch (Exception $e){
        	echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
        $dblFinalTime = microtime( true );
        return $strResult;
    }
    
    public function CargarUsuario( $negocio )
     { 
     	$objResult = $this->_invoke( 'coordinadoresNegocio', array(
            $negocio
        ));
        return $objResult['coordinadores'];
     }
     
     
    public function enviarSolucion($idReto, $reto, $idNegocio, $negocio, $idSolucion,$puntos, $enlace, $proponente, $evaluadores, $experto)
     {                            
        $objResult = $this->_invoke( 'datosSolucion', array(
            $idReto, $reto, $idNegocio, $negocio, $puntos,$idSolucion, $enlace, $proponente,$evaluadores,$experto
        ));
        return $objResult;
     }
     
     public function datosRespuesta($idSolucion, $remitente, $mensaje)
     {                            
        $objResult = $this->_invoke( 'datosRespuesta', array(
            $idSolucion, $remitente, $mensaje
        ));
        return $objResult;
     }
     
     public function datosPersona( $usuarioRed, $claveRed )
     { 
        $objResult = $this->_invoke( 'datosPersona', array(
            $usuarioRed,$claveRed
        ));
        return $objResult;
     }
     
    
    function formatGMT($date) {
        if (!$date) return '';
        $ano = substr ( $date, 0, 4 );
        $mes = substr ( $date, 5, 2 );
        $dia = substr ( $date, 8, 2 );
        $hora = substr ( $date, 11, 2 );
        $minuto = substr ( $date, 14, 2 );
        return "$ano-$mes-$dia $hora:$minuto";
    }
}


/*
	$lotus = new Lotus();
	$users = $lotus->datosPersona('afperez','cognox_2015');
	echo '<pre>' . print_r($lotus,true) . '</pre>'; exit;
    echo '<pre>' . print_r($users,true) . '</pre>'; exit;
*/
/*
$lotus = new Lotus();
$users = $lotus->datosPersona('afperez','cognox_2015');
print_r($users);*/
/*
$lotus = new Lotus();
$users = $lotus->datosPersona('afperez','cognox_2015');
//$arr =
$pop = array('nombre'=>$users['nombre'],'cedula'=>$users['cedula'], 'telefono'=>$users['telefono'],'email'=>$users['email']); 
//echo $users['nombre'];
//echo '<pre>' . print_r($users,true) . '</pre>'; exit;
$pops = array(0=>$pop);

//echo '<pre>' . print_r($pops,true) . '</pre>'; exit;
// $ss = $lotus->enviarSolucion('idReto','reto','idnegocio','negocio','100','idSolucion','Enlace','proponente' );
$ss = $lotus->enviarSolucion("1", "Primer reto", "1", "Noel_2", 1000, "sol-1", "http://www.google.com", $pops);
echo '<pre>' . print_r($ss,true) . '</pre>'; exit;
*/
/*
$lotus = new Lotus();
$res = $lotus->datosRespuesta('6-SOL-82-201203231009','andres perez','este es el innovador');
print_r($res);*/
