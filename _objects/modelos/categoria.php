<?php
defined('JPATH_PLATFORM') or die;

/**
 * Categoria class.  
 *
 */
class categoria extends JTable
{
    public $cat_id = null;
	public $cat_nombre = null;
	public $cat_descripcion = null;
	public $cat_estado = null;
	public $cat_fecha = null;
	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the categoria to load (optional).
	 *
	 * @return  Categoria
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'categoria', 'cat_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->cat_id = 0;
		}
	}
	
	public function listarCategorias(){
		$sql = "SELECT * FROM categoria 
                WHERE cat_estado='Activa'";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;      
	}
	
}



?>