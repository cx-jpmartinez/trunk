<?php
defined('JPATH_PLATFORM') or die;

/**
 * anexo class.  
 *
 */
class anexo extends JTable
{
    public $anx_id = null;
	public $anx_archivo = null;
	public $anx_sol_id = null;
	public $anx_fecha = null;
	public $anx_nombre = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the anexo to load (optional).
	 *
	 * @return  anexo
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'anexo', 'anx_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->anx_id = 0;
		}
	}
}
?>