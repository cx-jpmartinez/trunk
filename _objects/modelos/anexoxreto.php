<?php
defined('JPATH_PLATFORM') or die;

/**
 * anexoxreto class.  
 *
 */
class anexoxreto extends JTable
{
    public $axr_id = null;
	public $axr_archivo = null;
	public $axr_ret_id = null;
	public $axr_fecha = null;
	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the anexoxreto to load (optional).
	 *
	 * @return  anexoxreto
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'anexoxreto', 'axr_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->axr_id = 0;
		}
	}
	
	public function cargarAnexos($ret_id){
		$sql="SELECT * FROM anexoxreto where axr_ret_id='$ret_id'";
		$this->_db_retos->setQuery( $sql );
        $result  = $this->_db_retos->loadObjectList(); 
        return $result;
	}
}
?>