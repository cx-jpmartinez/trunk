<?php
defined('JPATH_PLATFORM') or die;

/**
 * retoxcompania class.  
 *
 */
class retoxcompania extends JTable
{
        public $rxc_id = null;
	public $rxc_ret_id = null;
	public $rxc_neg_id = null;
        public $rxc_puntos = null;
        
	/**
	 *
	 * @param   integer  $identifier  The primary key of the  to load (optional).
	 *
	 * @return  retoxcompania
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'retoxcompania', 'rxc_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->rxc_id = 0;
		}
	}
	
	public function limpiarCompaniaRetos($ret_id){
        $sql="DELETE FROM retoxcompania WHERE rxc_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
    }
    
     public function cargarCompaniasReto($ret_id){
        $sql = "SELECT
                neg_id,neg_nit,neg_nombre,rxc_puntos
                FROM retoxcompania r
                inner join negocio on (neg_id=rxc_neg_id)
                where rxc_ret_id='$ret_id'";
         $this->_db_retos->setQuery( $sql );
         $companias = $this->_db_retos->loadAssocList();
         return $companias;
    }
}
?>