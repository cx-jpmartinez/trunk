<?php

defined('JPATH_PLATFORM') or die;

class propiedad extends JTable {

    public $pro_id = null;
    public $pro_frm_id = null;
    public $pro_nombre = null;
    public $pro_tipo = null;
    public $pro_descripcion = null;
    public $pro_requerido = null;
    public $pro_ancho = null;
    public $pro_alto = null;
    public $pro_valor = null;
    public $pro_pro_id = null;
    public $pro_posicion = null;
    public $pro_caracteres = null;

    public function __construct($identifier = 0) {
        $db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
        parent::__construct('propiedad', 'pro_id', $this->_db_retos);
        if (!empty($identifier)) {
            $this->load($identifier);
        } else {
            $this->pro_id = 0;
        }
    }
    
   public function cargarPropiedad ($ret_id,$frm_id){
            $sql = "SELECT pro_id, pro_frm_id, pro_nombre, pro_tipo, pro_descripcion, pro_requerido, pro_ancho, pro_alto, pro_valor, pro_pro_id, pro_posicion, pro_caracteres, pro_texto, pro_estado
                    FROM propiedad
                    INNER JOIN formulario on pro_frm_id=frm_id
                    where frm_ret_id='$ret_id' and frm_id='$frm_id' and frm_estado='1'";
            $this->_db_retos->setQuery( $sql );
            $obj  = $this->_db_retos->loadObjectlist(); 
            return $obj;
     }
   public function cargar ($ret_id){
            $sql = "SELECT pro_id, pro_frm_id, pro_nombre, pro_tipo, pro_descripcion, pro_requerido, pro_ancho, pro_alto, pro_valor, pro_pro_id, pro_posicion, pro_caracteres, pro_texto, pro_estado
                    FROM propiedad
                    INNER JOIN formulario on pro_frm_id=frm_id
                    where frm_ret_id='$ret_id'  and frm_estado='1' and pro_estado='1'";
            $this->_db_retos->setQuery( $sql );
            $obj  = $this->_db_retos->loadObjectlist(); 
            return $obj;
     }
     
     public function cargarOption ($idLista){
            $sql = "SELECT *
                    FROM propiedad
                    where pro_pro_id='$idLista' and pro_estado='1' ";
            $this->_db_retos->setQuery( $sql );
            $obj  = $this->_db_retos->loadObjectlist(); 
            return $obj;
     }
}

?>