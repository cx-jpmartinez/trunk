<?php
defined('JPATH_PLATFORM') or die;

/**
 * notificacion class.  
 *
 */
class notificacion extends JTable
{
    public $not_id = null;
	public $not_ret_id = null;
	public $not_exp_id = null;
	public $not_fecha = null;
	public $not_usuario = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the notificacion to load (optional).
	 *
	 * @return  notificacion
	 *
	 */
	public function __construct($identifier = 0){
		$db =  JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'notificacion', 'not_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->not_id = 0;
		}
	}
}
?>