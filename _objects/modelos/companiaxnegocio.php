<?php
defined('JPATH_PLATFORM') or die;

/**
 * retoxnegocio class.  
 *
 */
class companiaxnegocio extends JTable
{
    public $cxn_id = null;
	public $cxn_neg_id = null;
	public $cxn_com_id = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the negocio to load (optional).
	 *
	 * @return  companiaxnegocio
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'companiaxnegocio', 'cxn_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->cxn_id = 0;
		}
	}
	
	public function limpiarCompaniaNegocios($com_id){
        $sql="DELETE FROM companiaxnegocio WHERE cxn_com_id='$com_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
    }
}
?>