<?php
defined('JPATH_PLATFORM') or die;

/**
 * Categoria class.  
 *
 */
class areainteres extends JTable
{
    public $are_id = null;
	public $are_nombre = null;
	public $are_descripcion = null;
	public $are_estado = null;
	public $are_fecha = null;
	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the categoria to load (optional).
	 *
	 * @return  Categoria
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'areainteres', 'are_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->cat_id = 0;
		}
	}
	
	public function selectAreasInteres($label=true, $accion=false){
            $area = JRequest::getVar('area');
            $sql = "SELECT * FROM areainteres 
                    WHERE are_estado='Activa'";
            $this->_db_retos->setQuery( $sql );
            $objetos  = $this->_db_retos->loadObjectList();
            if($label){
                $str="<select name='area' id='areas'>";
                $var= JText::_('MOD_BUSCAR_AREA_INTERES');
            }else{
                $str="<select name='area' id='areas' onChange='document.busquedahome.submit ()'>";
                $var= '........';
            }
            $str.="<option value=''>$var</option>";
           
            foreach ($objetos as $obj){
                    $selected = '';
                    if($obj->are_id==$area){
                        $selected = "selected";
                    }
                    $str.="<option value='$obj->are_id' $selected>$obj->are_nombre</option>";
            }
            $str.="</select>";    
            return $str;      
        }
    }



?>