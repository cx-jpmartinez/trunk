<?php
defined('JPATH_PLATFORM') or die;

/**
 * retoxnegocio class.  
 *
 */
class retoxnegocio extends JTable
{
    public $rxn_id = null;
	public $rxn_ret_id = null;
	public $rxn_com_id = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the negocio to load (optional).
	 *
	 * @return  retoxnegocio
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'retoxnegocio', 'rxn_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->rxn_id = 0;
		}
	}
	
	public function limpiarNegociosRetos($ret_id){
        $sql="DELETE FROM retoxnegocio WHERE rxn_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
    }
}
?>