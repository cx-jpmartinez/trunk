<?php
defined('JPATH_PLATFORM') or die;

/**
 * _Reto class.  
 *
 */
class _reto extends JTable
{
    public $rte_id = null;  
    public $rte_nombre = null;
    public $rte_com_id = null;
    public $rte_cat_id = null;
    public $rte_exp_id = null;
    public $rte_resumen = null;
    public $rte_descripcion = null;
    public $rte_antecedente = null;
    public $rte_resultadoEsperado = null;
    public $rte_nosebusca = null;
    public $rte_puntos = null;
    public $rte_imagen = null;
    public $rte_anexo = null;
    public $rte_estado = null;
    public $rte_fechaPublicacion = null;
    public $rte_fechaVigencia = null;
    public $rte_ares_id = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the reto to load (optional).
	 *
	 * @return  _Reto
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
        parent::__construct( 'composretos', 'rte_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->rte_ret_id = 0;
		}
	}
	
	public function cargarcomposReto($ret_id){
		$sql = "SELECT * FROM composretos WHERE rte_ret_id = '$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObject(); 
        return $obj;
		
		
	}
	
	
    public function eliminarPublicarCampos ($_ret_id){
		$sql = "DELETE FROM composretos  
                WHERE rte_ret_id = '$_ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query( $sql );
        return $obj;
	}
	
    
	
    
    
   
    
}
?>