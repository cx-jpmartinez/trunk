<?php
defined('JPATH_PLATFORM') or die;

/**
 * notificarretovencido class.  
 *
 */
class notificarretovencido extends JTable
{
        public $ntv_id = null;
	public $ntv_ret_id = null;
	public $ntv_msjAutomatico = null;
	public $ntv_msjManual = null;
	public $ntv_diasantes = null;
        public $ntv_fechaCreacionAT = null;
        public $ntv_fechaCreacionMN = null;
        public $ntv_fechaModificacionAT = null;
        public $ntv_fechaModificacionMN = null;
        
        
       /**
	 *
	 * @param   integer  $identifier  The primary key of the respuesta to load (optional).
	 *
	 * @return  notificarretovencido
	 *
	 */
	public function __construct($identifier = 0){
            $db = JFactory::getDBO();
            $this->_db_retos = $db->retos;
            parent::__construct( 'notificarretovencido', 'ntv_id', $this->_db_retos);
            if (!empty($identifier)) {
                $this->load($identifier);
            }else {
		  $this->ntv_id = 0;
		}
            }

        public function cargarNotificaion($ret_id){
            $sql="SELECT * FROM notificarretovencido where ntv_ret_id='$ret_id'";
            $this->_db_retos->setQuery( $sql );
            $notificacion  = $this->_db_retos->loadObject();
            return $notificacion;
        }
        
        public function enviarNotificacionMN($participantes, $reto){
            $configMail = JFactory::getConfig();
	    $sitio= 'http://us.orb.cognox.com/htdocs/retos';
            //$sitio= 'http://www.solucionesinnovadoras.com.co/';
            $retoEnlace = "<a href='$sitio'>$sitio</a>";
            foreach ($participantes as $participante){
                $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.= $this->ntv_msjManual;
                $cuerpo = str_replace("%_nombre_%", $participante->usu_nombre, $cuerpo);
                $cuerpo = str_replace("%_enlace_%", $retoEnlace, $cuerpo);
                $cuerpo = str_replace("\n", '<br>', $cuerpo);
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                //$correos = "$participante->usu_email";
                $correos = "afperez@cognox.com";
                $subject = "Vencimiento Reto Aviso Manual.";
                JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            }
        }
        
        public function enviarNotificacionAT($participantes, $reto){
            $configMail = &JFactory::getConfig();
	    $sitio= 'http://us.orb.cognox.com/htdocs/retos';
            //$sitio= 'http://www.solucionesinnovadoras.com.co/';
            $retoEnlace = "<a href='$sitio'>$sitio</a>";
            foreach ($participantes as $participante){
                $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.= $this->ntv_msjAutomatico;
                $cuerpo = str_replace("%_nombre_%", $participante->usu_nombre, $cuerpo);
                $cuerpo = str_replace("%_enlace_%", $retoEnlace, $cuerpo);
                $cuerpo = str_replace("\n", '<br>', $cuerpo);
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                //$correos = "$participante->usu_email";
                $correos = "afperez@cognox.com";
                $subject = "Vencimiento Reto Aviso Automatico.";
                JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            }
        }
        
}



?>