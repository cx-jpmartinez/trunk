<?php
defined('JPATH_PLATFORM') or die;

/**
 * Solucion class.  
 *
 */
class solucion extends JTable
{
    public $sol_id = null;
	public $sol_usu_id = null;
	public $sol_ret_id = null;
	public $sol_estado = null;
	public $sol_descripcion= null;
	public $sol_fechaCreacion= null;
	public $sol_fechaEstado= null;
	public $sol_anexo= null;
	public $sol_nameanexo= null;
	public $sol_titulo= null;
	public $sol_sol_id= null;
	public $_sol_solucionCompartida= null;
	public $_sol_aceptarInvitados= null;
	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the solucion to load (optional).
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'solucion', 'sol_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->sol_id = 0;
		}
	}
	
	
    public function getby ($usu_id,$sol_sol_id){
        $sql="SELECT * FROM solucion  
              WHERE sol_usu_id='$usu_id' 
              AND sol_sol_id='$sol_sol_id'";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObject();
        $this->bind($obj);
    }
	
	
	public function cargarSolucion ($ret_id,$usu_id)
    {
            $sql="SELECT if(sol_sol_id,sol_sol_id,sol_id) as sol_id FROM solucion  
                  WHERE sol_sol_id is not null 
                  AND (sol_usu_id='$usu_id' AND sol_ret_id='$ret_id' )";


            $this->_db_retos->setQuery( $sql );
            $otrasol  = $this->_db_retos->loadResult();

            if($otrasol)
            {
                $sql = "SELECT  sol_id, sol_usu_id, sol_ret_id, sol_estado,
 		                sol_descripcion, sol_fechaCreacion, sol_fechaEstado,
		                sol_anexo, sol_nameanexo, sol_titulo, sol_sol_id
                        FROM solucion  
                        WHERE sol_id='$otrasol' 
                        AND sol_ret_id='$ret_id'";
                $this->_db_retos->setQuery( $sql );
                $obj  = $this->_db_retos->loadObject();


                if($obj)
                {
                    $this->bind($obj);
                }
            }else
            {
                $sql = "SELECT  sol_id, sol_usu_id, sol_ret_id, sol_estado,
 		                sol_descripcion, sol_fechaCreacion, sol_fechaEstado,
		                sol_anexo, sol_nameanexo, sol_titulo, sol_sol_id
                        FROM solucion 
                    WHERE sol_usu_id='$usu_id' 
                    AND sol_ret_id='$ret_id'";
                $this->_db_retos->setQuery( $sql );
                $obj  = $this->_db_retos->loadObject();
                if($obj){
                    $this->bind($obj);
                }


            }
    }
    
    public function cargarSolucionCompartida($ret_id){
    	$sql="SELECT * FROM solucioncompartida 
    	   WHERE slc_ret_id='$ret_id' and slc_sol_id='$this->sol_id'";
    	$this->_db_retos->setQuery( $sql );
    	$this->_sol_solucionCompartida = $this->_db_retos->loadObjectList();
    }
    
    public function validarAceptacion($ret_id){
    	$sql="SELECT count(slc_sol_id) as cantidad
                FROM solucioncompartida
                left join solucion on (sol_id=slc_sol_id and sol_usu_id=slc_usu_id)
                where slc_ret_id='$ret_id' and slc_sol_id='$this->sol_id' and slc_usu_id is null";
        $this->_db_retos->setQuery( $sql );
        $this->_sol_aceptarInvitados = $this->_db_retos->loadResult();
    }
    
    public function validarSolucionCompartida($mail,$ret_id){
        $sql="SELECT * FROM solucioncompartida 
    	   where slc_usu_email='$mail' and slc_ret_id=$ret_id";
    	$this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadObjectList();
        return $result;
    }
    
    public function cargarParticipantesReto($ret_id,$sol_id){
    	$sql="SELECT 
           usu_id,
    	   usu_nombre as nombre,
           usu_email,
           usu_telefono,
           if(usu_cedula<>'',usu_cedula,'xxxxx') as usu_cedula,
           usu_nombreUsuario
           FROM usuario
           inner join solucion on (usu_id=sol_usu_id)
           where sol_ret_id='$ret_id' and (sol_id='$sol_id' OR sol_sol_id='$sol_id')";
    	$this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadObjectList();
    	return $result;
    }
    
    public function actualizarSolucionCompartida(){
    	$db =& JFactory::getDBO();
		$sql="UPDATE solucion SET sol_estado='Finalizado'
	        WHERE
	        sol_ret_id='$this->sol_ret_id'
	        AND sol_sol_id='$this->sol_id'";
        $db->retos->setQuery( $sql );    
        $db->retos->query();
    }
    
    public function cargarAnexos($idsol){
    	$sql="SELECT * FROM anexo where anx_sol_id='$idsol'";
    	$this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadObjectList();
        return $result;
    }
    
    public function cargarTitularReto($ret_id,$sol_id){
        $sql="SELECT 
           usu_id,
           usu_nombre as nombre,
           usu_email,
           usu_telefono 
           FROM usuario
           inner join solucion on (usu_id=sol_usu_id)
           where sol_ret_id='$ret_id' and (sol_id='$sol_id')";
        $this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadObject();
        return $result;
    }
    
    
	public function listarSolucionReto($ret_id,$usu_id){
	    $sql="SELECT group_concat(if(sol_sol_id,sol_sol_id,sol_id)) as sol_id  FROM solucion  
              WHERE sol_usu_id='$usu_id' 
              AND sol_ret_id='$ret_id'";
	    $this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadResult();
        $sql ="SELECT * FROM solucion where sol_ret_id='$ret_id' AND sol_id in ($result)";
        $this->_db_retos->setQuery( $sql );
        $soluciones = $this->_db_retos->loadObjectList();
        return $soluciones;
	}
	
	public function cantidadSoluciones($ret_id,$usu_id){
		$sql="SELECT if(count(sol_id)>0, count(sol_id)+1 , 0)  as sol_id  FROM solucion  
              WHERE sol_usu_id='$usu_id' 
              AND sol_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadResult();
        return $result ;
	}
        
        public function preguntasComite(){
           $sql="SELECT pco_pregunta,pco_fechapregunta,pco_remitente FROM preguntacomite
                WHERE pco_sol_id='$this->sol_id'";
           $this->_db_retos->setQuery( $sql );
           $mensajes = $this->_db_retos->loadObjectList();
           return $mensajes;
        }

    public function notificarEquipo($ret_nombre, $participantes, $solucion)
    {
        if ($participantes) {
            $configMail = JFactory::getConfig();
            foreach ($participantes as $participante)
            {
                $sitio = JURI::base();
                $cuerpo = "<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo .= " Hola, </br></br>";
                $cuerpo .= "La solución '$solucion' que enviaste </br>";
                $cuerpo .= "al reto '$ret_nombre' ha sido recibida con éxito. </br>";
                $cuerpo .= "Una vez vez se cierre el reto el comité evaluador revisara todas las soluciones enviadas.</br></br>";
                $cuerpo .= "Esperamos verte muy pronto participando en un nuevo reto.</br></br>";
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $correos = "$participante->usu_email";
                $subject = "Solución Recibida con éxito";

                try
                {
                    JFactory::getMailer()->sendMail($from, $fromname, $correos, $subject, $cuerpo, 1);
                }
                catch (Exception $e)
                {
                    JFactory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
                }

            }
        } else {
            return false;
        }

    }
        
}