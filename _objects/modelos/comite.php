<?php
defined('JPATH_PLATFORM') or die;

/**
 * comite class.  
 *
 */
class comite extends JTable
{
    public $cim_id = null;
    public $cim_ret_id = null;
	public $cim_nombre = null;
	public $cim_email = null;
	public $cim_fecha = null;
	/**
	 *
	 *
	 * @return  comite
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'comite', 'cim_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->cim_id = 0;
		}
	}
	
	public function limpiarComite($ret_id){
        $sql="DELETE FROM comite WHERE cim_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
    }
    
    public function cargarComiteReto($ret_id){
        $sql = "SELECT * FROM comite
            WHERE cim_ret_id='$ret_id'
            ORDER BY cim_id";
    	 $this->_db_retos->setQuery( $sql );
    	 $evaluadores = $this->_db_retos->loadAssocList();
    	 return $evaluadores;
    }
	
}
?>