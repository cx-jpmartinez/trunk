<?php
defined('JPATH_PLATFORM') or die;

/**
 * Respuesta class.  
 *
 */
class respuesta extends JTable
{
    public $res_id = null;
	public $res_pre_id = null;
	public $res_fecha = null;
	public $res_tipo = null;
	public $res_respuesta = null;
	 	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the respuesta to load (optional).
	 *
	 * @return  respuesta
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'respuesta', 'res_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->res_id = 0;
		}
	}
	
	public function cargarRespuestas ($pre_id){
            $sql="SELECT * FROM respuesta where res_pre_id='$pre_id'";
            $this->_db_retos->setQuery( $sql );
            $respuestas  = $this->_db_retos->loadObjectList();
            return $respuestas;
	}
	

	
}



?>