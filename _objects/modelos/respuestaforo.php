<?php
defined('JPATH_PLATFORM') or die;

/**
 * respuestaforo class.  
 *
 */
class respuestaforo extends JTable
{
    
    public $rpf_id = null;
    public $rpf_for_id = null;
    public $rpf_usu_id = null;
    public $rpf_texto = null;
    public $rpf_rpf_id = null;
    public $rpf_fecha = null;
    public $rpf_cantidad = null;
	
	
    /**
    *
    * @param   integer  $identifier  The primary key of the foro to load (optional).
    *
    * @return  foro
    *
    */
    public function __construct($identifier = 0){
        $db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
        parent::__construct( 'respuestaforo', 'rpf_id', $this->_db_retos);
        if (!empty($identifier)) {
            $this->load($identifier);
        }else {
            $this->rpf_id = 0;
        }
    }
    
    public function cargarRespuestaHijo (){
        $sql = "SELECT * FROM respuestaforo 
            INNER JOIN usuario ON (usu_id=rpf_usu_id)
            WHERE rpf_rpf_id=$this->rpf_id";
        $this->_db_retos->setQuery( $sql );
        $objLista  = $this->_db_retos->loadObjectList(); 
        return $objLista;
        
    }
    
}
?>