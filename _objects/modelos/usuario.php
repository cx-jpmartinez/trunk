<?php

/**
 *
 * @copyright   Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Usuario class.  Handles all application interaction with a user
 *
 */
class usuario extends JTable
{

	/**
	 * Unique id
	 *
	 * @var    integer
	 * @since  11.1
	 */
	public $usu_id = null;

	/**
	 * The users real name (or nickname)
	 * @var    string
	 * @since  11.1
	 */
	public $usu_nombre = null;
	
	/**
	 * The users real last name
	 * @var    string
	 * @since  11.1
	 */
	public $usu_apellido = null;

	/**
	 * The login username
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_nombreUsuario = null;
	
		/**
	 * MD5 encrypted password
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_clave = null;
	
	/**
	 * The country
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_pais = null;
	
	/**
	 * The city
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_ciudad = null;
	
	/**
	 * The state
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_estado = null;

	/**
	 * The email
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_email = null;
	
		/**
	 * Nivel de Educacion 
	 * 
	 * @var    string
	 * @since  11.1
	 */
	public $usu_nivelEducacion = null;
	
		/**
	 * The Experiencia
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_experiencia = null;
	
	/**
	 * Direccion
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_direccion = null;
	
	/**
	 * Telefono
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_telefono = null;
	public $usu_cedula = null;
        public $usu_compania = null;
        public $usu_area = null;
        public $usu_cargo = null;
        public $usu_regional = null;
	/**
	 * fotografia perfil
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $usu_foto = null;

	/**
	 * Error message
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $_errorMsg	= null;

	/**
	 * Constructor activating the default information of the language
	 *
	 * @param   integer  $identifier  The primary key of the user to load (optional).
	 *
	 * @return  JUser
	 *
	 * @since   11.1
	 */
	public function __construct($identifier = 0)
	{
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		
		parent::__construct( 'usuario', 'usu_id', $this->_db_retos);
		
		// Load the user if it exists
		if (!empty($identifier)) {
			$this->load($identifier);
		}
		else {
			//initialise
			$this->usu_id		 = 0;
		}
	}
   public function getareasinteres(){
	   $db = JFactory::getDBO();
       $sql="SELECT * 
             FROM areainteres 
             WHERE are_estado='Activa'";
       $db->retos->setQuery( $sql );
       $obj  = $db->retos->loadObjectList();
       return $obj;
	}
	
    public function getusuarioxareainteres($usu_id){
	   $db = JFactory::getDBO();
       $sql="SELECT * 
             FROM usuarioxareainteres 
             WHERE uxa_usu_id='$usu_id'";
       $db->retos->setQuery( $sql );
       $obj  = $db->retos->loadObjectList();
       return $obj;
	}
    public function removeareaxusu($usu_id){
	   $db = JFactory::getDBO();
       $sql="DELETE FROM usuarioxareainteres 
             WHERE uxa_usu_id='$usu_id'";
	   $db->retos->setQuery( $sql );
	   $db->retos->query($sql );
	}
    public function updateSendInfo($sendinfo , $usu_id){
	   $db = JFactory::getDBO();
       $sql="UPDATE usuario 
             SET usu_envioInfo='$sendinfo' 
             WHERE usu_id='$usu_id'";
	   $db->retos->setQuery( $sql );
	   $db->retos->query($sql );
	}

	Static function getusuario($usu_id){
	   $db = JFactory::getDBO();
       $sql="SELECT * 
             FROM usuario 
             WHERE usu_id='$usu_id'";
	   $db->retos->setQuery( $sql );
	   $obj  = $db->retos->loadObject();
       return $obj;
	}
	
        public function cargarArrCompania(){
            $db = JFactory::getDBO();
            $sql="SELECT 
                        neg_id,
                        neg_nombre 
                    FROM negocio 
                    GROUP BY neg_nombre
                    ORDER BY neg_nombre
                ";
            $db->retos->setQuery( $sql );
            $obj  = $db->retos->loadObjectList();
            return $obj;
	}
        
        public function validarCamposRegistro(){
            $arrResult = array('status' => '', 'message' => '');
            $nombre         = JRequest::getVar('usu_nombre', false);
            $nombreUsuario  = JRequest::getVar('usu_nombreUsuario', false);
            $email          = JRequest::getVar('usu_email', '');
            $telefono       = JRequest::getVar('usu_telefono', false);
            $direccion      = JRequest::getVar('usu_direccion', false);
            $cedula         = JRequest::getVar('usu_cedula', false);
            $compania       = JRequest::getVar('usu_compania', false);
            if(!$nombre
                || !$nombreUsuario
                || !$telefono
                || !$direccion
                || !$cedula
                || !$compania ){
                $arrResult['status'] = false;
                $arrResult['message'] = JText::_('COM_USUARIO_REGISTRO_MSG_CAMPO_REQUERIDO');
                return $arrResult;
            }
            
            if(!is_numeric($telefono) || !is_numeric($cedula)){
                $arrResult['status'] = false;
                $arrResult['message'] = JText::_('COM_USUARIO_REGISTRO_MSG_TELEFONO_CEDULA_NUMERICO');
                return $arrResult;
            }
            
            if($email != '' && !filter_var($email, FILTER_VALIDATE_EMAIL)){
                $arrResult['status'] = false;
                $arrResult['message'] = JText::_('COM_USUARIO_REGISTRO_MSG_EMAIL_NO_VALIDO');
                return $arrResult;
            }
            
            if(
                    strlen($cedula) > 20
                    || strlen($telefono) > 20
                    || strlen($direccion) > 50
                    || strlen($nombre) > 50
                    || strlen($nombreUsuario) > 50
                    || strlen($compania) > 50
                    || strlen($email) > 50
                    ){
                $arrResult['status'] = false;
                $arrResult['message'] = JText::_('COM_USUARIO_REGISTRO_MSG_MAXLENGTH');
                return $arrResult;
            }
            
            if($email == ''){
                $email = $cedula.'@UsuarioSinCorreo.com';
            }
            $this->usu_nombre = $nombre;
            $this->usu_nombreUsuario = $nombreUsuario;
            $this->usu_email = $email;
            $this->usu_telefono = $telefono;
            $this->usu_direccion = $direccion;
            $this->usu_cedula = $cedula;
            $this->usu_compania = $compania;
            $arrResult['status'] = true;
            $arrResult['message'] = '';
            return $arrResult;
        }
        
        public function buscarUsuario($strBuscar, $campo){
            $db = JFactory::getDBO();
            $sql="SELECT 
                        usu_id
                        , usu_nombre
                        , usu_apellido
                        , usu_nombreUsuario
                        , usu_clave
                        , usu_pais
                        , usu_ciudad
                        , usu_estado
                        , usu_email
                        , usu_nivelEducacion
                        , usu_experiencia
                        , usu_telefono
                        , usu_direccion
                        , usu_foto
                        , usu_envioInfo
                        , usu_cedula
                        , usu_compania
                        , usu_area
                        , usu_cargo
                        , usu_regional 
                    FROM usuario 
                    WHERE {$campo} LIKE '%{$strBuscar}%'
                    ORDER BY usu_nombre
                ";
            $db->retos->setQuery( $sql );
            $obj  = $db->retos->loadObjectList();
            return $obj;
        }
        
        
        function validarAccesoAdministracionUsuarios($redir=true){
            $session = JFactory::getSession();
            $usuAdmin = $session->get('idUsuarioAdmin', false);
            if($usuAdmin){
                $usuId = $usuAdmin;
            }else{
                $usuId = JFactory::getUser()->id;
            }
            
            
            $db = JFactory::getDBO();
            $sql="SELECT 
                    COUNT(0)
                  FROM directorinnovacion
                  WHERE din_usu_id = '{$usuId}'
                ";

          
            $db->retos->setQuery( $sql );
            $res  = $db->retos->loadResult();
            if(!$res){
                if($redir){
                $app = JFactory::getApplication();
                $app->redirect('index.php?option=com_home&id=2&Itemid=530', JText::_('COM_USUARIO_NO_AUTORIZADO'), 'error');
                }
                return false;
            }else{
                return true;
            }
        }
}



?>