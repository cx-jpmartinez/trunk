<?php
defined('JPATH_PLATFORM') or die;

/**
 * reporte class.  
 *
 */
class grafica extends JTable
{

    public $rep_id = null;
    public $rep_nombre = null;
    public $rep_estado = null;
    public $rep_grafica = null;
    public $rep_excel = null;
    public $rep_textox = null;
    public $rep_color = null;
    public $rep_orden = null;
    public $rep_consultaAdd = null;
    
	
    /**
    *
    * @param   integer  $identifier  The primary key of the reto to load (optional).
    *
    * @return  reporte
    *
    */
    public function __construct($identifier = 0){
        $db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
        parent::__construct( 'reporte', 'rep_id', $this->_db_retos);
        if (!empty($identifier))
        {
                        $this->load($identifier);
        }else
        {
                    $this->rep_id = 0;
        }
    }
	
    function cargarRetosGrafica(){
        $sql="SELECT ret_id,ret_nombre,ret_estado,DATE(ret_fechaPublicacion) AS ret_fechaPublicacion, DATE(ret_fechaVigencia) AS ret_fechaVigencia FROM reto where ret_estado<>'borrador'  order by ret_fechaVigencia desc";
        $this->_db_retos->setQuery( $sql );
        return $this->_db_retos->loadObjectList();
    }
    
    function cargarNegocios(){
        $sql="SELECT com_id,com_nombre,com_descripcion FROM compania ";
        $this->_db_retos->setQuery( $sql );
        return $this->_db_retos->loadObjectList();
    }
    
    function cargarGrafica($sql,$idretos=''){
        $and = '';
        $where= ''; 
        $existew = strstr($sql, 'where');
        if($idretos && !$existew){
            $where = " WHERE sol_ret_id $idretos ";
        }else{
           $and  = " AND ret_id $idretos ";
        }
        $consulta= $sql;
        $consulta=str_replace("%_WHERE_%", $where, $consulta);
        $consulta=str_replace("%_AND_%", $and, $consulta);
        if($this->rep_nombre=='SOLUCIONES ENVIADAS POR NEGOCIO' 
         || $this->rep_nombre=='(DESARROLLO) Soluciones enviadas por negocio'
         || $this->rep_nombre=='(DESARROLLO) TOTAL DE SOLUCIONES ENVIADAS POR NEGOCIO'){
            $this->_db_retos->setQuery($consulta);
            $this->_db_retos->query();
            $consulta ="select
                    count(0) as cantidad ,
                    if(a.negocio is null,'Externos',a.negocio ) as 'negocio'
                    from
                    (
                    select count(NEGOCIO_PARTICIPANTE) as cantidad,NEGOCIO_PARTICIPANTE as negocio
                    FROM tmpsolucionesNegocio group by SOLUCION,NEGOCIO_PARTICIPANTE
                    order by cantidad desc
                    ) a
                    group by a.negocio  order by cantidad desc";
        }
        //echo $consulta;
        $this->_db_retos->setQuery( $consulta );
        return $this->_db_retos->loadObjectList();
    }
    
    
    public function exportarExcel($reporte,$sql,$idretos='',$consolidado=false) {
        $rep_nombre = $reporte->rep_nombre;
        $and = '';
        $where= ''; 
        $existew = strstr($sql, 'where');
        if($consolidado){
             $where  = " WHERE ret_id $idretos ";
        }else{
            if($idretos && !$existew){
                $where = " WHERE sol_ret_id $idretos ";
            }else{
            $and  = " AND ret_id $idretos ";
            }
        }
        $config = JFactory::getConfig();
        $rutaReporte = $config->get('root_report');
        $nombreArchivo = "$rep_nombre". '_' . date('Ymd_His') . '.xls';
        
        $consulta= $sql;
        $consulta=str_replace("%_WHERE_%", $where, $consulta);
        $consulta=str_replace("%_AND_%", $and, $consulta);
        if($rep_nombre=='SOLUCIONES ENVIADAS POR NEGOCIO' 
                || $rep_nombre=='(DESARROLLO) Soluciones enviadas por negocio' 
                || $rep_nombre=='(DESARROLLO) TOTAL DE SOLUCIONES ENVIADAS POR NEGOCIO'
                || $rep_nombre=='(DESARROLLO)  TOTAL DE SOLUCIONES ENVIADAS POR RETO'){
            $this->_db_retos->setQuery($consulta);
            $this->_db_retos->query();
            
            if($rep_nombre=='SOLUCIONES ENVIADAS POR NEGOCIO' 
                    || $rep_nombre=='(DESARROLLO)  TOTAL DE SOLUCIONES ENVIADAS POR RETO'  
                    || $rep_nombre=='(DESARROLLO) Soluciones enviadas por negocio'){
                    $consulta = $reporte->rep_consultaAdd;
               // exit($consulta);
            }elseif($rep_nombre=='(DESARROLLO) TOTAL DE SOLUCIONES ENVIADAS POR NEGOCIO'){
                
                 /* $sql = "create temporary table tmpsolucionesNegocioTotal
                        SELECT
                        ret_id,
                        COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        count(0) as TOTAL,
                        RETO,
                        NEGOCIO_DEL_RETO,
                        ANO_DE_ENVIO
                        FROM
                        (SELECT COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        count(0),
                        RETO,
                        NEGOCIO_DEL_RETO,
                        ANO_DE_ENVIO, ret_id
                        FROM tmpsolucionesNegocioParaTotales GROUP BY
                        SOLUCION,NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION
                        order by ret_id
                        ) a group by a.COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,RETO ORDER BY a.ret_id;";*/
                
                 /* $sql = "create temporary table tmpsolucionesNegocioTotal
                        SELECT
                        ret_id,
                        COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        count(0) as TOTAL,
                        RETO,
                        NEGOCIO_DEL_RETO,
                        ANO_DE_ENVIO,
                        ANO_SOLUCION
                        FROM
                        (SELECT COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,
                        count(0),
                        RETO,
                        NEGOCIO_DEL_RETO,
                        ANO_DE_ENVIO,
                        ANO_SOLUCION,
                        ret_id
                        FROM tmpsolucionesNegocioParaTotales GROUP BY
                        SOLUCION,NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION
                        order by ret_id
                        ) a group by a.COMPANIA_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,RETO ORDER BY a.ret_id";   */ 
                    $sql="create temporary table tmpsolucionesNegocioTotal
                        select * from tmpsolucionesNegocioParaTotales
                        group by NECGOCIO_DE_LA_PERSONA_QUE_ENVIO_SOLUCION,SOLUCION";
                    $this->_db_retos->setQuery($sql);
                    $this->_db_retos->query(); 
                    $consulta = $reporte->rep_consultaAdd;
            }
        }
       
     
        
        $consulta.=" INTO OUTFILE '{$rutaReporte}{$nombreArchivo}'";//cho $consulta;exit;
        
        $this->_db_retos->setQuery($consulta);
        $this->_db_retos->query();
        $rutaReporte = $config->get('root_report_download');
        $archivo = $rutaReporte . $nombreArchivo;
        echo "<script> window.location = 'index.php?option=com_reportes'; </script>";
        header("Location: " . $rutaReporte . $nombreArchivo);
    }
    
    function cargarPie($sql,$idretos='',$idnegocios=''){
        $andreto = '';
        $andnegocio = '';
        if($idretos ){
           $andreto = " WHERE ret_id $idretos ";
        }
        if($idnegocios ){
           $andnegocio = " AND neg_descripcion  $idnegocios ";
        }
        $consulta= $sql;
        $consulta=str_replace("%_ANDRETOS_%", $andreto, $consulta);
        $consulta=str_replace("%_ANDNEGOCIOS_%", $andnegocio, $consulta);
        //echo $consulta;exit;
        $this->_db_retos->setQuery( $consulta );
        return $this->_db_retos->loadObjectList();
    }
    
    function exportarPieExcel($rep_nombre, $sql,$idretos='',$idnegocios=''){
        $andreto = '';
        $andnegocio = '';
        if($idretos ){
           $andreto = " WHERE ret_id $idretos ";
        }
        if($idnegocios ){
           $andnegocio = " AND neg_descripcion  $idnegocios ";
        }
        $config = JFactory::getConfig();
        $rutaReporte = $config->get('root_report');
        $nombreArchivo = "$rep_nombre". '_' . date('Ymd_His') . '.xls';
        $consulta= $sql;
        $consulta=str_replace("%_ANDRETOS_%", $andreto, $consulta);
        $consulta=str_replace("%_ANDNEGOCIOS_%", $andnegocio, $consulta);
        $consulta.=" INTO OUTFILE '{$rutaReporte}{$nombreArchivo}'";
        //echo $consulta;exit;
        $this->_db_retos->setQuery($consulta);
        $this->_db_retos->query();
        $rutaReporte = $config->get('root_report_download');
        $archivo = $rutaReporte . $nombreArchivo;
        echo "<script> window.location = 'index.php?option=com_reportes'; </script>";
        header("Location: " . $rutaReporte . $nombreArchivo);
    }
    
    
    function cargarNombreNegocio($idnegocios){
        $sql="SELECT group_concat(com_nombre) as compania FROM compania where com_nombre $idnegocios";
        $this->_db_retos->setQuery( $sql );
        return $this->_db_retos->loadResult();
    }
    
    
}
?>