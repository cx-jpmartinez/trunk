<?php
defined('JPATH_PLATFORM') or die;

/**
 * retoxareainteres class.  
 *
 */
class retoxareainteres extends JTable
{
    public $rxa_id = null;
	public $rxa_ret_id = null;
	public $rxa_are_id = null;
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the categoria to load (optional).
	 *
	 * @return  retoxareainteres
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'retoxareainteres', 'rxa_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->rxa_id = 0;
		}
	}
	
	public function limpiarAreasRetos($ret_id){
        $sql="DELETE FROM retoxareainteres WHERE rxa_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
    }
}
?>