<?php
defined('JPATH_PLATFORM') or die;

/**
 * director class.  
 *
 */
class director extends JTable
{
    public $dir_id = null;
    public $dir_ret_id = null;
	public $dir_nombre = null;
	public $dir_email = null;
	public $dir_fecha = null;
	/**
	 *
	 *
	 * @return  director
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'director', 'dir_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->dir_id = 0;
		}
	}
	
	public function limpiarDirectorRetos ($ret_id){
		$sql="DELETE FROM director WHERE dir_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_db_retos->query();
	}
	
    public function cargarDirectorReto($ret_id){
        $sql = "SELECT * FROM director
            WHERE dir_ret_id='$ret_id'
            ORDER BY dir_id";
         $this->_db_retos->setQuery( $sql );
         $evaluadores = $this->_db_retos->loadAssocList();
         return $evaluadores;
    }
    
    public function notificarDirector($ret_id,$ret_nombre,$enlace){
        $sql = "SELECT * FROM director
            WHERE dir_ret_id='$ret_id'
            ORDER BY dir_id";
         $this->_db_retos->setQuery( $sql );
         $directores = $this->_db_retos->loadObjectList();
         if($directores){
         	$configMail = &JFactory::getConfig();
         	foreach ($directores as $director){
                $sitio= JURI::base() ;
                $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
	            $cuerpo.=" Hola, {$director->dir_nombre}.</br></br>";
	            $cuerpo.="se ha registrado una nueva solución para el reto '$ret_nombre'</br></br>";
                $sitiolotus = "http://aplica.gruponutresa.com/Aplicaciones/Portales/Soluciones_Innovadoras/si.nsf?OpenDatabase&login";
	            $cuerpo.="Para conocer más sobre la solución haz click aquí: <a href='$sitiolotus'>Abrir página.</a></br></br>";
	            $cuerpo.="Soluciones Innovadoras</br>";
                $cuerpo.="<a href='$sitio'>$sitio</a></br>";
	            $from = $configMail->get('mailfrom');
	            $fromname = $configMail->get('fromname');
	            $correos = "$director->dir_email";
	            $subject = "Nueva Solución";
                JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
         	}
         }else{
         	  return false;
         }
         
    }
    
    public function notificarDirectorPregunta($ret_id,$ret_nombre,$pre_nombre,$pre_comentario,$name,$email){
        $sql = "SELECT * FROM director
            WHERE dir_ret_id='$ret_id'
            ORDER BY dir_id";
         $this->_db_retos->setQuery( $sql );
         $directores = $this->_db_retos->loadObjectList();
         if($directores){
            $configMail = JFactory::getConfig();
            foreach ($directores as $director){
                $sitio= JURI::base() ;
                $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.=" Hola, {$director->dir_nombre}.</br></br>";
                $cuerpo.="se ha registrado una nueva pregunta de $name / $email para el reto '$ret_nombre'.</br></br>";
                $cuerpo.= "'$pre_nombre:</br>";
                $cuerpo.= "$pre_comentario'</br></br></br>";
                $cuerpo.="Soluciones Innovadoras</br>";
                $cuerpo.="<a href='$sitio'>$sitio</a></br>";
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $correos = "$director->dir_email";
                $subject = "Duda para el experto.";
                JMail::getInstance()->sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            }
         }else{
              return false;
         }
         
    }
    
}
?>