<?php
defined('JPATH_PLATFORM') or die;

/**
 * tyc class.  
 *
 */
class tyc extends JTable
{
    public $tyc_id = null;
	public $tyc_ret_id = null;
	public $tyc_usu_id = null;
	public $tyc_fecha = null;
	
/**
	 *
	 * @param   integer  $identifier  The primary key of the categoria to load (optional).
	 *
	 * @return  tyc
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'tyc', 'tyc_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->tyc_id = 0;
		}
	}
	
	
}
?>