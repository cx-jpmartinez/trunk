<?php
defined('JPATH_PLATFORM') or die;

/**
 * Reto class.  
 *
 */
class reto extends JTable
{

	public $ret_id = null;
	public $ret_cat_id = null;
	public $ret_nombre = null;
	public $ret_imagen = null;
	public $ret_imagenPeq = null;
	public $ret_resumen = null;
	public $ret_descripcion = null;
	public $ret_resultadoEsperado = null;
	public $ret_premio = null;
	public $ret_nosebusca = null;
	public $ret_costo = null;
	public $ret_comite = null;
	public $ret_estado = null;
	public $ret_fechaPublicacion = null;
	public $ret_fechaCreacion = null;
	public $ret_fechaVigencia = null;
	public $ret_anexos = null;
	public $ret_visitas = null;
	public $ret_alcance = null;
	public $ret_puntos = null;
	public $ret_antecedente = null;
	public $ret_com_id = null;
	public $ret_galeria = null;
	public $ret_criterioEvaluacion = null;
        public $ret_imgDetalle = null;
	public $_categoria = null;
	public $_favorito = null;
	public $_solucion = null;
	public $_postulaciones = null;
	public $_tycusuario = null;
	public $_expertos = null;
	public $_comite = null;
	public $_areasInteres = null;
	public $_compania = null;
	public $_negocios = null;
        
	
	
    /**
	 *
	 * @param   integer  $identifier  The primary key of the reto to load (optional).
	 *
	 * @return  Reto
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'reto', 'ret_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->ret_id = 0;
		}
	}
	
	public function listarRetosBusqueda($buscar='',$area='',$arrEstado=''){
         	$like = '';
		$inner='';
		$and='';
		$order='';
		if($buscar){
		  $like ="AND (ret_nombre like '%$buscar%'
                OR ret_resumen like '%$buscar%'
                OR ret_descripcion like '%$buscar%'
                OR ret_resultadoEsperado like '%$buscar%'
                OR ret_nosebusca like '%$buscar%')";
		}
		if($area){
			$inner="INNER JOIN  retoxareainteres ON (rxa_ret_id=ret_id)
                    INNER JOIN  areainteres ON (rxa_are_id=are_id and are_id='$area')";
           // $inner="INNER JOIN  areainteres ON (are_id=ret_are_id and are_id='$area')";        
            $order = "ORDER BY ret_fechaPublicacion desc";        
		}
                $and = '';
                
                if($arrEstado){
                   /* $estados='';
                    if($arrEstado[1]!=''){
                    $estados=$arrEstado[1]; 
                    }
                    if($arrEstado[2]!=''){
                    if($estados){$estados.=',';}
                    $estados.=$arrEstado[2];  
                    }
                    if($arrEstado[3]!=''){
                    if($estados){$estados.=',';}
                    $estados.=$arrEstado[3]; 
                    }
                    if($estados){
                        // $and = "AND ret_estado IN ($estados)";	
                        $and = '';
                    }*/
                     if($arrEstado[1]!='' && $arrEstado[2]!=''){
                        $sql = "SELECT *,DATE (ret_fechaPublicacion) AS _fechaPublicacion,
                        DATE (ret_fechaVigencia) AS _fechaVigencia,
                        0
                        -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret_id group by ret_id) as _ret_postulacion 
                        FROM reto $inner
                        WHERE  ret_estado in ('Cerrada','Vigente') 
                        AND ret_fechaVigencia $and $like $order";
                     }elseif($arrEstado[2]!=''){
                        $sql = "SELECT *,DATE (ret_fechaPublicacion) AS _fechaPublicacion,
                        DATE (ret_fechaVigencia) AS _fechaVigencia,
                        0
                        -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret_id group by ret_id) as _ret_postulacion 
                        FROM reto $inner
                        WHERE  ret_estado='Cerrada' 
                        AND ret_fechaVigencia $and $like $order";
                    }else{
                      $sql = "SELECT *,DATE (ret_fechaPublicacion) AS _fechaPublicacion,
                        DATE (ret_fechaVigencia) AS _fechaVigencia,
                        0
                        -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret_id group by ret_id) as _ret_postulacion 
                        FROM reto $inner
                        WHERE  ret_estado='Vigente' AND  NOW()
                        BETWEEN ret_fechaPublicacion 
                        AND ret_fechaVigencia $and $like $order";   
                    }
		}else{
                    $sql = "SELECT *,DATE (ret_fechaPublicacion) AS _fechaPublicacion,
                    DATE (ret_fechaVigencia) AS _fechaVigencia,
                    0
                    -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret_id group by ret_id) as _ret_postulacion 
                    FROM reto $inner
                    WHERE  ret_estado='Vigente' AND  NOW()
                    BETWEEN ret_fechaPublicacion 
                    AND ret_fechaVigencia $and $like $order"; 
                }
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
	}
	
    public function listarRetosVigentes(){
        $sql = "SELECT 
                DATE (ret.ret_fechaPublicacion) AS _fechaPublicacion,
                DATE (ret.ret_fechaVigencia) AS _fechaVigencia,
                ret.*,
                0
                -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret.ret_id group by ret_id) as _ret_postulacion    
                FROM reto ret
                WHERE NOW()
                BETWEEN ret_fechaPublicacion 
                AND ret_fechaVigencia AND ret_estado='Vigente' Order by ret_fechaPublicacion desc";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    
     public function listarRetosTodos($estado=''){
        $where = '';
        if($estado){
            $where = " WHERE ret_estado in ($estado)";
        }
        $sql = "SELECT 
                DATE (ret.ret_fechaPublicacion) AS _fechaPublicacion,
                DATE (ret.ret_fechaVigencia) AS _fechaVigencia,
                ret.*,
                0
                -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret.ret_id group by ret_id) as _ret_postulacion    
                FROM reto ret $where
                Order by ret_fechaPublicacion desc";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    
    public function listarRetosHome(){
        $sql = "SELECT DATE (r.ret_fechaPublicacion) AS _fechaPublicacion,
		        DATE (r.ret_fechaVigencia) AS _fechaVigencia,
                ret_id, ret_cat_id, ret_nombre, ret_imagen,
		        ret_resumen, ret_descripcion, ret_puntos, ret_exp_id, ret_imagenPeq
                FROM reto r
                WHERE ret_estado='Vigente' AND NOW() BETWEEN ret_fechaPublicacion AND ret_fechaVigencia 
                ORDER BY ret_fechaPublicacion DESC LIMIT  10;";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    

    public function cargarFavorito($ret_id,$usu_id){
        $sql = "SELECT fav_id FROM favoritos
            WHERE fav_usu_id='$usu_id' AND fav_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $result  = $this->_db_retos->loadResult(); 
        return $result;
    }
    
    public function cargarSolucion($ret_id,$usu_id){
        $sql = "SELECT sol_id FROM solucion
            WHERE sol_usu_id='$usu_id' AND sol_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $result  = $this->_db_retos->loadResult(); 
        return $result;
    }
    
    public function cargartycSolucion($ret_id,$usu_id){
        $sql = "SELECT tyc_id FROM tyc
            WHERE tyc_usu_id='$usu_id' AND tyc_ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $result  = $this->_db_retos->loadResult(); 
        return $result;
    }
    
    public function listarRetosUsuario($usu_id){
    	$sql = "SELECT 
    	        DATE (ret.ret_fechaPublicacion) AS _fechaPublicacion,
                DATE (ret.ret_fechaVigencia) AS _fechaVigencia,
    	        ret.*,sol.*
    	        -- (SELECT count(sol_ret_id)  FROM solucion where sol_ret_id=ret.ret_id group by ret_id) as _ret_postulacion 
    	        FROM reto ret
                INNER JOIN solucion sol ON (ret_id=sol_ret_id)
                WHERE sol_usu_id=$usu_id  group by ret_id order by ret_id desc";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    
    public function numeroPostulaciones($ret_id){
         $sql = "SELECT count(sol_id) as postulaciones FROM solucion 
            INNER JOIN tyc ON (sol_ret_id=tyc_ret_id and tyc_usu_id=sol_usu_id) 
            where sol_ret_id='$ret_id' group by sol_usu_id";
         $this->_db_retos->setQuery( $sql );
         //$result  = $this->_db_retos->loadResult(); 
         $result  = $this->_db_retos->loadObjectList(); 
         $result = count($result);
         return $result;
    }
    
    function cargarExperto (){
        $sql="SELECT experto.* FROM experto
            inner join reto on (ret_exp_id=exp_id)
            where ret_id=$this->ret_id";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObject();
        return $obj;
    }
    
    function cargarTerminosCondiciones(){
    	$sql="SELECT ter_texto FROM terminosycondiciones 
    	WHERE ter_ret_id=$this->ret_id 
    	AND ter_idioma='es'";
        $this->_db_retos->setQuery( $sql );
        $terycon  = $this->_db_retos->loadResult();
        return $terycon;
    }
    
    
    function cargarDatosExperto(){
    	$sql="SELECT
    	   exp_nombre as nombre,
           '' as cedula,
           exp_mail as email,
           exp_telefono as telefono 
    	 FROM experto WHERE exp_id='$this->ret_exp_id'";
    	$this->_db_retos->setQuery( $sql );
    	$this->_expertos = $this->_db_retos->loadAssocList();
    	return $this->_expertos;
    }
    
    function cargarComite(){
    	$sql="SELECT usl_fullname as nombre FROM comite
            left join usuariolotus on (cim_email=usl_email)
            where cim_ret_id='$this->ret_id'";
     	$this->_db_retos->setQuery( $sql );
        $this->_comite  = $this->_db_retos->loadAssocList();
        return $this->_comite;
    }
    
    function cargarAreasInteresReto($ret_id){
    	$sql="SELECT group_concat(concat(' ',are_nombre)) as are_nombre FROM areainteres
            INNER JOIN retoxareainteres ON (are_id=rxa_are_id)
            INNER JOIN reto ON (ret_id=rxa_ret_id)
            where ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_areasInteres = $this->_db_retos->loadResult();
    	return $this->_areasInteres;
    }
    
    function cargarCategoriaReto($ret_id){
        $sql="SELECT cat_nombre FROM categoria
            INNER JOIN reto ON (ret_cat_id=cat_id)
            WHERE ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_categoria = $this->_db_retos->loadResult();
        return $this->_areasInteres;
    }
    
    public function listarRetosExperto($usu_email){
       $sql = "SELECT
            DATE (ret.ret_fechaPublicacion)
            AS _fechaPublicacion,
            DATE (ret.ret_fechaVigencia) AS _fechaVigencia, ret.*,
            (SELECT count(pre_ret_id)  FROM pregunta where pre_ret_id=ret.ret_id group by ret_id) as _ret_postulacion 
            FROM usuario
            inner join experto on (exp_mail=usu_email)
            inner join reto ret on (ret_exp_id=exp_id)
            where usu_email = '$usu_email'
            group by ret_id desc";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    function cargarCompaniaReto($ret_id){
        $sql="SELECT neg_nombre FROM negocio
            INNER JOIN reto ON (ret_com_id=neg_id)
            WHERE ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_compania = $this->_db_retos->loadResult();
        return $this->_compania;
    }
    
    function cargarNegocioReto($ret_id){
        $sql="SELECT group_concat(concat(' ',com_nombre)) as com_nombre FROM compania
            INNER JOIN retoxnegocio ON (com_id=rxn_com_id)
            INNER JOIN reto ON (ret_id=rxn_ret_id)
            where ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $this->_negocios = $this->_db_retos->loadResult();
        return $this->_negocios;
    }
    
    function cargarNombreNegocio($ret_id){
        $sql="SELECT group_concat(com_nombre) as Nombrenegocio FROM compania
            INNER JOIN retoxnegocio ON (com_id=rxn_com_id)
            INNER JOIN reto ON (ret_id=rxn_ret_id)
            where ret_id='$ret_id'";
        $this->_db_retos->setQuery( $sql );
        $result = $this->_db_retos->loadResult();
        return $result;
    }
    
    function listarParticipantes (){
       $sql = "SELECT usu_id,usu_nombre, usu_email FROM solucion
            inner join usuario on (usu_id=sol_usu_id)
            where sol_ret_id='$this->ret_id' group by usu_id";
        $this->_db_retos->setQuery( $sql );
        $participantes  = $this->_db_retos->loadObjectList(); 
        return $participantes;
    }
    
    public function cargarFormSolucion($ret_id,$sol_id){
        //$ret_id = 24;
        $sql = "SELECT p.*,d.* FROM formulario
            INNER JOIN propiedad p ON (pro_frm_id=frm_id)
            LEFT JOIN regsolucion ON (reg_frm_id=pro_frm_id AND reg_sol_id='$sol_id')
            LEFT JOIN  detallesolucion d ON (reg_id=det_reg_id AND det_pro_id=pro_id)
            WHERE  frm_ret_id='$ret_id' 
            and pro_estado=1 
            AND  pro_pro_id IS NULL 
            ORDER BY pro_posicion";
         $this->_db_retos->setQuery( $sql );
         $inputs  = $this->_db_retos->loadObjectList(); 
         foreach ($inputs as $input){
             if($input->pro_tipo=='select'){
                $sql = "SELECT p.* FROM formulario
                    INNER JOIN propiedad p on (pro_frm_id=frm_id)
                    WHERE frm_ret_id='$ret_id' AND pro_pro_id='$input->pro_id' AND pro_estado=1 ORDER BY pro_posicion";
                $this->_db_retos->setQuery( $sql );
                $input->options = $option  = $this->_db_retos->loadObjectList(); 
             }
             if($input->pro_tipo=='check'){
                 $sql = "SELECT  p.*,d.* FROM formulario
                    INNER JOIN propiedad p on (pro_frm_id=frm_id)
                    LEFT JOIN regsolucion ON (reg_frm_id=pro_frm_id AND reg_sol_id='$sol_id')
                    LEFT JOIN  detallesolucion d ON (reg_id=det_reg_id AND det_pro_id=pro_id)
                    WHERE frm_ret_id='$ret_id' AND pro_pro_id='$input->pro_id' AND pro_estado=1 ORDER BY pro_posicion";
                $this->_db_retos->setQuery( $sql );
                $input->options = $option  = $this->_db_retos->loadObjectList(); 
             }
         }
         return $inputs;
   }
   
   public function guardarSolucionForm($regsolucion,$form,$solucion){

      if($form->pro_tipo=='check'){
          foreach ($form->options as $option){
            $sql ="SELECT det_id FROM detallesolucion
                    INNER JOIN regsolucion ON (reg_id=det_reg_id)
                    INNER JOIN propiedad ON (det_pro_id=pro_id)
                    WHERE reg_sol_id='$solucion->sol_id' AND pro_id = '$option->pro_id'";
            $this->_db_retos->setQuery( $sql );
            $registro  = $this->_db_retos->loadObject();
            if($registro){
                $detallesolucion = new detallesolucion($registro->det_id);
            }else{
                $detallesolucion = new detallesolucion();
                $detallesolucion->det_reg_id = $regsolucion->reg_id;
                $detallesolucion->det_pro_id = $option->pro_id;
                $detallesolucion->det_fechaCreacion = date ( 'Y-m-d H:i:s' );
            }
            $detallesolucion->det_valor = JRequest::getVar( "$option->pro_nombre",'');
            
            $detallesolucion->store();
          }
       }elseif($form->pro_tipo=='file'){
           if( isset($_FILES["$form->pro_nombre"]['name']) && $_FILES["$form->pro_nombre"]['name']!=''){
                $uploads_dir = './images/retos/soluciones/';
                $tamanoMax = 5000000;
                $ext = explode('.', $_FILES["$form->pro_nombre"]['name']);
                $num = count($ext)-1;
                if($_FILES["$form->pro_nombre"]['name'] > $tamanoMax){
                    return false;
                }
                $carga=move_uploaded_file($_FILES["$form->pro_nombre"]['tmp_name'], $uploads_dir .$solucion->sol_titulo."$form->pro_id.".$ext[1] );         
                if($carga){
                    $name = $solucion->sol_titulo.''.$form->pro_id;
                    $anexo = $name.'.'.$ext[1];
                    guardarAnexo($anexo,$solucion,$_FILES["$form->pro_nombre"]['name']);
                    $sql ="SELECT det_id FROM detallesolucion
                    INNER JOIN regsolucion ON (reg_id=det_reg_id)
                    INNER JOIN propiedad ON (det_pro_id=pro_id)
                    WHERE reg_sol_id='$solucion->sol_id' AND pro_id = '$form->pro_id'";
                    $this->_db_retos->setQuery( $sql );
                    $registro  = $this->_db_retos->loadObject();
                    if($registro){
                        $detallesolucion = new detallesolucion($registro->det_id);
                    }else{
                        $detallesolucion = new detallesolucion();
                        $detallesolucion->det_reg_id = $regsolucion->reg_id;
                        $detallesolucion->det_pro_id = $form->pro_id;
                        $detallesolucion->det_fechaCreacion = date ( 'Y-m-d H:i:s' );
                    }
                    $detallesolucion->det_valor = $anexo;
                    $detallesolucion->det_texto = $_FILES["$form->pro_nombre"]['name'];
                    $detallesolucion->store();
                }
            }
       }else{
            $sql ="SELECT det_id FROM detallesolucion
                    INNER JOIN regsolucion ON (reg_id=det_reg_id)
                    INNER JOIN propiedad ON (det_pro_id=pro_id)
                    WHERE reg_sol_id='$solucion->sol_id' AND pro_id = '$form->pro_id'";
            $this->_db_retos->setQuery( $sql );
            $registro  = $this->_db_retos->loadObject();
            if($registro){
                $detallesolucion = new detallesolucion($registro->det_id);
            }else{
                $detallesolucion = new detallesolucion();
                $detallesolucion->det_reg_id = $regsolucion->reg_id;
                $detallesolucion->det_pro_id = $form->pro_id;
                $detallesolucion->det_fechaCreacion = date ( 'Y-m-d H:i:s' );
            }
            $detallesolucion->det_valor = JRequest::getVar( "$form->pro_nombre" );
            $detallesolucion->store();
       }
   }
   
    public function CargarDatoSolucionForm($regsolucion,$form,$solucion){
        $sql ="SELECT det_id FROM detallesolucion
            INNER JOIN regsolucion ON (reg_id=det_reg_id)
            INNER JOIN propiedad ON (det_pro_id=pro_id)
            WHERE reg_sol_id='$solucion->sol_id' AND pro_id = '$form->pro_id' AND pro_estado=1 ORDER BY pro_posicion";
       $this->_db_retos->setQuery( $sql );
       $this->_db_retos->loadObject();
    }
   
    function cargarSoluciones($ret_id){
        $sql ="SELECT sol_titulo,sol_id,sol_estado,sol_sol_id
            FROM solucion where sol_ret_id='$ret_id' and sol_titulo<>'' and sol_titulo not in ('nada') order by sol_titulo";
       $this->_db_retos->setQuery( $sql );
       return $this->_db_retos->loadObjectList();
    }
    /** NUEVOS REPORTES **/
    
    function cargarAnoPublicacion(){
        $sql ="SELECT YEAR(ret_fechaPublicacion) AS anoPublicacion FROM reto
            WHERE ret_estado IN ('Vigente','Cerrada')
            GROUP BY YEAR(ret_fechaPublicacion)";
       $this->_db_retos->setQuery( $sql );
       return $this->_db_retos->loadObjectList();
    }
    
    function cargarAnoSolucion(){
        $sql ="SELECT YEAR(sol_fechaEstado) AS anoSolucion FROM solucion
            GROUP BY YEAR(sol_fechaEstado)";
       $this->_db_retos->setQuery( $sql );
       return $this->_db_retos->loadObjectList();
    }
    
    function cargarAnoInscripcion(){
        $sql ="SELECT YEAR(sol_fechaCreacion) AS anoInscripcion FROM solucion
            WHERE YEAR(sol_fechaCreacion) NOT IN (0)
            GROUP BY YEAR(sol_fechaCreacion)  ";
       $this->_db_retos->setQuery( $sql );
       return $this->_db_retos->loadObjectList();
    }
    
     function cargarNegocios($arreglo){
        $sql ="SELECT neg_descripcion FROM negocio WHERE neg_descripcion <>'' AND neg_estado='Activa' GROUP BY neg_descripcion";
       $this->_db_retos->setQuery( $sql );
       if($arreglo){
            $obResult = $this->_db_retos->loadObjectList();
            $array = array();
            if($obResult){
                $i = 1;
                foreach ($obResult as $objeto) {
                    $obj = new stdClass();
                    $obj->neg_descripcion = $objeto->neg_descripcion;
                    $array[$i] = $obj;
                    $i++;
                }
                $obj = new stdClass();
                $obj->neg_descripcion = 'Externo';
                $array[$i] = $obj;
               return $array;
            }
       }else{
           return $this->_db_retos->loadObjectList();
       }
       
       
    }
    
    
    public function listarRetosReporte($estado='', $negocio='', $anoSolucion = '', $anoLanzamiento=''){
        $andEstado = " AND ret_estado IN ('Vigente','Cerrada')";
        if($estado){
            $andEstado = " AND ret_estado IN ($estado)";
        }
        if($anoLanzamiento){
            $andFechaPublicacion = " AND YEAR(ret_fechaPublicacion) IN ($anoLanzamiento)" ;
        }else{
            $andFechaPublicacion = '';
        }
        
        $innerNegocio = '';
        if($negocio){
            $innerNegocio="INNER JOIN retoxnegocio ON (ret_id=rxn_ret_id)
                        INNER JOIN compania ON (com_id=rxn_com_id)";
        }
        
        $innerAnoSolucion = '';
        if($anoSolucion){
            $innerAnoSolucion = "INNER JOIN solucion on (sol_ret_id=ret_id AND YEAR(sol_fechaEstado) IN ($anoSolucion) )";
            
        }
        
        $sql = "SELECT 
                DATE (ret.ret_fechaPublicacion) AS _fechaPublicacion,
                DATE (ret.ret_fechaVigencia) AS _fechaVigencia,
                ret.*,
                0
                FROM reto ret 
                $innerNegocio
                $innerAnoSolucion
                WHERE ret_id
                $andEstado
                $andFechaPublicacion
                GROUP BY ret_id
                ORDER BY ret_fechaPublicacion DESC";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    /** FIN NUEVOS REPORTES **/
    
}
?>