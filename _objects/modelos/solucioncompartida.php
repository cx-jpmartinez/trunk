<?php
defined('JPATH_PLATFORM') or die;

/**
 * solucioncompartida class.
 *
 */
class solucioncompartida extends JTable
{
	public $slc_id = null;
  public $slc_ret_id = null;
	public $slc_sol_id = null;
	public $slc_usu_nombre = null;
	public $slc_usu_email = null;
	public $slc_usu_id = null;
	public $slc_fechaCreacion = null;
	public $slc_fechaModificacion= null;
  public $slc_estado= null;
	/**
	 *
	 *
	 * @return  solucioncompartida
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'solucioncompartida', 'slc_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->slc_id = 0;
		}
	}

	public function asociarParticipante ($email,$solucion)
	{
		$db = JFactory::getDBO();

		$sql ="SELECT count(slc_id) as slc_id FROM solucioncompartida
            where slc_usu_email='$email'
            and slc_ret_id='$solucion->sol_ret_id' and slc_sol_id='$solucion->sol_id'";
        $this->_db_retos->setQuery( $sql );
        $slc_id = $this->_db_retos->loadResult();


        if( $slc_id > 1)
		{
        	return false;
        }

		$sql ="SELECT usu_id FROM usuario where usu_email='$email'";
		$this->_db_retos->setQuery( $sql );

        $usu_id = $this->_db_retos->loadResult();

		/*
		$soluc = new solucion();
		$soluc->sol_ret_id = $solucion->sol_ret_id;
		$soluc->sol_sol_id = $solucion->sol_id;
		$fechaCreacion = date('Y-m-d H:i');
		if($usu_id > 0)
		{
		  $soluc->sol_usu_id = $usu_id;
		}
		try {
			$soluc->store();
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), ' :warning');
		}
		*/
		$fechaCreacion = date('Y-m-d H:i');
		if($usu_id > 0)
		{
			$sql = "INSERT INTO solucion (sol_id,sol_ret_id,sol_sol_id, sol_usu_id,sol_fechaCreacion,sol_descripcion) VALUES (0,'$solucion->sol_ret_id','$solucion->sol_id','$usu_id','$fechaCreacion','');";
			 $usu_id;
		}else{

			$sql = "INSERT INTO solucion (sol_id,sol_ret_id,sol_sol_id,sol_fechaCreacion,sol_descripcion) VALUES (0,'$solucion->sol_ret_id','$solucion->sol_id','$fechaCreacion','');";
		}
		$this->_db_retos->setQuery( $sql );
		$db->query ();


/*
		echo '<pre>';
		echo print_r($soluc);echo '</pre>';
		die;
*/
		$sql="UPDATE usuario
            inner join solucion on (usu_id=sol_usu_id)
            inner join solucioncompartida on (usu_email=slc_usu_email)
            SET slc_usu_id=usu_id
            where usu_email='$email' and
            sol_ret_id='$solucion->sol_ret_id'";

        $db->retos->setQuery( $sql );
        $db->retos->query();

	}

        public function cargarSolucionesCompartidas($usu_email, $ret_id){
            $sql="SELECT * FROM solucioncompartida
                inner join usuario on (usu_id=slc_usu_id)
                inner join solucion on (sol_id=slc_sol_id)
                where
                slc_usu_email='$usu_email'
                and slc_estado='Pendiente'
                and slc_ret_id='$ret_id'";
            $this->_db_retos->setQuery( $sql );
            $result = $this->_db_retos->loadObjectList();
            return $result;
        }
}
?>
