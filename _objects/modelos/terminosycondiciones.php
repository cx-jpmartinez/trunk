<?php
defined('JPATH_PLATFORM') or die;

/**
 * tyc class.  
 *
 */
class terminosycondiciones extends JTable
{
	public $ter_id = null;
	public $ter_ret_id = null;
	public $ter_fecha = null;
	public $ter_idioma = null;
	public $ter_texto = null;
	
    /**
	 *
	 * @param   integer  $identifier  The primary key of the terminosycondiciones to load (optional).
	 *
	 * @return  terminosycondiciones
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'terminosycondiciones', 'ter_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->ter_id = 0;
		}
	}
	
	public function cargarTerminosReto($idret){
		$sql="SELECT * FROM terminosycondiciones where ter_ret_id='$idret' order by ter_idioma asc";
		$this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
	}
	
	
}
?>