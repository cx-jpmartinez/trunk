<?php
defined('JPATH_PLATFORM') or die;

/**
 * Favoritos class.  
 *
 */
class favoritos extends JTable
{
    public $fav_id = null;
	public $fav_usu_id = null;
	public $fav_ret_id = null;
	public $_fav_postulado = null;
	
	
    /**
	 *
	 * @param   integer  $identifier  The primary key of the favoritos to load (optional).
	 *
	 * @return  Favoritos
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'favoritos', 'fav_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->fav_id = 0;
		}
	}
	
    public function cargarFavoritos($usu_id){
        $sql = "SELECT * FROM reto
            INNER JOIN favoritos on (fav_ret_id=ret_id)
            INNER JOIN usuario on (usu_id=fav_usu_id)
            LEFT JOIN solucion on (usu_id=sol_usu_id and ret_id=sol_ret_id)
            WHERE fav_usu_id='$usu_id'";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;      
    }
    
    public function validarFavoritos($ret_id,$usu_id){
    	$sql = "SELECT fav_id FROM favoritos
            WHERE fav_usu_id='$usu_id' AND fav_ret_id='$usu_id'";
        $this->_db_retos->setQuery( $sql );
        $result  = $this->_db_retos->loadResult(); 
        return $result;
    }
    
    
	
}



?>