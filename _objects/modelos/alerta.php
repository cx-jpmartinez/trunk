<?php
defined('JPATH_PLATFORM') or die;

/**
 * alerta class.  
 *
 */
class alerta extends JTable
{
    public $ale_id = null;
	public $ale_ret_id = null;
	public $ale_directores = null;
	public $ale_diasdirectores = null;
	public $ale_director = null;
	public $ale_diasdirector = null;
	public $ale_presidente = null;
	public $ale_diaspresidente = null;
	public $ale_tipo = null;

	/**
	 *
	 * @param   integer  $identifier  The primary key of the alerta to load (optional).
	 *
	 * @return  alerta
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'alerta', 'ale_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->ale_id = 0;
		}
	}
	
	public function cargarDatos ($ret_id){
	   $sql ="SELECT * FROM alerta where ale_ret_id=$ret_id";	
	   $this->_db_retos->setQuery( $sql );
       $obj  = $this->_db_retos->loadObject();
        if( $obj )
        {
            $this->bind($obj);
        }

	}
}
?>