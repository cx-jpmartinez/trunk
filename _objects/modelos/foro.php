<?php
defined('JPATH_PLATFORM') or die;

/**
 * Foro class.  
 *
 */
class foro extends JTable
{
    
    public $for_id = null;
    public $for_tema = null;
    public $for_fecha = null;
    public $for_estado = null;
    public $for_ret_id = null;
    public $for_texto = null;
	
	
    /**
    *
    * @param   integer  $identifier  The primary key of the foro to load (optional).
    *
    * @return  foro
    *
    */
    public function __construct($identifier = 0){
        $db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
        parent::__construct( 'foro', 'for_id', $this->_db_retos);
        if (!empty($identifier)) {
            $this->load($identifier);
        }else {
            $this->for_id = 0;
        }
    }
    
    public function listarForos($foroestado=''){
        $where = '';
        if($foroestado){
            $where = "where for_estado='$foroestado'";
        }
        $sql = "SELECT for_id, for_tema, date(for_fecha) as for_fecha, for_estado, for_ret_id, for_texto FROM foro $where";
        $this->_db_retos->setQuery( $sql );
        $obj  = $this->_db_retos->loadObjectList(); 
        return $obj;
    }
    
    public function cargarRespuestas(){
        $sql = "SELECT * FROM respuestaforo 
        INNER JOIN usuario ON (usu_id=rpf_usu_id)
        WHERE rpf_for_id='$this->for_id' AND rpf_rpf_id is null";
        $this->_db_retos->setQuery( $sql );
        $objLista  = $this->_db_retos->loadObjectList(); 
        return $objLista;
    }
    
}
?>