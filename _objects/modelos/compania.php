<?php
defined('JPATH_PLATFORM') or die;

/**
 * compania class.  
 *
 */
class compania extends JTable
{
    public $com_id = null;
	public $com_nombre = null;
	public $com_descripcion = null;
	public $com_estado = null;
	public $com_fecha = null;
	public $com_codigo = null;
	
	/**
	 *
	 *
	 * @return  Compania
	 *
	 */
	public function __construct($identifier = 0)
    {
		$db = JFactory::getDBO();

        $this->_db_retos = $db->retos;

		parent::__construct( 'negocio', 'neg_id', $this->_db_retos);

        if (!empty($identifier))
        {
            try {

                $this->load($identifier);

            } catch (Exception $e)
            {
                echo 'Error : ',  $e->getMessage(), "\n";
                JError::raiseError(null, $e->getMessage());
            }


		}else
        {
		  $this->com_id = null;
		}
	}
	
}
?>