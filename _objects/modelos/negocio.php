<?php
defined('JPATH_PLATFORM') or die;

/**
 * negocio class.  
 *
 */
class negocio extends JTable
{
        public $neg_id = null;
	public $neg_nombre = null;
	public $neg_descripcion = null;
	public $neg_estado = null;
	public $neg_fecha = null;
	
	/**
	 *
	 *
	 * @return  Negocio
	 *
	 */
	public function __construct($identifier = 0){
		$db =& JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'negocio', 'neg_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->neg_id = 0;
		}
	}
	
}
?>