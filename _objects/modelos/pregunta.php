<?php
defined('JPATH_PLATFORM') or die;

/**
 * Categoria class.  
 *
 */
class pregunta extends JTable
{
    public $pre_id = null;
	public $pre_titulo = null;
	public $pre_descipcion = null;
	public $pre_respuesta = null;
	public $pre_tipo = null; 
	public $pre_ret_id = null;
	public $pre_usu_id = null;
	public $pre_fechaCreacion = null;
	public $pre_fechaEstado = null;
	public $pre_estado = null;
	
	
	/**
	 *
	 * @param   integer  $identifier  The primary key of the categoria to load (optional).
	 *
	 * @return  Pregunta
	 *
	 */
	public function __construct($identifier = 0){
		$db = JFactory::getDBO();
        $this->_db_retos = $db->retos;
		parent::__construct( 'pregunta', 'pre_id', $this->_db_retos);
        if (!empty($identifier)) {
			$this->load($identifier);
		}else {
		  $this->pre_id = 0;
		}
	}
	

	
}



?>