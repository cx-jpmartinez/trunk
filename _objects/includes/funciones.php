<?php

function guardarSolucionCompartida($solucion, $mailParticipante, $nombreParticipante, $amigo,$reto)
{
	$guardar = validarParticipante($mailParticipante,$reto->ret_id,$solucion->sol_id);

	if($guardar)
    {
		$solucioncompartida = new solucioncompartida();
	    $solucioncompartida->slc_ret_id = $reto->ret_id;
	    $solucioncompartida->slc_sol_id = $solucion->sol_id;
	    $solucioncompartida->slc_usu_nombre = $nombreParticipante;
	    $solucioncompartida->slc_usu_email = $mailParticipante;
	    $solucioncompartida->slc_fechaCreacion = date('Y-m-d H:i');


	    if($solucioncompartida->store())
        {
	    	$solucioncompartida->asociarParticipante($mailParticipante,$solucion);


            try
            {
                $configMail = JFactory::getConfig();
                $sitio= JURI::base();
                $retoEnlace = "<a href='$sitio/index.php?option=com_retos&view=reto&idreto=$reto->ret_id&Itemid=444'>$reto->ret_nombre</a>";
                $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
                $cuerpo.=" Hola, {$nombreParticipante}.</br></br>";
                $cuerpo.="$amigo  te  ha invitado a que hagas parte de su equipo ";
                $cuerpo.="para solucionar  el  reto  $retoEnlace; si estás interesado ingresa con tu usuario</br>";
                $cuerpo.="y contraseña a la página de Soluciones Innovadoras y suscríbete en el reto</br></br>";
                $cuerpo.= "Esperamos que muy pronto seas uno de nuestros solucionadores.</br></br>";
                $cuerpo.="Soluciones Innovadoras</br>";
                //$cuerpo.="<a href='$sitio'>$sitio</a></br>";
                $from = $configMail->get('mailfrom');
                $fromname = $configMail->get('fromname');
                $correos = "$mailParticipante";
                $subject = "Invitación para participar en un reto.";

                $mail = JFactory::getMailer();
                $mail->addRecipient($from);
                $mail->addRecipient($correos);
                $mail->setSender(array($from, $fromname));
                $mail->setSubject($subject);
                $mail->setBody($cuerpo);
                $mail->IsHTML(true);

                $sent = $mail->Send();

                if ( $sent !== true )
                {
                    echo 'Error sending email: ' . $sent->__toString();
                    return false;
                }
                // JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
            }
            catch (Exception $e)
            {
                JFactory::getApplication()->enqueueMessage($e->getMessage(), ' :warning');
            }

	    }else{
	    	return false;
	    }
	}else{
		return false;
	}
}

function guardarAnexo($nombreAnexo,$solucion,$archivo)
{
	$anexo = new anexo();
    $anexo->anx_sol_id= $solucion->sol_id;
    $anexo->anx_archivo = $nombreAnexo;
    $anexo->anx_nombre = $archivo;
    $anexo->anx_fecha = date('Y-m-d H:i');
    
    if($anexo->store())
    {
    	return true;
    }else{
    	return false;
    }
}   


function getAjaxUsuariosLotus( $search='' ){
	$db = JFactory::getDBO();
	 $document = JFactory::getDocument();
	if (strlen($search) >= "4") {  
	    $sql = "
	            SELECT
	                CONCAT(
	                  (
	                    CASE 1 WHEN (usl_nombre LIKE '%$search%') THEN ''
	                        WHEN (usl_nombre LIKE '%$search%') THEN CONCAT(usl_nombre, ' -> ')
	                        WHEN (usl_email LIKE '%$search%') THEN CONCAT(usl_email, ' -> ') 
	                    END
	                  ),
	                  concat(usl_nombre,' | ',usl_email)
	                ) value,
	                usl_nombre text
	            FROM usuariolotus a
	            WHERE 
	               usl_nombre LIKE '%$search%'
	               OR usl_email LIKE '%$search%'
	                
	        ";
	        $db->retos->setQuery( $sql );
	        $arr = $db->retos->loadAssocList();
	        $document->setMimeEncoding( 'application/json' );
	        echo json_encode( $arr );     
	}  
}

function validarParticipante($mail,$ret_id,$sol_id){
	//return false;
	$db = JFactory::getDBO();
    $sql ="SELECT slc_id FROM solucioncompartida
        where slc_usu_email='$mail'
        and slc_ret_id='$ret_id' and slc_sol_id='$sol_id'";
    $db->retos->setQuery( $sql );
    $slc_id = $db->retos->loadResult();
    if($slc_id){
        return false;
    }else{
    	return true;
    }
}

function listarNotificacion(){
    $db = JFactory::getDBO();
    $sql="SELECT usu_nombre,usu_email,ret_id FROM areainteres
        inner join retoxareainteres on (are_id=rxa_are_id)
        inner join reto on (ret_id=rxa_ret_id)
        inner join usuarioxareainteres on (uxa_are_id=are_id)
        inner join usuario on (usu_id=uxa_usu_id)
        where ret_visitas is null and ret_estado in ('Vigente') and ret_fechaPublicacion = date(now())
        group by usu_id
        order by ret_id";
    $db->retos->setQuery( $sql );
    $arr = $db->retos->loadObjectList();
    enviarNotificacion($arr);
}

function enviarNotificacion($arrs){
	$db = JFactory::getDBO();
	$actu_retos = '(0';
	foreach ($arrs as $arr){
	    $configMail = &JFactory::getConfig();
	    $sitio= JURI::base();
	    $retoEnlace = "<a href='$sitio'>$sitio</a>";
            $cuerpo="<img src='$sitio/images/cabezote_correo.png' width='100%'>";
	    $cuerpo.=" Hola, {$arr->usu_nombre}.</br></br>";
	    $cuerpo.="Existen nuevos retos pertenecientes a las áreas de tu interés, para verlos y conocer todo ";
	    $cuerpo.="sobre ellos visita nuestra página.</br></br>";
	    $cuerpo.= "Esperamos  que muy pronto quieras ser solucionador de uno de ellos.</br></br>";
	    $cuerpo.="Soluciones Innovadoras</br>";
	    //$cuerpo.="<a href='$sitio'>$sitio</a></br>";
	    $from = $configMail->get('mailfrom');
	    $fromname = $configMail->get('fromname');
	    $correos = "$arr->usu_email";
	    //$correos = "afperez@cognox.com";
	    $subject = "Novedades en tus áreas de interés.";
	    JUtility::sendMail($from, $fromname, $correos, $subject, $cuerpo,1);
	    $actu_retos = $actu_retos.','.$arr->ret_id;
	}
	$actu_retos = $actu_retos.')';
    $sql ="UPDATE reto set ret_visitas = '1'
	   WHERE ret_id in $actu_retos";
	$db->retos->setQuery( $sql );    
    $db->retos->query();
}

function validarExperto($usu_email){
	  $db = JFactory::getDBO();
      $sql = "SELECT
            count(exp_id) 
            FROM usuario
                inner join experto on (exp_mail=usu_email)
                inner join reto ret on (ret_exp_id=exp_id)
                where usu_email = '$usu_email'
            group by ret_id desc";
        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult();
    //var_dump($obj);
   // die;
        return $obj;
}

function validarComite($usu_email){
         $db = JFactory::getDBO();
       $sql = "SELECT
            count(cim_id)
            FROM usuario
               inner join comite on (cim_email=usu_email)
                inner join reto ret on (cim_ret_id=ret_id)
                where usu_email = '$usu_email'
            group by ret_id desc";
        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult(); 
        return $obj;
}

function validarDirector($usu_email){
         $db = JFactory::getDBO();
       $sql = "SELECT
            count(dir_id)
            FROM usuario
               inner join director on (dir_email=usu_email)
                inner join reto ret on (dir_ret_id=ret_id)
                where usu_email = '$usu_email'
            group by ret_id desc";
        $db->retos->setQuery( $sql );
        $obj  = $db->retos->loadResult(); 
        return $obj;
}

function cargarMensajeRetos (){
	$db = JFactory::getDBO();
	$sql="SELECT introtext 
	   FROM retos_rts.rts_content 
	   WHERE alias='frase-retos'";
	$db->retos->setQuery( $sql );
    $text = $db->retos->loadResult();
    return  $text;
}

function cerrarReto(){
	$db = JFactory::getDBO();
	$sql ="SELECT ret_id FROM reto
        WHERE NOW()
        NOT BETWEEN  ret_fechaPublicacion AND ret_fechaVigencia
        AND ret_estado='Vigente'";
    $db->retos->setQuery( $sql );
    $vigentes = $db->retos->loadObjectList();
    foreach ($vigentes as $vigente){
    	$sql="UPDATE reto SET ret_estado='Cerrada'
            WHERE
            ret_id='$vigente->ret_id'
            AND ret_estado='Vigente'";
        $db->retos->setQuery( $sql );    
        $db->retos->query();
    }
	
}

function validarCantidadSoluciones($ret_id){
	$db = JFactory::getDBO();
	$user = JFactory::getUser();
    $usuario = new usuario($user->id);
    $sql ="SELECT count(sol_id) as cantidad FROM solucion where sol_ret_id='$ret_id' and sol_usu_id='$usuario->usu_id'";
    $db->retos->setQuery( $sql );
    $result = $db->retos->loadResult();
    return $result;
}

function cargarNumeroParticipantes($ret_id){
	$db = JFactory::getDBO();
	$sql ="SELECT COUNT(0) FROM (
        SELECT
        count(0)
        FROM solucion s
        where sol_ret_id='$ret_id' and sol_titulo<>'' and sol_titulo not in ('nada')
        group by sol_ret_id, sol_usu_id) A
	";
    $db->retos->setQuery( $sql );
    $result = $db->retos->loadResult();
    return $result;
}

function cargarNumeroSolucionesReto($ret_id){
	$db = JFactory::getDBO();
	$sql ="SELECT COUNT(0) FROM (
        SELECT
        count(0)
        FROM solucion s
        where sol_ret_id='$ret_id' and sol_titulo<>'' and sol_titulo not in ('nada')
        group by sol_id) A
	";
    $db->retos->setQuery( $sql );
    $result = $db->retos->loadResult();
    return $result;
}

function cantidadSolucionReto($ret_id,$usu_id){
    $db = JFactory::getDBO();
    $sql ="SELECT count(0) as cantidad FROM solucion where sol_usu_id='$usu_id' and sol_ret_id='$ret_id'";
    $db->retos->setQuery( $sql );
    $result = $db->retos->loadResult();
    return $result;
}

function validarVerParticipantes(){
	 $user = JFactory::getUser();
	 $arrper = array('pagaviria@serviciosnutresa.com','narbelaez@serviciosnutresa.com','mnino@cremhelado.com.co','mig@cognox.com','mrochel@serviciosnutresa.com','pinnovacionsn@serviciosnutresa.com');
     if(in_array("$user->email",$arrper)){
     	return true;
     }else{
     	return false;
     }
}

function validarSolucionCompatida($sol_id,$usu_id){
   $db = JFactory::getDBO();
   $sql="SELECT * FROM solucioncompartida  where slc_usu_id='$usu_id'  and slc_sol_id='$sol_id' and slc_estado='Aceptada'";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadResult();
   return $result;
}

function cargarNombreUsuario($usu_id){
    $db = JFactory::getDBO();
   $sql="SELECT usu_nombre FROM usuario where usu_id='$usu_id'";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadResult();
   return $result;
}

function listarRetosProximoVencer(){
    $db = JFactory::getDBO();
    $sql="SELECT
        ntv_id,
        ret_id,
        ntv_diasantes,
        datediff(ret_fechaVigencia,now()) as dias
        FROM notificarretovencido
        inner join reto on (ret_id=ntv_ret_id)
        where ret_estado='Vigente'
        and datediff(ret_fechaVigencia,now()) = ntv_diasantes";
    $db->retos->setQuery( $sql );
    $ntretos  = $db->retos->loadObjectList(); 
    if($ntretos){
        foreach($ntretos as $ntreto){
            $reto = new reto($ntreto->ret_id);
            if($reto){
                 $participantes = $reto->listarParticipantes();
                 $notificar = new notificarretovencido($ntreto->ntv_id);
                 $notificar->enviarNotificacionAT($participantes,$reto);
            }
        }
    }else{
        return false;
    }
    return true;
}

function cargarRespuestaPregunta($pre_id){
    $db = JFactory::getDBO();
    $sql="SELECT res_respuesta,res_fecha FROM respuesta where res_pre_id='$pre_id'";
    $db->retos->setQuery( $sql );
    $objRespuesta = $db->retos->loadObjectList();
    $respuesta= '';
    foreach ($objRespuesta as $obj){
        $respuesta.="Fecha: $obj->res_fecha <br>$obj->res_respuesta" .'<br>';
    }
    return $respuesta;
}

function cargarListaEstadoRetos($estado=''){
    $db = JFactory::getDBO();
    $sql="SELECT ret_estado FROM reto group by ret_estado";
    $db->retos->setQuery( $sql );
    $objResult = $db->retos->loadObjectList();
    $option = '';
    $selected = '';
    foreach ($objResult as $obj){
        if($obj->ret_estado==$estado){
            $selected='selected';
        }
        $option.= "<option value='$obj->ret_estado' $selected>$obj->ret_estado</option>";
        $selected = '';
    }
    return $option;
}

function cargarListaRetos($ret_id){
    $db = JFactory::getDBO();
    $sql="SELECT ret_id,ret_nombre FROM reto";
    $db->retos->setQuery( $sql );
    $objResult = $db->retos->loadObjectList();
    $option = '';
    $selected = '';
    foreach ($objResult as $obj){
        if($obj->ret_id==$ret_id){
            $selected='selected';
        }
        $option.= "<option value='$obj->ret_id' $selected>$obj->ret_nombre</option>";
        $selected = '';
    }
    return $option;
}

function cargarListaNegocios($negid= ''){
    $db = JFactory::getDBO();
    $sql="SELECT com_nombre,rxn_com_id FROM compania
        inner join retoxnegocio on (rxn_com_id=com_id)
        group by com_nombre";
    $db->retos->setQuery( $sql );
    $objResult = $db->retos->loadObjectList();
    $option = '';
    $selected = '';
    foreach ($objResult as $obj){
        if($obj->rxn_com_id==$negid){
            $selected='selected';
        }
        $option.= "<option value='$obj->rxn_com_id' $selected>$obj->com_nombre</option>";
        $selected = '';
    }
    return $option;
    
}

function validarTYC($email,$ret_id){
   $db = JFactory::getDBO();
   $sql="SELECT tyc_id FROM usuario
        INNER JOIN tyc ON (usu_id=tyc_usu_id)
        WHERE usu_email = '$email'
        AND tyc_ret_id='$ret_id' LIMIT 1";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadResult();
   return $result;
}

function getSolucionesCompatidas($ret_id ,$email){
   $db = JFactory::getDBO();
   $sql="SELECT * FROM solucioncompartida  
    WHERE slc_usu_email='$email'  
    AND slc_ret_id='$ret_id' 
    AND slc_estado='Pendiente'";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadResult();
   return $result;
}

function validarUserTYC($usu_id,$ret_id){
   $db = JFactory::getDBO();
   $sql="SELECT tyc_id FROM tyc
        WHERE tyc_usu_id = '$usu_id'
        AND tyc_ret_id='$ret_id'";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadResult();
   return $result;
}

function getUsuarioSolucion($sol_id){
   $db = JFactory::getDBO();
   $sql="SELECT * FROM usuario
        INNER JOIN solucion ON (sol_usu_id=usu_id)
        WHERE sol_id = '$sol_id'";
   $db->retos->setQuery( $sql );    
   $result = $db->retos->loadObject();
   return $result;
    
}

function validarIngresoGraficas($correo=''){
     $db = JFactory::getDBO();
     if($correo){
        $sql ="SELECT usu_email FROM usuario WHERE usu_email  
            IN ('amrestrepo@noel.com.co',
                'cmontoya@noel.com.co',
                'ecgomez@chocolates.com.co',
                'hospina@comercialnutresa.com.co',
                'jecheverri@colcafe.com.co',
                'KOLARTE@COMERCIALNUTRESA.COM.CO',
                'lilianafeghali@hotmail.com',
                'lvaron@pastasdoria.com',
                'mcrodriguez@larecetta.com',
                'mrochel@serviciosnutresa.com',
                'narbelaez@serviciosnutresa.com',
                'ocgiraldo@chocolates.com.co',
                'ricky-gd@hotmail.com',
                'sjimenez@zenu.com.co',
                'pagaviria@serviciosnutresa.com',
                'mig@cognox.com') AND usu_email='{$correo}'
                ";
        $db->retos->setQuery( $sql );    
        $result = $db->retos->loadResult();
        if($result){
            return true;
        }else{
            return false;
        }
     }else{
         return false;
     }
}

function cargarAnexoTYC($ret_id, $usu_id){
    
    $db = JFactory::getDBO();
    $sql ="SELECT tyc_anexo FROM tyc
        inner join reto on (ret_id=tyc_ret_id)
        inner join usuario on (usu_id=tyc_usu_id)
        where tyc_anexo is not null
        and tyc_anexo<>''
        and tyc_usu_id='$usu_id'
        and tyc_ret_id='$ret_id'";
    $db->retos->setQuery( $sql );
    $objResult = $db->retos->loadResult();
    return $objResult;
    
}


?>